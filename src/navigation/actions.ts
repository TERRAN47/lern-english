import {
    NavigationAction,
    NavigationActions,
    DrawerActions,
    StackActions,
    NavigationToggleDrawerAction,
    NavigationCloseDrawerAction
} from "react-navigation";
import {SimpleThunk} from "../common/simpleThunk";
import {actionCreator} from "../core/store";
import {getBackAction} from "./navigation";
import {Pages} from "./pages";

const NavigationPages = new Pages();

function simpleToRoute(routeName: string): () => NavigationAction {
    return (): NavigationAction => NavigationActions.navigate({routeName});
}

function routeWithParams<T>(routeName: string): (params: T) => NavigationAction {
    return (params: T): NavigationAction => NavigationActions.navigate({routeName, params});
}

function toggleDrawer(): () => NavigationToggleDrawerAction {
    return (): NavigationToggleDrawerAction => DrawerActions.toggleDrawer();
}

function closeMenu(): () => NavigationCloseDrawerAction {
    return (): NavigationCloseDrawerAction => DrawerActions.closeDrawer();
}
//@ts-ignore
function routeToStack<T>(stack: string, screen: string): (params?: T) => NavigationAction {
    return (params: T): NavigationAction =>
    NavigationActions.navigate({
        routeName: stack,
        params,
        action: NavigationActions.navigate({routeName: screen, params})
    });
}

function navToAuthScreen(screen: string): () => NavigationAction {
  return (): NavigationAction =>

    StackActions.reset({
      index: 0,
      key: null,
      actions: [
        NavigationActions.navigate({
          routeName: screen
        }),
      ]
    });
}

function navToBack(): () => NavigationAction {
  return (): NavigationAction =>
    NavigationActions.back();
}

export class Actions {
  toggleDrawer = toggleDrawer();
  closeMenu = closeMenu();
  navReToBack = navToBack();
  navigateToAuth = navToAuthScreen(NavigationPages.login);

  navigateToInDevelopment = simpleToRoute(NavigationPages.inDeveloping);
  navigateToPlayground = simpleToRoute(NavigationPages.playground);
  navigateToCurrentRun = simpleToRoute(NavigationPages.currentRun);
  navigateToRun = routeWithParams<ICommonNavParams>(NavigationPages.currentRun);
  navigateToPlannedRuns = simpleToRoute(NavigationPages.plannedRuns);

  navigateToProfileSettings = simpleToRoute(NavigationPages.profileSettings);
  navigateToAbout = simpleToRoute(NavigationPages.about);

  navigateToRegistration = simpleToRoute(NavigationPages.registration);
  navigateToHome = simpleToRoute("tabs");
  navigateToContacts = simpleToRoute(NavigationPages.contacts);
  navigateToOnlineLesson = simpleToRoute(NavigationPages.appOnlineLesson);
  navigateToHomeCalendar = simpleToRoute(NavigationPages.homeCalendar);
  navigateToStatistics = simpleToRoute(NavigationPages.homeStatistics);
  navigateToPhoneticsInstr = simpleToRoute(NavigationPages.phoneticsInstr);
  navigateToHomeWork = simpleToRoute(NavigationPages.homeWork);
  navigateToTraining = simpleToRoute(NavigationPages.training);
  navigateToTrainingInstr = simpleToRoute(NavigationPages.trainingInstr);
  navigateToAssumptionInstr = routeWithParams<number>(NavigationPages.assumptionInstr);

  navigateToMyScreen = simpleToRoute(NavigationPages.myScreen);

  //Phonetics
  navigateToPhoneticLevels = simpleToRoute(NavigationPages.phoneticLevels);
  navigateToPhoneticTopic = routeWithParams<{assumption: any}>(NavigationPages.phoneticTopic);
  navigateToPhoneticAssumption1 = simpleToRoute(NavigationPages.phoneticAssumption1);
  navigateToPhoneticAssumption2 = simpleToRoute(NavigationPages.phoneticAssumption2);
  navigateToPhoneticTheory = routeWithParams<{data: any}>(NavigationPages.phoneticTheory);

  navigateToPhoneticTesting = simpleToRoute(NavigationPages.phoneticTesting);

  navigateToVocabularyInstr = simpleToRoute(NavigationPages.vocabularyInstr);
  navigateToVocabularyEssay = simpleToRoute(NavigationPages.vocabularyEssay);
  navigateToVocabularyLevels = simpleToRoute(NavigationPages.vocabularyLevels);
  navigateToVocabularyTopic = routeWithParams<{item: any}>(NavigationPages.vocabularyTopic);
  navigateToVocabularyTesting = simpleToRoute(NavigationPages.vocabularyTesting);
  navigateToVocabularyAssumptionInstr = simpleToRoute(NavigationPages.vocabularyAssumptionInstr);
  navigateToVocabularyAssumption1 = simpleToRoute(NavigationPages.vocabularyAssumption1);

  navigateToTestingSuccessfulFinish = routeWithParams<number>(NavigationPages.testingSuccessfulFinish);
  navigateToFinishWithErrors = routeWithParams<{seeErrors: any, testingAgain: any}>(NavigationPages.finishWithErrors);
  navigateToSuccessfulFinish = simpleToRoute(NavigationPages.successfulFinish);

  navigateToEssayFinish = routeWithParams<any>(NavigationPages.essayFinish);

  //grammar
  navigateToGrammarInstruction = simpleToRoute(NavigationPages.grammarInstruction);
  navigateToGrammarLevels = simpleToRoute(NavigationPages.grammarLevels);

  navigateToGrammarAssumptionInstr = routeWithParams<number>(NavigationPages.grammarAssumptionInstr);

  navigateToGrammarAssumption1 = simpleToRoute(NavigationPages.grammarAssumption);
  navigateToGrammarAssumption2 = simpleToRoute(NavigationPages.grammarAssumption);
  navigateToGrammarAssumption3 = simpleToRoute(NavigationPages.grammarAssumption);
  navigateToGrammarAssumption4 = simpleToRoute(NavigationPages.grammarAssumption);
  navigateToGrammarAssumption5 = simpleToRoute(NavigationPages.grammarAssumption);

  navigateToGrammarTheory = routeWithParams<{data: any}>(NavigationPages.grammarTheory);
  navigateToGrammarTopic = routeWithParams<{item: any}>(NavigationPages.grammarTopic);

  //audition
  navigateToAuditionInstruction = simpleToRoute(NavigationPages.auditionInstruction);
  navigateToAuditionLevels = simpleToRoute(NavigationPages.auditionLevels);

  navigateToAuditionTopic = routeWithParams<{item: any}>(NavigationPages.auditionTopic);

  navigateToAuditionAssumptionInstr = simpleToRoute(NavigationPages.auditionAssumptionInstr);
  navigateToAuditionRecording = simpleToRoute(NavigationPages.auditionRecording);
  navigateToAuditionTesting1 = simpleToRoute(NavigationPages.auditionTesting1);
  navigateToAuditionTesting2 = simpleToRoute(NavigationPages.auditionTesting2);

  //reading
  navigateToReadingEssayInstr = simpleToRoute(NavigationPages.readingEssayInstr);
  navigateToReadingEssay = routeWithParams<{data: any}>(NavigationPages.readingEssay);
  navigateToReadingLevels = simpleToRoute(NavigationPages.readingLevels);
  navigateToReadingEssayFeedback = routeWithParams<{data: any}>(NavigationPages.readingEssayFeedback);

  navigateToReadingText = routeWithParams<{data: any}>(NavigationPages.readingText);

  //writing
  navigateToWritingMain = simpleToRoute(NavigationPages.writingMain);
  navigateToWritingEssay = simpleToRoute(NavigationPages.writingEssay);
  navigateToWritingEssayDone = simpleToRoute(NavigationPages.writingEssayDone);
  navigateToWritingEssayFeedBack = simpleToRoute(NavigationPages.writingEssayFeedback);
  navigateToWritingEssayNoFeedBack = simpleToRoute(NavigationPages.writingEssayNoFeedback);

  //Dictionary
  navigateToDictionary = simpleToRoute(NavigationPages.dictionary);
  navigateToDictionaryWordDetail = simpleToRoute(NavigationPages.dictionaryWordDetail);
  navigateToDictionaryTraining = simpleToRoute(NavigationPages.dictionaryTraining);
  navigateToDictionaryTesting = simpleToRoute(NavigationPages.dictionaryTesting);
  navigateToDictionaryAddWord = simpleToRoute(NavigationPages.dictionaryAddWord);

  navigateToConnectMessage = simpleToRoute(NavigationPages.connectMessage);

  navigateToPrivacyPolicy = simpleToRoute(NavigationPages.privacyPolicy);
  navigateToOffer = simpleToRoute(NavigationPages.offer);

  navigateToBack = (): SimpleThunk => {
    return async (dispatch, getState): Promise<void> => {
      const backAction = getBackAction(getState().navigation);
      if (backAction !== null) {
        dispatch(backAction);
      }
    };
  };

  navigateGoBack = (): SimpleThunk => {
    return async (dispatch): Promise<void> => {
      dispatch(NavigationActions.back());
    };
  };

  navigateToEndAssemt = (): SimpleThunk => {
    return async (dispatch): Promise<void> => {
      dispatch(NavigationActions.back());
      dispatch(NavigationActions.back());
      dispatch(NavigationActions.back());
    };
  };

  internal = {
      backInRoot: actionCreator("AppNavigation/BACK_IN_ROOT"),
  };
}

export interface ICommonNavParams {
    id: string;
}