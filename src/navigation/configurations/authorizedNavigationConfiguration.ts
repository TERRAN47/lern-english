import {
  createStackNavigator,
  createDrawerNavigator
} from 'react-navigation'
import {NavigationPages} from '../navigation'
import { OnlineLesson, Statistics, HomeCalendar } from '../../modules/home'
//import {Instruction} from '../../modules/training'
import {Colors, windowWidth, isIos} from '../../core/theme'
import {TabsNavigator} from './tabsNavigator'
import { DrawerScreen } from '../menu/components/DrawerScreen'
import { About } from '../../modules/About'
import { ProfileSettings } from '../../modules/ProfileSettings'
import { FinishWithErrors } from '../../common/modules/FinishWithErrors'
import { SuccessfulFinish } from '../../common/modules/SuccessfulFinish'
import { TestingSuccessfulFinish } from '../../common/modules/TestingSuccessfulFinish'
import { EssayFinish } from '../../common/modules/EssayFinish'
import { HomeWork } from '../../modules/homeWork/HomeWork'
import { 
  //Training,
  VocabularyInstr, 
  VocabularyEssay,
  VocabularyLevels,
  VocabularyTopic,
  VocabularyTesting,
  VocabularyAssumptionInstr,
  VocabularyAssumption1,
  AssumptionInstr, 
  PhoneticAssumption1,  
  PhoneticAssumption2, 
  PhoneticsInstruction,
  PhoneticTheory,
  PhoneticTesting,
  PhoneticTraningInstr,
  GrammarInstruction,
  GrammarLevels,
  GrammarTopic,
  GrammarTheory,
  GrammarAssumptionInstr,
  GrammarAssumption,
  AuditionInstruction,
  AuditionLevels,
  AuditionTopic,
  AuditionAssumptionInstr,
  AuditionRecording,
  AuditionTesting1,
  AuditionTesting2,
  ReadingEssayInstr,
  ReadingText,  
  ReadingEssay,
  ReadingLevels,
  ReadingEssayFeedback,
  WritingMain,
  WritingEssay,
  WritingEssayDone,
  WritingEssayFeedback,
  WritingEssayNoFeedback,
  Dictionary,
  DictionaryWordDetail,
  DictionaryTraining,
  DictionaryTesting,
  DictionaryAddWord,
  PhoneticTopic,
  PhoneticLevels
} from '../../modules/training'
import { Contacts } from '../../modules/Contacts'
import { PrivacyPolicy } from '../../modules/PrivacyPolicy'
import { Offer } from '../../modules/Offer'
import { ConnectMessage } from '../../modules/connect/ConnectMessage'
import { MyScreen } from '../../modules/training/playground/MyScreen'

const DrawerNavigator = createDrawerNavigator({
  tabs: TabsNavigator,

},{
  contentComponent: DrawerScreen,
  drawerWidth: windowWidth * .9,
})

export const AuthorizedNavigar = createStackNavigator({
  
  drawer:{ 
    screen: DrawerNavigator,
    navigationOptions: {header:null},
  },

  [NavigationPages.about]: {
    screen: About,
  },
  [NavigationPages.profileSettings]: {
    screen: ProfileSettings
  },
  [NavigationPages.contacts]: {
    screen: Contacts,
  },

  [NavigationPages.privacyPolicy]: { screen: PrivacyPolicy },
  [NavigationPages.offer]: { screen: Offer },

  //Home
  [NavigationPages.homeCalendar]: { screen: HomeCalendar},
  [NavigationPages.appOnlineLesson]: { screen: OnlineLesson},
  [NavigationPages.homeStatistics]: { screen: Statistics },
  [NavigationPages.homeCalendar]: { screen: HomeCalendar},

  [NavigationPages.homeWork]: { screen: HomeWork},

  //Connect
  [NavigationPages.connectMessage]: { screen: ConnectMessage },
  
  //common
  [NavigationPages.successfulFinish]: { screen: SuccessfulFinish},
  [NavigationPages.finishWithErrors]: { screen: FinishWithErrors},
  [NavigationPages.testingSuccessfulFinish]: { screen: TestingSuccessfulFinish },
  [NavigationPages.essayFinish]: { screen: EssayFinish },
  [NavigationPages.myScreen]: { screen: MyScreen },
  
  //Vocabulary
  [NavigationPages.vocabularyInstr]: { screen: VocabularyInstr },
  [NavigationPages.vocabularyEssay]: { screen: VocabularyEssay },
  [NavigationPages.vocabularyLevels]: { screen: VocabularyLevels },
  [NavigationPages.vocabularyTopic]: { screen: VocabularyTopic },
  [NavigationPages.vocabularyTesting]: { screen: VocabularyTesting },
  [NavigationPages.vocabularyAssumptionInstr]: { screen: VocabularyAssumptionInstr },
  [NavigationPages.vocabularyAssumption1]: { screen: VocabularyAssumption1 },

  //Phonetics
  [NavigationPages.phoneticsInstr]: { screen: PhoneticsInstruction },
  [NavigationPages.assumptionInstr]: { screen: AssumptionInstr },
  [NavigationPages.phoneticAssumption1]: { screen: PhoneticAssumption1 },
  [NavigationPages.phoneticAssumption2]: { screen: PhoneticAssumption2 },
  [NavigationPages.trainingInstr]: { screen: PhoneticTraningInstr },
  [NavigationPages.phoneticTopic]: { screen: PhoneticTopic },
  [NavigationPages.phoneticLevels]: { screen: PhoneticLevels },
  [NavigationPages.phoneticTheory]: { screen: PhoneticTheory },
  [NavigationPages.phoneticTesting]: { screen: PhoneticTesting },

  //Grammar

  
  [NavigationPages.grammarInstruction]: { screen: GrammarInstruction },
  [NavigationPages.grammarLevels]: { screen: GrammarLevels },
  [NavigationPages.grammarTopic]: { screen: GrammarTopic },
  [NavigationPages.grammarTheory]: { screen: GrammarTheory },
  [NavigationPages.grammarAssumptionInstr]: { screen: GrammarAssumptionInstr },
  [NavigationPages.grammarAssumption]: { screen: GrammarAssumption },

  //Audition 
  [NavigationPages.auditionInstruction]: { screen: AuditionInstruction },
  [NavigationPages.auditionLevels]: { screen: AuditionLevels },
  [NavigationPages.auditionTopic]: { screen: AuditionTopic },
  [NavigationPages.auditionAssumptionInstr]: { screen: AuditionAssumptionInstr },
  [NavigationPages.auditionRecording]: { screen: AuditionRecording },
  [NavigationPages.auditionTesting1]: { screen: AuditionTesting1 },
  [NavigationPages.auditionTesting2]: { screen: AuditionTesting2 },

  //Reading
  [NavigationPages.readingEssayInstr]: { screen: ReadingEssayInstr },
  [NavigationPages.readingText]: { screen: ReadingText },
  [NavigationPages.readingEssay]: { screen: ReadingEssay },
  [NavigationPages.readingLevels]: { screen: ReadingLevels },
  [NavigationPages.readingEssayFeedback]: { screen: ReadingEssayFeedback },

  //Writing
  [NavigationPages.writingMain]: { screen: WritingMain },
  [NavigationPages.writingEssay]: { screen: WritingEssay },
  [NavigationPages.writingEssayDone]: { screen: WritingEssayDone },
  [NavigationPages.writingEssayFeedback]: { screen: WritingEssayFeedback },
  [NavigationPages.writingEssayNoFeedback]: { screen: WritingEssayNoFeedback },

  //Dictionary
  [NavigationPages.dictionary]: { screen: Dictionary },
  [NavigationPages.dictionaryWordDetail]: { screen: DictionaryWordDetail },
  [NavigationPages.dictionaryTraining]: { screen: DictionaryTraining },
  [NavigationPages.dictionaryTesting]: { screen: DictionaryTesting },
  [NavigationPages.dictionaryAddWord]: { screen: DictionaryAddWord },

  //[NavigationPages.phoneticsInstr]: { screen: PhoneticsInstr },
}, {
  //headerMode: "screen",
   navigationOptions: {header:null},
  //defaultNavigationOptions: headerOptions,
  cardStyle: {
    backgroundColor: isIos ? Colors.white : Colors.transparent
  },
})
