import { createStackNavigator } from "react-navigation";
import { extendWithDontPushTwoPageInStack } from "../../extendWithDontPushTwoPageInStack";
import { NavigationPages } from "../../navigation";
import { Colors } from "../../../core/theme";
import {Training } from "../../../modules/training";

export const TrainingStackNavigator = createStackNavigator(
  {
    [NavigationPages.training]: { screen: Training },

  },
  {
    headerMode: "screen",
    cardStyle: { backgroundColor: Colors.greyCE },
    headerLayoutPreset: "center",
  }
);

extendWithDontPushTwoPageInStack(TrainingStackNavigator.router);