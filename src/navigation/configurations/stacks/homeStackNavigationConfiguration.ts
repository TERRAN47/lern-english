import { createStackNavigator } from 'react-navigation'
import { extendWithDontPushTwoPageInStack } from '../../extendWithDontPushTwoPageInStack'
import { NavigationPages } from '../../navigation'
import { Colors } from '../../../core/theme'
import {Home} from '../../../modules/home'

export const HomeStackNavigator = createStackNavigator(
  {
    [NavigationPages.home]: { screen: Home},
  },
  {
    headerMode: 'screen',
    cardStyle: { backgroundColor: Colors.greyCE },
    headerLayoutPreset: 'center',
  }
)

extendWithDontPushTwoPageInStack(HomeStackNavigator.router)