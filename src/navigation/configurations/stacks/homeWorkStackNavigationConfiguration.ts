import { createStackNavigator } from 'react-navigation'
import { extendWithDontPushTwoPageInStack } from '../../extendWithDontPushTwoPageInStack'
import { NavigationPages } from '../../navigation'
import { Colors } from '../../../core/theme'
import { HomeWork } from '../../../modules/homeWork/HomeWork'

export const HomeWorkStackNavigator = createStackNavigator(
  {
    [NavigationPages.homeWork]: { screen: HomeWork }
  },
  {
    headerMode: 'screen',
    cardStyle: { backgroundColor: Colors.greyCE },
    headerLayoutPreset: 'center',
  }
)

extendWithDontPushTwoPageInStack(HomeWorkStackNavigator.router)