import { createStackNavigator } from 'react-navigation'
import { extendWithDontPushTwoPageInStack } from '../../extendWithDontPushTwoPageInStack'
import { NavigationPages } from '../../navigation'
import { Colors } from '../../../core/theme'
import { Connect } from '../../../modules/connect/Connect'

export const ConnectStackNavigator = createStackNavigator(
  {
    [NavigationPages.connect]: { screen: Connect }
  },
  {
    headerMode: 'screen',
    cardStyle: { backgroundColor: Colors.greyCE },
    headerLayoutPreset: 'center',
  }
)

extendWithDontPushTwoPageInStack(ConnectStackNavigator.router)