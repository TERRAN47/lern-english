import {
    createStackNavigator,
    NavigationAction,
    NavigationActions,
    NavigationState,
    StackActions
} from "react-navigation";
import {CoreActions} from "../../core/store";
import {IAppState} from "../../core/store/appState";
import {LoginActions} from "../../modules/login/loginActions";
import {extendWithDontPushTwoPageInStack} from "../extendWithDontPushTwoPageInStack";
import {NavigationPages} from "../navigation";
import { Registration } from '../../modules/login/Registration'
import { Login } from '../../modules/login/Login'
import { AuthorizedNavigar } from './authorizedNavigationConfiguration'
import {Colors, isIos} from '../../core/theme'

//PhoneticAssumption2,
//import { PlainHeader } from '../../common/components/Headers'

// export const RootNavigator = createStackNavigator({
//     [NavigationPages.login]: {screen: Login},
//     [NavigationPages.registration]: {screen: ProfileSettings},
//     [NavigationPages.inDeveloping]: {screen: InDeveloping},
//     [NavigationPages.menu]: {
//         getScreen: (): NavigationComponent => NavigationConfig.instance.getNavigationComponent("menu")
//     },

//import { headerOptions } from '../components/headerOptions'



export const RootNavigator = createStackNavigator({
  [NavigationPages.login]: {
    screen: Login
  },
  [NavigationPages.registration]: {screen: Registration},
  Authed:AuthorizedNavigar
}, {
  headerMode: "screen",
  navigationOptions: {header:null},
  //initialRouteName: NavigationPages.login,
  //defaultNavigationOptions: headerOptions,
  cardStyle: {
    backgroundColor: isIos ? Colors.white : Colors.transparent
  },
});

extendWithDontPushTwoPageInStack(RootNavigator.router);

export const RootNavigationInitialState = RootNavigator.router.getStateForAction(NavigationActions.init({}), undefined);

export function navigateAuthScreens(): NavigationState {
  return RootNavigator.router.getStateForAction(StackActions.reset({
    index: 0,
    key:null,
    actions: [
      NavigationActions.navigate({
        routeName: 'Authed',
      })
    ]
  }), RootNavigationInitialState);
}

export function rootNavigationReducer(
  state: NavigationState = RootNavigationInitialState,
  action: NavigationAction): NavigationState {
  switch (action.type) {
    case CoreActions.rehydrate.type:
      const appState = (action as any).payload as IAppState;
      if (appState != null && appState.system.authToken != null) {
        return RootNavigator.router.getStateForAction(StackActions.reset(
          {
            index: 0,
            key:null,
            actions: [
              NavigationActions.navigate({
                routeName:'Authed',
              })
            ]
          }
        ), state);
      }
      return {...RootNavigationInitialState};

    case LoginActions.authUser.done.type:
      return RootNavigator.router.getStateForAction(StackActions.reset(
        {
          index: 0,
          key:null,
          actions: [
              NavigationActions.navigate({
                  routeName: 'Authed',
              })
          ]
        }
      ), state);

    case LoginActions.registerUser.done.type:
      return RootNavigator.router.getStateForAction(StackActions.reset(
        {
          index: 0,
          key:null,
          actions: [
              NavigationActions.navigate({
                  routeName: 'Authed',
              })
          ]
        }
      ), state);
    default:
      return RootNavigator.router.getStateForAction(action, state);
  }
}