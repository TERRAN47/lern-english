import { createBottomTabNavigator } from 'react-navigation'
import { tabBarOptions } from '../components/TabBarOptions'
import { tabBarStyle } from '../../core/theme/commonStyles'
//import { EmptyComponent } from '../../common/components/EmptyComponent'
import { HomeStackNavigator } from './stacks/homeStackNavigationConfiguration'
import { HomeWorkStackNavigator } from './stacks/homeWorkStackNavigationConfiguration'
import { ConnectStackNavigator } from './stacks/connectStackNavigationConfiguration'
import { TrainingStackNavigator } from './stacks/trainingStackNavigationConfiguration'

export const TabsNavigator = createBottomTabNavigator(
  {
    home: HomeStackNavigator,
    connect: ConnectStackNavigator,
    homeWork: HomeWorkStackNavigator,
    training: TrainingStackNavigator,
  },
  {
    defaultNavigationOptions: tabBarOptions,
    tabBarOptions: { style: tabBarStyle.container },
  }
)

