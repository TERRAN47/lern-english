import React from "react";
import { Text, Platform, 	Dimensions, TouchableOpacity, ViewStyle, TextStyle, ScrollView, View} from "react-native";
import { Dispatch } from 'redux'
import {styleSheetCreate} from "../../../common/utils";
import { DrawerItemsProps } from 'react-navigation'
import {Colors} from "../../../core/theme";
import Entypo from 'react-native-vector-icons/Entypo'
import { NoHeader } from '../../../common/components/Headers'
import { BaseReduxComponent } from '../../../core/BaseComponent'
import { connectAdv } from '../../../core/store/connectAdv'
import { NavigationActions } from '../../../navigation/navigation'
import { localization as l } from '../../../common/localization/localization'
import {IAppState} from "../../../core/store/appState";
import {BaseRequest} from "../../../core/api/BaseRequest";

const { width, height } = Dimensions.get('window')
interface IStateProps {
  user:{firstName:string, surName:string}
}

let paddingTop = 0
if(Platform.OS == 'ios'){
	if(height >= 812 || width >= 812){
		paddingTop = 25
	}else{
		paddingTop = 0
	}
}

interface IDispatchProps {
  navigateToAbout(): void
  navigateToProfSettings(): void
  navigateToContacts(): void
  navigateToPrivacyPolicy():void
  navigateToOffer(): void
  navigateToAuth(): void
}

interface IState {

}

@connectAdv(
  (state: IAppState): IStateProps => ({
    user: state.common.user,
  }),
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToAbout(): void {
      dispatch(NavigationActions.navigateToAbout())
    },
    async navigateToAuth(): Promise<void> {
      await BaseRequest.globalOptions.setToken(null)
      dispatch(NavigationActions.navigateToAuth())
    },
    navigateToProfSettings(): void {
      dispatch(NavigationActions.navigateToProfileSettings())
    },
    navigateToContacts(): void {
      dispatch(NavigationActions.navigateToContacts())
    },
    navigateToPrivacyPolicy(): void {
      dispatch(NavigationActions.navigateToPrivacyPolicy())
    },
    navigateToOffer(): void {
      dispatch(NavigationActions.navigateToOffer())
    },
  })
)
export class DrawerScreen extends BaseReduxComponent<IStateProps, IDispatchProps, IState, DrawerItemsProps> {
  static navigationOptions = NoHeader()

  goToBack(){
    this.props.navigation.goBack()  
  }
  render(): JSX.Element {
    let {user} = this.stateProps

    return(
      <View>
        <ScrollView>
          <View style={styles.headBlock}>
            <View 
             
              style={[styles.drawerBlock, {borderBottomWidth:0}]}
            >
              <Text style={[styles.drawerText, {color:Colors.white}]}>
                {user.firstName ? `${user.firstName} ${user.surName}` : l.common.fillYourName}
              </Text>
            </View>
            <TouchableOpacity 
              onPress={this.goToBack.bind(this)} 
              style={styles.backIcon}
            >
              <Entypo name="chevron-thin-right" size={25} color={Colors.white} />
            </TouchableOpacity>
          </View>
          <TouchableOpacity onPress={this.dispatchProps.navigateToAbout} style={styles.drawerBlock}>
            <Text style={styles.drawerText}>{l.common.infoCompany}</Text>
          </TouchableOpacity>
          <TouchableOpacity 
            style={styles.drawerBlock} 
            onPress={this.dispatchProps.navigateToPrivacyPolicy}
          >
            <Text style={styles.drawerText}>{l.common.privacyPolicy}</Text>
          </TouchableOpacity>
          <TouchableOpacity 
            style={styles.drawerBlock} 
            onPress={this.dispatchProps.navigateToContacts}
          >
            <Text style={styles.drawerText}>{l.common.companyContacts}</Text>
          </TouchableOpacity>
          <TouchableOpacity 
            style={styles.drawerBlock} 
            onPress={this.dispatchProps.navigateToOffer}
          >
            <Text style={styles.drawerText}>{l.common.offer}</Text>
          </TouchableOpacity>

          <TouchableOpacity 
            style={styles.drawerBlock} 
            onPress={this.dispatchProps.navigateToProfSettings}
          >
            <Text style={styles.drawerText}>{l.common.profile}</Text>
          </TouchableOpacity>  

          <TouchableOpacity 
            style={styles.drawerBlock} 
            onPress={this.dispatchProps.navigateToAuth}
          >
            <Text style={styles.drawerText}>{l.common.exit}</Text>
          </TouchableOpacity>                  
        </ScrollView>
      </View>
    );
  }
}

const styles = styleSheetCreate({
  drawerBlock:{
    height:70,
    justifyContent:'center',
    padding:15,
    borderBottomWidth:1,
    borderBottomColor:'#EEEEEE'
  } as ViewStyle,
  drawerText:{
    fontSize:16,
    color:'#1F3059'
  } as TextStyle,
  headBlock:{
    backgroundColor:'#4E71EC',
    justifyContent:'space-between',
    flexDirection:'row',
    paddingTop,
    alignItems:'center'
  } as ViewStyle,
  backIcon:{
    padding:10,
  } as ViewStyle,
});