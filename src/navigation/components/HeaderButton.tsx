import React from "react";
import {TouchableOpacity, ViewStyle} from "react-native";
import {Dispatch} from "redux";
import {styleSheetCreate} from "../../common/utils";
import {BaseReduxComponent} from "../../core/BaseComponent";
import {connectAdv} from "../../core/store";

interface IProps {
    action?: () => any;
    onPress?: () => void;
}

interface IDispatchProps {
    dispatch: Dispatch;
}

@connectAdv(null, dispatch => ({dispatch}))
export class HeaderButton extends BaseReduxComponent<IEmpty, IDispatchProps, IEmpty, IProps> {
    constructor(props: IProps) {
        super(props);
        this.onPress = this.onPress.bind(this);
    }

    render(): JSX.Element {
        return (
            <TouchableOpacity style={styles.container} onPress={this.onPress} activeOpacity={0.7}>
                {this.props.children}
            </TouchableOpacity>
        );
    }

    private onPress(): void {
        this.props.action && this.dispatchProps.dispatch(this.props.action());
        this.props.onPress && this.props.onPress();
    }
}

const styles = styleSheetCreate({
    container: {
        minWidth: 50,
        minHeight: 39,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center"
    } as ViewStyle,
});