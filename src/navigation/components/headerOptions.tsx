// import { ImageResources } from '../../common/ImageResources.g'
// import { Image, Text, TextStyle, ImageURISource } from 'react-native'
import { NavigationScreenConfig, NavigationScreenOptions } from 'react-navigation'
import { PlainHeader } from '../../common/components/Headers'

export const headerOptions: NavigationScreenConfig<NavigationScreenOptions> = ({ navigation }): NavigationScreenOptions => {
    const routeName: string = navigation.state.routeName
    return PlainHeader(routeName, true, true)
}