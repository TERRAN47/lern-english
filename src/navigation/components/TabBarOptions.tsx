import { ImageResources } from '../../common/ImageResources.g'
import { Image, Text, TextStyle, ImageURISource } from 'react-native'
import { NavigationActions, NavigationScreenConfig, NavigationScreenOptions } from 'react-navigation'
import * as React from 'react'
import { tabBarStyle } from '../../core/theme/commonStyles'
import { windowHeight } from '../../core/theme'
import { NavigationPages } from '../navigation'
import { localization } from '../../common/localization/localization'
import { styleSheetCreate,  } from '../../common/utils' //styleSheetFlatten
import { isIphoneX, Colors } from "../../core/theme"

const tabBarIcons = {
  home: [ImageResources.home_inactive, ImageResources.home_active],
  connect: [ImageResources.connect_inactive, ImageResources.connect_active],
  homeWork: [ImageResources.homeWork_inactive, ImageResources.homeWork_active],
  training: [ImageResources.training_inactive, ImageResources.training_active],
} as { [key: string]: ImageURISource[] }

let isLogged: boolean = false
export const setLoggedState = (state: boolean): void =>  {
  isLogged = state
};
// eventRegister.addEventListener(EventNames.login, setLoggedState.bind(null, true));
// eventRegister.addEventListener(EventNames.logout, setLoggedState.bind(null, false));

export const tabBarOptions: NavigationScreenConfig<NavigationScreenOptions> =
  ({ navigation }): NavigationScreenOptions => {
    const routeName: string = navigation.state.routeName
    return (
      {
        tabBarIcon: (iconInfo: { tintColor: string | null; focused: boolean; horizontal: boolean }):
          JSX.Element => {
            const uri: ImageURISource = tabBarIcons[routeName][+iconInfo.focused]
            return (
              <Image
                source={uri}
                style={tabBarStyle.icon}
                resizeMode="contain"
              />
            )
          },
         
          tabBarLabel: (options: {tintColor: string | null; focused: boolean }): React.ReactElement<any> | null => {
            // const style: any = styleSheetFlatten(
            //   [styles.label, options.focused ? {color: Colors.greenish} : {}]
            // )

            return (
              <Text style={styles.label}>{(localization.pages as any)[routeName]}</Text>
            )
          },

          tabBarOnPress: async (options: any): Promise<void> => {
            const navKey: string = options.navigation.state.key;
              if ( navKey === 'profile' ) {
                if ( !isLogged ) {
                  options.navigation.navigate({
                    routeName: 'auth',
                    action: NavigationActions.navigate({routeName: NavigationPages.login})
                  })

                  return
                }
              } else if ( navKey === 'create' ) {
                if ( !isLogged ) {
                  options.navigation.navigate({
                    routeName: 'auth',
                    action: NavigationActions.navigate({routeName: NavigationPages.login})
                  })
                  return
                }
              } else if ( navKey === 'messages' ) {
                return
              }
              options.defaultHandler();
          }
        }
      )
  }

  const styles = styleSheetCreate({
    label: {
      fontSize: isIphoneX ? windowHeight * 0.012 : windowHeight * 0.014,
  
      color: Colors.warmGrey,
      marginBottom: windowHeight * .003,
      textAlign: 'center',
    } as TextStyle
  })