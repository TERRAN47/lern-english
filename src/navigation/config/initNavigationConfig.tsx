import {appSettingsProvider} from "../../core/settings";
import {mainHeaderNavigation} from "../../core/theme/navigation"; //NoHeaderNavigation
import {menuNavigationReducer, MenuNavigator} from "../configurations/menuNavigationConfiguration";
import {rootNavigationReducer, RootNavigator} from "../configurations/rootNavigationConfiguration";
import {AuthorizedNavigar} from "../configurations/authorizedNavigationConfiguration";

import {NavigationActions} from "../navigation";
import {NavigationConfig} from "./NavigationConfig";
import {reduxHelper} from "./reduxHelper";

export function initNavigationConfig(): void {
    const isRehydrateEnabled = appSettingsProvider.settings.environment == "Development";

    NavigationConfig.instance = reduxHelper({
        root: {
            isRehydrateEnabled,
            customReducer: rootNavigationReducer,
            navigator: RootNavigator,
            navigationOptions: mainHeaderNavigation,
            backAction: NavigationActions.internal.backInRoot
        },
        authStack: {
            isRehydrateEnabled,
            navigator: AuthorizedNavigar,
            navigationOptions: mainHeaderNavigation,
            backAction: NavigationActions.internal.backInRoot
        },
        menu: {
            isRehydrateEnabled,
            customReducer: menuNavigationReducer,
            navigator: MenuNavigator,
            navigationOptions: mainHeaderNavigation("menu", "none"),
        },
    });
}