export class Pages {
  menu = "menu"
  main = "main"

  login = "login"
  registration = "registration"
  about = "about"
  profileSettings = "profileSettings"
  contacts = "contacts"

  plannedRuns = "plannedRuns"
  currentRun = "currentRun"

  playground = "playground"
  inDeveloping = "inDeveloping"
  

  //Footer menu
  home = "home"
  connect = "connct"
  homeWork = "homeWork"
  training = "training"

  //Home creen
  appOnlineLesson = "appOnlineLesson"
  homeCalendar = "homeCalendar"
  homeStatistics = "homeStatistics"

  //common
  successfulFinish = "successfulFinish"
  finishWithErrors = "finishWithErrors"
  testingSuccessfulFinish = "testingSuccessfulFinish"
  essayFinish = "essayFinish"
  myScreen = "myScreen"

  //Connect
  connectMessage = "connectMessage"

  //Vocabulary 
  vocabularyInstr = "vocabularyInstr"
  vocabularyEssay = "vocabularyEssay"
  vocabularyLevels = "vocabularyLevels"
  vocabularyTopic = "vocabularyTopic"
  vocabularyTesting = "vocabularyTesting"
  vocabularyAssumptionInstr = "vocabularyAssumptionInstr"
  vocabularyAssumption1 = "VocabularyAssumption1"

  //Phonetics
  phoneticsInstr = "phoneticsInstr"
  assumptionInstr = "assumptionInstr"
  phoneticAssumption1 = "phoneticAssumption1"
  phoneticAssumption2 = "phoneticAssumption2"
  phoneticLevels = "phoneticLevels"
  phoneticTopic = "phoneticTopic"
  phoneticTheory = "phoneticTheory"
  phoneticSuccessfulFinish = "phoneticSuccessfulFinish"
  phoneticTesting = "phoneticTesting"    
  phoneticFinishWithErrors = "PhoneticFinishWithErrors"
  trainingInstr = "trainingInstr"

  //Grammar
  grammarInstruction = "grammarInstruction"
  grammarLevels = "grammarLevels"
  grammarTopic = "grammarTopic"
  grammarTheory = "grammarTeory"
  grammarAssumptionInstr = "grammarAssumptionInstr"
  grammarAssumption = "grammarAssumption"

  //Audition
  auditionInstruction = "auditionInstruction"
  auditionLevels = "auditionLevels"
  auditionTopic = "auditionTopic"
  auditionAssumptionInstr = "auditionAssumptionInstr"
  auditionRecording = "auditionRecording"
  auditionTesting1 = "auditionTesting1"
  auditionTesting2 = "auditionTesting2"
  
  //Reading
  readingEssayInstr = "readingEssayInstr"
  readingText = "readingText"
  readingEssay = "readingEssay"
  readingLevels = "readingLevels"
  readingEssayFeedback = "readingEssayFeedback"

  //Writing
  writingMain = "writingMain"
  writingEssay = "writingEssay"
  writingEssayDone = "writingEssayDone"
  writingEssayFeedback = "writingEssayFeedback"
  writingEssayNoFeedback = "writingEssayNoFeedback"

  //Dictionary
  dictionary = "dictionary"
  dictionaryWordDetail = "dictionaryWordDetail"
  dictionaryTraining = "dictionaryTraining"
  dictionaryTesting = "dictionaryTesting"
  dictionaryAddWord = "dictionaryAddWord"
  
  privacyPolicy = "privacyPolicy"
  offer = "offer"
}