/*tslint:disable*/
import { BaseRequest } from "../BaseRequest";

export class ProfileApiRequest extends BaseRequest {
    constructor(protected baseurl: string) {
        super()
    }
    async getUserInfo(): Promise<any> {
        return await this.fetch('/app/v1/api/user/user_info', {
            method: 'GET'
        })
            .catch(BaseRequest.handleError)
    }
}