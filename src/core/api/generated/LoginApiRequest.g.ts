/*tslint:disable*/
import {BaseRequest} from "../BaseRequest";
//import {SignInRequestDto} from "./dto/SignInRequest.g";
//import {SignInResponseDto} from "./dto/SignInResponse.g";
//import { AsyncStorage } from 'react-native'
//import RNFetchBlob from 'rn-fetch-blob';
import { writeJsonFile } from '../../../common/helpers/appFiles'

export class LoginApiRequest extends BaseRequest {
  constructor(protected baseurl: string) {
    super();
    this.signIn = this.signIn.bind(this);
  }

  async signIn(request:any): Promise<any> {

    const responce = await this.fetch(`/app/v1/api/user_auth/auth`, {
      method: "POST",
      body: {...request, device_id:'dsfnsoidfhpsidfgdfgdfgoudhfopdfhdfop'}
    }).catch(BaseRequest.handleError);

    if(responce.token){
      await BaseRequest.globalOptions.setToken(responce.token)
      await BaseRequest.globalOptions.setEmail(request.username)
    }

    return responce
  }

  async registration(request:any): Promise<any> {
    return await this.fetch('/app/v1/api/user_post/new', {
      method: "POST",
      body: {...request, device_id:'dsfnsoidfhpsidfgdfgdfgoudhfopdfhdfop'}
    }).then(async(responce)=>{
      if(responce.x_token){
        await BaseRequest.globalOptions.setToken(responce.x_token)
        await BaseRequest.globalOptions.setEmail(request.username)
      }
    }).catch(BaseRequest.handleError);
  }

  async getLists(): Promise<any> {
    try{
      const data = await this.fetch(`/app/v1/api/user/lists`, {
        method: "GET",
      })

      console.log('ЛИСТ', data)

      return await writeJsonFile(data, 'lists.json')
    }catch(err){
      return BaseRequest.handleError
    }
  }
}
