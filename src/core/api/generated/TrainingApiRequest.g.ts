/*tslint:disable*/
import {BaseRequest} from "../BaseRequest";
//import {SignInRequestDto} from "./dto/SignInRequest.g";
//import {SignInResponseDto} from "./dto/SignInResponse.g";
import { getJsonFile, writeJsonFile } from '../../../common/helpers/appFiles'
import RNFetchBlob from 'rn-fetch-blob'
import public_url from "../../../../resources/settings/configDev"

export class TrainingApiRequest extends BaseRequest {
  constructor(protected baseurl: string) {
    super();
  }
  
  async getPhonetics(netStatus:boolean): Promise<any> {
    if(netStatus == true){
      try{
        const data = await this.fetch(`/app/v1/api/user/ex_fonetics`, {
          method: "GET",
        })

        console.log('ФОНЕТИКА', data)

        return await writeJsonFile(data, 'phonetic.json')
      }catch(err){
        return await getJsonFile('phonetic.json')
      }
    }else{
      try{
        return await getJsonFile('phonetic.json')
      }catch(err){
        return BaseRequest.handleError
      }
    }
  }

  async getVocabularys(netStatus:boolean): Promise<any> {
    if(netStatus == true){
      try{
        const data = await this.fetch(`/app/v1/api/user/ex_lex`, {
          method: "GET",
        })

        console.log('ЛЕКСИКА', data)

        return await writeJsonFile(data, 'vocabulary.json')
      }catch(err){
        return await getJsonFile('vocabulary.json')
      }
    }else{
      try{
        return await getJsonFile('vocabulary.json')
      }catch(err){
        return BaseRequest.handleError
      }
    }
  }

  async getReading(netStatus:boolean): Promise<any> {
    if(netStatus == true){
      try{
        console.log('ЧТЕНИЕ GET')
        const data = await this.fetch(`/app/v1/api/user/ex_read`, {
          method: "GET",
        })

       return await writeJsonFile(data, 'reading.json')
      }catch(err){
        console.log('Error', err)
        return await getJsonFile('reading.json')
      }
    }else{
      console.log('test reading', netStatus)
      try{
        return await getJsonFile('reading.json')
      }catch(err){
        return BaseRequest.handleError
      }
    }
  }
  
  async getGrammars(netStatus:boolean): Promise<any> {
    if(netStatus == true){
      try{
        const data = await this.fetch(`/app/v1/api/user/ex_grammar`, {
          method: "GET",
        })

        console.log('ГРАМАТИКА', data)

        return await writeJsonFile(data, 'grammar.json')
      }catch(err){
        return await getJsonFile('grammar.json')
      }
    }else{
      try{
        return await getJsonFile('grammar.json')
      }catch(err){
        return BaseRequest.handleError
      }
    }
  }


  async sendAssumption(request: any): Promise<any> {
    
    //console.log('Отправка сообщения>>>>>>', request)
    return this.fetch(`/app/v1/api/user_post/add_res`, {
      method: "POST",
      body: {
        section: 31,
        ex_id: request.data.item.ex_id,
        ex_done: 1,
        ex_item_id: 1,
        essy_theme: request.data.itemessy_theme,
        essy_text: request.data.textEssay,
      }
    }).catch(BaseRequest.handleError)
  }

  async addNewWordPost(data: any): Promise<any> {
    
    console.log('Отправка слова в словарь>>>>>>', data.newWord)
    return this.fetch(`/app/v1/api/user_post/add_to_bank`, {
      method: "POST",
      body: {
        ru_world: data.newWord,
        eng_world: data.newTranslateWord,
      }
    }).catch(BaseRequest.handleError)
  }

  async getAuditions(netStatus:boolean): Promise<any> {
    if(netStatus == true){
      try{
        const data = await this.fetch(`/app/v1/api/user/ex_aud`, {
          method: "GET",
        })

        console.log('АУДИРОВАНИЕ', data)

        return await writeJsonFile(data, 'audition.json')
      }catch(err){
        return await getJsonFile('audition.json')
      }
    }else{
      try{
        return await getJsonFile('audition.json')
      }catch(err){
        return BaseRequest.handleError
      }
    }
  }

  async assumptReady(data:any): Promise<any> {
    console.log('data-->>',data)
    try{
      let reqest = await this.fetch(`/app/v1/api/user_post/add_res`, {
        method: "POST",
        body: data
      })
      console.log(reqest)
    }catch(err){
      console.log(err)
    }
  }
  
  async uploadSounds(mapObject:any): Promise<any> {
    //await getJsonFile('phonetic.json')

    RNFetchBlob.fetch('GET', `${public_url}${mapObject[0].lem.dialog}`)
    .then((res) => RNFetchBlob.fs.scanFile([ { path : res.path(), mime : 'audio/mpeg' } ]))
    .then((file) => {
      console.log("MP3",file)
        // scan file success
    })
    .catch((err) => {
      console.log("ERR_MP3",err)
        // scan file error
    })

    return 'ok'
    // mapObject.forEach((elem:any)=>{
  }
}
