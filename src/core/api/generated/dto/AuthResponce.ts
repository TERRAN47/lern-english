export interface Auth {
  id: number;
  sender_address: string;
  recipient_address: string;
  cargo: string;
}