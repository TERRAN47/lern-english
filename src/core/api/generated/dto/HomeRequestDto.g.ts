export interface HomeRequestDto {
  lessonData: {
    timeDuration: string
    desiredDays: string
    classTime: string
    teacher: string
  }
  comment: string
}