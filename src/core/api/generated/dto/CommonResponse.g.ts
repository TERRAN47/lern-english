/*tslint:disable*/

export interface Profile {
    userLevel: string,
    company: string,
    userPhone: string,
    userEmail: string,
    firstName: string, 
    surName: string,
    userTariff: string,
}

export interface IEssayStatus {
    sentEssayReading: boolean,
}