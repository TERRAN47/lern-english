export interface IUserInfoResponse {
  user_data: {
    [key: string]: IUserInfo
  }
}

export interface IUserInfo {
  date_tarif: string
  level: string
  name: string | null
  user_tarif: string
}
