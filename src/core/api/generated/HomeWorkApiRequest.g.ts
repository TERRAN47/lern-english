/*tslint:disable*/
import {BaseRequest} from "../BaseRequest";
//import {SignInRequestDto} from "./dto/SignInRequest.g";
//import {SignInResponseDto} from "./dto/SignInResponse.g";
//import { AsyncStorage } from 'react-native'
import { getJsonFile, writeJsonFile } from '../../../common/helpers/appFiles'

export class HomeWorkApiRequest extends BaseRequest {
  constructor(protected baseurl: string) {
    super();
  }

  async getHomeWork(netStatus:boolean): Promise<any> {
    if(netStatus == true){
      try{
        const data = await this.fetch('/app/v1/api/user/user_dz', {
          method: "GET"
        })

        console.log('ДОМАШКА', data)

        return await writeJsonFile(data, 'homeWork.json')
      }catch(err){
        return await getJsonFile('homeWork.json')
      }
    }else{
      try{
        return await getJsonFile('homeWork.json')
      }catch(err){
        return BaseRequest.handleError
      }
    }
  }
}
