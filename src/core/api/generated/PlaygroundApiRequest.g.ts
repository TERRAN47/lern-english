import { BaseRequest } from '../BaseRequest'
import { IUserInfoResponse } from './dto/PlaygroundResponse.g'

export class PlaygroundApiRequest extends BaseRequest {
  constructor(protected baseurl: string) {
      super()
  }
  async getUserInfo(): Promise<IUserInfoResponse> {
    return await this.fetch('/app/v1/api/user/user_info', {
      method: 'GET'
    })
      .catch(BaseRequest.handleError)
  }
}
