//import {AuthenticationApiRequest} from "./AuthenticationApiRequest.g";
import { ProfileApiRequest } from "./ProfileApiRequest.g"
import { LoginApiRequest } from './LoginApiRequest.g'
import { HomeApiRequest } from './HomeApiRequest.g'
import { HomeWorkApiRequest } from './HomeWorkApiRequest.g'
import { TrainingApiRequest } from './TrainingApiRequest.g'
import { PlaygroundApiRequest } from './PlaygroundApiRequest.g'

export class RequestsRepository {
    //authenticationApiRequest = new AuthenticationApiRequest(this.baseurl);
    profileApiRequest = new ProfileApiRequest(this.baseurl)
    loginApiRequest = new LoginApiRequest(this.baseurl)
    homeApiRequest = new HomeApiRequest(this.baseurl)
    homeWorkApiRequest = new HomeWorkApiRequest(this.baseurl)
    trainingApiRequest = new TrainingApiRequest(this.baseurl)
    PlaygroundApiRequest = new PlaygroundApiRequest(this.baseurl)
    constructor(private baseurl: string) {
    }
}