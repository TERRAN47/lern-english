/*tslint:disable*/
import {BaseRequest} from "../BaseRequest";
//import {SignInRequestDto} from "./dto/SignInRequest.g";
//import {SignInResponseDto} from "./dto/SignInResponse.g";
import {HomeRequestDto} from "./dto/HomeRequestDto.g";

import { getJsonFile, writeJsonFile } from '../../../common/helpers/appFiles'

export class HomeApiRequest extends BaseRequest {
  constructor(protected baseurl: string) {
    super();
  }
  
  async sendLessons(request:HomeRequestDto): Promise<any> {

    return this.fetch(`/app/v1/api/user_post/add_lesson_odrer`, {
      method: "POST",
      body: { 
        p1:request.lessonData.timeDuration, 
        p2:request.lessonData.desiredDays, 
        p3:request.lessonData.classTime, 
        p4:request.lessonData.teacher, 
        comment:request.comment
      }
    }).catch(BaseRequest.handleError)
  }

  async getHomeCalendar(netStatus:boolean): Promise<any> {
    if(netStatus == true){
      try{
        let data = await this.fetch(`/app/v1/api/user/user_calendar`, {
          method: "GET",
        })

        console.log('КАЛЕНДАРЬ', data)

        return await writeJsonFile(data, 'homeCalendar.json')
      }catch(err){
        return await getJsonFile('homeCalendar.json')
      }
    }else{
      try{
        return await getJsonFile('homeCalendar.json')
      }catch(err){
        return BaseRequest.handleError
      }
    }
  }

  async getHomeStatistic(netStatus:boolean): Promise<any> {
    if(netStatus == true){
      try{
        let data = await this.fetch(`/app/v1/api/user/user_stat`, {
          method: "GET",
        })

        console.log('СТАТИСТИКА', data)

        return await writeJsonFile(data, 'statistic.json')
      }catch(err){
        return await getJsonFile('statistic.json')
      }
    }else{
      try{
        return await getJsonFile('statistic.json')
      }catch(err){
        return BaseRequest.handleError
      }
    }
  }
}
