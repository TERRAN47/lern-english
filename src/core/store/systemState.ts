import {INotificationInfo} from "./systemActions";

export interface ISystemState {
    buildNumber: number;
    notificationInfo: INotificationInfo | null;
    authToken: string | null;
    email: string;
}

export const SystemInitialState: ISystemState = {
    authToken: null,
    email:'',
    notificationInfo: null,
    buildNumber: 1,
};