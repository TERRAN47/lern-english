import {combineReducers} from "redux";
import {NavigationConfig} from "../../navigation/config";
import {IAppState, INavigationState} from "./appState";
import {Reducers} from "./Reducers";
import {systemReducer} from "./systemReducer";

import { loginReducer} from "../../modules/login/loginReducer";
import { homeReducer } from '../../modules/home/homeReducer'
import { homeWorkReducer } from '../../modules/homeWork/homeWorkReducer'
import { trainingReducer } from '../../modules/training/trainingReducer'
import { commonReducer } from '../../modules/commonReducer'
import { playgroundReducer } from '../../modules/training/playground/playgroudReducer'

export function createMainReducer(): any {
  const navigationReducers: Reducers<INavigationState> = NavigationConfig.instance.getReducer();

  const reducers: Reducers<IAppState> = {
    navigation: combineReducers(navigationReducers),
    system: systemReducer,
    login: loginReducer,
    home: homeReducer,
    homeWork: homeWorkReducer,
    training: trainingReducer,
    common: commonReducer,
    playground: playgroundReducer,
  }

  return combineReducers(reducers)
}