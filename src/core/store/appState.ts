import {NavigationState, DrawerNavigationState} from "react-navigation";
import {NavigationConfig} from "../../navigation/config";
import {ISystemState, SystemInitialState} from "./systemState";

import { ILoginState, LoginInitialState } from "../../modules/login/loginState";
import { HomeState, HomeInitialState } from "../../modules/home/homeState";
import { IhomeWorkState, HomeWorkInitialState } from "../../modules/homeWork/homeWorkState";
import { ITrainingState, TrainingInitialState } from "../../modules/training/trainingState";
import { CommonState, CommonInitialState } from "../../modules/commonState";
import { IPlaygroundState, PlaygroundInitialState } from "../../modules/training/playground/playgroundState";

export interface IAppState {
  navigation: INavigationState;
  system: ISystemState;
  login: ILoginState;
  home: HomeState;
  homeWork: IhomeWorkState;
  training: ITrainingState;
  common: CommonState;
  playground: IPlaygroundState;
}

export interface INavigationState {
  root: NavigationState;
  menu: DrawerNavigationState;
  currentRun: NavigationState;
  plannedRuns: NavigationState;
  authStack: NavigationState;
}

export function getAppInitialState(): IAppState {
  const NavigationInitialState: INavigationState = NavigationConfig.instance.getCombinedInitialState();

  return {
    navigation: NavigationInitialState,
    system: SystemInitialState,
    login: LoginInitialState,
    home: HomeInitialState,
    homeWork: HomeWorkInitialState,
    training: TrainingInitialState,
    common: CommonInitialState,
    playground: PlaygroundInitialState,
  };
}