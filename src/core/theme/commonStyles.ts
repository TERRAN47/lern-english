import {Platform, ImageStyle, TextStyle, ViewStyle} from "react-native";
import {styleSheetCreate, styleSheetFlatten} from "../../common/utils";
import {windowWidth, windowHeight} from './common'
import {Colors} from "./colors";
import {Fonts} from "./fonts";

export const CommonStyles = styleSheetCreate({
    flex1: {
        flex: 1
    } as ViewStyle,
    flexWhiteBackground: {
        flex: 1,
        backgroundColor: Colors.white
    } as ViewStyle,
    bottomEmptySpace: {
        height: 50,
        backgroundColor: Colors.white
    } as ViewStyle,
    iPhoneXFooter: {
        height: 20
    } as ViewStyle,
});

const commonHeaderTitleStyle = {
    alignSelf: "center",
    textAlign: "center",
    width: windowWidth*.65,
    fontSize: 20,
    fontFamily: Fonts.medium,
    color: Colors.white,
} as TextStyle;

const commonHeaderStyle = {
    borderBottomWidth: 0,
    zIndex:-1,
    alignItems:'center',
    backgroundColor: Colors.white,
    borderBottomColor: Colors.transparent,
    ...Platform.select({
        ios: {
            shadowRadius: 4,
            shadowOpacity: 0.3,
            shadowOffset: {width: 0, height: 4},
        },
        android: {
            elevation: 8
        }}),
} as ViewStyle;

export const CommonHeaderStyles = styleSheetCreate({
    headerTitleStyle: styleSheetFlatten(commonHeaderTitleStyle),
    headerStyle: styleSheetFlatten(commonHeaderStyle),
});

export const tabBarStyle = styleSheetCreate({
  icon: {
    height: windowWidth * .075,
    width: windowWidth * .075,
    alignSelf: 'center',
  } as ImageStyle,
  iconBig: {
    height: windowHeight * .05,
    width: windowHeight * .05,
    marginTop: windowHeight * .018,
  } as ImageStyle,
  container: {
    backgroundColor: Colors.white,
    height: windowWidth * .14,
    paddingBottom:5
  } as ViewStyle,
})
