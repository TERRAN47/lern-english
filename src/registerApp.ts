import {AppRegistry} from "react-native";
import {appSettingsProvider} from "./core/settings";
import {Playground} from "./common/playground";
import {App} from "./App";



// noinspection JSUnusedGlobalSymbols | used from js code
export function registerApp(): void {
    const rootComponent = appSettingsProvider.settings.devOptions.showAllComponentsOnStart ? Playground : App;
    AppRegistry.registerComponent(appSettingsProvider.settings.appName, () => rootComponent);
}

//@ts-ignore
global.XMLHttpRequest = global.originalXMLHttpRequest

//@ts-ignore
  ? global.originalXMLHttpRequest
  
//@ts-ignore
  : global.XMLHttpRequest
  
//@ts-ignore
global.FormData = global.originalFormData

//@ts-ignore
  ? global.originalFormData
  
//@ts-ignore
  : global.FormData

fetch // Ensure to get the lazy property

//@ts-ignore
if (window.__FETCH_SUPPORT__) {
  // it's RNDebugger only to have
  
//@ts-ignore
  window.__FETCH_SUPPORT__.blob = false
} else {
  /*
   * Set _FETCH_SUPPORT_ to false is just work for `fetch`.
   * If you're using another way you can just use the native Blob and remove the `else` statement
   */
  
//@ts-ignore
  global.Blob = global.originalBlob ? global.originalBlob : global.Blob
  
//@ts-ignore
  global.FileReader = global.originalFileReader
  
//@ts-ignore
    ? global.originalFileReader
    
//@ts-ignore
    : global.FileReader
}