import RNFS from "react-native-fs";
import _ from "underscore";
import config from "../../../resources/settings/configDev";

const getJsonFile = async(fileName: string): Promise<any>  => {

  const result: any = await RNFS.readDir(RNFS.DocumentDirectoryPath).then((resul: any): any => {
    console.log("GOT RESULT", resul);
    if (resul.length > 1) {
      const file = _.findWhere(resul, {
        name: fileName
      });

      if (file !== undefined) {
        //@ts-ignore
        return Promise.all([RNFS.stat(file.path), file.path]);
      } else {
        return Promise.all([]);
      }
    }
  }).then((statResult: any) => {
    if (statResult[0].isFile()) {
      return RNFS.readFile(statResult[1]);
    }

    return "no file";
  }).catch((err: any) => {
    console.log(err.message, err.code);

    return {};
  });

  return JSON.parse(result);
};

const writeJsonFile = async (data: any, fileName: string): Promise<any>  => {
  const path = RNFS.DocumentDirectoryPath + `/${fileName}`;
  const json = JSON.stringify(data);

  await RNFS.writeFile(path, json);

  return data;
};

const getAudioFile = async (fileName: string): Promise<any>  => {

  let dir: any = null;
  try {
    dir = await RNFS.readDir(RNFS.DocumentDirectoryPath).then((result): any => {
      console.log("FILE RESULT", result);
      if (result.length > 1) {
        const file = _.findWhere(result, {
          name: fileName
        });

        return file !== undefined ? file.path : null;
      }

      return null;
    }).catch((err: any) => {
      console.log(err.message, err.code);

      return null;
    });
  } catch (err) {
    console.log(err);
  }

  return dir;
};

const writeAudioFile = async (data: any): Promise<any> => {
  const path = RNFS.DocumentDirectoryPath + `${data.audio.slice(13)}`;
  try {
    const filteLoad = await RNFS.downloadFile({
      fromUrl: `${config.public_url}${data.audio}`,
      toFile: path
    }).promise.then((r) => {
      return r;
    }).catch(() => {
      return null;
    });

    return filteLoad;
  } catch (err) {
    alert("Ошибка загрузки файла, проверьте интернет");

    return null;
  }
};

export {getJsonFile, writeJsonFile, getAudioFile, writeAudioFile};