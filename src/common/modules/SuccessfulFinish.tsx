import React from 'react'
import {
  View,
  ViewStyle,
  Text,
  TextStyle,
  TouchableOpacity,
  //Platform,
  //Animated,
  //Easing,
  Image,
  ImageStyle,
} from 'react-native'
import { Dispatch } from 'redux'
import { BaseReduxComponent } from '../../core/BaseComponent'
import { connectAdv } from '../../core/store/connectAdv'
import { Colors, windowWidth } from '../../core/theme'
import { styleSheetCreate } from '../../common/utils'
import { NavigationActions } from '../../navigation/navigation'
import { ImageResources } from '../../common/ImageResources.g'
import { EndAssepHeader } from '../../common/components/Headers'
import { ButtonNoBg } from '../../common/components/ButtonNoBg'
import { localization as l } from '../../common/localization/localization'
//import LinearGradient from 'react-native-linear-gradient'

interface IStateProps {

}

interface IDispatchProps {
  navReToPhoneticTopic (): void
}

interface IState {
	
}

interface IProps {
  category: string
  title: string
  modalClose(): any
}

@connectAdv(
  null,
  (dispatch: Dispatch): IDispatchProps => ({
    navReToPhoneticTopic(): void {
      dispatch(NavigationActions.navReToBack())
      dispatch(NavigationActions.navReToBack())
      dispatch(NavigationActions.navReToBack())
    },
  })
)
export class SuccessfulFinish extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  static navigationOptions = EndAssepHeader(l.training.succesfulFinish, true, false)

  render(): JSX.Element {
    return (
      <View  style={styles.container}>
        <Image  style={styles.medal}
          source={ImageResources.medal}
        />
        <View style={styles.contentText}>
          <Text style={styles.title}>{l.training.congratulations}</Text>
          <Text  style={styles.text}>{l.training.youSuccessfullyCompletedTask}</Text>
        </View>
        <TouchableOpacity onPress={this.dispatchProps.navReToPhoneticTopic}>
          <ButtonNoBg titleButton={l.training.continueTraining} width={windowWidth * .9}/>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = styleSheetCreate({
  container: {
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: Colors.blueEC,
    paddingTop: windowWidth * .1,
  } as ViewStyle,
  medal: {
    height: windowWidth * .52,
    width: windowWidth * .4,
  } as ImageStyle,
  contentText: {
    alignItems: 'center',
    paddingHorizontal: windowWidth * .1,
  } as ViewStyle,
  title: {
    fontSize: windowWidth * .063,
		fontWeight: 'bold',
		color: Colors.white,
  } as TextStyle,
  text: {
    paddingTop: windowWidth * .047, 
    paddingHorizontal: windowWidth * .1,
		fontSize: windowWidth * .044,
    textAlign: 'center',
    color: Colors.white,
  } as TextStyle,
})

