import React from "react";
import {
  View,
  ViewStyle,
  Text,
  TextStyle,
  TouchableOpacity,
  //Platform,
  //Animated,
  //Easing,
  Image,
  ImageStyle,

} from "react-native";
import { Dispatch } from "redux";
import { localization as l } from "../../common/localization/localization";
import { BaseReduxComponent } from "../../core/BaseComponent";
import { connectAdv } from "../../core/store/connectAdv";
import { Colors, windowWidth } from "../../core/theme";
import { styleSheetCreate } from "../../common/utils";
import { NavigationActions } from "../../navigation/navigation";
import { ImageResources } from "../../common/ImageResources.g";
import { EndAssepHeader } from "../../common/components/Headers";
import { ButtonNoBg } from "../../common/components/ButtonNoBg";
//import LinearGradient from 'react-native-linear-gradient'
import {
  NavigationRoute,
  NavigationScreenProp,
} from "react-navigation";
interface IStateProps {

}

interface IDispatchProps {
  navReToPhoneticTopic (): void;
  backEnd(count: number): void;
}

interface IState {

}

interface IProps {
  category: string;
  title: string;
  navigation: NavigationScreenProp<NavigationRoute<any>>;
}

@connectAdv(
  null,
  (dispatch: Dispatch): IDispatchProps => ({
    navReToPhoneticTopic(): void {
      dispatch(NavigationActions.navReToBack());
      dispatch(NavigationActions.navReToBack());
      dispatch(NavigationActions.navReToBack());
    },
    backEnd(count: number): void {
      for (let a = 0; a <= count; a++) {
        dispatch(NavigationActions.navReToBack());
      }
    },
  })
)
export class TestingSuccessfulFinish extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  static navigationOptions = EndAssepHeader(l.training.succesfulFinish, true, false);
  backEnd = (): void => {
    const {params} = this.props.navigation.state;
    this.dispatchProps.backEnd(params);
  }

  render(): JSX.Element {
    return (
      <View  style={styles.container}>
        <Image  style={styles.cup} source={ImageResources.finishCup} />
        <View style={styles.contentText}>
          <Text style={styles.title}>{l.training.congratulations}</Text>
          <Text  style={styles.text}>{l.training.youSuccessfullyCompletedTask}</Text>
        </View>
        <TouchableOpacity onPress={this.backEnd}>
          <ButtonNoBg titleButton={l.training.continueTraining} width={windowWidth * .9}/>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = styleSheetCreate({
  container: {
    flex: 1,
    justifyContent: "space-around",
    alignItems: "center",
    backgroundColor: Colors.blueEC,
    paddingTop: windowWidth * .1,
  } as ViewStyle,
  cup: {
    height: windowWidth * .52,
    width: windowWidth * .54,
  } as ImageStyle,
  contentText: {
    alignItems: "center",
    paddingHorizontal: windowWidth * .1,
  } as ViewStyle,
  title: {
    fontSize: windowWidth * .063,
    fontWeight: "bold",
    color: Colors.white,
  } as TextStyle,
  text: {
    paddingTop: windowWidth * .047,
    paddingHorizontal: windowWidth * .1,
    fontSize: windowWidth * .044,
    textAlign: "center",
    color: Colors.white,
  } as TextStyle,
});
