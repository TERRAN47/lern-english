import React from 'react'
import {
  View,
  ViewStyle,
  Text,
  TextStyle,
  TouchableOpacity,
  Image,
  ImageStyle,
} from 'react-native'
import { Dispatch } from 'redux'
import { NavigationRoute, NavigationScreenProp } from 'react-navigation'
import { BaseReduxComponent } from '../../core/BaseComponent'
import { connectAdv } from '../../core/store/connectAdv'
import { Colors, windowWidth } from '../../core/theme'
import { localization as l } from '../../common/localization/localization'
import { styleSheetCreate } from '../utils'
import { NavigationActions } from '../../navigation/navigation'
import { ImageResources } from '../ImageResources.g'
import { PlainHeader } from '../components/Headers'
import { ButtonNoBg } from '../components/ButtonNoBg'


interface IStateProps {

}

interface IDispatchProps {
  navigateToVocabularyTopic (): void
}

interface IState {
	
}

interface IProps {
  category: string
  title: string
  modalClose(): any
  navigation: NavigationScreenProp<NavigationRoute<any>>
}

@connectAdv(
  null,
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToVocabularyTopic(): void {
      dispatch(NavigationActions.navReToBack())
      dispatch(NavigationActions.navReToBack())
      dispatch(NavigationActions.navReToBack())
    },
  })
)
export class EssayFinish extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  static navigationOptions = PlainHeader(l.training.succesfulFinish, true, false)
  render(): JSX.Element {
    //console.log('FinishData=', this.props.navigation.state)
    return (
      <View  style={styles.container}>
        <Image  style={styles.mail}
          source={ImageResources.essaySent}
        />
        <View style={styles.contentText}>
          <Text style={styles.title}>{l.training.essaySubmittedSuccessfully}</Text>
        </View>
        <TouchableOpacity onPress={this.dispatchProps.navigateToVocabularyTopic}>
          <ButtonNoBg titleButton={'Продолжить обучение'} width={windowWidth * .9}/>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = styleSheetCreate({
  container: {
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: Colors.blueEC,
    paddingTop: windowWidth * .1,
  } as ViewStyle,
  mail: {
    height: windowWidth * .5,
    width: windowWidth * .5,
  } as ImageStyle,
  contentText: {
    alignItems: 'center',
    paddingHorizontal: windowWidth * .1,
  } as ViewStyle,
  title: {
    fontSize: windowWidth * .063,
		fontWeight: 'bold',
    color: Colors.white,
    textAlign: 'center',
  } as TextStyle,
})

