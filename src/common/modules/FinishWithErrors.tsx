import React from "react";
import {
	View,
  ViewStyle,
  Text,
  TextStyle,
  TouchableOpacity,
  Image,
  ImageStyle,
  Platform
} from "react-native";
import { BaseReduxComponent } from "../../core/BaseComponent";
import { connectAdv } from "../../core/store/connectAdv";
import { Dispatch } from "redux";
import { Colors, windowWidth} from "../../core/theme";
import { styleSheetCreate } from "../../common/utils";
import { ImageResources } from "../../common/ImageResources.g";
import { localization as l } from "../../common/localization/localization";
import { Button } from "../../common/components/Button";
import { ButtonNoBg } from "../../common/components/ButtonNoBg";
import { EndAssepHeader } from "../../common/components/Headers";
import {
  NavigationRoute,
  NavigationScreenProp,
} from "react-navigation";

interface IStateProps {

}

interface IDispatchProps {
  navigateToAssumptionInstr (): void;
}

interface IState {

}

interface IProps {
	navigation: NavigationScreenProp<NavigationRoute<any>>;
}
@connectAdv(
  null,
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToAssumptionInstr(): void {
    },
  })
)
export class FinishWithErrors extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  static navigationOptions = EndAssepHeader("", true, false);

  render(): JSX.Element {
    let {seeErrors, testingAgain} = this.props.navigation.state.params;

    return (
      <View  style={styles.container}>
        <Image style={styles.imageFinishErr} source={ImageResources.finishWithErrors} />
        <View style={styles.content}>
          <Text style={styles.title}>Надо подготовиться!</Text>
          <Text style={styles.text}>Чтобы завершить тестирование, нужно ответить на все вопросы без ошибок</Text>
        </View>
        <View>
          <TouchableOpacity onPress={seeErrors}>
            <ButtonNoBg titleButton={l.common.viewErrors} width={windowWidth * .9} />
          </TouchableOpacity>
          <View style={styles.bottomButton}>
            <TouchableOpacity onPress={testingAgain}>
              <Button titleButton={l.common.passAgain} width={windowWidth * .9}/>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = styleSheetCreate({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    justifyContent: "space-between",
		alignItems: "center",
		padding: windowWidth * .06,
	} as ViewStyle,
	content: {
		alignItems: "center",
		paddingHorizontal: windowWidth * .1,
	} as ViewStyle,
	imageFinishErr: {
		width: windowWidth * .57,
		height: windowWidth * .57,
	} as ImageStyle,
	title: {
		fontSize: windowWidth * .063,
		fontWeight: "bold",
		color: Colors.dark5E,
	} as TextStyle,
	text: {
		paddingTop: windowWidth * .047,
		fontSize: windowWidth * .044,
		textAlign: "center",
	} as TextStyle,
	bottomButton: {
		marginTop: windowWidth * .04,
		...Platform.select({
			ios: {
				shadowRadius: 10,
				shadowOpacity: .4,
				shadowOffset: { width: 0, height: 24 },
			},
			android: {
				elevation: 7,
			}
		}),
	} as ViewStyle,
});
