export const authLocalization = {
    "ru": {
        "signInSplash": "Вход в систему",
        "signIn": "Войти",
        "email": "Электронная почта",
        "password": "Пароль",
        "logoutDrawer": "Выйти из системы",
        "logoutTitle": "Выход из системы",
        "logoutConfirmation": "Вы точно хотите выйти?",
        "logout": "Выйти",
        "stay": "Остаться",
        "fillEmailField": "Запоните поле Email",
        "passDoNotMatch": "Пароли не совпадают",
        "fillLoginPassword": "Заполните поля Логин, Пароль",
    }
};