export const emptyLocalization = {
    "ru": {
        "emptyPlannedRuns": "Прямо сейчас у вас нет планируемых рейсов, обратитесь к своему диспетчеру.",
        "emptyCurrentRun": "Прямо сейчас у вас нет текущего рейса, выберите рейс среди списка планируемых рейсов.",
        "aplicationReqest":"Важно! Чтобы мы могли связаться с Вами по телефону, он должен быть указан в профиле. Если у Вас не указан телефон, мы свяжемся по E-mail или в разделе “Связь с куратором”"
    }
};