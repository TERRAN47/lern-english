/* tslint:disable */
import { ImageURISource } from "react-native";

export class ImageResources {
    static readonly image_arow_gray: ImageURISource = require("../../../resources/images/image_arow_gray.png");
    static readonly image_arow_white: ImageURISource = require("../../../resources/images/image_arow_white.png");
    static readonly image_back: ImageURISource = require("../../../resources/images/image_back.png");
    static readonly image_eye: ImageURISource = require("../../../resources/images/image_eye.png");
    static readonly image_eye_non: ImageURISource = require("../../../resources/images/image_eye_non.png");
    static readonly image_menu: ImageURISource = require("../../../resources/images/image_menu.png");
    static readonly image_more: ImageURISource = require("../../../resources/images/image_more.png");
    static readonly image_photo: ImageURISource = require("../../../resources/images/image_photo.png");
    static readonly image_search: ImageURISource = require("../../../resources/images/image_search.png");
    static readonly image_x: ImageURISource = require("../../../resources/images/image_x.png");
    static readonly splash: ImageURISource = require("../../../resources/images/splash.png");
    static readonly logo: ImageURISource = require("../../../resources/images/image_logo.png");
    static readonly backgroundApp: ImageURISource = require("../../../resources/images/image_background_app.png");
    static readonly backgroundHeader: ImageURISource = require("../../../resources/images/header.jpg");

    static readonly home_inactive: ImageURISource = require("../../../resources/images/menu/home_inactive.png");
    static readonly home_active: ImageURISource = require("../../../resources/images/menu/home_active.png");
    static readonly training_inactive: ImageURISource = require("../../../resources/images/menu/training_inactive.png");
    static readonly training_active: ImageURISource = require("../../../resources/images/menu/training_active.png");
    static readonly connect_inactive: ImageURISource = require("../../../resources/images/menu/connect_inactive.png");
    static readonly connect_active: ImageURISource = require("../../../resources/images/menu/connect_active.png");
    static readonly homeWork_inactive: ImageURISource = require("../../../resources/images/menu/homeWork_inactive.png");
    static readonly homeWork_active: ImageURISource = require("../../../resources/images/menu/homeWork_active.png");

    static readonly iconMyTasks: ImageURISource = require("../../../resources/images/image_menu_button_icon_my_tasks.png");
    static readonly iconTraining: ImageURISource = require("../../../resources/images/image_menu_button_icon_training.png");
    static readonly iconStatistics: ImageURISource = require("../../../resources/images/image_menu_button_icon_my_statistics.png");
    static readonly iconCalendar: ImageURISource = require("../../../resources/images/image_menu_button_icon_calendar.png");
    static readonly iconBalance: ImageURISource = require("../../../resources/images/image_menu_button_icon_balance.png");
    static readonly iconOnlineLesson: ImageURISource = require("../../../resources/images/image_menu_button_icon_online_lesson.png");
    static readonly iconPhonetics: ImageURISource = require("../../../resources/images/image_menu_button_icon_phonetics.png");
    static readonly iconListening: ImageURISource = require("../../../resources/images/image_menu_button_icon_listening.png");
    static readonly iconLexis: ImageURISource = require("../../../resources/images/image_menu_button_icon_lexis.png");
    static readonly iconGrammar: ImageURISource = require("../../../resources/images/image_menu_button_icon_grammar.png");
    static readonly iconReading: ImageURISource = require("../../../resources/images/image_menu_button_icon_reading.png");
    static readonly iconWriting: ImageURISource = require("../../../resources/images/image_menu_button_icon_writing.png");

    static readonly iconHomeworkDone: ImageURISource = require("../../../resources/images/image_homework_icon_done.png");
    static readonly iconHomeworkTime: ImageURISource = require("../../../resources/images/image_hamework_icon_time.png");
    static readonly iconHomeworkAttention: ImageURISource = require("../../../resources/images/image_homework_icon_attention.png");

    static readonly iconBook: ImageURISource = require("../../../resources/images/bookIcon.png");
    static readonly iconPlus: ImageURISource = require("../../../resources/images/plus.png");

    static readonly dayIcon: ImageURISource = require("../../../resources/images/day.png");
    static readonly morningIcon: ImageURISource = require("../../../resources/images/morning.png");
    static readonly eveningIcon: ImageURISource = require("../../../resources/images/evening.png");
    static readonly nightIcon: ImageURISource = require("../../../resources/images/night.png")

    static readonly ruFlagIcon: ImageURISource = require("../../../resources/images/ruFlag.png");
    static readonly enFlagIcon: ImageURISource = require("../../../resources/images/enFlag.png")

    static readonly volumeIcon: ImageURISource = require("../../../resources/images/volumeIcon.png")
    static readonly microIcon: ImageURISource = require("../../../resources/images/microIcon.png")

    static readonly dropDownTriangle: ImageURISource = require("../../../resources/images/image_dropdown_triangle.png")
    static readonly bigStar: ImageURISource = require("../../../resources/images/image_big_star.png")
    static readonly medalTop: ImageURISource = require("../../../resources/images/image_medal_top.png")
    static readonly medalTapeFront: ImageURISource = require("../../../resources/images/image_medal_tape_front.png")
    static readonly medalTapeBack: ImageURISource = require("../../../resources/images/image_medal_tape_back.png")
    static readonly littleStar: ImageURISource = require("../../../resources/images/image_little_star.png")
    static readonly finishWithErrors: ImageURISource = require("../../../resources/images/image_finish_with_errors.png")
    static readonly assumpCoplit: ImageURISource = require("../../../resources/images/assumpCoplit.png")
    static readonly medal: ImageURISource = require("../../../resources/images/image_medal.png")
    static readonly iconMicrophone: ImageURISource = require("../../../resources/images/image_icon_microphone.png")
    static readonly finishCup: ImageURISource = require("../../../resources/images/image_testing_success_finish.png")
    static readonly essaySent: ImageURISource = require("../../../resources/images/image_send_essay.png")
    static readonly book: ImageURISource = require("../../../resources/images/image_book.png")
    static readonly starOn: ImageURISource = require("../../../resources/images/image_star_on.png")
    static readonly starOff: ImageURISource = require("../../../resources/images/image_star_off.png")
    static readonly rework: ImageURISource = require("../../../resources/images/image_rework.png")
    static readonly slowTransparent: ImageURISource = require("../../../resources/images/image_slow_tr.png")
    static readonly slowBlue: ImageURISource = require("../../../resources/images/image_slow_bl.png")
    static readonly soundTransparent: ImageURISource = require("../../../resources/images/image_sound_tr.png")
    static readonly soundBlue: ImageURISource = require("../../../resources/images/image_sound_bl.png")
    static readonly delete: ImageURISource = require("../../../resources/images/image_delete.png")
    static readonly edit: ImageURISource = require("../../../resources/images/image_edit.png")
    static readonly checkboxOn: ImageURISource = require("../../../resources/images/image_checkbox_on.png")
    static readonly checkboxOff: ImageURISource = require("../../../resources/images/image_checkbox_off.png")
    static readonly user1: ImageURISource = require("../../../resources/images/image_user1.png")
    static readonly userBg: ImageURISource = require("../../../resources/images/image_user_bg.png")
    static readonly sendMessage: ImageURISource = require("../../../resources/images/image_send_message.png")
}
