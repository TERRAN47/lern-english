import React, { PureComponent } from "react"
import {
  Text,
  TextStyle,
  ViewStyle,
  View,
  TouchableOpacity
} from 'react-native'
import { Colors, windowWidth } from '../../core/theme'
import { styleSheetCreate } from '../utils'
//import { ShowStatusTraining } from './ShowStatusTraining'
//import { localization as l } from '../localization/localization'
import { DropDown } from './DropDown'

interface IProps {
  changeLevel(level:string, levelTitle:string): void
  level:string
}

interface ILevels {
  title: string
  isActive: boolean
  onPress(): void
  level: string
}

interface IState {
  statusSelect:boolean
  levels:ILevels[]
  levelButton: string
}

export class LevelSelect extends PureComponent<IProps, IState> {
  constructor(props: any) {
    super(props);
    this.state = {  
      statusSelect:false,
      levelButton: '',
      levels: [
        { 
          title: 'Уровень А0',
          isActive: true,
          level:'7',
          onPress:()=>this.selectLevel('7', 'А0'),
        }, 
        { 
          title: 'Уровень А1',
          isActive: true,
          level:'8',
          onPress:()=> this.selectLevel('8', 'А1'),
        },     
        { 
          title: 'Уровень А2',
          isActive: true,
          level:'9',
          onPress: () => this.selectLevel('9', 'А2'),
        },     
        { 
          title: 'Уровень B1',
          isActive: true,
          level:'10',
          onPress: () => this.selectLevel('10', 'B1'),
        },     
        { 
          title: 'Уровень B2',
          isActive: true,
          level:'11',
          onPress: () => this.selectLevel('11', 'B2'),
        },     
        { 
          title: 'Уровень C1',
          isActive: true,
          level:'12',
          onPress: () => this.selectLevel('12', 'C1'),
        },
        { 
          title: 'Уровень C2',
          isActive: true,
          level:'13',
          onPress: () => this.selectLevel('13', 'C2'),
        },
      ]  
    }
  }

  // level: {
  //   '7': 'А0',
  //   '8': 'А1',
  //   '9': 'А2',
  //   '10': 'B1',
  //   '11': 'B2',
  //   '12': 'C1',
  //   '13': 'C2',
  // }


  componentDidMount(){
    let levels = this.state.levels.map((elem:any)=>{
      if(+(elem.level) <= +(this.props.level)){
        return(elem)
      }else{
        elem.isActive = false
        return(elem)
      }
    })
    // let level = this.props.level
    // let levelFirst = this.level[level]
    // this.setState({levelButton: levelFirst})
    this.setState({levels})
  }
  selectLevel = (level:string, levelTitle:string):void=>{
    this.props.changeLevel(level, levelTitle)
    this.statusSelect(true)
    this.setState({levelButton: levelTitle})
  }

  statusSelect(status:boolean){
    this.setState({statusSelect:status})
  }

  render(): JSX.Element {
    return (
      <DropDown itemSelect={this.statusSelect.bind(this)} statusSelect={this.state.statusSelect} levelButton={this.state.levelButton}>
        {
          this.state.levels.map((item) => {
              return(
                <View style={styles.levelsContainer} key={item.title} >
                  {
                    item.isActive
                    ? <TouchableOpacity onPress={item.onPress}>
                        <Text style={styles.activeTitle}>{item.title}</Text>
                      </TouchableOpacity>
                    : <TouchableOpacity >
                        <Text style={styles.notActiveTitile}>{item.title}</Text>  
                      </TouchableOpacity>                      
                  }
                </View>
              )
            }
          )
        }
      </DropDown>
    )
  }
}

const styles = styleSheetCreate({
  levelsContainer: {
    padding: windowWidth * .05,
    justifyContent: 'space-around',
  } as ViewStyle,
  activeTitle: {
    color: Colors.dark5E,
    fontSize: windowWidth * .039,
  } as TextStyle,
  notActiveTitile: {
    color: Colors.greyCE,
    fontSize: windowWidth * .039,
  } as TextStyle,
})