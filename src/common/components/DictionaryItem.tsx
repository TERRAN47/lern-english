import React, { PureComponent } from "react"
import {
  Text,
  TextStyle,
	View,
  ViewStyle,
  Image,
  ImageStyle,
  TouchableOpacity,
} from "react-native"
import { Colors, windowWidth } from '../../core/theme'
//import { localization as l } from '../../common/localization/localization'
import { styleSheetCreate } from '../utils'
import { ImageResources } from '../ImageResources.g'

interface IProps {
  en: string
  ru: string
  onPress?(): void
}

interface IState {

}

export class DictionaryItem extends PureComponent<IProps, IState> {
  
  state = {
    pressStatus: false,
  }
//TODO: сделать нажатие кнопки
  render(): JSX.Element {

    const { en, ru, onPress } = this.props
    
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={onPress}>
          <View style={styles.words}>
            <Text style={styles.enText}>{en}</Text>
            <Text style={styles.ruText}>{ru}</Text>
          </View>
        </TouchableOpacity>
        <View style={styles.buttons}>
          <TouchableOpacity>
            <Image 
              style={styles.icon}
              source={ImageResources.slowTransparent}
            />
          </TouchableOpacity>
          <TouchableOpacity>
            <Image 
              style={styles.icon}
              source={ImageResources.soundTransparent}
            />
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = styleSheetCreate({
  container: {
    width: windowWidth,
    padding: windowWidth * .055,
    flexDirection: 'row',
    justifyContent: 'space-between',
  } as ViewStyle,
  words: {
    justifyContent: 'space-between',
  } as ViewStyle,
  enText: {
    fontSize: windowWidth * .039,
    color: Colors.dark5E,
    paddingBottom: windowWidth * .02,
  } as TextStyle,
  ruText: {
    fontSize: windowWidth * .039,
    color: Colors.darkGrey9F,
  } as TextStyle,
  buttons: {
    flexDirection: 'row',
  } as ViewStyle,
  icon: {
    width: windowWidth * .15,
    height: windowWidth * .15,
    marginLeft: windowWidth * .03,
  } as ImageStyle,
}) 