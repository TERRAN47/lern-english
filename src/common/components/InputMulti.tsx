import React, { PureComponent } from "react"
import {
  TextInput,
  ViewStyle,
  View
} from "react-native"
import { Colors, windowWidth, windowHeight } from '../../core/theme'
import { styleSheetCreate } from '../utils'

interface IProps {
  placeholder: string
  isError?: boolean,
  changeText:(text:string) => void
}

interface IState {
  isFocused: boolean
}

export class InputMulti extends PureComponent<IProps, IState> {

  state = {
    isFocused: false,
  }

  handleOnFocus = (): void => {
    this.setState({ isFocused: true })
  }
  handleOnBlur = (): void => {
    this.setState({ isFocused: false })
  }

  onChangeText = (func:(texts:string) => void, texts:string): void => {
    func(texts)
  }

  render(): JSX.Element {
    const { placeholder, isError, changeText } = this.props

    const inputUnderLine =
        isError
        ? styles.inputUnderlineRed 
        : this.state.isFocused  
          ? styles.inputUnderlineBlue
          : styles.inputUnderlineGrey
          
    return (
      <View style={styles.container}>
        <TextInput
          multiline
          style={styles.input}
          onFocus={this.handleOnFocus}
          onBlur={this.handleOnBlur}
          placeholder={placeholder}
          placeholderTextColor={Colors.greyCE}
          underlineColorAndroid={Colors.transparent}
          onChangeText={(texts:string)=>this.onChangeText(changeText, texts)}
        />
        <View style={inputUnderLine} />
      </View>
    )
  }
}

const styles = styleSheetCreate({
  container: {
    maxHeight: windowHeight * .6,
    justifyContent: 'space-between',
  } as ViewStyle,
  input: {
    color: Colors.dark5E,
    fontSize: windowWidth * .046,
    paddingTop: windowWidth * .09,
    paddingLeft: - (windowWidth * .11),
  } as ViewStyle,
  inputUnderlineGrey: {
    backgroundColor: Colors.greyDB,
    height: windowWidth * .002,
  } as ViewStyle,
  inputUnderlineBlue: {
    backgroundColor: Colors.blueEC,
    height: windowWidth * .006,
  } as ViewStyle,
  inputUnderlineRed: {
    backgroundColor: Colors.red61,
    height: windowWidth * .002,
  } as ViewStyle,
})