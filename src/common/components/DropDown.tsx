import React, { Component } from 'react'
import {
  Text,
  TextStyle,
  View,
  ViewStyle,
  Image,
  ImageStyle,
  TouchableOpacity,
  Platform,
  Animated,
} from 'react-native'
import { 
  Colors,
  windowWidth,
  windowHeight, 
} from '../../core/theme'
import { styleSheetCreate } from '../utils'
import { ImageResources } from '../../common/ImageResources.g'
import { localization as l } from '../../common/localization/localization'

interface IState {
  isOpened: boolean
  animation: any
}

interface IProps {
  statusSelect:boolean
  itemSelect(status:boolean):void
  levelButton: string
}

export class DropDown extends Component<IProps, IState> {

  state = {
    isOpened: false,
    animation: new Animated.Value(0),
  }

  shouldComponentUpdate(nextProps:any, nextState:any){
    if(this.props.statusSelect !== nextProps.statusSelect){
      this.props.itemSelect(false)
      this.closeDropDown()
    }
    return true
  }

  toggleDropDown = (): void => {
    if (this.state.isOpened || this.props.statusSelect) {
      this.closeDropDown()
    } else {
      this.openDropDown()
    }
  }

  openDropDown = (): void => {
    Animated.timing(this.state.animation, {
      toValue: 1,
      duration: 200,
    }).start(() => this.setState({
      isOpened: true
    }))
  }

  closeDropDown = (): void => {
    this.setState({ isOpened: false }, () => {
      Animated.timing(this.state.animation, {
        toValue: 0,
        duration: 200,
      }).start()
    })
  }

  render(): JSX.Element {    

    console.log('levelDROPDOWN>>>>>' , this.props.levelButton)

    const showListStyle = {
      height: this.state.animation.interpolate({
        inputRange: [0, 1],
        outputRange: [0, windowWidth * 1.1],
      })
    }

    return (
      //TODO: развернуть треугольник при анимации
      <>
        <TouchableOpacity onPress={this.toggleDropDown}>
          <View style={styles.levelDropDown} >
            <Text style={styles.levelTitle}>
              {l.common.level} {this.props.levelButton}
            </Text>
            <Image 
              style={styles.dropDownTriangle}
              source={ImageResources.dropDownTriangle}
            />              
          </View>
        </TouchableOpacity> 
        {
          //TODO: перелать стили на флаттен
          <Animated.View style={[styles.levelsList, showListStyle]}>
            {this.state.isOpened ? this.props.children : null}
          </Animated.View>
        }
      </>
    )
  }

}

const styles = styleSheetCreate({
  levelDropDown: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.white,
    borderRadius: windowWidth * .03,
    padding: windowWidth * .04,
    margin: windowHeight * .02,
    marginBottom: windowWidth * .06,
    borderWidth: windowWidth * .003,
    borderColor: Colors.greyDB,
    borderStyle: 'solid',
    zIndex: 400,
  } as ViewStyle,
  levelTitle: {
    color: Colors.dark5E,
    fontSize: windowWidth * .039,
    fontWeight: '400',
  } as TextStyle,
  levelsList: {
    position: 'absolute',
    zIndex: 20,
    backgroundColor: Colors.white,
    marginTop: windowWidth * .21,
    marginHorizontal: windowWidth * .04,
    width: windowWidth * .92,
    ...Platform.select({
      ios: {
        shadowRadius: 15,
        shadowOpacity: 0.3,
        shadowOffset: { width: 1, height: 4 },
      },
      android: {
        elevation: 10,
    }}),
  },
  dropDownTriangle: {
    position: 'absolute',
    width: windowWidth * .02,
    height: windowWidth * .015,
    right: windowWidth * .06,
  } as ImageStyle,
})