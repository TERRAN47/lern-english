import React, { PureComponent } from "react"
import {
  Text,
  TextStyle,
	View,
  ViewStyle,
  Image,
  ImageStyle,
	Platform,
  TouchableOpacity,
} from "react-native"
import { Colors, windowWidth } from '../../core/theme'
import { styleSheetCreate } from '../utils'
import { ImageResources } from '../../common/ImageResources.g'


interface IProps {
  onPress(data: any): void
  item: any
}

interface IState {

}

export class ReadingItem extends PureComponent<IProps, IState> {

  render(): JSX.Element {

    const { item, onPress } = this.props
    //console.log('status', item)
    
    return (
      <TouchableOpacity onPress={onPress}>
        <View>
          <View style={styles.container}>
            { item.state === 1
              ? <View style={styles.statusLine} />
              : null              
            }
            <Image
              source={ImageResources.book}
              style={styles.img} />
            <Text style={styles.title}>{item.theme}</Text>
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}

const styles = styleSheetCreate({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    width: windowWidth * .92,
    minHeight: windowWidth * .245,
    paddingHorizontal: windowWidth * .05,
    backgroundColor: Colors.white,
    margin: windowWidth * .02,
    borderRadius: windowWidth * .02,
    overflow: 'hidden',
    ...Platform.select({
      ios: {
          shadowRadius: 2,
          shadowOpacity: 0.05,
          shadowOffset: {width: 0, height: 3},
      },
      android: {
        elevation: 8
    }}),
  } as ViewStyle,
  title: {
    fontSize: windowWidth * .039,
    color: Colors.black59,
    paddingLeft: windowWidth * .06,
    width: windowWidth * .6,
  } as TextStyle,
  img: {
    width: windowWidth * .136,
    height: windowWidth * .145,
  } as ImageStyle,
  statusLine: {
    position: 'absolute',
    left: 0,
    width: windowWidth * .027,
    height: '100%',
    borderStyle: 'solid',
    backgroundColor: Colors.greenish,
  } as ViewStyle,
}) 