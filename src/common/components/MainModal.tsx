import React, { PureComponent } from "react";
import {
	Modal,
	Platform,
	Text,
	TextStyle,
	TouchableOpacity,
	TouchableWithoutFeedback,
	View,
	ViewStyle
} from "react-native";
import { Colors, Fonts, windowWidth } from "../../core/theme";
import { styleSheetCreate } from "../utils";

interface IProps {
	title: string;
	body: {} | string | null;
	isVisible: boolean;
	closeModal: () => void;
}

export class MainModal extends PureComponent<IProps> {
	render(): JSX.Element {
		const { title, body, closeModal, isVisible, children } = this.props;

		return (
			<Modal transparent={true} animationType={"fade"} visible={isVisible} onRequestClose={closeModal}>
				<TouchableWithoutFeedback onPress={closeModal}>
					<View style={styles.modalBackground}>
						<TouchableOpacity activeOpacity={1} style={styles.contentContainer}>
							<Text style={styles.title}>{title}</Text>
							<Text style={styles.body}>{body}</Text>
							<View style={styles.buttonsContainer}>
								{children}
							</View>
						</TouchableOpacity>
					</View>
				</TouchableWithoutFeedback>
			</Modal>
		)
	}
}

const styles = styleSheetCreate({
	modalBackground: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: Colors.transparentBlack,
	} as ViewStyle,
	contentContainer: {
		paddingTop: windowWidth * .057,
		paddingLeft: windowWidth * .068,
		paddingRight: windowWidth * .069,
		paddingBottom: windowWidth * .0525,
		minHeight: windowWidth * .438,
		width: windowWidth * .91,
		alignItems: 'flex-start',
		justifyContent: 'flex-start',
		borderRadius: windowWidth * .022,
		backgroundColor: Colors.white,
		...Platform.select({
				ios: {
						shadowRadius: 10,
						shadowOpacity: .4,
						shadowOffset: { width: 0, height: 24 },
				},
				android: {
						elevation: 3,
				}
		}),
	} as ViewStyle,
	buttonsContainer: {
		flex: 1,
		flexDirection: 'row',
		alignSelf: 'flex-end',
		alignItems: 'flex-end',
		justifyContent: 'flex-end',
	} as ViewStyle,
	title: {
		fontSize: windowWidth * .052,
		fontFamily: Fonts.medium,
		color: Colors.dark5E,
	} as TextStyle,
	body: {
		fontSize: windowWidth * .0415,
		paddingTop: windowWidth * .034,
		fontFamily: Fonts.medium,
		color: Colors.darkGrey9F,
	} as TextStyle,
});