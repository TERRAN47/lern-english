import React from "react";
import {BaseReduxComponent} from "../../core/BaseComponent";
import {LoadingView} from "./LoadingView";

interface IStateProps {
    isLoading: boolean;
}

export class RootLoadingView extends BaseReduxComponent<IStateProps, IEmpty> {
    render(): JSX.Element {
        return <LoadingView isLoading={this.stateProps.isLoading} transparent={false}/>;
    }
}