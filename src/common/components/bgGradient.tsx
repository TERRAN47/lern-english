import React, { PureComponent } from 'react'
import LinearGradient from 'react-native-linear-gradient'

interface IProps {

}

interface IState {
	
}

export class BgGradient extends PureComponent<IProps, IState> {

  render(): JSX.Element {
		return (
      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 0 }}
        colors={['#4E71EC', '#4EAAEC']}
        {...this.props}
      />
    )
  }
}