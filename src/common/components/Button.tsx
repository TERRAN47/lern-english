import React, { PureComponent } from "react"
import {
  Text,
  TextStyle,
  ViewStyle
} from "react-native"
import { Colors, windowWidth } from '../../core/theme'
import { styleSheetCreate } from '../utils'
import LinearGradient from 'react-native-linear-gradient'

interface IProps {
  titleButton: string
  width?: number,
  onIconPress?: () => void
  isError?: boolean
}

interface IState {

}

export class Button extends PureComponent<IProps, IState> {

  render(): JSX.Element {

    const { titleButton, width = windowWidth * .8 } = this.props
//TODO: переделать стиль на флаттен
    return (
      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 0 }}
        colors={['#4E71EC', '#4EAAEC']}
        style={[styles.linearGradient, {width}]}
      >
        <Text style={styles.buttonText}>
          {titleButton}
        </Text>
      </LinearGradient>
    )
  }
}

const styles = styleSheetCreate({
  buttonText: {
    color: Colors.white,
    fontSize: windowWidth * .046,
  } as TextStyle,
  linearGradient: {
    height: windowWidth * .147,
    borderRadius: windowWidth * .025,
    alignItems: 'center',
    justifyContent: 'center',
  } as ViewStyle,
})