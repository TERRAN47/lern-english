import React, { PureComponent } from "react"
import {
  Text,
  TextStyle,
	View,
  ViewStyle,
  Image,
  ImageStyle,
} from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { Colors, windowWidth } from '../../core/theme'
//import { localization as l } from '../../common/localization/localization'
import { styleSheetCreate } from '../utils'
import { ImageResources } from '../ImageResources.g'

interface IProps {
  foto?: ImageResources
  text: string
  name: string
  messageIn?: boolean
}

interface IState {

}

export class MessageItem extends PureComponent<IProps, IState> {
  
  state = {
    pressStatus: false,
  }
  
  render(): JSX.Element {

    const { text, name, messageIn, foto } = this.props
    
    return (
      <View>
        {
          messageIn
            ? <View style={styles.container}>
                <View style={styles.user}>
                  <View style={styles.fotoContainer}>
                    <LinearGradient
                      start={{ x: 0, y: 0 }}
                      end={{ x: 1, y: 0 }}
                      colors={['#4E71EC', '#4EAAEC']}
                      style={styles.fotoBg}
                    >
                      { foto
                        ? <Image 
                            style={styles.foto}
                            source={foto}
                          />
                        : <Text style={styles.letter}>{name.charAt(0)}</Text>
                      }
                    </LinearGradient>
                  </View>
                  <View style={styles.textContainerIn}>
                    <Text style={styles.textIn}>{text}</Text>
                  </View>
                </View>
              </View>
            : <View style={styles.container}>
                <View style={styles.user}>
                  <View style={styles.textContainerOut}>
                    <Text style={styles.textOut}>{text}</Text>
                  </View>
                </View>
              </View>
        }
      </View>
    )
  }
}

const styles = styleSheetCreate({
  container: {
    flexDirection: 'row',
    width: windowWidth * .9,
    justifyContent: 'space-between',
    alignItems: 'center',
    margin: windowWidth * .02,
    paddingTop: windowWidth * .03,    
  } as ViewStyle,
  user: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between'
  } as ViewStyle,
  textIn: {
    fontSize: windowWidth * .039,
    lineHeight: windowWidth * .075, 
    color: Colors.dark5E,
  } as TextStyle,
  textOut: {
    fontSize: windowWidth * .039,
    lineHeight: windowWidth * .075, 
    color: Colors.white,
  } as TextStyle,
  fotoContainer: {
    position: 'relative',
    paddingRight: windowWidth * .04,
  } as ViewStyle,
  fotoBg: {
    width: windowWidth * .11,
    height: windowWidth * .11,
    borderRadius: windowWidth * .7,
    justifyContent: 'center',
    alignItems: 'center',
  } as ImageStyle,
  foto: {
    width: windowWidth * .11,
    height: windowWidth * .11,
    borderRadius: windowWidth * .7,
  } as ImageStyle,
  letter: {
    fontSize: windowWidth * .06,
    color: Colors.white,
    fontWeight: '500',
  } as TextStyle,
  textContainerIn: {
    width: windowWidth * .74,
    backgroundColor: Colors.white,
    borderTopEndRadius: windowWidth * .03,
    borderTopStartRadius: windowWidth * .03,
    borderBottomEndRadius: windowWidth * .03,
    paddingHorizontal: windowWidth * .05,
    paddingVertical: windowWidth * .03,
  } as ViewStyle,
  textContainerOut: {
    width: windowWidth * .9,
    backgroundColor: Colors.blueEC, 
    borderTopEndRadius: windowWidth * .03,
    borderTopStartRadius: windowWidth * .03,
    borderBottomStartRadius: windowWidth * .03,
    paddingHorizontal: windowWidth * .05,
    paddingVertical: windowWidth * .03,
  } as ViewStyle,
})
