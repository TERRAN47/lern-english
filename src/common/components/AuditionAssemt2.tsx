import React, { PureComponent } from 'react'
import {
  View, 
  ViewStyle,
  Platform,
  TouchableOpacity,
  TextStyle,
  Text,
} from 'react-native'
import { styleSheetCreate } from '../utils'
import { Colors, windowWidth } from '../../core/theme'
import { Answer } from './Answer'

interface IProps {
  items: [] 
  item: any
  id: number
  finishSreen(): any
  testFinesh: boolean
  nextSlide(): any
}

interface IState {
  targetAnswer: number
}

export class AuditionAssemt2 extends PureComponent<IProps, IState> {

  constructor(props: any) {
    super(props);
      this.state = {
        targetAnswer:-1,
    }
  }

  selectAnswer(index:number){
    let {item, items, id, finishSreen, nextSlide} = this.props

    let resultAnswer = (index == item[4])
   
    item.push(resultAnswer)

    this.setState({targetAnswer:index}, ()=> setTimeout(nextSlide, 300))
    if(id >= items.length){
      finishSreen()
    }  
  }

  render(): JSX.Element {
    let {targetAnswer} = this.state
    let {items, item,  testFinesh, id} = this.props
    /* answerState - isSelected, isNotSelected, CorrectNotSelected, isError, isCorrect */

		return (
      <View style={styles.slideBlock}>
          <View style={styles.content}>
            <Text style={styles.question}>{`${item[0]}`}</Text>
            <View style={styles.center}>
              {
                item.map((elem:string, index:number)=>{
                  if(testFinesh){
                    if(3 >= index && index > 0){
                      return(
                        <View key={index}>
                          <Answer 
                            title={elem} 
                            answerState={
                              targetAnswer === index && targetAnswer == item[4] ? 
                                'isCorrect' 
                              : 
                                index == item[4] ?
                                  'isCorrect'
                                :
                                  targetAnswer === index ?
                                    'isError'
                                  :
                                    'CorrectNotSelected'
                            } 
                          />
                        </View>
                      )
                    }else{
                      return null
                    }
                  }else{
                    if(3 >= index && index > 0){
                      return(
                        <TouchableOpacity onPress={this.selectAnswer.bind(this, index)} key={index}>
                          <Answer 
                            title={elem} 
                            answerState={targetAnswer === index ? 'isSelected' : 'isNotSelected'}
                          />
                        </TouchableOpacity>
                      )
                    }else{
                      return null
                    }
                  }
                })
              }
            </View>
          </View>
          <View style={styles.footer}>

            <View style={styles.countBlock}>
              <Text style={styles.countPageText}>{id}/{items.length}</Text>
            </View>

          </View>
      </View>
    )
  }
}
const styles = styleSheetCreate({ 
  bgStyle: {
    flexGrow: 1,
    paddingBottom: windowWidth * .04,
    justifyContent: 'space-around',
  } as ViewStyle,
  center:{
    alignItems:'center'
  } as ViewStyle,
	description: {
    paddingBottom: windowWidth * .1,
    paddingHorizontal: windowWidth * .05,
    textAlign: 'center',
		fontSize: windowWidth * .045,
    color: Colors.darkGrey9F,
	} as TextStyle,
  question:{
    marginBottom:80,
    textAlign:'center',
    fontSize:16
  } as TextStyle,
  footer:{
    marginTop: windowWidth * .07,
  } as ViewStyle,
  controls: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  } as ViewStyle,
  listenButton: {
    paddingTop: windowWidth * .07,
  } as ViewStyle,
  countBlock: {
    backgroundColor: Colors.blueEC,
    width: windowWidth * .8,
    flex:1,
    padding: windowWidth * .03,
  } as ViewStyle,
  button: {
    padding: windowWidth * .02,
  } as ViewStyle,
  activeButtonText: {
    fontSize: 20,
    color: "#B81F00"
  } as TextStyle,
  content:{
    paddingVertical: windowWidth * .14,
    width:  windowWidth * .9,
    backgroundColor: Colors.white,
    alignItems: 'center',
    justifyContent: 'space-between',
    borderRadius: 15,
    ...Platform.select({
      ios: {
        shadowRadius: 8,
        shadowOpacity: 0.2,
        shadowOffset: {width: 0, height: 4},
      },
      android: {
        elevation: 8
    }}),
  } as ViewStyle,

  slideBlock:{
    alignItems:'center', 
    paddingTop: windowWidth * .04,
  } as ViewStyle,
  contentTitle:{
    color: Colors.black60,
    fontSize: 28,
    padding: windowWidth * .04,
  } as TextStyle,
  countPageText:{
    fontSize: 20,
    color: Colors.white,
    textAlign: 'center'
  } as TextStyle,
})