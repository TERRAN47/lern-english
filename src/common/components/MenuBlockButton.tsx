import React, { PureComponent } from "react"
import {
  Platform,
  Text,
  TextStyle,
  ViewStyle,
  Image,
  ImageStyle,
  TouchableOpacity,
  ImageURISource,
} from "react-native"
import { Colors, windowWidth } from '../../core/theme'
import { styleSheetCreate } from '../utils'

interface IProps {
  buttonText: string
  buttonImage: ImageURISource
  onPress?(): void
}

interface IState {

}


export class MenuBlockButton extends PureComponent<IProps, IState> {

  render(): JSX.Element {

    const { buttonText, onPress, buttonImage } = this.props

    return (      
      <TouchableOpacity onPress={onPress} style={styles.menuButtonContainer}>
        <Image
          style={styles.menuButtonIcon}
          source={buttonImage}
        />
        <Text style={styles.menuButtonText}>
          {buttonText}
        </Text>
      </TouchableOpacity>      
    )
  }
}

const styles = styleSheetCreate({
  menuButtonContainer: {
    width: windowWidth * .43,
    height: windowWidth * .39,
    borderRadius: windowWidth * .018,
    marginHorizontal: windowWidth * .02,
    marginVertical: windowWidth * .017,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.white,
    ...Platform.select({
      ios: {
          shadowRadius: 3,
          shadowOpacity: .1,
          shadowOffset: { width: 0, height: 3 },
      },
      android: {
          elevation: 4,
      }
  }),
  } as ViewStyle,
  menuButtonIcon: {
    width: windowWidth * 0.191,
    height: windowWidth * 0.191,
  } as ImageStyle,
  menuButtonText: {
    fontSize: windowWidth * .043,
    paddingTop: windowWidth * 0.03,
  } as TextStyle,
})
