import React, { PureComponent } from 'react'
import {
  View, 
  ViewStyle,
  Platform,
  TouchableOpacity,
  TextStyle,
  Text,
} from 'react-native'
import { styleSheetCreate } from '../utils'
import { Colors, windowWidth } from '../../core/theme'
import { ButtonNoBg } from './ButtonNoBg'
import { localization as l } from '../../common/localization/localization'
import { Button } from './/Button'
//import { ImageResources } from "../../common/ImageResources.g"

interface IProps {
  items:[] 
  item:any
  index:number
  recordStatus(status:boolean):any
  recording:boolean
  record(index:number):any
  play(index:number):any
  listen(word:string, index:number):any
  finishSreen():any
  //finishImage: ImageResources
  stop(index:number):any
}

interface IState {

}

export class Assemtion1 extends PureComponent<IProps, IState> {
  
  renderButton(title:string, onPress:()=> void) {
    return (
      <TouchableOpacity style={styles.button} onPress={onPress}>
        <Button titleButton={title} />
      </TouchableOpacity>
    )
  }

  renderButtonNoBg(title:string, onPress:()=> void) {
    return (
      <TouchableOpacity style={styles.button} onPress={onPress}>
        <ButtonNoBg titleButton={title} />
      </TouchableOpacity>
    );
  }

  render(): JSX.Element {
    let {items, item, recording, record, play, stop, listen, finishSreen, index} = this.props
    let id = index+1

		return (
      <View style={styles.slideBlock}>
        <View style={styles.bgStyle}>
          <View style={styles.content}>
              
            <Text style={styles.contentTitle}>{item[0]}</Text>

            <View style={styles.controls}>
              {this.renderButton(l.training.listen, ()=>listen(item[0], index))}
              {
                recording ? 
                  this.renderButtonNoBg(l.training.stop, ()=>stop(index) ) 
                : 
                  this.renderButtonNoBg(l.training.record, ()=>record(index))
              }
              {this.renderButtonNoBg(l.training.play, ()=>play(index))}
            </View>
          </View>
          {
            id >= items.length ?
              <View style={styles.compitButtonBlock}>
                <TouchableOpacity style={styles.button} onPress={finishSreen}>
                  <Button titleButton={l.training.completeExercise} />
                </TouchableOpacity>
              </View>
            :
              <Text style={styles.countPageText}>{id}/{items.length}</Text>
          }
        </View>
      </View>
    )
  }
}
const styles = styleSheetCreate({ 
  bgStyle: {
    flexGrow: 1,
    paddingBottom: windowWidth * .04,
    justifyContent: 'space-around',
  } as ViewStyle, 
  slideBlock:{
    alignItems:'center', 
    justifyContent: 'space-around',
  } as ViewStyle,
  content:{
    margin: windowWidth * .03,
    width: windowWidth * .9,
    backgroundColor: Colors.white,
    paddingVertical: windowWidth * .05,
    alignItems: 'center',
    justifyContent: 'space-between',
    borderRadius: windowWidth * .03,
    ...Platform.select({
      ios: {
          shadowRadius: 6,
          shadowOpacity: 0.1,
          shadowOffset: {width: 0, height: 3},
      },
      android: {
        elevation: 8
    }}),
  } as ViewStyle,
  controls: {
    alignItems: 'center',
  } as ViewStyle,
  button: {
    padding: windowWidth * .02
  } as ViewStyle,
  activeButtonText: {
    fontSize: windowWidth * .039,
    color: "#B81F00"
  } as TextStyle,
  compitButtonBlock:{
    marginTop: 20,
    alignItems: 'center'
  }  as ViewStyle,
  contentTitle:{
    color: Colors.dark5E,
    fontWeight: '500',
    fontSize: windowWidth * .063,
    paddingVertical: windowWidth * .15,
  } as TextStyle,
  countPageText:{
    fontSize:  windowWidth * .039,
    color: Colors.blueEC,
    marginTop: windowWidth * .04,
    textAlign: 'center'
  } as TextStyle,
})
