import {NavigationStackScreenOptions} from "react-navigation";
import {CommonHeaderStyles} from "../../core/theme/commonStyles";
import React from "react";
import {HeaderButton} from "../../navigation/components/HeaderButton";
import {ImageResources} from "../ImageResources.g";
import {NavigationActions} from "../../navigation/navigation";
import {
  View,
  ViewStyle,
  Image,
  ImageStyle,
  //TouchableOpacity,
} from "react-native";
import { styleSheetCreate } from '../../common/utils'
import { Colors } from '../../core/theme'
import Feather from 'react-native-vector-icons/Feather'

export function NoHeader(): NavigationStackScreenOptions | null {
  return ({
    header: <React.Fragment/>
  });
}

export function PlainHeader(title: string, showLeftButton?: boolean, showDrawerIcon?: boolean, plus:boolean = true, background:boolean = true):
NavigationStackScreenOptions {

  return ({
    headerTitle: title,
    headerTitleStyle: CommonHeaderStyles.headerTitleStyle as any,
    headerLeft: showLeftButton ? (
      <HeaderButton
        action={showDrawerIcon ? NavigationActions.toggleDrawer : NavigationActions.navigateGoBack}
      >
        {showDrawerIcon ? <Feather name="menu" size={30} color={Colors.white} /> : <Feather name="arrow-left" size={30} color={Colors.white} />}
      </HeaderButton>
    ) : undefined,
    headerRight: plus ?
      <View style={styles.IconsBlock}>
        <HeaderButton
            action={NavigationActions.navigateToDictionaryAddWord}
          >
            <Feather name="plus" size={35} color={Colors.white} />
          </HeaderButton>
      </View>
    : undefined ,
    headerBackground: background ? <Image style={styles.headerBackground} source={ImageResources.backgroundHeader} /> : undefined,
    headerBackTitle: null,
    headerStyle: CommonHeaderStyles.headerStyle as any,
    headerTitleAllowFontScaling: false,
  });
}

export function EndAssepHeader(title: string, showLeftButton?: boolean, plus:boolean = true, background:boolean = true):
  NavigationStackScreenOptions {

  return ({
    headerTitle: title,
    headerTitleStyle: CommonHeaderStyles.headerTitleStyle as any,
    headerLeft: showLeftButton ? (
      <HeaderButton
        action={NavigationActions.navigateToEndAssemt}
      >
        <Feather name="arrow-left" size={30} color={Colors.white} />
      </HeaderButton>
    ) : undefined,
    headerRight: plus ?
      <View style={styles.IconsBlock}>
          <HeaderButton
            action={NavigationActions.navigateToDictionaryAddWord}
          >
            <Feather name="plus" size={35} color={Colors.white} />
          </HeaderButton>
      </View>
    : undefined ,
    headerBackground: background ? <Image style={styles.headerBackground} source={ImageResources.backgroundHeader} /> : undefined,
    headerBackTitle: null,
    headerStyle: CommonHeaderStyles.headerStyle as any,
    headerTitleAllowFontScaling: false,
  });
}

export function BigHeader(title: string, showLeftButton?: boolean, showDrawerIcon?: boolean, plus:boolean = true, background:boolean = true, book?:boolean,):
NavigationStackScreenOptions {
  
  return ({
    headerTitle: title,
    headerTitleStyle: CommonHeaderStyles.headerTitleStyle as any,
    headerLeft: showLeftButton ? (
      <HeaderButton
        action={showDrawerIcon ? NavigationActions.toggleDrawer : NavigationActions.navigateGoBack}
      >
        {showDrawerIcon ? <Feather name="menu" size={30} color={Colors.white} /> : <Feather name="arrow-left" size={30} color={Colors.white} />}
      </HeaderButton>
    ) : undefined,
    headerRight: plus ? 
      <View style={styles.IconsBlock}>
        {
          book &&
          <HeaderButton
            action={NavigationActions.navigateToDictionary}
          >
            <Feather name="book-open" size={30} color={Colors.white} />
          </HeaderButton>
        }
          <HeaderButton
            action={NavigationActions.navigateToDictionaryAddWord}
          >
            <Feather name="plus" size={35} color={Colors.white} />
          </HeaderButton>
      </View>
    : undefined ,
    headerBackground: background ? <Image style={styles.headerBackground} source={ImageResources.backgroundHeader} /> : undefined,
    headerBackTitle: null,
    headerStyle: CommonHeaderStyles.headerStyle as any,
    headerTitleAllowFontScaling: false,
  });
}

const styles = styleSheetCreate({
  headerBackground: {
      flex:1,
      width:'100%',
  } as ImageStyle,
  IconsBlock: {
    flexDirection:'row',
  } as ViewStyle,
  image: {
      resizeMode: "center",
  } as ImageStyle,
  imageTintColor: {
      tintColor: Colors.white,
      resizeMode: "center"
  } as ImageStyle
})
