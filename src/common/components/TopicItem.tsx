import React, { PureComponent } from "react";
import {
  Text,
  TextStyle,
  View,
  ViewStyle,
  Platform,
  TouchableOpacity
} from "react-native";
import { Colors, windowWidth } from "../../core/theme";
import { styleSheetCreate } from "../utils";

interface IProps {
  category: string;
  title: string;
  status: string;
  onPress(): void;
  isFirstItem: number;
  isLastItem: boolean;
  access: boolean;
}

interface IState {

}

export class TopicItem extends PureComponent<IProps, IState> {

  render(): JSX.Element {

    const { category, title, status, isFirstItem, access, isLastItem, onPress } = this.props;

    let statusLineStyle = styles.statusCircleGreen;
    if (status !== "isDone") {
      statusLineStyle = styles.statusCircleWhite;
    }

    return (

        <View style={styles.containerLineAndTopicInfo}>
          <View style={styles.statusLineContainer}>
            <View style={styles.statusLine}>
              {
                isFirstItem == 0
                  ? <View style={styles.emptySpace} />
                  : <View style={styles.statusLineTop} />
              }
              <View style={statusLineStyle} />
              {
                isLastItem
                  ? <View style={styles.emptySpace} />
                  : <View style={styles.statusLineBottom} />
              }
            </View>
          </View>
          <TouchableOpacity onPress={status == "isDone" || access ? onPress : undefined}>
            <View style={[styles.topicInfoContainer, {opacity: access ? 1 : .3}]}>
              <View style={styles.topicInfoBox}>
                <View style={styles.contentBox}>
                  <View>
                    <Text style={styles.topicName}>{category}</Text>
										{
											title === ""
											? null
											: <Text style={styles.categoryText}>{title}</Text>
										}
                  </View>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </View>

    );
  }
}

const styles = styleSheetCreate({
  containerLineAndTopicInfo: {
    flexDirection: "row",
    justifyContent: "space-around",
    paddingHorizontal: windowWidth * .025,
  } as ViewStyle,

  // -- topic info --//

  topicInfoContainer: {
    paddingVertical: windowWidth * .022,

    ...Platform.select({
      ios: {
          shadowRadius: 9,
          shadowOpacity: 0.1,
          shadowOffset: {width: 1, height: 4},
      },
      android: {
        elevation: 8
    }}),
  } as ViewStyle,
  topicInfoBox: {
    width: windowWidth * .8,
    height: windowWidth * .242,
    backgroundColor: Colors.white,
    borderRadius: windowWidth * .02,
    overflow: "hidden",
  } as ViewStyle,
  contentBox: {
		justifyContent: "center",
		height: "100%",
    paddingHorizontal: windowWidth * .062,
  } as ViewStyle,
  topicName: {
    color: Colors.dark5E,
    fontSize: windowWidth * .039,
    fontWeight: "400",
  } as TextStyle,
  categoryText: {
    color: Colors.blueEC,
    fontSize: windowWidth * .037,
    paddingTop: windowWidth * .021,
  } as TextStyle,

  // -- satus line --//

  emptySpace: {
    height: windowWidth * .169,
  },
  statusLineContainer: {
    alignItems: "center",
    justifyContent: "center",
  } as ViewStyle,
  statusLine: {
  } as ViewStyle,
  statusLineTop: {
    width: windowWidth * .01,
    height: windowWidth * .169,
    backgroundColor: Colors.greyCE,
    alignSelf: "center",
  } as ViewStyle,
  statusLineBottom: {
    width: windowWidth * .01,
    height: windowWidth * .169,
    backgroundColor: Colors.greyCE,
    alignSelf: "center",
  } as ViewStyle,
  statusCircleBlue: {
    width: windowWidth * .062,
    height: windowWidth * .062,
    borderRadius: windowWidth * .062,
    zIndex: 2,
    alignSelf: "center",
    position: "absolute",
    top: windowWidth * .122,
    backgroundColor: Colors.blueEC,
  } as ViewStyle,
  statusCircleGreen: {
    width: windowWidth * .062,
    height: windowWidth * .062,
    borderRadius: windowWidth * .062,
    zIndex: 2,
    alignSelf: "center",
    position: "absolute",
    top: windowWidth * .122,
    backgroundColor: Colors.green,
  } as ViewStyle,
  statusCircleWhite: {
    width: windowWidth * .062,
    height: windowWidth * .062,
    borderRadius: windowWidth * .062,
    zIndex: 2,
    alignSelf: "center",
    position: "absolute",
    top: windowWidth * .122,
    backgroundColor: Colors.white,
    borderColor: Colors.greyCE,
    borderWidth: windowWidth * .01,
    borderStyle: "solid",
  } as ViewStyle,
});