import React, { PureComponent } from "react"
import {
  Text,
  TextStyle,
  ViewStyle,
  View,
  TouchableOpacity
} from "react-native"
import { Colors, windowWidth } from '../../core/theme'
import { styleSheetCreate } from '../utils'

interface IProps {
  day: string,
  month: string,
  title: string,
  deadline: string,
  onPress(title: string, deadline: string): void,
}

interface IState {

}

export class CalendarHomeWorkBox extends PureComponent<IProps, IState> {

  private openHomeworkDescription = (): void => {
    const { title, deadline, onPress } = this.props
      onPress(title, deadline)
  }

  render(): JSX.Element {

    const { day, month, title, deadline } = this.props    

    return (
      <TouchableOpacity onPress={this.openHomeworkDescription}>
        <View style={styles.dayContainer}>
          <View style={styles.dateHomeWorkBox}>
            <Text style={styles.day}>{day}</Text>
            <Text style={styles.month}>{month}</Text>
          </View>
          <View style={styles.descriptionHomeWorkBox}>
            <Text style={styles.homeWorkName} numberOfLines={1}>{title}</Text>
            <Text style={styles.homeWorkDeadline}>{deadline}</Text>
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}

const styles = styleSheetCreate({
  
  dayContainer: {
    width: windowWidth * .473,
    height: windowWidth * .475,
    backgroundColor: Colors.white,
    borderRadius: windowWidth * .01,
    marginTop: windowWidth * .049,
    marginRight: windowWidth * .046,
  } as ViewStyle,
  dateHomeWorkBox: {
    height: windowWidth * .275,
    borderStyle: 'solid',
    borderBottomColor: Colors.greyDB,
    borderBottomWidth: windowWidth * .003,
    paddingTop: windowWidth * .02,
    paddingLeft: windowWidth * .048,
    paddingBottom: windowWidth * .02,
  } as ViewStyle,
  descriptionHomeWorkBox: {
    paddingTop: windowWidth * .031,
    paddingHorizontal: windowWidth * .048,
    paddingBottom: windowWidth * .02,
  } as ViewStyle,
  homeWorkName: {
    color: Colors.dark5E,
    fontSize: windowWidth * .038,
    overflow: 'hidden',
  } as TextStyle,
  homeWorkDeadline: {
    paddingTop: windowWidth * .021,
    color: Colors.transparentGrey,
    fontSize: windowWidth * .038,
  } as TextStyle,
  day: {
    color: Colors.blueEC,
    fontSize: windowWidth * .11,
  } as TextStyle,
  month: {
    color: Colors.blueEC,
    fontSize: windowWidth * .05,
  } as TextStyle,
})
