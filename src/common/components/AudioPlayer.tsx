import React, { PureComponent } from 'react'
import {
  View, 
  ViewStyle,
  Platform,
  Slider,
  TouchableOpacity,
  Alert,
  Text
} from 'react-native'
import Sound from 'react-native-sound'
import LinearGradient from 'react-native-linear-gradient'
import { styleSheetCreate } from '../utils'
import { Colors, windowWidth } from '../../core/theme'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

interface IProps {
  audioPath: string
  white?: boolean
  record?:boolean
}

interface IState {
  duration:number,
  playSeconds: number,
  paused:boolean,
}

export class AudioPlayer extends PureComponent<IProps, IState> {

  sound: Sound | null
  sliderEditing:boolean
  timeout: any
  
  constructor(props: any) {
    super(props);
    this.state = {  
      duration:0,
      playSeconds: 0,
      paused:true,
    }

    this.sliderEditing = false
    this.sound = null
    this.timeout = null
  }

  getAudioTimeString(seconds:number){
    //const h = parseInt(seconds/(60*60));
    //@ts-ignore
    const m = parseInt(seconds%(60*60)/60);
    //@ts-ignore
    const s = parseInt(seconds%60);

    return ((m<10?'0'+m:m) + ':' + (s<10?'0'+s:s));
  }

  componentWillUnmount(){
    this.playComplete
    if(this.sound){
        this.sound.release();
        this.sound = null;
    }
    if(this.timeout){
      clearInterval(this.timeout);
    }
  }
  
  playComplete = (success:any) => {
    if(this.sound){
      if (success) {
        console.log('successfully finished playing');
      } else {
        console.log('playback failed due to audio decoding errors');
        Alert.alert('Notice', 'audio file error. (Error code : 2)');
      }
      this.setState({paused:true, playSeconds:0});
      this.sound.setCurrentTime(0);
    }
  }

  private pause = ():void => {
    if(this.sound){
      this.sound.pause();
      this.setState({paused:true});
    }
  }

  private play = ():void => {
    if(this.sound){ 
      this.sound.play(this.playComplete);
      this.setState({paused:false});
    } else {
      let {audioPath, record} = this.props
      let global = record ? '' : Sound.MAIN_BUNDLE

      this.sound = new Sound(audioPath, global, (e:any) => {
        if (e) {
          console.log('error loading track:', e)
        }
        if(this.sound){
          this.sound.play(this.playComplete)
  
          this.setState({duration:this.sound.getDuration()})
    
          this.timeout = setInterval(():void => {
            if(this.sound && !this.sliderEditing){
              this.sound.getCurrentTime((seconds:number, isPlaying:boolean):void => {
                if(isPlaying){
                  this.setState({playSeconds: seconds});
                }else{
                  this.pause
                }
              })
            }
          }, 100);  
        }
      })

      this.setState({paused:false});
    } 
  }

  onSliderEditStart = () => {
    this.sliderEditing = true;
  }
  onSliderEditEnd = () => {
    this.sliderEditing = false;
  }

  onSliderEditing = (value:any):void => {
    if(this.sound){
        this.sound.setCurrentTime(value);
        this.setState({playSeconds:value});
    }
  }

  whitePlayer = () => {

    let {duration, paused, playSeconds} = this.state

    const durationString = this.getAudioTimeString(duration-playSeconds)

    return(
      <View style={styles.playerWhite}>
        <View style={styles.containerWhite}>
          {/* <Text style={{color:'white', alignSelf:'center'}}>{currentTimeString}</Text> */}
          
            {
            paused             
            ? <TouchableOpacity onPress={this.play} style={styles.playButton}>
                  <LinearGradient
                    style={styles.playButton}
                    start={{ x: 0, y: 0 }}
                    end={{ x: 1, y: 0 }}
                    colors={['#4E71EC', '#4EAAEC']}
                  >
                    <MaterialCommunityIcons name="play" size={windowWidth * .046} color={Colors.white} />		
                  </LinearGradient>
              </TouchableOpacity>
            : <TouchableOpacity onPress={this.pause} style={styles.playButton}>
                <LinearGradient
                  style={styles.playButton}
                  start={{ x: 0, y: 0 }}
                  end={{ x: 1, y: 0 }}
                  colors={['#4E71EC', '#4EAAEC']}
                >
                  <MaterialCommunityIcons name="pause" size={windowWidth * .046} color={Colors.white} />
                </LinearGradient>
              </TouchableOpacity>
            }          
          <Slider
            onTouchStart={this.onSliderEditStart}
            // onTouchMove={() => console.log('onTouchMove')}
            onTouchEnd={this.onSliderEditEnd}
            // onTouchEndCapture={() => console.log('onTouchEndCapture')}
            // onTouchCancel={() => console.log('onTouchCancel')}
            onValueChange={this.onSliderEditing}
            value={playSeconds} 
            maximumValue={duration} 
            maximumTrackTintColor='blue' 
            minimumTrackTintColor='black' 
            thumbTintColor={Colors.blueEC}
            style={{flex:1, alignSelf:'center',  marginHorizontal:Platform.select({ios:5})}}
          />
          <Text style={{color: Colors.dark5E, alignSelf:'center'}}>{durationString}</Text>
        </View>
      </View>
    )
  }

  bluePlayer = () => {

    let {duration, paused, playSeconds} = this.state

    const durationString = this.getAudioTimeString(duration-playSeconds)

    return(
      <LinearGradient
        style={styles.player}
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 0 }}
        colors={['#4E71EC', '#4EAAEC']}
      >
        <View style={{width: '100%', flexDirection:'row'}}>
          {/* <Text style={{color:'white', alignSelf:'center'}}>{currentTimeString}</Text> */}
          {
          paused ? 
            <TouchableOpacity onPress={this.play} style={[styles.playButton, {backgroundColor:Colors.white}]}>
              <MaterialCommunityIcons name="play" size={windowWidth * .046} color={Colors.blueEC} />										
            </TouchableOpacity>
          :
            <TouchableOpacity onPress={this.pause} style={[styles.playButton, {backgroundColor:Colors.white}]}>
              <MaterialCommunityIcons name="pause" size={windowWidth * .046} color={Colors.blueEC} />
            </TouchableOpacity>
          }
          <Slider
            onTouchStart={this.onSliderEditStart}
            // onTouchMove={() => console.log('onTouchMove')}
            onTouchEnd={this.onSliderEditEnd}
            // onTouchEndCapture={() => console.log('onTouchEndCapture')}
            // onTouchCancel={() => console.log('onTouchCancel')}
            onValueChange={this.onSliderEditing}
            value={playSeconds} 
            maximumValue={duration} 
            maximumTrackTintColor='white' 
            minimumTrackTintColor='white' 
            thumbTintColor='white' 
            style={{flex:1, alignSelf:'center',  marginHorizontal:Platform.select({ios:5})}}
          />
          <Text style={{color:'white', alignSelf:'center'}}>{durationString}</Text>
        </View>
      </LinearGradient>
    )
  }

  render(): JSX.Element {
		return (
      <View style={styles.controls}>
        {
          this.props.white
          ? this.whitePlayer()
          : this.bluePlayer()
        }
      </View>
    )
  }
}
const styles = styleSheetCreate({  
  containerWhite: {
    width: '100%', 
    flexDirection:'row',
    backgroundColor: Colors.white,
  } as ViewStyle,
  controls: {
    alignItems: 'center',
  } as ViewStyle,
  playButton: {
		height: windowWidth * .1,
		width: windowWidth * .1,
    backgroundColor: Colors.blueEC, 
    borderRadius: windowWidth * .05, 
		padding: windowWidth * .024, 
		justifyContent: 'center',
		alignItems: 'center',
  } as ViewStyle,
  player: {
		flexDirection: 'row',
    backgroundColor: Colors.blueEC, 
    width: windowWidth * .9,
    borderRadius: windowWidth * .03, 
		overflow: 'hidden',
		alignItems: 'center',
		padding: windowWidth * .034, 
  } as ViewStyle,
  playerWhite: {
		flexDirection: 'row',
    backgroundColor: Colors.white, 
    width: windowWidth * .8,
    borderRadius: windowWidth * .03, 
		overflow: 'hidden',
		alignItems: 'center',
    padding: windowWidth * .034,
    ...Platform.select({
      ios: {
        shadowRadius: 15,
        shadowOpacity: 0.3,
        shadowOffset: {width: 1, height: 4},
      },
      android: {
        elevation: 5
    }}),
  } as ViewStyle,
})
