import React, { PureComponent } from "react"
import {
  Text,
  TextStyle,
  View,
  ViewStyle,
  TouchableWithoutFeedback,
} from "react-native"
import { Colors, windowWidth, windowHeight } from '../../core/theme'
import { styleSheetCreate } from '../utils'

interface IProps {
  date: string,
  title: string,
  deadline: string,
  isShow: boolean,
  closeDescription(): void,
}

interface IState {

}

export class CalendarHomeworkDescription extends PureComponent<IProps, IState> {

  private closeBlock = () => {
    const { closeDescription } = this.props
    closeDescription()
  }
  
  render(): JSX.Element {

    const { date, title, deadline, isShow } = this.props
    
    return (
      <View style={styles.container}>
        {
          isShow === false
          ? null
          : <View style={styles.homeworkInfoBg}>
              <TouchableWithoutFeedback onPress={this.closeBlock}>
              <View style={styles.homeworkInfo}>
                <View style={styles.homeworkInfoContent}>
                    <View style={styles.topLine} />
                  <View style={styles.homeworkInfoBox}>
                    <View style={styles.homeworkInfoDateBox}>
                      <Text style={styles.homeworkInfoDate}>{date}</Text>
                    </View>
                    <View style={styles.homeworkInfoTitleDeadlineContent}>
                      <Text style={styles.homeworkInfoTitle}>{title}</Text>
                      <Text style={styles.homeworkInfoDeadline}>{deadline}</Text>
                    </View>
                  </View>
                </View>
              </View>
              </TouchableWithoutFeedback>
            </View>
        }
      </View>
    )
  }
}

const styles = styleSheetCreate({
  container: {
    backgroundColor: Colors.red61,
    position: 'absolute',
    alignSelf: 'flex-end',
    bottom: 0,
  } as ViewStyle,
  homeworkInfoBg: {
    height: windowHeight * .39,
    width: windowWidth,
    backgroundColor: Colors.blueEC,
    zIndex: 2,
  } as ViewStyle,
  homeworkInfo: {
    width: windowWidth,
    height: windowWidth * .9,
    marginTop: windowWidth * .01,
    backgroundColor: Colors.white,
    borderTopLeftRadius: windowWidth * .03,
    borderTopRightRadius: windowWidth * .03,
  } as ViewStyle,
  homeworkInfoContent: {
    borderBottomColor: Colors.greyCE,
    borderBottomWidth: windowWidth * .003,
    borderStyle: 'solid',
  } as ViewStyle,
  topLine: {
    width: windowWidth * .2,
    height: windowWidth * .01,
    marginTop: windowWidth * .03,
    borderRadius: windowWidth * .003,
    backgroundColor: Colors.greyDB,
    alignSelf: 'center',
  } as ViewStyle,
  homeworkInfoBox: {
    paddingHorizontal: windowWidth * .03,    
  } as ViewStyle,
  homeworkInfoDateBox: {
    height: windowWidth * .2,
    justifyContent: 'center',
    backgroundColor: Colors.white,
    borderBottomColor: Colors.greyCE,
    borderBottomWidth: windowWidth * .003,
    borderStyle: 'solid',
  } as ViewStyle,
  homeworkInfoDate: {
    color: Colors.transparentGrey,
    fontSize: windowWidth * .038,
  } as TextStyle,
  homeworkInfoTitleDeadlineContent: {
    height: windowWidth * .2,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Colors.white,
  } as ViewStyle,
  homeworkInfoTitle:{
    color: Colors.dark5E,
    fontSize: windowWidth * .038,
  } as TextStyle,
  homeworkInfoDeadline: {
    color: Colors.transparentGrey,
    fontSize: windowWidth * .038,
  } as TextStyle,
})