import React, { PureComponent } from "react";
import {
  View,
  ViewStyle,
  Platform,
  TouchableOpacity,
  TextStyle,
  Text,
} from "react-native";
import { styleSheetCreate } from "../utils";
import { Colors, windowWidth } from "../../core/theme";
import { ButtonNoBg } from "./ButtonNoBg";
import { Button } from "./Button";
import { Answer } from "./Answer";

interface IProps {
  items: [];
  item: any;
  id: number;
  finishSreen(): any;
  testFinesh: boolean;
  nextSlide(): any;
}

interface IState {
  targetAnswer: string;
}

export class AuditionAssemtOne extends PureComponent<IProps, IState> {

  constructor(props: any) {
    super(props);
      this.state = {
        targetAnswer: ""
    };
  }

  renderButton = (title: string, onPress: () => void): JSX.Element => {
    return (
      <TouchableOpacity style={styles.button} onPress={onPress}>
        <Button titleButton={title} />
      </TouchableOpacity>
    );
  }

  renderButtonNoBg = (title: string, onPress: () => void): JSX.Element  => {
    return (
      <TouchableOpacity style={styles.button} onPress={onPress}>
        <ButtonNoBg titleButton={title} />
      </TouchableOpacity>
    );
  }

  selectAnswer = (index: string): any => {
    const {item, items, finishSreen, id, nextSlide} = this.props;

    const resultAnswer = (index == item[1]);

    item[2] = resultAnswer;
    this.setState({targetAnswer: index}, () => setTimeout(nextSlide, 300));

    if (id >= items.length) {
      finishSreen();
    }
  }

  render(): JSX.Element {
    const {targetAnswer} = this.state;
    const {items, item,  testFinesh, id} = this.props;
    /* answerState - isSelected, isNotSelected, CorrectNotSelected, isError, isCorrect */

    return (
      <View style={styles.slideBlock}>
          <View style={styles.content}>
            {
              testFinesh ?
                <View style={styles.center}>
                  <Text style={styles.question}>{`${item[0]}`}</Text>
                  <Answer title={"Да"} answerState={
                    targetAnswer === "1" && item[item.length - 1] ?
                      "isCorrect"
                    :
                    targetAnswer === "1" && !item[item.length - 1] ?
                      "isError"
                    :
                      "CorrectNotSelected"}
                    />

                  <Answer title={"Нет"} answerState={
                    targetAnswer === "0" && item[item.length - 1] ?
                      "isCorrect"
                    :
                    targetAnswer === "0" && !item[item.length - 1] ?
                      "isError"
                    :
                      "CorrectNotSelected"}
                  />
                </View>
              :
                <View style={styles.center}>
                  <Text style={styles.question}>{`${item[0]}`}</Text>
                  <TouchableOpacity onPress={this.selectAnswer.bind(this, "1")}>
                    <Answer title={"Да"} answerState={targetAnswer === "1" ? "isSelected" : "isNotSelected"}/>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={this.selectAnswer.bind(this, "0")}>
                    <Answer title={"Нет"} answerState={targetAnswer === "0" ? "isSelected" : "isNotSelected"}/>
                  </TouchableOpacity>
                </View>
            }
          </View>
          <View style={styles.footer}>

            <View style={styles.countBlock}>
              <Text style={styles.countPageText}>{id}/{items.length}</Text>
            </View>

          </View>
      </View>
    );
  }
}
const styles = styleSheetCreate({
  bgStyle: {
    flexGrow: 1,
    paddingBottom: windowWidth * .04,
    justifyContent: "space-around",
  } as ViewStyle,
  center: {
    alignItems: "center"
  } as ViewStyle,
  question: {
    marginBottom: windowWidth * .12,
    textAlign: "center",
    fontSize: windowWidth * .039,
    minHeight: windowWidth * .087,
  } as TextStyle,
  footer: {
    marginTop: windowWidth * .07,
  } as ViewStyle,
  controls: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
  } as ViewStyle,
  listenButton: {
    paddingTop: windowWidth * .07,
  } as ViewStyle,
  compitButtonBlock: {

    alignItems: "center"
  }  as ViewStyle,
  countBlock: {
    backgroundColor: Colors.blueEC,
    width: windowWidth * .8,
    flex: 1,
    padding: windowWidth * .04,
  } as ViewStyle,
  button: {
    padding: windowWidth * .02,
  } as ViewStyle,
  activeButtonText: {
    fontSize: 20,
    color: "#B81F00"
  } as TextStyle,
  content: {
    paddingVertical: windowWidth * .14,
    width:  windowWidth * .9,
    backgroundColor: Colors.white,
    alignItems: "center",
    justifyContent: "space-between",
    borderRadius: 15,
    ...Platform.select({
      ios: {
        shadowRadius: 8,
        shadowOpacity: 0.2,
        shadowOffset: {width: 0, height: 4},
      },
      android: {
        elevation: 8
    }}),
  } as ViewStyle,

  slideBlock: {
    alignItems: "center",
    paddingTop: windowWidth * .04,
  } as ViewStyle,
  contentTitle: {
    color: Colors.black60,
    fontSize: 28,
    padding: windowWidth * .04,
  } as TextStyle,
  countPageText: {
    fontSize: 20,
    color: Colors.white,
    textAlign: "center"
  } as TextStyle,
});