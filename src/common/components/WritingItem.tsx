import React, { PureComponent } from "react";
import {
  Text,
  TextStyle,
  View,
  ViewStyle,
  Image,
  ImageStyle,
  Platform,
  TouchableOpacity,
} from "react-native";
import { Colors, windowWidth } from "../../core/theme";
import { localization as l } from "../../common/localization/localization";
import { styleSheetCreate } from "../utils";
import { ImageResources } from "../ImageResources.g";

interface IProps {
  title: string;
  status: string;
  date: string;
  stars: number;
  onPress(): void;
}

interface IState {

}

export class WritingItem extends PureComponent<IProps, IState> {

  render(): JSX.Element {

    const { title, onPress, status, stars } = this.props;

    let statusText = l.common.notAvailable;
    if (status === "isDone") {
      statusText = l.common.done;
    } else if (status === "onCheck") {
      statusText = l.common.onCheck;
    } else if (status === "isRework") {
      statusText = l.common.toDo;
    }

    let statusIcon = ImageResources.iconHomeworkAttention;
    if (status === "isDone") {
      statusIcon = ImageResources.iconHomeworkDone;
    } else if (status === "onCheck") {
      statusIcon = ImageResources.iconHomeworkTime;
    } else if (status === "isRework") {
      statusIcon = ImageResources.rework;
    }

    //TODO: сделать вывод звезд
    return (
      <TouchableOpacity onPress={onPress}>
        <View style={styles.container}>
          <Text style={styles.title}>{title}</Text>
          { stars === 0
            ? null
            : <View style={styles.stars}>
                <Image
                  style={styles.star}
                  source={ImageResources.starOn}
                />
                <Image
                  style={styles.star}
                  source={ImageResources.starOn}
                />
                <Image
                  style={styles.star}
                  source={ImageResources.starOn}
                />
                <Image
                  style={styles.star}
                  source={ImageResources.starOn}
                />
                <Image
                  style={styles.star}
                  source={ImageResources.starOff}
                />
              </View>
          }
            <View style={styles.statusAndDate}>
              <View style={styles.statusContainer}>
                <Image
                  style={styles.statusIcon}
                  source={statusIcon}
                />
                <Text style={styles.statusText}>{statusText}</Text>
              </View>
              <Text style={styles.statusText}>12 марта 2019</Text>
            </View>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = styleSheetCreate({
  container: {
    width: windowWidth * .92,
    paddingHorizontal: windowWidth * .07,
    paddingTop: windowWidth * .055,
    paddingBottom: windowWidth * .03,
    backgroundColor: Colors.white,
    margin: windowWidth * .02,
    borderRadius: windowWidth * .02,
    ...Platform.select({
      ios: {
        shadowRadius: 3,
        shadowOpacity: 0.1,
        shadowOffset: { width: 0, height: 3 },
      },
      android: {
        elevation: 2
    }}),
  } as ViewStyle,
  title: {
    fontSize: windowWidth * .039,
    fontWeight: "500",
    color: Colors.black59,
  } as TextStyle,
  statusAndDate: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  } as ViewStyle,
  statusContainer: {
    flexDirection: "row",
    alignItems: "flex-start",
    paddingTop: windowWidth * .053,
  } as ViewStyle,
  statusIcon: {
    width: windowWidth * .056,
    height: windowWidth * .09,
    marginRight: windowWidth * .02,
  } as ImageStyle,
  statusText: {
    fontSize: windowWidth * .034,
    color: Colors.darkGrey9F
  } as TextStyle,
  stars: {
    paddingTop: windowWidth * .025,
    flexDirection: "row",
  } as ViewStyle,
  star: {
    width: windowWidth * .047,
    height: windowWidth * .045,
    marginRight: windowWidth * .014,
  } as ImageStyle,
});