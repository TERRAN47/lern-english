import React, { PureComponent } from 'react'
import {
  //Text,
  //TextStyle,
  View,
  ViewStyle,
  Image,
  ImageStyle,
} from 'react-native'
import { windowWidth } from '../../core/theme'
import { ImageResources } from '../../common/ImageResources.g'
import { styleSheetCreate } from '../../common/utils'

interface IProps {
  stars: number | null
}

export class Stars extends PureComponent<IProps> {

 

  render(): JSX.Element{

    const stars = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    if(this.props.stars) {
      for(let i = 0; i < this.props.stars; i++) {
        stars[i] = 1
      }
    }
    
    console.log('STARS', stars)
    console.log('PROPS', this.props.stars)

    return (
      <View>
        <View style={styles.stars}>
          {
            stars.map((item: any, index: any) => {
              return(
                <View key={index} >
                    {
                      item === 1
                      ? <Image
                          style={styles.star} 
                          source={ImageResources.starOn}
                        />
                      :	<Image 
                          style={styles.star}                  
                          source={ImageResources.starOff}
                        />                																	
                    }
                </View>
              )
            })
          }
        </View>
      </View>
      
    )
  }   
}

const styles = styleSheetCreate({
  stars: {
    paddingTop: windowWidth * .025,
    flexDirection: 'row',
  } as ViewStyle,
  star: {
    width: windowWidth * .047,
    height: windowWidth * .045,
    marginRight: windowWidth * .014,
  } as ImageStyle,
})