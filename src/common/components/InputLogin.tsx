import React, { PureComponent } from "react"
import {
  TextInput,
  ViewStyle,
  View,
} from "react-native"
import { Colors, windowWidth } from '../../core/theme'
import { styleSheetCreate } from '../utils'

interface IProps {
  placeholder: string
  secureTextEntry?: boolean
  onIconPress?: () => void
  isError?: boolean
  val?: string
  changeText:(text:string) => void
}

interface IState {
  isFocused: boolean
}

export class InputLogin extends PureComponent<IProps, IState> {

  state = {
    isFocused: false,
  }

  handleOnFocus = (): void => {
    this.setState({ isFocused: true })
  }
  handleOnBlur = (): void => {
    this.setState({ isFocused: false })
  }

  onChangeText = (func:(texts:string) => void, texts:string): void => {
    func(texts)
  }

  render(): JSX.Element {
    const { placeholder, val, isError, changeText, secureTextEntry } = this.props

    const inputUnderLine =
        isError
        ? styles.inputUnderlineRed 
        : this.state.isFocused  
          ? styles.inputUnderlineBlue
          : styles.inputUnderlineGrey
          
    return (
      <View style={styles.container}>
        <TextInput
          style={styles.input}
          onFocus={this.handleOnFocus}
          onBlur={this.handleOnBlur}
          defaultValue={val}
          placeholder={placeholder}
          secureTextEntry={secureTextEntry}
          placeholderTextColor={Colors.greyCE}
          underlineColorAndroid={Colors.transparent}
          onChangeText={(texts:string)=>this.onChangeText(changeText, texts)}
        />
        <View style={inputUnderLine} />
      </View>
    )
  }
}

const styles = styleSheetCreate({
  container: {
    width: windowWidth * .8,
    height: windowWidth * .195,
    justifyContent: 'space-between',
  } as ViewStyle,
  input: {
    color: Colors.dark5E,
    fontSize: windowWidth * .046,
    paddingTop: windowWidth * .09,
    paddingLeft: - (windowWidth * .11),
  } as ViewStyle,
  inputUnderlineGrey: {
    backgroundColor: Colors.greyDB,
    height: windowWidth * .002,
  } as ViewStyle,
  inputUnderlineBlue: {
    backgroundColor: Colors.blueEC,
    height: windowWidth * .006,
  } as ViewStyle,
  inputUnderlineRed: {
    backgroundColor: Colors.red61,
    height: windowWidth * .002,
  } as ViewStyle,
})