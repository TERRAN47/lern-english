import React, { PureComponent } from "react";
import {
  TextStyle,
  ViewStyle,
  View,
} from "react-native";
import { Colors, windowWidth } from "../../core/theme";
import { styleSheetCreate, styleSheetFlatten } from "../utils";

interface IProps {
  status?: string;
}

interface IState {

}

export class Triangle extends PureComponent<IProps, IState> {
  render(): JSX.Element {

    const { status } = this.props;

    let color = styles.colorTransparent;
    if (status === "isDone") {
      color = styles.colorGreen;
    } else if (status === "isSentReview") {
      color = styles.colorBlue;
    } else if (status === "isInToDo") {
      color = styles.colorYellow;
    } else if (status === "Error") {
      color = styles.colorRed;
    }
    const triangle = styleSheetFlatten([styles.triangle, color]);

    return (
      <View style={triangle} />
    );
  }
}

const styles = styleSheetCreate({
  triangle: {
    position: "absolute",
    top: 0,
    right: 0,
    width: 0,
    height: 0,
    backgroundColor: "transparent",
    borderStyle: "solid",
    borderTopWidth: 0,
    borderRightWidth: windowWidth * .07,
    borderBottomWidth: windowWidth * .07,
    borderLeftWidth: windowWidth * .07,
    borderTopColor: "transparent",
    borderRightColor: Colors.greenish,
    borderBottomColor: "transparent",
    borderLeftColor: "transparent",
  } as TextStyle,
  colorGreen: {
    borderRightColor: Colors.greenish,
  } as ViewStyle,
  colorBlue: {
    borderRightColor: Colors.blueEC,
  } as ViewStyle,
  colorYellow: {
    borderRightColor: Colors.orange,
  } as ViewStyle,
  colorRed: {
    borderRightColor: Colors.red61,
  } as ViewStyle,
  colorTransparent: {
    borderRightColor: Colors.transparent,
  } as ViewStyle,
});