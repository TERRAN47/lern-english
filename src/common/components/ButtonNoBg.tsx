import React, { PureComponent } from "react"
import {
  Text,
  View,
  TextStyle,
  ViewStyle
} from "react-native"
import { Colors, windowWidth } from '../../core/theme'
import { styleSheetCreate } from '../utils'

interface IProps {
  titleButton: string
  width?: number,
  onIconPress?: () => void
  isError?: boolean
}

interface IState {

}

export class ButtonNoBg extends PureComponent<IProps, IState> {

  render(): JSX.Element {

    const { titleButton, width = windowWidth * .8  } = this.props

    return (
      <View style={[styles.buttonBackground, {width} ]}>
        <Text style={styles.buttonText}>
          {titleButton}
        </Text>
      </View>
    )
  }
}

const styles = styleSheetCreate({
  buttonText: {
    color: Colors.dark5E,
    fontSize: windowWidth * .046,
  } as TextStyle,
  buttonBackground: {
    fontSize: windowWidth * .046,
    borderRadius: windowWidth * .026,
    width: windowWidth * .8,
    alignItems: 'center',
    justifyContent: 'center',
    height: windowWidth * .147,
    borderWidth:2,
    borderColor: Colors.blueEC,
    backgroundColor:Colors.white
  } as ViewStyle,
  linearGradient: {
    width: windowWidth * .8,
    height: windowWidth * .147,
    borderRadius: windowWidth * .025,
    alignItems: 'center',
    justifyContent: 'center',
  } as ViewStyle,
})