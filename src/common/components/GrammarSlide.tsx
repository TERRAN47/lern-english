import React, { PureComponent } from "react";
import {
  View,
  ViewStyle,
  Platform,
  TouchableOpacity,
  TextStyle,
  Text,
  Alert,

} from "react-native";
import { styleSheetCreate } from "../utils";
import { localization as l } from '../../common/localization/localization'
import { Colors, windowWidth } from "../../core/theme";
import { InputMulti } from "./InputMulti";
import { Button } from "./Button";
import FontAwesome from "react-native-vector-icons/FontAwesome";

interface IProps {
  items: [];
  item: any;
  id: number;
  finishSreen(): any;
  testFinesh: boolean;
  nextSlide(): any;
  openModal(): void;
}

interface IState {
  text: string;
}

export class GrammarSlide extends PureComponent<IProps, IState> {
  constructor(props: any) {
    super(props);
      this.state = {
        text: "",
    };
  }

  changeText = (text: string): void => {
    this.setState({text});
  }

  nextTask(): void {
    let {text} = this.state;
    text = text.trim();
    if (text !== "") {
      const {item, finishSreen, items, id, nextSlide} = this.props;
      const resultAnswer = (text == item[1]);

      item[2] = resultAnswer;

      if (items.length ===  id) {
        finishSreen();
      } else {
        setTimeout(nextSlide, 300);
      }
    } else {
      Alert.alert(l.training.attention, l.training.fillResponseField);
    }
  }

  render(): JSX.Element {
    const {items, item, testFinesh, openModal, id} = this.props;
    const {text} = this.state;

    return (
      <View style={styles.container} >
        <View style={styles.content} >
          <View style={styles.task}>
            <View style={styles.taskBlock}>
              <Text style={styles.description}>
                {item[0]}
              </Text>
              <TouchableOpacity onPress={() => openModal()}>
                <FontAwesome name="question-circle" size={35} color={Colors.blueEC} />
              </TouchableOpacity>
            </View>
            {
              testFinesh ?
                <Text style={[styles.textAnswer, {borderBottomColor: item[item.length - 1] ? "green" : "red"}]}>{text}</Text>
              :
                <InputMulti
                  placeholder={"Write here"}
                  changeText={this.changeText}
                />
            }
          </View>
          <TouchableOpacity onPress={this.nextTask.bind(this)}>
            <Button
              titleButton={l.common.ready}
              width={windowWidth * .7}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.countBlock}>
          <Text style={styles.numberSlide}>{id}/{items.length}</Text>
        </View>
      </View>
    );
  }
}
const styles = styleSheetCreate({
  numberSlide: {
    color: Colors.white,
    textAlign: "center",
    fontSize: windowWidth * .045,
  } as TextStyle,
  textAnswer: {
    borderBottomWidth: 2,
    marginTop: 15,
    fontSize: windowWidth * .045
  } as TextStyle,
  button: {
    padding: windowWidth * .02
  } as ViewStyle,
  compitButtonBlock: {
    marginTop: 20,
    alignItems: "center"
  }  as ViewStyle,
  countBlock: {
    backgroundColor: Colors.blueEC,
    marginTop: windowWidth * .04,
    width: windowWidth * .9,
    flex: 1,
    padding: windowWidth * .03,
  } as ViewStyle,
  taskBlock: {
    flexDirection: "row",
  } as ViewStyle,
  container: {
    flexGrow: 1,
    marginTop: windowWidth * .04,
    alignItems: "center",
    justifyContent: "space-around",
  } as ViewStyle,
  content: {
    width: windowWidth * .9,
    height: windowWidth * 1.12,
    backgroundColor: Colors.white,
		borderRadius: windowWidth * .03,
		alignItems: "center",
		justifyContent: "space-between",
		paddingHorizontal: windowWidth * .07,
		paddingTop: windowWidth * .12,
		paddingBottom: windowWidth * .07,
		...Platform.select({
      ios: {
          shadowRadius: 8,
          shadowOpacity: 0.1,
          shadowOffset: {width: 1, height: 4},
      },
      android: {
        elevation: 8
    }}),
  } as ViewStyle,
	task: {
		marginHorizontal: windowWidth * .02,
		maxHeight: windowWidth * .6,
	} as ViewStyle,
	description: {
    fontSize: windowWidth * .045,
    width: windowWidth * .7,
		color: Colors.black59,
	} as TextStyle,
	buttonStyle: {
		paddingHorizontal: windowWidth * .05,
	} as ViewStyle,
});