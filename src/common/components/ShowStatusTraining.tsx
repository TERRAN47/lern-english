import React, { PureComponent } from "react";
import {
  TextStyle,
  ViewStyle,
  View,
} from "react-native";
import { Colors, windowWidth } from "../../core/theme";
import { styleSheetCreate, styleSheetFlatten } from "../utils";

interface IProps {
  status?: string;
}

interface IState {

}

export class ShowStatusTraining extends PureComponent<IProps, IState> {
  render(): JSX.Element {

    const { status } = this.props;

    let color = styles.colorTransparent;

    if (status === "isDone") {
      color = styles.colorGreen;
    } else if (status === "isSentReview") {
      color = styles.colorBlue;
    } else if (status === "isInToDo") {
      color = styles.colorYellow;
    }

    const triangle = styleSheetFlatten([styles.triangle, color]);

    return (
      <View style={triangle} />
    );
  }
}

const styles = styleSheetCreate({
  triangle: {
    position: "absolute",
    left: 0,
    width: windowWidth * .027,
    height: "100%",
    backgroundColor: "transparent",
    borderStyle: "solid",
  } as TextStyle,
  colorGreen: {
    backgroundColor: Colors.greenish,
  } as ViewStyle,
  colorBlue: {
    backgroundColor: Colors.blueEC,
  } as ViewStyle,
  colorYellow: {
    backgroundColor: Colors.blueF9,
  } as ViewStyle,
  colorTransparent: {
    backgroundColor: Colors.transparent,
  } as ViewStyle,
});