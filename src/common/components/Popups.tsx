import React, { PureComponent } from 'react'
import {
	Modal,
	Platform,
	Text,
	TextStyle,
	TouchableOpacity,
	TouchableWithoutFeedback,
	View,
	ViewStyle,
	Image,
} from 'react-native'
import { Colors, Fonts, windowWidth } from '../../core/theme'
import { styleSheetCreate } from '../utils'
import { Triangle } from '../../common/components/Triangle'
import { ImageResources } from '../ImageResources.g'

interface IProps {
	title: string
	body: {} | string | null
	isVisible: boolean
	closeModal: () => void
	status: string
}

export class Popups extends PureComponent<IProps> {
	render(): JSX.Element {
		const { title, body, closeModal, isVisible, status } = this.props;

		return (
			<Modal transparent={true} animationType={"fade"} visible={isVisible} onRequestClose={closeModal}>
				<TouchableWithoutFeedback onPress={closeModal}>
					<View style={styles.modalBackground}>
						<TouchableOpacity activeOpacity={1} style={styles.contentContainer}>
							<Triangle status={status} />
							{ status === 'Error'
								? <Image 
										style={styles.statusIcon} 
										source={ImageResources.rework}
									/>
								: <Image 
										source={ImageResources.iconHomeworkDone}
										style={styles.statusIcon}
								  />
							}
							<Text style={styles.title}>{title}</Text>
							<Text style={styles.body}>{body}</Text>
						</TouchableOpacity>
					</View>
				</TouchableWithoutFeedback>
			</Modal>
		)
	}
}

const styles = styleSheetCreate({
	modalBackground: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: Colors.transparentBlack,
	} as ViewStyle,
	contentContainer: {
		paddingTop: windowWidth * .057,
		paddingLeft: windowWidth * .068,
		paddingRight: windowWidth * .069,
		paddingBottom: windowWidth * .0525,
		minHeight: windowWidth * .438,
		width: windowWidth * .91,
		overflow: 'hidden',
		alignItems: 'center',
		justifyContent: 'flex-start',
		borderRadius: windowWidth * .022,
		backgroundColor: Colors.white,
		...Platform.select({
				ios: {
					shadowRadius: 10,
					shadowOpacity: .4,
					shadowOffset: { width: 0, height: 24 },
				},
				android: {
						elevation: 3,
				}
		}),
	} as ViewStyle,
	buttonsContainer: {
		flex: 1,
		flexDirection: 'row',
		alignSelf: 'flex-end',
		alignItems: 'flex-end',
		justifyContent: 'flex-end',
	} as ViewStyle,
	title: {
		fontSize: windowWidth * .039,
		fontFamily: Fonts.medium,
		color: Colors.dark5E,
		textAlign: 'center',		
	} as TextStyle,
	body: {
		fontSize: windowWidth * .039,
		paddingTop: windowWidth * .034,
		fontFamily: Fonts.medium,
		color: Colors.darkGrey9F,
		textAlign: 'center',
	} as TextStyle,
	statusIcon: {
    width: windowWidth * .084,
		height: windowWidth * .135,
	}
})
