import React, { PureComponent } from "react"
import {
  Text,
  TextStyle,
	View,
  ViewStyle,
  Image,
  ImageStyle,
  Platform,
  TouchableOpacity,
} from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { Colors, windowWidth } from '../../core/theme'
//import { localization as l } from '../../common/localization/localization'
import { styleSheetCreate } from '../utils'
import { ImageResources } from '../ImageResources.g'

interface IProps {
  foto?: ImageResources
  title: string
  message?: number
  online?: boolean
  onPress(): void
}

interface IState {

}

export class ConnectItem extends PureComponent<IProps, IState> {
  
  state = {
    pressStatus: false,
  }
  
  render(): JSX.Element {

    const { title, foto, message, online, onPress } = this.props
    
    return (
      <TouchableOpacity onPress={onPress}>
        <View style={styles.container}>
          <View style={styles.user}>
            <View style={styles.fotoContainer}>
              <LinearGradient
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
                colors={['#4E71EC', '#4EAAEC']}
                style={styles.fotoBg}
              >
                { foto
                  ? <Image 
                      style={styles.foto}
                      source={foto}
                    />
                  : <Text style={styles.letter}>{title.charAt(0)}</Text>
                }
              </LinearGradient>
              { online
                ? <View style={styles.online} />
                : null
              }
            </View>
            <Text style={styles.title}>{title}</Text>
          </View>
          {
            message
            ? <View style={styles.messageContainer}>
                <Text style={styles.numberMessage}>{message}</Text>
              </View>
            : null
          }
        </View>
      </TouchableOpacity>
    )
  }
}

const styles = styleSheetCreate({
  container: {
    width: windowWidth * .9,
    padding: windowWidth * .055,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Colors.white,
    margin: windowWidth * .02,
    borderRadius: windowWidth * .03,
    ...Platform.select({
      ios: {
          shadowRadius: 2,
          shadowOpacity: 0.1,
          shadowOffset: {width: 0, height: 3},
      },
      android: {
        elevation: 8
    }}),  
  } as ViewStyle,
  user: {
    flexDirection: 'row',
    alignItems: 'center',
  } as ViewStyle,
  messageContainer: {
    backgroundColor: Colors.blueEC,
    width: windowWidth * .11,
    height: windowWidth * .11,
    borderRadius: windowWidth * .7,
    justifyContent: 'center',
    alignItems: 'center',
  } as ViewStyle,
  numberMessage: {
    color: Colors.white,
    fontSize: windowWidth * .039,
    fontWeight: '500',
  } as TextStyle,
  title: {
    fontSize: windowWidth * .039,
    color: Colors.dark5E,
    paddingLeft: windowWidth * .04,
  } as TextStyle,
  fotoContainer: {
    position: 'relative',
  } as ViewStyle,
  fotoBg: {
    width: windowWidth * .15,
    height: windowWidth * .15,
    borderRadius: windowWidth * .08,
    justifyContent: 'center',
    alignItems: 'center',
  } as ImageStyle,
  online: {
    width: windowWidth * .04,
    height: windowWidth * .04,
    borderRadius: windowWidth * .04,
    backgroundColor: Colors.green,
    zIndex: 3,
    position: 'absolute',
    right: 0,
  } as ViewStyle,
  foto: {
    width: windowWidth * .15,
    height: windowWidth * .15,
    borderRadius: windowWidth * .08,
  } as ImageStyle,
  letter: {
    fontSize: windowWidth * .06,
    color: Colors.white,
    fontWeight: '500',
  } as TextStyle,
}) 