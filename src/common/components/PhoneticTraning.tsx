import React, { PureComponent } from 'react'
import {
  View, 
  ViewStyle,
  Platform,
  TouchableOpacity,
  TextStyle,
  Text,
} from 'react-native'
import { styleSheetCreate } from '../utils'
import { Colors, windowWidth } from '../../core/theme'
import { ButtonNoBg } from './ButtonNoBg'
import { Button } from './Button'
import { Answer } from './Answer'
import { localization as l } from '../localization/localization'
import Sound from 'react-native-sound'

interface IProps {
  items: [] 
  item: any
  id: number
  finishSreen(): any
  testFinesh: boolean
  nextSlide(): any
}

interface IState {
  targetAnswer: number,
  listen: boolean
  finish:boolean
}

export class PhoneticTraning extends PureComponent<IProps, IState> {

  constructor(props: any) {
    super(props);
      this.state = {
        targetAnswer:-1,
        listen:false,
        finish:false
    }
  }

  renderButton(title:string, onPress:()=> void) {
    return (
      <TouchableOpacity style={styles.button} onPress={onPress}>
        <Button titleButton={title} />
      </TouchableOpacity>
    )
  } 

  renderButtonNoBg(title:string, onPress:()=> void) {
    return (
      <TouchableOpacity style={styles.button} onPress={onPress}>
        <ButtonNoBg titleButton={title} />
      </TouchableOpacity>
    );
  }
  
  selectAnswer(index:number){
    let {item, items, id, nextSlide} = this.props
    let finish = false
    let resultAnswer = (item[index] == item[item.length-1])

    item.push(resultAnswer)
    if(items.length ===  id) finish = true
    this.setState({targetAnswer:index, finish}, ()=> setTimeout(nextSlide, 500))
  }

  private stop = async():Promise<any> => {
    if (!this.state.listen) {
      console.warn('Can\'t stop, not recording!');
      return;
    }

    this.setState({listen: false});
  }

  async listenAudio(item:any){
    if (this.state.listen) {
      await this.stop()
    }

    let sound = new Sound(`sound_${item[2].toLowerCase().replace(/\s/g, '')}.mp3`, Sound.MAIN_BUNDLE, (error) => {
    
      if (error) {
        console.log('failed to load the sound', error)
      }
    });
    
    setTimeout(() => {
      sound.play((success) => {
        if (success) {
          console.log('successfully finished playing')
        } else {
          console.log('playback failed due to audio decoding errors')
        }
      })
    }, 100)
  }

  render(): JSX.Element {
    let {targetAnswer, finish} = this.state
    let {items, item,  testFinesh, finishSreen, id} = this.props
    /* answerState - isSelected, isNotSelected, CorrectNotSelected, isError, isCorrect */

		return (
      <View style={styles.slideBlock}>
          <View style={styles.content}>
            <View>
              {
                item.map((elem:string, index:number)=>{
                  if(testFinesh){
                    if(2 > index){
                      return(
                        <View key={index}>
                          <Answer title={elem} answerState={
                            targetAnswer === index && item[item.length-1] ? 
                            'isCorrect' 
                            : 
                            targetAnswer === index && !item[item.length-1] ?
                            'isError' 
                            :  
                            'CorrectNotSelected'}
                          />
                        </View>
                      )
                    }else{
                      return null
                    }
                  }else{
                    if(2 > index){
                      return(
                        <TouchableOpacity onPress={this.selectAnswer.bind(this, index)} key={index}>
                          <Answer title={elem} answerState={targetAnswer === index ? 'isSelected' : 'isNotSelected'}/>
                        </TouchableOpacity>
                      )
                    }else{
                      return null
                    }
                  }
                })
              }
            </View>

            <TouchableOpacity onPress={this.listenAudio.bind(this, item)} style={styles.listenButton}>
              <Button
                titleButton={l.common.listen} 
                width={windowWidth * .6}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.footer}>
          {
            id >= items.length && finish ?
              <View style={styles.compitButtonBlock}>
                {this.renderButtonNoBg(l.training.completeExercise, finishSreen)}
              </View>
            :
              <View style={styles.countBlock}>
                <Text style={styles.countPageText}>{id}/{items.length}</Text>
              </View>
          }
          </View>
      </View>
    )
  }
}
const styles = styleSheetCreate({ 
  bgStyle: {
    flexGrow: 1,
    paddingBottom: windowWidth * .04,
    justifyContent: 'space-around',
  } as ViewStyle,
  footer:{
    marginTop: windowWidth * .07,
  } as ViewStyle,
  controls: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  } as ViewStyle,
  listenButton: {
    paddingTop: windowWidth * .07,
  } as ViewStyle,
  compitButtonBlock:{

    alignItems: 'center'
  }  as ViewStyle,
  countBlock: {
    backgroundColor: Colors.blueEC,
    width: windowWidth * .8,
    flex:1,
    padding: windowWidth * .03,
  } as ViewStyle,
  button: {
    padding: windowWidth * .02,
  } as ViewStyle,
  activeButtonText: {
    fontSize: 20,
    color: "#B81F00"
  } as TextStyle,
  content:{
    paddingVertical: windowWidth * .14,
    width:  windowWidth * .9,
    backgroundColor: Colors.white,
    alignItems: 'center',
    justifyContent: 'space-between',
    borderRadius: 15,
    ...Platform.select({
      ios: {
        shadowRadius: 8,
        shadowOpacity: 0.2,
        shadowOffset: {width: 0, height: 4},
      },
      android: {
        elevation: 8
    }}),
  } as ViewStyle,

  slideBlock:{
    alignItems:'center', 
    paddingTop: windowWidth * .04,
  } as ViewStyle,
  contentTitle:{
    color: Colors.black60,
    fontSize: 28,
    padding: windowWidth * .04,
  } as TextStyle,
  countPageText:{
    fontSize: 20,
    color: Colors.white,
    textAlign: 'center'
  } as TextStyle,
})