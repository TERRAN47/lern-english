import React, { PureComponent } from "react";
import {
  Text,
  Image,
  ImageStyle,
  TextStyle,
  ViewStyle,
  View,
  TouchableOpacity
} from "react-native";
import { Colors, windowWidth } from "../../core/theme";
import { styleSheetCreate } from "../utils";
import { ImageResources } from "../ImageResources.g";
import { ShowStatusTraining } from "./ShowStatusTraining";
import { localization as l } from "../localization/localization";

interface IProps {
  category: string;
  status: string;
  onPress(data:any): void;
  progress: any;
  data: any;
  theory:boolean;
  index:number;
}

interface IState {

}

export class LevelItem extends PureComponent<IProps, IState> {
  navigationToScreen(){
    let {onPress, index, data} = this.props; 
    onPress({...data, index});
  }

  render(): JSX.Element {

    const { category, theory, status, progress } = this.props;

    let statusText = l.common.notAvailable;
    let statusIcon = ImageResources.iconHomeworkAttention;

    if (status === "isDone") {
      statusText = l.common.done;
      statusIcon = ImageResources.iconHomeworkDone;
    } else if (status === "isSentReview") {
      statusText = l.common.beInProgress;
      statusIcon = ImageResources.iconHomeworkTime;
    } else if (status === "isInToDo") {
      statusText = l.common.availableToPerform;
    }

    return (
      <View>
        <View style={styles.containerLevelLesson}>
          <TouchableOpacity onPress={status != "" ? this.navigationToScreen.bind(this) : (): void =>{}} >
            <View style={styles.lesoonInfoContainer}>
                <View style={styles.lesoonInfoBox}>
                  <ShowStatusTraining status={status} />
                     <View style={styles.contentBox}>
                      <View style={{maxWidth: windowWidth * .7}}>
                        <Text style={styles.lessonName}>{`${category}`}</Text>
                      </View>
                      {
                        status === "" || status === "isInToDo" ? 
                          <View style={styles.statusContainer}>
                            <Text style={styles.statusText}>{statusText}</Text>
                          </View>                                    
                        : <View style={styles.statusContainer}>
                            <Image
                                style={styles.statusIcon}
                                source={statusIcon}
                            />
                            <Text style={styles.statusText}>{statusText}</Text>
                          </View>
                        }
                    </View>
                    <View style={styles.progressBox}>
                      {theory || status === "isDone" ? <View style={styles.greenProgress} /> : <View style={styles.greyProgress} />}
                      {
                        progress.map((item: boolean, index: any) => {
                            return(
                              <View key={index}>
                                  {
                                  (status === "isDone") 
                                    ? <View style={styles.greenProgress} />	
                                  :	(status !== "isSentReview" && status !== "isDone")	
                                    ?	<View style={styles.greyProgress} />																		
                                  :(item && status === "isSentReview")
                                      ?<View style={styles.greenProgress} />
                                      :<View style={styles.blueToDoProgress} />																		
                                  }
                              </View>
                            );
                        })
                      }
                    </View>
                </View>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = styleSheetCreate({
    containerLevelLesson: {
    flexDirection: "row",
  } as ViewStyle,

  // -- lesoon info --//

  lesoonInfoContainer: {
  } as ViewStyle,
  lesoonInfoBox: {
    height: windowWidth * .25,
    backgroundColor: Colors.white,
    borderBottomColor: Colors.greyDB,
    borderBottomWidth: windowWidth * .003,
    width: windowWidth * .933,
    overflow: "hidden",
    flexDirection: "row",
    justifyContent: "space-between",
  } as ViewStyle,
  contentBox: {
    paddingTop: windowWidth * .048,
    paddingLeft: windowWidth * .062,
  } as ViewStyle,
  lessonName: {
    color: Colors.dark5E,
    fontSize: windowWidth * .039,
    fontWeight: "400",
  } as TextStyle,
  categoryText: {
    color: Colors.blueEC,
    fontSize: windowWidth * .037,
    paddingTop: windowWidth * .021,
  } as TextStyle,
  statusContainer: {
    flexDirection: "row",
    alignItems: "flex-start",
    paddingTop: windowWidth * .053,
  } as ViewStyle,
  statusIcon: {
    width: windowWidth * .056,
    height: windowWidth * .09,
    marginRight: windowWidth * .02,
  } as ImageStyle,
  statusText: {
    color: Colors.darkGrey9F,
    fontSize: windowWidth * .037,
	} as TextStyle,
	
	//-- progress --//
	progressBox: {
		height: "100%",
		justifyContent: "center",
		paddingRight: windowWidth * .04,
	} as ViewStyle,
	greenProgress: {
		height: windowWidth * .013,
		width: windowWidth * .092,
		borderRadius: windowWidth * .003,
		marginVertical: windowWidth * .013,
		backgroundColor: Colors.greenish,
	} as ViewStyle,
	blueDoneProgress: {
		height: windowWidth * .013,
		width: windowWidth * .092,
		borderRadius: windowWidth * .003,
		marginVertical: windowWidth * .013,
		backgroundColor: Colors.blueEC,
	} as ViewStyle,
	blueToDoProgress: {
		height: windowWidth * .013,
		width: windowWidth * .092,
		borderRadius: windowWidth * .003,
		marginVertical: windowWidth * .013,
		backgroundColor: Colors.blueF9,
	} as ViewStyle,
	greyProgress: {
		height: windowWidth * .013,
		width: windowWidth * .092,
		borderRadius: windowWidth * .003,
		marginVertical: windowWidth * .013,
		backgroundColor: Colors.lateF1,
	} as ViewStyle,
});
