import React, { PureComponent } from "react";
import {
	Text,
	TextStyle,
  View,
  ViewStyle,
} from "react-native";
import { Colors, windowWidth } from '../../core/theme';
import { styleSheetCreate } from '../utils';

interface IProps {
	title: string;
	answerState: string;
}

interface IState {

}

export class Answer extends PureComponent<IProps, IState> {

  render(): JSX.Element {

    const { title, answerState } = this.props;

		return (
			<View>
				{
					answerState === 'isSelected'
					? <View style={styles.answerSelected} >
							<View style={styles.circleSelected}>
								<View style={styles.circleSelectedInner}/>
							</View>
							<Text style={styles.answerText}>{title}</Text>
						</View> 
					: answerState === 'isError' 
						?	<View style={styles.errorAnswer} >
								<Text style={styles.answerText}>{title}</Text>
							</View> 
						: answerState === 'CorrectNotSelected' ?
								<View style={styles.possibleAnswer} >
									<Text  style={styles.answerText}>{title}</Text>
								</View>
							: answerState === 'isCorrect'
								?	<View style={styles.correctAnswer} >
										<Text style={styles.answerText}>{title}</Text>
									</View> 
								:	answerState === 'isNotSelectedNoCircle'
									?	<View style={styles.possibleAnswer} >
											<Text  style={styles.answerText}>{title}</Text>
										</View>
									:	<View style={styles.possibleAnswer} >
											<View style={styles.circleNotSelected}/>
											<Text  style={styles.answerText}>{title}</Text>
										</View>
				}
			</View>
    );
  }
}

const styles = styleSheetCreate({
  possibleAnswer: {
		width: windowWidth * .8,
    height: windowWidth * .15,
		borderColor: Colors.greyCE,
		borderWidth: windowWidth * .007,
		borderStyle: 'solid',
		borderRadius: windowWidth * .03,
		marginBottom: windowWidth * .05,
		justifyContent: 'center',
		alignItems: 'center',
		flexDirection: 'row',
	} as ViewStyle,
	answerSelected: {
		width: windowWidth * .8,
		height: windowWidth * .15,
		borderColor: Colors.blueEC,
		borderWidth: windowWidth * .007,
		borderStyle: 'solid',
		borderRadius: windowWidth * .03,
		marginBottom: windowWidth * .05,
		justifyContent: 'center',
		alignItems: 'center',
		flexDirection: 'row',
	} as ViewStyle,
	errorAnswer: {
		width: windowWidth * .8,
		height: windowWidth * .15,
		borderColor: Colors.red61,
		backgroundColor: Colors.pink,
		borderWidth: windowWidth * .007,
		borderStyle: 'solid',
		borderRadius: windowWidth * .03,
		marginBottom: windowWidth * .05,
		justifyContent: 'center',
		alignItems: 'center',
	} as ViewStyle,
	correctAnswer: {
		width: windowWidth * .8,
		height: windowWidth * .15,
		borderColor: Colors.green,
		backgroundColor: Colors.lightGreen,
		borderWidth: windowWidth * .007,
		borderStyle: 'solid',
		borderRadius: windowWidth * .03,
		marginBottom: windowWidth * .05,
		justifyContent: 'center',
		alignItems: 'center',
	} as ViewStyle,
	circleNotSelected: {
		width: windowWidth * .07,
		height: windowWidth * .07,
		borderColor: Colors.greyCE,
		borderWidth: windowWidth * .007,
		borderStyle: 'solid',
		borderRadius: windowWidth * .07,
		position: 'absolute',
		top: windowWidth * .035,
		left: windowWidth * .035,
	} as ViewStyle,
	circleSelected: {
		width: windowWidth * .07,
		height: windowWidth * .07,
		borderColor: Colors.blueEC,
		borderWidth: windowWidth * .007,
		borderStyle: 'solid',
		borderRadius: windowWidth * .07,
		position: 'absolute',
		justifyContent: 'center',
		alignItems: 'center',
		top: windowWidth * .035,
		left: windowWidth * .035,
	} as ViewStyle,
	circleSelectedInner: {
		width: windowWidth * .04,
		height: windowWidth * .04,
		backgroundColor: Colors.blueEC,
		borderRadius: windowWidth * .04,
	} as ViewStyle,
	answerText: {
		color: Colors.dark5E,
		fontSize: windowWidth * .043 
	} as TextStyle,
});