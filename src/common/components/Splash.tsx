import React from "react"; // {PureComponent}
import {Image, ImageStyle, View} from "react-native";
import {appSettingsProvider} from "../../core/settings";
import {CommonStyles, isIos, windowHeight, windowWidth} from "../../core/theme";
import {ImageResources} from "../ImageResources.g";
import {styleSheetCreate} from "../utils";
import { connectAdv } from '../../core/store/connectAdv'
import { Dispatch } from 'redux'
import { NavigationActions } from '../../navigation/navigation'
import { BaseReduxComponent } from '../../core/BaseComponent'
import { AsyncStorage } from 'react-native'

interface IDispatchProps {
  navigateToHome(): void
 
}
interface IStateProps {

}

interface IState {
}

interface IProps {
}

@connectAdv(
  null,
  (dispatch: Dispatch): IDispatchProps => ({  
    navigateToHome(): void {
      dispatch(NavigationActions.navigateToHome())
    }
  })
)

export class Splash extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  async componentDidMount(){
    let token = await AsyncStorage.getItem('Authorization')
    if(token){
      await this.dispatchProps.navigateToHome()
    }
  }

  render(): JSX.Element | null {
    if (isIos) {
      const useDefaultSource = appSettingsProvider.settings.environment != "Development" && __DEV__;

      return (
        <View style={CommonStyles.flexWhiteBackground}>
          <Image
            style={styles.image}
            defaultSource={useDefaultSource ? ImageResources.splash : undefined}
            source={ImageResources.splash}
            resizeMode={"contain"}
          />
        </View>
      );
    } else {
      return null;
    }
  }
}

const styles = styleSheetCreate({
    image: {
      alignSelf: "center",
      marginTop: windowHeight / 3.51052,
      width: windowWidth / 1.7391,
      height: windowHeight / 14.1914,
    } as ImageStyle,
});