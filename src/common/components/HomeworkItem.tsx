import React, { PureComponent } from "react"
import {
  Text,
  Image,
  ImageStyle,
  TextStyle,
  ViewStyle,
  View,
  TouchableOpacity
} from "react-native"
import { Colors, windowWidth } from '../../core/theme'
import { styleSheetCreate } from '../utils'
import { ImageResources } from '../../common/ImageResources.g'
import { Triangle } from '../../common/components/Triangle'
import { localization as l } from '../../common/localization/localization'

interface IProps {
  category: string,
  title: string,
  status: string,
  onPress(): void
  isFirstItem: boolean
  isLastItem: boolean
}

interface IState {

}

export class HomeworkItem extends PureComponent<IProps, IState> {

  render(): JSX.Element {

    const { category, title, status, isFirstItem, isLastItem, onPress } = this.props

    let statusText = l.common.notAvailable
    if (status === 'isDone') {
      statusText = l.common.done
    } else if (status === 'isSentReview') {
      statusText = l.common.sentForReview
    } else if (status === 'isInToDo') {
      statusText = l.common.toDo
    }

    let statusIcon = ImageResources.iconHomeworkAttention
    if (status === 'isDone') {
      statusIcon = ImageResources.iconHomeworkDone
    } else if (status === 'isSentReview') {
      statusIcon = ImageResources.iconHomeworkTime
    } else if (status === 'isInToDo') {
      statusIcon = ImageResources.iconHomeworkAttention
    }

    let statusLineStyle = styles.statusCircleBlue
    if (status !== 'isDone' && status !== 'isSentReview') {
      statusLineStyle = styles.statusCircleWhite
    }

    return (
      <View>
        <View style={styles.containerLineAndLesoonInfo}>
          <View style={styles.statusLineContainer}>
            <View style={styles.statusLine}>
              {
                isFirstItem
                  ? <View style={styles.emptySpace} />
                  : <View style={styles.statusLineTop} />
              }
              <View style={statusLineStyle} />
              {
                isLastItem
                  ? <View style={styles.emptySpace} />
                  : <View style={styles.statusLineBottom} />
              }
            </View>
          </View>
          <TouchableOpacity onPress={onPress}>
            <View style={styles.lesoonInfoContainer}>
              <View style={styles.lesoonInfoBox}>
                <Triangle status={status} />
                <View style={styles.contentBox}>
                  <View>
                    <Text style={styles.lessonName}>{category}</Text>
                    <Text style={styles.categoryText}>{title}</Text>
                  </View>
                  {
                    (status === '')
                      ? null
                      : <View style={styles.statusContainer}>
                          <Image
                            style={styles.statusIcon}
                            source={statusIcon}
                          />
                          <Text style={styles.statusText}>{statusText}</Text>
                        </View>
                  }
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = styleSheetCreate({
  containerLineAndLesoonInfo: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  } as ViewStyle,

  // -- lesoon info --//

  lesoonInfoContainer: {
    paddingVertical: windowWidth * .022,
  } as ViewStyle,
  lesoonInfoBox: {
    width: windowWidth * .8,
    height: windowWidth * .334,
    backgroundColor: Colors.white,
    borderRadius: windowWidth * .02,
    overflow: 'hidden',
  } as ViewStyle,
  contentBox: {
    paddingTop: windowWidth * .048,
    paddingHorizontal: windowWidth * .062,
  } as ViewStyle,
  lessonName: {
    color: Colors.dark5E,
    fontSize: windowWidth * .039,
    fontWeight: '400',
  } as TextStyle,
  categoryText: {
    color: Colors.blueEC,
    fontSize: windowWidth * .037,
    paddingTop: windowWidth * .021,
  } as TextStyle,
  statusContainer: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    paddingTop: windowWidth * .053,
  } as ViewStyle,
  statusIcon: {
    width: windowWidth * .056,
    height: windowWidth * .09,
  } as ImageStyle,
  statusText: {
    color: Colors.darkGrey9F,
    fontSize: windowWidth * .037,
    paddingLeft: windowWidth * .032,
  } as TextStyle,

  // -- satus line --//

  emptySpace: {
    height: windowWidth * .167 + windowWidth * .022,
  },
  statusLineContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  } as ViewStyle,
  statusLine: {
  } as ViewStyle,
  statusLineTop: {
    width: windowWidth * .01,
    height: windowWidth * .167 + windowWidth * .022,
    backgroundColor: Colors.greyCE,
    alignSelf: 'center',
  } as ViewStyle,
  statusLineBottom: {
    width: windowWidth * .01,
    height: windowWidth * .167 + windowWidth * .022,
    backgroundColor: Colors.greyCE,
    alignSelf: 'center',
  } as ViewStyle,
  statusCircleBlue: {
    width: windowWidth * .062,
    height: windowWidth * .062,
    borderRadius: windowWidth * .062,
    zIndex: 2,
    alignSelf: 'center',
    position: 'absolute',
    top: windowWidth * .167 + windowWidth * .022 - windowWidth * .026,
    backgroundColor: Colors.blueEC,
  } as ViewStyle,
  statusCircleWhite: {
    width: windowWidth * .062,
    height: windowWidth * .062,
    borderRadius: windowWidth * .062,
    zIndex: 2,
    alignSelf: 'center',
    position: 'absolute',
    top: windowWidth * .167 + windowWidth * .022 - windowWidth * .03,
    backgroundColor: Colors.white,
    borderColor: Colors.greyCE,
    borderWidth: windowWidth * .01,
    borderStyle: 'solid',
  } as ViewStyle,
})