// noinspection JSUnusedGlobalSymbols | public api
// tslint:disable-next-line: promise-function-async
export function delayPromise<T>(millis: number, value?: T): Promise<T> {
    return new Promise((resolve): void => {
        setTimeout(() => {
            resolve(value);
        }, millis);
    });
}
