const progressStatus = (item: any): any => {
    return item.exercises.map((el: any, index: number): any => {
      if (item.state_ex && item.state_ex[2 + index] == 1) {
        if (item.state == 0) { item.state = 2; }

        return true;
      } else if (item.state_ex && item.state_ex[index + 1] == 1) {
        if (item.state == 0) { item.state = 2; }

        return false;
      } else {
        return false;
      }
    });
};

const getStatus = (item: number): string => {
    let status = "isInToDo";
    if (item == 1) {
      status = "isDone";
    } else if (item == 2) {
      status = "isSentReview";
    }

    return status;
};

const fillterLevel = (result: any, statusAssuumtion: any, level: any): any => {
    return result.filter((item: any) => {
        if (item.level == level) {
          if (item.state == 3 && item.state != 2 && statusAssuumtion) {
              statusAssuumtion = false;
              item.state = 0;

              return item;
          }
          if (item.state == 0 && statusAssuumtion) {
              statusAssuumtion = false;

              return item;
          } else if (item.state == 0 && !statusAssuumtion) {
              item.state = 3;
          }

          return item;
        }
    });
};

export {fillterLevel, getStatus, progressStatus};