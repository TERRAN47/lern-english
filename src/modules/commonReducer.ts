
import {reducerWithInitialState} from "typescript-fsa-reducers";
import {newState} from "../common/newState";
//import {CoreActions} from "../../core/store";
import {CommonActions} from "./commonActions";
import {CommonInitialState, CommonState} from "./commonState";
import { Profile } from '../core/api/generated/dto/CommonResponse.g'

function netStatus(state: CommonState, payload: boolean): CommonState {
  return newState(state, {netStatus: payload})
}

function saveProfile(state: CommonState, payload:Profile): CommonState {
  return newState(state, {user: payload})
}

function updateEssayStatus(state: CommonState, payload: any): CommonState {
  return newState(state, { sentEssayStatus: payload })
}

export const commonReducer = reducerWithInitialState(CommonInitialState)
  .case(CommonActions.netStatus, netStatus)
  .case(CommonActions.saveProvile, saveProfile)
  .case(CommonActions.updateEssayStatus, updateEssayStatus)