import {Dispatch} from "redux";
// import {AuthHelper} from "../../common/helpers/authHelper";
// import {localization} from "../../common/localization/localization";
import {SimpleThunk} from "../../common/simpleThunk";
import {HomeRequestDto} from "../../core/api/generated/dto/HomeRequestDto.g";
import {requestsRepository} from "../../core/api/requestsRepository";
import moment from 'moment'
import { homeActions } from './homeActions'

export class homeActionsAsync {
  static sendOnLessons(request: HomeRequestDto): SimpleThunk {
    return async (dispatch: Dispatch): Promise<void> => {
      try{
        dispatch(homeActions.sendLessons.started({isLoading: true}))
        const response = await requestsRepository.homeApiRequest.sendLessons(request)
        dispatch(homeActions.sendLessons.done({
          params:{isLoading: false}, result:response
        }))
      }catch(err){
        dispatch(homeActions.sendLessons.failed({params:request, error:err}))
      }
    }
  }

  static getCalendar(netStatus: boolean): any {
    return async (dispatch: Dispatch): Promise<void> => {
      try{
        dispatch(homeActions.getHomeCalendar.started({isLoading: true}))
        const response = await requestsRepository.homeApiRequest.getHomeCalendar(netStatus)

        let mapObject = Object.keys(response.user_calendar['276']).map((elem)=>{
          return response.user_calendar['276'][elem]
        })

        let homeWorkDate = moment(`${mapObject && mapObject[0]}`).format('YYYY-MM-DD')
        let homeWorkDay = moment(`${mapObject && mapObject[0]}`).format('DD')
        let homeWorkMonth = moment(`${mapObject && mapObject[0]}`).format('MMMM')
        let homeWorkTitle = mapObject ? mapObject[1] : ''
        let deadline = moment(homeWorkDate).add(14, 'days').format('DD-MM-YYYY')
        let dayWeek = moment(`${mapObject && mapObject[0]}`).format('dddd')
        
        let homeWork = {
          homeWorkDay,
          homeWorkMonth,
          homeWorkDate,
          homeWorkTitle,
          deadline,
          dayWeek
        }

        dispatch(homeActions.getHomeCalendar.done({
          params:{isLoading: false}, result:homeWork
        }))
      }catch(err){
        dispatch(homeActions.getHomeCalendar.failed({params:{}, error:err}))
      }
    };
  }

  static getStatistic(netStatus: boolean): any {
    return async (dispatch: Dispatch): Promise<void> => {
      try{
        dispatch(homeActions.getHomeStatistic.started({isLoading: true}))
        const response = await requestsRepository.homeApiRequest.getHomeStatistic(netStatus)

        dispatch(homeActions.getHomeStatistic.done({
          params:{isLoading: false}, result:response
        }))
      }catch(err){
        dispatch(homeActions.getHomeStatistic.failed({params:{}, error:err}))
      }
    };
  }
}