import { Home } from './Home'
import { OnlineLesson } from './onloneLessions'
import {HomeCalendar} from './HomeCalendar'
import { Statistics } from './Statistics'

export {Home, HomeCalendar, Statistics, OnlineLesson}