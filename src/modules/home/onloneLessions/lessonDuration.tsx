import React from "react"
import { BaseReduxComponent } from '../../../core/BaseComponent'
import {
  View,
  ViewStyle,
  Text,
  TouchableOpacity,
  TextStyle,
} from 'react-native'
import { Colors, windowWidth } from '../../../core/theme'
import { styleSheetCreate } from '../../../common/utils'
//import { localization as l } from '../../../common/localization/localization'
import ProgressCircle from 'react-native-progress-circle'

interface IStateProps {

}

interface IDispatchProps {

}

interface IState {

}

interface IProps {
  nextConten: ()=>void
  lessonData:{timeDuration:string}
}

export class LessonsDuration extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  nextPage(timeDuration:string):void{
    let {nextConten, lessonData} = this.props

    lessonData.timeDuration = timeDuration
    //@ts-ignore
    nextConten(2, .4, lessonData)
  }

  componentDidMount(){
    //alert(this.stateProps.lessonsDuration)
  }

  render(): JSX.Element {
    
    return (
      <View style={styles.circleBlock}>
        <View style={styles.circleChildren}>
          <ProgressCircle
            percent={75}
            radius={windowWidth * .153}
            borderWidth={windowWidth * .024}
            color={Colors.blueEC}
            shadowColor="#d9e0f4"
            bgColor={Colors.white}
          >
            <TouchableOpacity onPress={this.nextPage.bind(this, '45 мин')} hitSlop={styles.tachBlock}>
              <Text style={styles.textInCircle}>{`45 \n мин`}</Text>
            </TouchableOpacity>
          </ProgressCircle>
        </View>
        <View style={styles.circleChildren}>
          <ProgressCircle
            percent={100}
            radius={windowWidth * .153}
            borderWidth={windowWidth * .024}
            color={Colors.blueEC}
            shadowColor="#d9e0f4"
            bgColor={Colors.white}
          >
            <TouchableOpacity onPress={this.nextPage.bind(this, '60 мин')} hitSlop={styles.tachBlock}>
              <Text style={styles.textInCircle}>{`60 \n мин`}</Text>
            </TouchableOpacity>
          </ProgressCircle>
        </View>
      </View>
    )
  }
}

const styles = styleSheetCreate({
  circleBlock:{
    flexDirection: 'row',
    justifyContent:'center',
    alignItems:'center'
  } as ViewStyle,
  tachBlock:{
    top: windowWidth * .04,
    left: windowWidth * .04,
    right: windowWidth * .04,
    bottom: windowWidth * .04,
  },
  circleChildren:{
    margin: windowWidth * .06,
  } as ViewStyle,
  textInCircle:{
    fontSize: windowWidth * .044,
    textAlign:'center'
  } as TextStyle,
})