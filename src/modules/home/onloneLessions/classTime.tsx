import React from "react"
import { BaseReduxComponent } from '../../../core/BaseComponent'
import {
  View,
  ViewStyle,
  Text,
  TouchableOpacity,
  Image,
  ImageStyle,
} from 'react-native'
import { 
//Colors, 
  windowWidth,
 } from '../../../core/theme'
import { PlainHeader } from '../../../common/components/Headers'
import { ImageResources } from '../../../common/ImageResources.g'
import { styleSheetCreate } from '../../../common/utils'
import { localization as l } from '../../../common/localization/localization'

interface IStateProps {

}

interface IDispatchProps {

}

interface IState {

}

interface IProps {
  nextConten: ()=>void
  lessonData:{classTime:string}
}

export class ClassTime extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  static navigationOptions = PlainHeader(l.common.onlineLesson, true, false, true, true)
  constructor(props: any) {
    super(props);
  
    this.state = {

    };
  }

  nextPage(day:string):void{
    let {nextConten, lessonData} = this.props
    lessonData.classTime = day
    //@ts-ignore
    nextConten(4, .8, lessonData)
  }
  
  render(): JSX.Element {
    return (
        <View style={styles.container}>
          <View style={styles.dayContent}>
            <TouchableOpacity onPress={this.nextPage.bind(this, 'Утро')} style={styles.dayBlock}>
              <Image
                style={styles.imageDay}
                source={ImageResources.morningIcon}
              />
              <Text>Утро</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.nextPage.bind(this, 'День')} style={styles.dayBlock}>
              <Image
                style={styles.imageDay}
                source={ImageResources.dayIcon}
              />
              <Text>День</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.dayContent}>
            <TouchableOpacity onPress={this.nextPage.bind(this, 'Вечер')} style={styles.dayBlock}>
              <Image
                style={styles.imageDay}
                source={ImageResources.eveningIcon}
              />
              <Text>Вечер</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.nextPage.bind(this, 'Ночь')} style={styles.dayBlock}>
              <Image
                style={styles.imageDay}
                source={ImageResources.nightIcon}
              />
              <Text>Ночь</Text>
            </TouchableOpacity>
          </View>
          <View>
            
          </View>
        </View>
    )
  }
}

const styles = styleSheetCreate({
  container:{
    //justifyContent:'center'
  } as ViewStyle,
  dayBlock:{
    alignItems:'center',
    margin: windowWidth * .06,
  } as ViewStyle,
  dayContent:{
    alignItems:'center',
    flexDirection:'row',
    justifyContent:'center'
  } as ViewStyle,
  imageDay:{
    width: windowWidth * .3,
    height:windowWidth * .3,
  } as ImageStyle,
})