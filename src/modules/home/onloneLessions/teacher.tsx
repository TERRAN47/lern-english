import React from "react"
import { BaseReduxComponent } from '../../../core/BaseComponent'
import {
  View,
  ViewStyle,
  Text,
  TouchableOpacity,
  Image,
  ImageStyle,
  //TextStyle,
} from 'react-native'
import { Colors, windowWidth } from '../../../core/theme'
import { ImageResources } from '../../../common/ImageResources.g'
import { styleSheetCreate } from '../../../common/utils'
//import { localization as l } from '../../../common/localization/localization'

interface IStateProps {

}

interface IDispatchProps {

}

interface IState {

}

interface IProps {
  nextConten: any,
  lessonData:{teacher:string}
}

export class Teacher extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {
  
  nextPage(teacher:string): any{

    let {nextConten, lessonData} = this.props
    lessonData.teacher = teacher
    nextConten(5, 1, lessonData)

  }
  
  render(): JSX.Element {
    return (
      <View style={styles.dayContent}>
        <TouchableOpacity  onPress={this.nextPage.bind(this, 'Носитель')}  style={styles.dayBlock}>
          <View style={styles.imageBlock}>
            <Image
              style={styles.imageDay}
              source={ImageResources.enFlagIcon}
            />
          </View>
          <Text style={styles.lengText}>Носитель</Text>
        </TouchableOpacity>
        <TouchableOpacity  onPress={this.nextPage.bind(this, 'Русский')} style={styles.dayBlock}>
          <View style={styles.imageBlock}>
            <Image
              style={styles.imageDay}
              source={ImageResources.ruFlagIcon}
            />
          </View>
          <Text style={styles.lengText}>Русский</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = styleSheetCreate({
  container:{
    //justifyContent:'center'
  } as ViewStyle,
  dayContent:{
    alignItems:'center',
    justifyContent:'center',
    flexDirection:'row',
  } as ViewStyle,
  dayBlock:{
    alignItems:'center',
    margin: windowWidth * .06,
  } as ViewStyle,
  lengText:{
    color:Colors.black59,
    marginTop: windowWidth * .03,
  } as ViewStyle,
  imageBlock:{
    width: windowWidth * .3,
    height: windowWidth * .3,
    alignItems:'center',
    justifyContent: 'center',
    borderRadius: windowWidth * .15,
    backgroundColor: Colors.white,
  } as ViewStyle,
  imageDay:{
    width: windowWidth * .1,
    height: windowWidth * .1,
  } as ImageStyle,
})