import React from "react";
import { BaseReduxComponent } from "../../../core/BaseComponent";
import {
  View,
  ViewStyle,
  TouchableOpacity,
  Text,
  Platform,
  Modal,
  TextStyle,
  ScrollView,
  Alert,
  Keyboard,
} from "react-native";
import { Colors, windowWidth } from "../../../core/theme";
import { PlainHeader } from "../../../common/components/Headers";
import { styleSheetCreate } from "../../../common/utils";
import { localization as l } from "../../../common/localization/localization";
import * as Progress from "react-native-progress";
import { LessonsDuration } from "./lessonDuration";
import { SelectDays } from "./selectDays";
import { ClassTime } from "./classTime";
import { Teacher } from "./teacher";
import { ApplicationReady } from "./applicationReady";
import { Button } from "../../../common/components/Button";
import { connectAdv } from "../../../core/store/connectAdv";
import { Dispatch } from "redux";
import { homeActionsAsync } from "../homeActionsAsync";
import { homeActions } from "../homeActions";
import {IAppState, INavigationState} from "../../../core/store/appState";
import {getBackAction} from "../../../navigation/navigation";
import { LoadingView } from "../../../common/components/LoadingView";

interface IStateProps {
  resultLessons: any;
  error: any;
  isLoading: boolean;
  navigation: INavigationState;
  netStatus: boolean;
}

interface IDispatchProps {
  sendInLessons(lessonData: {}, comment: string): any;
  clearState(): any;
  goBack(backAction: any): any;
}

interface IState {
  keyboardIsOpen: boolean;
  contentPage: number;
  isModalVisible: boolean;
  progressBar: number;
  lessonData: {timeDuration: string, desiredDays: string, classTime: string, teacher: string};
  titleList: {1: string, 2: string, 3: string, 4: string, 5: string};
  daysWeek: [
    {day: string, status: boolean},
    {day: string, status: boolean},
    {day: string, status: boolean},
    {day: string, status: boolean},
    {day: string, status: boolean},
    {day: string, status: boolean},
    {day: string, status: boolean}
  ];
  comment: string;
}

interface IProps {

}

@connectAdv(
  (state: IAppState): IStateProps => ({
      resultLessons: state.home.resultLessons,
      error: state.home.error,
      netStatus: state.common.netStatus,
      isLoading: state.home.isLoading,
      navigation: state.navigation
  }),
  (dispatch: Dispatch): IDispatchProps => ({
    sendInLessons(lessonData, comment): void {
      //@ts-ignore
      return dispatch(homeActionsAsync.sendOnLessons({lessonData, comment})); 
    },
    goBack(backAction): void {
      dispatch(backAction);
    },
    clearState(): void {
      dispatch(homeActions.clearState());
    }
  })
)
export class OnlineLesson extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  keyboardDidShowListener: any;
  keyboardDidHideListener: any;

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      "keyboardDidShow",
      this.keyboardDidShow,
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      "keyboardDidHide",
      this.keyboardDidHide,
    );
  }

  keyboardDidShow = (): void => {
    this.setState({ keyboardIsOpen: true });
  }

  keyboardDidHide = (): void => {
    this.setState({ keyboardIsOpen: false });
  }

  static navigationOptions = PlainHeader(l.common.onlineLesson, true, false, true, true);
  constructor(props: any) {
    super(props);
    
    this.state = {
      keyboardIsOpen: false,
      contentPage: 1,
      progressBar: .2,
      lessonData: { 
        timeDuration: "", 
        desiredDays: "", 
        classTime: "", 
        teacher: "",
      },
      isModalVisible:false,
      titleList:{
        1: "Выберите продолжительность урока",
        2: "Выберите желаемые дни",
        3: "Удобное время занятий",
        4: "С каким преподавателем Вы хотите заниматься?",
        5: "Ваша заявка почти готова",
      },
      comment:"",
      daysWeek:[
        {day:"ПН", status:false},
        {day:"ВТ", status:false}, 
        {day:"СР", status:false},
        {day:"ЧТ", status:false},
        {day:"ПТ", status:false},
        {day:"СБ", status:false},
        {day:"ВС", status:false},
      ],
    };
    
    //@ts-ignore
    this.content = {
      //@ts-ignore
      1:<LessonsDuration lessonData={this.state.lessonData} nextConten={this.nextConten} />,
      //@ts-ignore
      2:<SelectDays selDays={this.selectDays} daysWeek={this.state.daysWeek} />,
      //@ts-ignore
      3:<ClassTime lessonData={this.state.lessonData} nextConten={this.nextConten} />,
      //@ts-ignore
      4:<Teacher lessonData={this.state.lessonData} nextConten={this.nextConten} />,
      5:<ApplicationReady changeComment={this.changeComment} />
    };
  }

  private openModal = (): void => {
    this.setState({ isModalVisible: true });
  };

  private closeModal = (): void => {
    this.dispatchProps.clearState();
    this.setState({ isModalVisible: false });
    const backAction = getBackAction(this.stateProps.navigation);
    if (backAction != null) {
     this.dispatchProps.goBack(backAction);
    }
  }

  private selectDays = (daysWeek: any): void => {
    this.setState({daysWeek});
  }

  private nextConten = (page: number, progressBar: number, lessonData: any): void => {

    this.setState({contentPage:page, progressBar, lessonData});
  }
  
  private changeComment = (): void =>{

  }

  private formWeekDate = (): void =>{
    let {lessonData, daysWeek} = this.state;
    let desiredDays = "";
    let seleact = false;
    daysWeek.forEach((elem)=>{
      if(elem.status){
        seleact = true;
        desiredDays = `${desiredDays && desiredDays} ${elem.day},`;
      }
    });
    if(seleact){
      lessonData.desiredDays = desiredDays;
      this.nextConten(3, .6, lessonData);
    }else{
      Alert.alert("Внимание!", "Вы не выбрали дни недели");
    }
  }
 
  async sendLession(){
    let {lessonData, comment} = this.state;
    if(this.stateProps.netStatus){
      await this.dispatchProps.sendInLessons(lessonData, comment);

      let {resultLessons} = this.stateProps;
      
      if(resultLessons){
        this.openModal();
      }
    }else{
      Alert.alert("Внимание!", "Нет подключения к интернету");
    }
  }

  render(): JSX.Element {
    let {contentPage, isModalVisible, titleList, lessonData, progressBar} = this.state;
    let {isLoading} = this.stateProps;

    return (
      <View style={styles.container}>
        <ScrollView contentContainerStyle={styles.bgStyle}>
          <LoadingView isLoading={isLoading} />
          <View style={styles.head}>
            <Text style={styles.textHeadLeft}>Заявка на онлайн урок</Text>
            
            <Text style={styles.textHeadRight}>шаг {contentPage} из 5</Text>
          </View>
          <View style={styles.lessonContent}>
            {
              lessonData.teacher ?
              <Text>{`${lessonData.timeDuration}, [ ${lessonData.desiredDays} ] ${lessonData.classTime}, ${lessonData.teacher}`}</Text>
              : null
            }
          
            <Text style={styles.textContent}>{
              //@ts-ignore
              titleList[contentPage]
            }</Text>
            {
              contentPage === 2 ?
                <Text style={styles.textChild}>(один или несколько дней)</Text>
              :
                null
            }
            <View style={styles.progressBar}>
              <Progress.Bar 
                borderRadius={1} 
                height={14} 
                progress={progressBar}
                color={Colors.blueEC} 
                width={windowWidth * .8} 
              />
            </View>
            {
              //@ts-ignore
              this.content[contentPage]
            }
            {
              contentPage === 2 ?
                <View style={styles.nextButton}>
                  <TouchableOpacity onPress={this.formWeekDate} >
                    <Button titleButton="Далее" />
                  </TouchableOpacity>
                </View>
              :
                contentPage === 5 ?
                  <View style={styles.sendButton}>
                    <TouchableOpacity onPress={this.sendLession.bind(this)} >
                      <Button titleButton="Отправить" />
                    </TouchableOpacity>
                  </View>
                :
                  null
            }
            <Modal
              animationType="slide"
              transparent={false}
              visible={isModalVisible}
            >
              <View style={styles.modalContainer}>
                <View style={styles.modalContent}>
                  <Text style={styles.modalTitleText}>{l.common.successAplication}</Text>
                  <Text style={styles.modalContentText}>{l.empty.aplicationReqest}</Text>
                  <View style={styles.sendButton}>
                  <TouchableOpacity onPress={this.closeModal} >
                    <Button titleButton={l.common.understandable} />
                  </TouchableOpacity>
                  </View>
                </View>
              </View>
            </Modal>
            {
              this.state.keyboardIsOpen
              ? <View style={styles.emptySpace}/>
              : null
            }
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = styleSheetCreate({
  bgStyle: {
    flexGrow: 1,
    paddingBottom: windowWidth * .04,
    justifyContent: "space-around",
  } as ViewStyle,
  lessonContent: {
    flex:1,
    zIndex:50,
    alignItems:"center",
    //marginTop:-100,
    paddingTop: windowWidth * .03,
    justifyContent:"center"
  } as ViewStyle,
  modalContentText: {
    marginTop: windowWidth * .06,
    color: Colors.dark5E,
    fontSize: windowWidth * .039,
    lineHeight: windowWidth * .075,
  } as TextStyle,
  modalContainer: {
    alignItems:"center",
    flex:1,
    padding: windowWidth * .03,
    backgroundColor:Colors.blueEC,
    justifyContent:"center"
  } as ViewStyle,
  modalTitleText:{
    fontWeight: "500",
    color: Colors.dark5E,
    fontSize: windowWidth * .048,
    textAlign:"center",
    lineHeight: windowWidth * .08,
  } as TextStyle,
  modalContent:{
    alignItems: "center",
    padding: windowWidth * .08,
    width: "100%",
    borderRadius: windowWidth * .03,
    ...Platform.select({
      ios: {
          shadowRadius: 15,
          shadowOpacity: 0.3,
          shadowOffset: {width: 1, height: 4},
      },
      android: {
        elevation: 8
    }}),
    backgroundColor: Colors.white,
    justifyContent: "center"
  } as ViewStyle,
  nextButton: {
    position: "absolute",
    bottom: windowWidth * .05,
    width: "100%",
    alignItems:"center",
    left:0
  } as ViewStyle,
  sendButton: {
    width: "100%",
    marginTop:  windowWidth * .06,
    alignItems:"center",
  } as ViewStyle,
  progressBar: {
    marginVertical: windowWidth * .06,

  } as ViewStyle,
  container: {
    flex: 1,
    backgroundColor: Colors.greyLight,
  } as ViewStyle,

  textContent:{
    fontSize: windowWidth * .039,
    color:Colors.dark5E,
    textAlign:"center"
  } as TextStyle,
  textChild:{
    fontSize: windowWidth * .029,

  } as TextStyle,
  head:{
    zIndex:100,
    backgroundColor: Colors.white,
    flexDirection: "row",
    justifyContent:"space-between",
    padding: windowWidth * .03,
  } as ViewStyle,
  textHeadLeft:{
    color:Colors.black59
  } as TextStyle,
  textHeadRight:{
    color:Colors.blueEC
  } as TextStyle,
  emptySpace: {
    height: windowWidth * .75,
  } as ViewStyle,
});