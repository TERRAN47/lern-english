import React from "react"
import { BaseReduxComponent } from '../../../core/BaseComponent'
import {
  View,
  ViewStyle,
  Text,
  TouchableOpacity,
  Platform,
  //Image,
  //ImageStyle,
  TextStyle,
  ScrollView,
} from 'react-native'
import { Colors, windowWidth } from '../../../core/theme'
import { styleSheetCreate } from '../../../common/utils'
//import { localization as l } from '../../../common/localization/localization'

interface IStateProps {

}

interface IDispatchProps {

}

interface IState {
  rend:boolean
}

interface IProps {
  daysWeek: [
    {day:string, status:boolean},
    {day:string, status:boolean},
    {day:string, status:boolean},
    {day:string, status:boolean},
    {day:string, status:boolean},
    {day:string, status:boolean},
    {day:string, status:boolean}
  ]
  selDays: (daysWeek:any)=>void
}

export class SelectDays extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  constructor(props: any) {
    super(props);
  
    this.state = {
      rend:false
    };
  }

  selectDay(index:number, status:boolean):void{
    let {daysWeek, selDays} = this.props

    let {rend} = this.state
    daysWeek[index].status = !status

    selDays(daysWeek)
    this.setState({rend:!rend})
  }

  render(): JSX.Element {
    let {daysWeek} = this.props

    return (
      <ScrollView contentContainerStyle={styles.bgStyle}>
        <View style={styles.container}>
            {
              daysWeek.map((item:{day:string, status:boolean}, index:number)=>{
                return(
                  <TouchableOpacity 
                    onPress={this.selectDay.bind(this, index, item.status)} 
                    style={item.status ? [styles.dayItem, {backgroundColor:Colors.blueEC}] : styles.dayItem} 
                    key={index}
                  >
                    <Text style={item.status ? [styles.dayText, {color:Colors.white}] : styles.dayText}>
                    {item.day}
                    </Text>
                  </TouchableOpacity>
                )
              })
            }
        </View>
      </ScrollView>
    )
  }
}

const styles = styleSheetCreate({
  bgStyle: {
    flexGrow: 1,
    paddingBottom: windowWidth * .04,
    justifyContent: 'space-between',
  } as ViewStyle, 
  container:{
    flexDirection: 'row',
    paddingHorizontal: windowWidth * .04,
    justifyContent: 'center',
    flexWrap: 'wrap',
  } as ViewStyle,
  dayItem:{
    padding: windowWidth * .04,
    margin: windowWidth * .02,
    backgroundColor:Colors.white,
    borderRadius: windowWidth * .01,
    ...Platform.select({
      ios: {
          shadowRadius: 5,
          shadowOpacity: 0.3,
          shadowOffset: {width: 1, height: 4},
      },
      android: {
        elevation: 8
      }}),
  } as ViewStyle,
  dayText:{
    fontSize: windowWidth * .039,
    fontWeight:'bold'
  } as TextStyle,
})