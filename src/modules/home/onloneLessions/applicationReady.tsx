import React from "react"
import { BaseReduxComponent } from '../../../core/BaseComponent'
import {
  View,
  ViewStyle,
  Text,
  TextStyle,
} from 'react-native'
import { Colors } from '../../../core/theme'
import { PlainHeader } from '../../../common/components/Headers'
import { styleSheetCreate } from '../../../common/utils'
import { localization as l } from '../../../common/localization/localization'

import { InputLogin } from '../../../common/components/InputLogin'

interface IStateProps {

}

interface IDispatchProps {

}

interface IState {

}

interface IProps {
  changeComment:(comment:string)=> void 
}

export class ApplicationReady extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  static navigationOptions = PlainHeader(l.common.onlineLesson, true, false, true, true)

  private changeText = (comment:string): void => {
    let {changeComment} = this.props
    changeComment(comment)
  }

  render(): JSX.Element {

    return (
      <View style={styles.container}>
        <Text style={styles.textAplication}>{`Перед отправкой Вы можете добавить \n комментарий или особые пожелания`}</Text>
          <InputLogin
            placeholder={"Hint text"}
            secureTextEntry={false}
            changeText={this.changeText}
          />
      </View>
    )
  }
}

const styles = styleSheetCreate({
  container:{
    alignItems:'center'
  } as ViewStyle,
  textAplication:{
    color:Colors.darkGrey9F,
    fontSize:15
  } as TextStyle,
  
})