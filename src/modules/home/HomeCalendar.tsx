import React from "react"
import { BaseReduxComponent } from '../../core/BaseComponent'
import {
  View,
  ViewStyle,
  FlatList,
  ListRenderItemInfo,
  ScrollView,
} from 'react-native'
import { PlainHeader } from '../../common/components/Headers'
import { Calendar } from 'react-native-calendars'
import { styleSheetCreate } from "../../common/utils"
import { Colors, windowWidth } from "../../core/theme"
import { localization as l } from '../../common/localization/localization'
import { CalendarHomeWorkBox } from '../../common/components/CalendarHomeWorkBox'
import { CalendarHomeworkDescription } from '../../common/components/CalendarHomeworkDescription'
import { connectAdv } from '../../core/store/connectAdv'
import { Dispatch } from 'redux'
import {IAppState} from "../../core/store/appState";
import { homeActionsAsync } from './homeActionsAsync'
import {LocaleConfig} from 'react-native-calendars';
import moment from 'moment'

LocaleConfig.locales['ru'] = {
  monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
  monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн','Июл','Авг','Сен','Окт','Ноб','Дек'],
  dayNames: ['Воскресенье','Понидельник','Вторник','Среда','Четверг','Пятница','Суббота'],
  dayNamesShort: ['Вос.','Пон','Втр','Срд','Чет','Пят','Суб']
}

LocaleConfig.defaultLocale = 'ru';

interface IStateProps {
  resultCalendar:  any | null;
  error:any
  isLoading:boolean
  netStatus:boolean
}

interface IDispatchProps {
  getCalendar(netStatus:boolean):any
}

interface IState {
  isShow: boolean
  title: string
  realDay: string
  listDays: {}
  homeworkItems:[{
    id:string,
    day:string,
    month:string,
    title:string,
    deadline:string,
    onPress():void
  }]
}

interface IProps {
  day: string,
  month: string,
  title: string,
  deadline: string,
}

interface ICalendarHomeworkList {
  id: string,
  day: string,
  month: string,
  title: string,
  deadline: string,
  onPress(title: string, deadline: string): void,
}

interface ListItem extends ListRenderItemInfo<ICalendarHomeworkList> {
  item: ICalendarHomeworkList
}

@connectAdv(
  (state: IAppState): IStateProps => ({
      resultCalendar: state.home.resultHomeCalendar,
      netStatus: state.common.netStatus,
      error: state.home.error,
      isLoading: state.home.isLoading
  }),
  (dispatch: Dispatch): IDispatchProps => ({
    getCalendar(netStatus): void {
      dispatch(homeActionsAsync.getCalendar(netStatus))
    }
  })
)
export class HomeCalendar extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  static navigationOptions = PlainHeader(l.common.calendar, true, false, true, true)

  constructor(props: any) {
    super(props)
    this.state = {
      isShow: false,
      title: '',
      realDay:'',
      listDays:{},
      homeworkItems:[
        {
          id: '1',
          day: '',
          month: '',
          title: '',
          deadline: `до`,
          onPress: ()=>{},
        },
      ]
    }
  }

  componentDidMount(){
    let {netStatus, resultCalendar} = this.stateProps

    this.dispatchProps.getCalendar(netStatus)
    let realDay = moment().format('YYYY-MM-DD')

    let homeworkItems = [
      {
        id: '1',
        day: resultCalendar ? resultCalendar.homeWorkDay : '',
        month: resultCalendar ? resultCalendar.homeWorkMonth : '',
        title: resultCalendar ? resultCalendar.homeWorkTitle : '',
        deadline: `до ${resultCalendar ? resultCalendar.deadline : '' }`,
        onPress: this.showDescription,
      },
    ]

    let listDays = {[realDay]: {selected: true, marked: false}}

    homeworkItems.forEach((el, index)=>{

      listDays = {...listDays, [resultCalendar ? resultCalendar.homeWorkDate : '']: {selected: false, marked: true}}

    })
    console.log(6, listDays)
    //@ts-ignore
    this.setState({realDay, homeworkItems, listDays})
  }

  private showDescription = (title: string, deadline: string): void => {
    this.setState({
      isShow: true,
      title
     })
  }

  private closeDescription = (): void => this.setState({ isShow: false });

  keyExtractor = (item: ICalendarHomeworkList): string => item.id

  renderItem = ({ item, index }: ListItem): JSX.Element => {

    const { day, month, title, deadline, onPress } = item

    return (
      <CalendarHomeWorkBox
        day={day}
        month={month}
        title={title}
        deadline={deadline}
        onPress={onPress}
      />
    )
  }

  render(): JSX.Element {
    let {resultCalendar} = this.stateProps

    const { isShow, homeworkItems, listDays, title, realDay } = this.state
    console.log(123, listDays, realDay, resultCalendar)

    return (
      <View style={styles.container}>
        <ScrollView style={styles.bgStyle}>
            <View style={styles.calendarBox}>
              <Calendar
                firstDay={1}
                minDate={realDay}
                theme={{
                    arrowColor: Colors.blueEC,
                    todayTextColor: Colors.blueEC,
                    selectedDayBackgroundColor: Colors.blueEC,
                }}
                markedDates={listDays}
                style={styles.calendarMain}
              />
            </View>
            {
              resultCalendar ?
              <View style={styles.homeworkList} >
                <FlatList
                  data={homeworkItems}
                  renderItem={this.renderItem}
                  keyExtractor={this.keyExtractor}
                  horizontal
                />
              </View>
              : null
            }
        </ScrollView>
        {
          resultCalendar ?
            <CalendarHomeworkDescription
              date={`${resultCalendar.dayWeek}, ${resultCalendar.homeWorkDay} ${resultCalendar.homeWorkMonth}`}
              title={title}
              deadline={resultCalendar.deadline ? resultCalendar.deadline : ''}
              isShow={isShow}
              closeDescription={this.closeDescription}
            />
          :
            null
        }
      </View>
    )
  }
}

const styles = styleSheetCreate({
  container: {
    flex: 1,
  } as ViewStyle,
  bgStyle: {
    backgroundColor: Colors.blueEC,
  } as ViewStyle,
  calendarBox: {
    paddingTop: windowWidth * .0365,
    paddingHorizontal: windowWidth * .043,
  } as ViewStyle,
  calendarMain: {
    borderRadius: windowWidth * .02,
    width: windowWidth * .906,
  } as ViewStyle,
  homeworkList: {
    paddingLeft: windowWidth * .043,
    paddingBottom: windowWidth * .0365,
  } as ViewStyle,
})