import React from 'react'
import { BaseReduxComponent } from '../../core/BaseComponent'
import {
  View,
  ViewStyle,
  Image,
  ImageStyle,
  FlatList,
  ListRenderItemInfo,
  ScrollView,
} from 'react-native'
import { Dispatch } from 'redux'
import { NavigationActions } from '../../navigation/navigation'
import { connectAdv } from '../../core/store/connectAdv'
import { Colors, windowWidth } from '../../core/theme'
import { ImageResources } from '../../common/ImageResources.g'
import { styleSheetCreate } from '../../common/utils'
import { localization as l } from '../../common/localization/localization'
import { BigHeader } from '../../common/components/Headers'
import { MenuBlockButton } from '../../common/components/MenuBlockButton'
//import RNFS from 'react-native-fs';

interface IStateProps {

}

interface IDispatchProps {
  navigateToOnlineLesson(): void
  navigateToHomeCalendar(): void
  navigateToStatistics(): void
  navigateToTraining(): void
  navigateToMyScreen(): void
  navigateToHomeWork(): void
}

interface IState {

}

interface IMenuButtonListItem {
  buttonText: string,
  buttonImage: ImageResources,
  onPress(): void,
}

interface ListItem extends ListRenderItemInfo<IMenuButtonListItem> {
  item: IMenuButtonListItem
}

interface IProps {

}

@connectAdv(
  null,
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToOnlineLesson(): void {
      dispatch(NavigationActions.navigateToOnlineLesson())
    },
    navigateToHomeCalendar(): void {
      dispatch(NavigationActions.navigateToHomeCalendar())
    },
    navigateToStatistics(): void {
      dispatch(NavigationActions.navigateToStatistics())
    },
    navigateToTraining(): void {
      dispatch(NavigationActions.navigateToTraining())
    },
    navigateToMyScreen(): void {
      dispatch(NavigationActions.navigateToMyScreen())
    },
    navigateToHomeWork(): void {
      dispatch(NavigationActions.navigateToHomeWork())
    },
  })
)
export class Home extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  static navigationOptions = BigHeader("Домой", true, true, true, true, true)

  menuButton: IMenuButtonListItem[] = [
    {
      buttonImage: ImageResources.iconMyTasks,
      buttonText: l.common.myTasks,
      onPress: this.dispatchProps.navigateToHomeWork
    },
    {
      buttonImage: ImageResources.iconTraining,
      buttonText: l.common.training,
      onPress: this.dispatchProps.navigateToTraining
    },
    {
      buttonImage: ImageResources.iconStatistics,
      buttonText: l.common.statistics,
      onPress: this.dispatchProps.navigateToStatistics 
    },
    {
      buttonImage: ImageResources.iconCalendar,
      buttonText: l.common.calendar,
      onPress: this.dispatchProps.navigateToHomeCalendar
    },
    {
      buttonImage: ImageResources.iconBalance,
      buttonText: l.common.balance,
      onPress: () => { alert('На данный момент раздел в разработке') }
    },
    {
      buttonImage: ImageResources.iconOnlineLesson,
      buttonText: l.common.onlineLesson,
      onPress: this.dispatchProps.navigateToOnlineLesson
    }
  ]
  async componentDidMount(){
    // On Android, use "RNFS.DocumentDirectoryPath" (MainBundlePath is not defined)
    // await RNFS.readDir(RNFS.DocumentDirectoryPath).then((result) => {
    //   console.log('GOT RESULT', result);

    //   if(result[0].name !== 'ReactNativeDevBundle.js'){
    //     return Promise.all([RNFS.stat(result[0].path), result[0].path]);
    //   }else{
    //     return Promise.all([RNFS.stat(result[1].path), result[1].path]);
    //   }
    // }).then((statResult) => {
    //   if (statResult[0].isFile()) {
    //     return RNFS.readFile(statResult[1]);
    //   }
    //   return 'no file';
    // }).then((contents) => {
    //   console.log(33, JSON.parse(contents));
    // }).catch((err) => {
    //   console.log(err.message, err.code);
    // });
  };
  

  keyExtractor = (item: IMenuButtonListItem): string => item.buttonText

  renderItem = ({ item: {buttonImage, buttonText, onPress} }: ListItem): JSX.Element => (
    <MenuBlockButton
      buttonText={buttonText}
      buttonImage={buttonImage}
      onPress={onPress}
    />
  )

  render(): JSX.Element {

    return (
      <ScrollView bounces={false} contentContainerStyle={styles.container}>
        <Image
          style={styles.imageBackground}
          source={ImageResources.backgroundApp}
        />
        <View style={styles.buttonsContainer}>
          <FlatList
            bounces={false}
            numColumns={2}
            data={this.menuButton}
            renderItem={this.renderItem}
            keyExtractor={this.keyExtractor}
          />
        </View>
      </ScrollView>
    )
  }
}

const styles = styleSheetCreate({
  container: {
    flexGrow: 1,
    justifyContent: 'center',
    zIndex: 3,
    backgroundColor: Colors.greyE5,
  } as ViewStyle,
  buttonsContainer: {
    zIndex: 2,
    justifyContent: 'center',
    alignItems: 'center',    
  } as ViewStyle,
  imageBackground: {
    height: windowWidth * 0.5,
    width: windowWidth * 1.05,
    position: 'absolute',
    alignSelf: 'center',
    bottom: 0,
    zIndex: -1,
  } as ImageStyle,
})
