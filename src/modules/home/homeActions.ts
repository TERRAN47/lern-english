import {actionCreator} from '../../core/store'
//import {ErrorSource} from "./authState";
//import {SignInRequestDto} from "../../core/api/generated/dto/SignInRequest.g";
//import {Profile} from "../../core/api/generated/dto/Profile.g";

export class homeActions {
  static sendLessons = actionCreator.async<IEmpty, any, Error>("home/SEND_ONLESSONS")
  static clearState = actionCreator('home/CLEAR_STATE')
  static getHomeCalendar = actionCreator.async<IEmpty, any, Error>('home/UPDATE_HOME_CALENDAR')
  static getHomeStatistic = actionCreator.async<IEmpty, any, Error>('home/UPDATE_HOME_STATISTIC')
}