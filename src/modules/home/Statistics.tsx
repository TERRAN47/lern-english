import React from "react"
import { BaseReduxComponent } from '../../core/BaseComponent'
import {
  View,
  ViewStyle,
  Text,
  TextStyle,
  ScrollView,
  //Image,
  //ImageStyle,
} from 'react-native'
import { Colors, windowWidth } from '../../core/theme'
import { PlainHeader } from '../../common/components/Headers'
//import { ImageResources } from '../../common/ImageResources.g'
import { styleSheetCreate } from '../../common/utils'
//import { localization as l } from '../../common/localization/localization'
//import { LineChart } from 'react-native-chart-kit'
import LinearGradient from 'react-native-linear-gradient';
import { connectAdv } from '../../core/store/connectAdv'
import { Dispatch } from 'redux'
import {IAppState} from "../../core/store/appState";
import { homeActionsAsync } from './homeActionsAsync'

interface IStateProps {
  resultStatistic: {user_graph:[], user_stat:any} | null;
  error:any
  isLoading:boolean
  netStatus:boolean
}

interface IDispatchProps {
  getStatistic(netStatus:boolean):void
}

interface IState {

}

interface IProps {

}

@connectAdv(
  (state: IAppState): IStateProps => ({
      resultStatistic: state.home.resultStatistic,
      error: state.home.error,
      netStatus: state.common.netStatus,
      isLoading: state.home.isLoading
  }),
  (dispatch: Dispatch): IDispatchProps => ({
    getStatistic(netStatus): void {
      dispatch(homeActionsAsync.getStatistic(netStatus))
    }
  })
)
export class Statistics extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  static navigationOptions = PlainHeader("Статистика", true, false, true, true)

  componentDidMount(){
    let {netStatus} = this.stateProps
    this.dispatchProps.getStatistic(netStatus)
  }

  render(): JSX.Element {
    let {resultStatistic, error, isLoading} = this.stateProps

    console.log(67, resultStatistic, isLoading, error)
    return (
      resultStatistic && resultStatistic.user_graph && resultStatistic.user_graph.length > 0 ?
        <ScrollView>
          {/* <LineChart
            data={{
              labels: ['Янв'],//'Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн'
              datasets: [{
                data: [
                  ...resultStatistic.user_graph.map((elem:any)=>{
                    return elem.points
                  })
                ]
                // [
                //   30,
                //   75,
                //   65
                //   // Math.random() * 100,
                //   // Math.random() * 100,
                //   // Math.random() * 100,
                //   // Math.random() * 100,
                //   // 90
                // ]
              }]
            }}
            fromZero={true}
            withDots={false}
            width={windowWidth}
            height={300}
            chartConfig={{
              backgroundColor: Colors.blueEC,
              backgroundGradientFrom: Colors.blueEC,
              backgroundGradientTo: Colors.blueEC,
              decimalPlaces: 0,
              color: (opacity = 1) => `#fff`, //rgba(255, 255, 255, ${opacity})
              style: {
                borderRadius: 16,
                fontWeight:'bold'
              }
            }}
            bezier
          /> */}
          <View style={styles.container}>
            <Text style={styles.titleText}>Результаты</Text>
            {
              resultStatistic && resultStatistic.user_stat && resultStatistic.user_stat['27'] > -1 ?
                <View style={styles.statBlock}>
                  <View style={styles.titleBlock}>
                    <Text style={styles.statText}>Фонетика</Text>
                    <Text style={styles.statText}>{Math.round(resultStatistic.user_stat['27'])}%</Text>
                  </View>
                  <View style={styles.linearGrBlock}>
                    <LinearGradient
                      start={{ x: 0, y: 0 }}
                      end={{ x: 1, y: 0 }}
                      colors={['#FEC860', '#E85A5A']}
                      style={[styles.linearGradient, {width: windowWidth * (Math.round(resultStatistic.user_stat['27'])/100)}]}
                    >
                    </LinearGradient>
                  </View>
                </View>
              :
                null
            }
            {
              resultStatistic && resultStatistic.user_stat && resultStatistic.user_stat['28'] > -1 ?
                <View style={styles.statBlock}>
                  <View style={styles.titleBlock}>
                    <Text style={styles.statText}>Аудирование</Text>
                    <Text style={styles.statText}>{Math.round(resultStatistic.user_stat['28'])}%</Text>
                  </View>
                  <View style={styles.linearGrBlock}>
                    <LinearGradient
                      start={{ x: 0, y: 0 }}
                      end={{ x: 1, y: 0 }}
                      colors={['#B474E7', '#FC7A7A']}
                      style={[styles.linearGradient, {width: windowWidth * (Math.round(resultStatistic.user_stat['28'])/100)}]}
                    >
                    </LinearGradient>
                  </View>
                </View>
              :
                null
            }

            {
              resultStatistic && resultStatistic.user_stat && resultStatistic.user_stat['215'] > -1 ?
                <View style={styles.statBlock}>
                  <View style={styles.titleBlock}>
                    <Text style={styles.statText}>Лексика</Text>
                    <Text style={styles.statText}>{Math.round(resultStatistic.user_stat['215'])}%</Text>
                  </View>
                  <View style={styles.linearGrBlock}>
                    <LinearGradient
                      start={{ x: 0, y: 0 }}
                      end={{ x: 1, y: 0 }}
                      colors={['#9262DF', '#FBAEF7']}
                      style={[styles.linearGradient, {width: windowWidth * (Math.round(resultStatistic.user_stat['215'])/100)}]}
                    >
                    </LinearGradient>
                  </View>
                </View>
              :
                null
            }

            {
              resultStatistic && resultStatistic.user_stat && resultStatistic.user_stat['30'] > -1 ?
                <View style={styles.statBlock}>
                  <View style={styles.titleBlock}>
                    <Text style={styles.statText}>Грамматика</Text>
                    <Text style={styles.statText}>{Math.round(resultStatistic.user_stat['30'])}%</Text>
                  </View>
                  <View style={styles.linearGrBlock}>
                    <LinearGradient
                      start={{ x: 0, y: 0 }}
                      end={{ x: 1, y: 0 }}
                      colors={['#5157F3', '#6EDFF7']}
                      style={[styles.linearGradient, {width: windowWidth * (Math.round(resultStatistic.user_stat['30'])/100)}]}
                    >
                    </LinearGradient>
                  </View>
                </View>
              :
                null
            }

            {
              resultStatistic && resultStatistic.user_stat && resultStatistic.user_stat['31'] > -1 ?
                <View style={styles.statBlock}>
                  <View style={styles.titleBlock}>
                    <Text style={styles.statText}>Чтение</Text>
                    <Text style={styles.statText}>{Math.round(resultStatistic.user_stat['31'])}%</Text>
                  </View>
                  <View style={styles.linearGrBlock}>
                    <LinearGradient
                      start={{ x: 0, y: 0 }}
                      end={{ x: 1, y: 0 }}
                      colors={['#FF7060', '#FFCB7C']}
                      style={[styles.linearGradient, {width: windowWidth * (Math.round(resultStatistic.user_stat['31'])/100)}]}
                    >
                    </LinearGradient>
                  </View>
                </View>
              :
                null
            }

            {
              resultStatistic && resultStatistic.user_stat && resultStatistic.user_stat['32'] > -1 ?
                <View style={styles.statBlock}>
                  <View style={styles.titleBlock}>
                    <Text style={styles.statText}>Письмо</Text>
                    <Text style={styles.statText}>{Math.round(resultStatistic.user_stat['32'])}%</Text>
                  </View>
                  <View style={styles.linearGrBlock}>
                    <LinearGradient
                      start={{ x: 0, y: 0 }}
                      end={{ x: 1, y: 0 }}
                      colors={['#2BCA61', '#C0ED60']}
                      style={[styles.linearGradient, {width: windowWidth * (Math.round(resultStatistic.user_stat['32'])/100)}]}
                    >
                    </LinearGradient>
                  </View>
                </View>
              :
                null
            }
          </View>
        </ScrollView>
      :
        <View style={styles.noSatutic}>
          <Text style={styles.statText}>Статистики нет</Text>
        </View>
    )
  }
}

const styles = styleSheetCreate({
  container: {
    flex: 1,
    paddingTop: 20,
    paddingHorizontal: 15,
    paddingBottom: 40,
    backgroundColor: Colors.white,
  } as ViewStyle,
  noSatutic: {
    padding:15,
    flex:1,
    justifyContent: 'center',
    alignItems:  'center',
  } as ViewStyle,
  statBlock:{
    flex: 1,
    marginTop: 30,
    minHeight: windowWidth * .05,
  } as ViewStyle,
  titleBlock: {
    flexDirection: 'row',
    justifyContent:'space-between',
    paddingVertical: 10,
  } as ViewStyle,
  linearGrBlock: {
    width: windowWidth - 30,
    height: 10,
    backgroundColor:Colors.greyE5,
    borderRadius: windowWidth * .025
  } as ViewStyle,
  linearGradient: {
    width:1,
    height: 10,
    borderRadius: windowWidth * .025,
    alignItems: 'center',
    justifyContent: 'center',
  } as ViewStyle,
  titleText: {
    fontSize:15,
    fontWeight:'bold'
  } as TextStyle,
  statText:{
    fontSize: windowWidth * .039,
  } as TextStyle,
})