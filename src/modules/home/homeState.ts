export interface HomeState {
  isLoading: boolean;
  resultLessons: {} | null;
  error: {} | string | null;
  resultHomeCalendar: any //{string, string, string, string, string} | null;
  resultStatistic:{user_graph:[], user_stat:any} | null;
}

export const HomeInitialState: HomeState = {
  isLoading: false,
  resultLessons: null,
  resultStatistic:null,
  resultHomeCalendar:null,
  error: null,
};
