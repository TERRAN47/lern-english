import {Failure, Success} from "typescript-fsa";
import {reducerWithInitialState} from "typescript-fsa-reducers";
import {newState} from "../../common/newState";
//import {CoreActions} from "../../core/store";
import {homeActions} from "./homeActions";
import {HomeInitialState, HomeState} from "./homeState";

function clearState(state: HomeState): HomeState {
  return newState(state, {resultLessons:null, error: null});
}

//Get Statistic 
function getStatisticStarted(state: HomeState): HomeState {
  return newState(state, {
    isLoading: true,
    error: null
  })
}
function getStatisticDone(state: HomeState, payload: Success<any, any>): HomeState {
  return newState(state, {
    isLoading: false,
    resultStatistic:payload.result,
    error: null
  })
}
function getStatisticFailed(state: HomeState, failed: Failure<any, Error>): HomeState {
  return newState(state, {
    isLoading: false,
    resultStatistic:null,
    error: failed.error,
  })
}

//Get Calendar 
function getCalendarStarted(state: HomeState): HomeState {
  return newState(state, {
    isLoading: true,
    error: null
  })
}
function getCalendarDone(state: HomeState, payload: Success<any, any>): HomeState {
  return newState(state, {
    isLoading: false,
    resultHomeCalendar:payload.result,
    error: null
  })
}
function getCalendarFailed(state: HomeState, failed: Failure<any, Error>): HomeState {
  return newState(state, {
    isLoading: false,
    resultHomeCalendar:null,
    error: failed.error,
  })
}

//Post Online Lessons
function sendLessonsStarted(state: HomeState): HomeState {
  return newState(state, {
    isLoading: true,
    resultLessons:null,
    error: null
  })
}
function sendLessonsDone(state: HomeState, payload: Success<any, any>): HomeState {
  return newState(state, {
    isLoading: false,
    resultLessons:payload.result,
    error: null
  })
}
function sendLessonsFailed(state: HomeState, failed: Failure<any, Error>): HomeState {
  return newState(state, {
    isLoading: false,
    resultLessons:null,
    error: failed.error.message,
  })
}

export const homeReducer = reducerWithInitialState(HomeInitialState)
  .case(homeActions.clearState, clearState)

  .case(homeActions.getHomeStatistic.started, getStatisticStarted)
  .case(homeActions.getHomeStatistic.done, getStatisticDone)
  .case(homeActions.getHomeStatistic.failed, getStatisticFailed)
  
  .case(homeActions.getHomeCalendar.started, getCalendarStarted)
  .case(homeActions.getHomeCalendar.done, getCalendarDone)
  .case(homeActions.getHomeCalendar.failed, getCalendarFailed)

  .case(homeActions.sendLessons.started, sendLessonsStarted)
  .case(homeActions.sendLessons.done, sendLessonsDone)
  .case(homeActions.sendLessons.failed, sendLessonsFailed)
