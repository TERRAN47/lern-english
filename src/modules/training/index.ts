import { Training } from './Training'
import { VocabularyInstr } from './vocabulary/VocabularyInstr'
import { VocabularyEssay } from './vocabulary/VocabularyEssay'
import { VocabularyLevels } from './vocabulary/VocabularyLevels'
import { VocabularyTopic } from './vocabulary/VocabularyTopic'
import { VocabularyTesting } from './vocabulary/VocabularyTesting'
import { VocabularyAssumptionInstr } from './vocabulary/VocabularyAssumptionInstr'
import { VocabularyAssumption1 } from './vocabulary/VocabularyAssumption1'
import { PhoneticsInstruction } from './phonetics/PhoneticsInstruction'
import { AssumptionInstr } from './phonetics/AssumptionInstr'
import { PhoneticAssumption1 } from './phonetics/PhoneticAssumption1'
import { PhoneticLevels } from './phonetics/PhoneticLevels'
import { PhoneticAssumption2 } from './phonetics/PhoneticAssumption2'
import { PhoneticTopic } from './phonetics/PhoneticTopic'
import { PhoneticTheory } from './phonetics/PhoneticTheory'
import { PhoneticTesting } from './phonetics/PhoneticTesting'
import { TestingSuccessfulFinish } from './TestingSuccessfulFinish'
import { PhoneticTraningInstr } from './phonetics/PhoneticTraningInstr'
import { GrammarInstruction } from './grammar/GrammarInstruction'
import { GrammarLevels } from './grammar/GrammarLevels'
import { GrammarTopic } from './grammar/GrammarTopic'
import { GrammarTheory } from './grammar/GrammarTheory'
import { GrammarAssumptionInstr } from './grammar/GrammarAssumptionInstr'
import { GrammarAssumption } from './grammar/GrammarAssumption'
import { AuditionInstruction } from './audition/AuditionInstruction'
import { AuditionLevels } from './audition/AuditionLevels'
import { AuditionTopic } from './audition/AuditionTopic'
import { AuditionAssumptionInstr } from './audition/AuditionAssumptionInstr'
import { AuditionRecording } from './audition/AuditionRecording'
import { AuditionTesting1 } from './audition/AuditionTesting1'
import { AuditionTesting2 } from './audition/AuditionTesting2'
import { ReadingEssayInstr } from './reading/ReadingEssayInstr'
import { ReadingText } from './reading/ReadingText'
import { ReadingEssay } from './reading/ReadingEssay'
import { ReadingLevels } from './reading/ReadingLevels'
import { ReadingEssayFeedback } from './reading/ReadingEssayFeedback'
import { WritingMain } from './writing/WritingMain'
import { WritingEssay } from './writing/WritingEssay'
import { WritingEssayDone } from './writing/WritingEssayDone'
import { WritingEssayFeedback } from './writing/WritingEssayFeedback'
import { WritingEssayNoFeedback } from './writing/WritingEssayNoFeedback'
import { Dictionary } from './dictionary/Dictionary'
import { DictionaryWordDetail } from './dictionary/DictionaryWordDetail'
import { DictionaryTraining } from './dictionary/DictionaryTraining'
import { DictionaryTesting } from './dictionary/DictionaryTesting'
import { DictionaryAddWord } from './dictionary/DictionaryAddWord'

export { 
  VocabularyInstr, 
  VocabularyEssay, 
  VocabularyLevels,  
  VocabularyTopic,
  VocabularyTesting,
  VocabularyAssumptionInstr,
  VocabularyAssumption1,

  PhoneticTopic, 
  PhoneticTesting, 
  PhoneticTheory, 
  PhoneticAssumption1, 
  PhoneticAssumption2, 
  PhoneticsInstruction, 
  PhoneticTraningInstr,

  GrammarInstruction,
  GrammarLevels,
  GrammarTopic,
  GrammarTheory,
  GrammarAssumptionInstr,
  GrammarAssumption,

  AuditionInstruction,
  AuditionLevels,
  AuditionTopic,
  AuditionAssumptionInstr,
  AuditionRecording,
  AuditionTesting1,
  AuditionTesting2,

  ReadingEssayInstr,
  ReadingText,
  ReadingEssay,
  ReadingLevels,
  ReadingEssayFeedback,

  WritingMain,
  WritingEssay,
  WritingEssayDone,
  WritingEssayFeedback,
  WritingEssayNoFeedback,

  Dictionary,
  DictionaryWordDetail,
  DictionaryTraining,
  DictionaryTesting,
  DictionaryAddWord,

  AssumptionInstr, 
  Training, 
  TestingSuccessfulFinish,
  PhoneticLevels,
}
