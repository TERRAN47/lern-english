export interface ITrainingState {
  isLoading: boolean;
  phoneticsResult: any; // [] | {} | null;
  error: {} | string | null;
  selectAssumption: {} | any;
  grammarResult: {} | null;
  vocabularyResult: any;
  readingResult: any;
  sendAssumption: any;
  auditionResult: any;
  selectLevel: any;
}

export const TrainingInitialState: ITrainingState = {
  isLoading: false,
  grammarResult: null,
  phoneticsResult: null,
  vocabularyResult: null,
  auditionResult: null,
  readingResult: null,
  error: null,
  selectAssumption: null,
  sendAssumption: null,
  selectLevel: null,
};
