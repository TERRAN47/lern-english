import React from "react";
import { BaseReduxComponent } from "../../core/BaseComponent";
import {
  View,
  ViewStyle,
  Image,
  ImageStyle,
  ListRenderItemInfo,
  FlatList,
  ScrollView,
} from "react-native";
import { Dispatch } from "redux";
import { connectAdv } from "../../core/store/connectAdv";
import { NavigationActions } from "../../navigation/navigation";
import { Colors, windowWidth } from "../../core/theme";
import { ImageResources } from "../../common/ImageResources.g";
import { styleSheetCreate } from "../../common/utils";
import { localization as l } from "../../common/localization/localization";
import { PlainHeader } from "../../common/components/Headers";
import { MenuBlockButton } from "../../common/components/MenuBlockButton";
import { IAppState } from "../../core/store/appState";
import { IEssayStatus } from "../../core/api/generated/dto/CommonResponse.g";

interface IStateProps {
  sentEssayStatus: IEssayStatus;
}

interface IDispatchProps {
  navigateToPhoneticsInstr(): void;
  navigateToVocabularyInstr(): void;
  navigateToAuditionInstruction(): void;

  navigateToGrammarInstruction(): void;
  navigateToReadingEssayInstr(essayStatus: boolean): void;
  navigateToWritingMain(): void;
}

interface IState {
  readingLink: any;
}

interface IProps {

}

interface IMenuButtonListItem {
  buttonText: string;
  buttonImage: ImageResources;
  onPress(): void;
}

interface IListItem extends ListRenderItemInfo<IMenuButtonListItem> {
  item: IMenuButtonListItem;
}

@connectAdv(
  (state: IAppState): IStateProps => ({
    sentEssayStatus: state.common.sentEssayStatus,
  }),
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToPhoneticsInstr(): void {

      dispatch(NavigationActions.navigateToPhoneticsInstr());
    },
    navigateToVocabularyInstr(): void {
      dispatch(NavigationActions.navigateToVocabularyInstr());
    },
    navigateToAuditionInstruction(): void {
      dispatch(NavigationActions.navigateToAuditionInstruction());
    },
    navigateToGrammarInstruction(): void {
      dispatch(NavigationActions.navigateToGrammarInstruction());
    },
    navigateToReadingEssayInstr(essayStatus: boolean): void {
      if (essayStatus) {
        dispatch(NavigationActions.navigateToReadingLevels());
      } else {
        dispatch(NavigationActions.navigateToReadingEssayInstr());
      }
    },
    navigateToWritingMain(): void {
      dispatch(NavigationActions.navigateToWritingMain());
    },
  })
)

export class Training extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  static navigationOptions = PlainHeader("Тренировка", true, true);

  array: any = [];

  readingLink = this.dispatchProps.navigateToReadingEssayInstr;

  navigateToReadingEssayInstr = (sentEssayReading: IEssayStatus): void => {
    this.dispatchProps.navigateToReadingEssayInstr(sentEssayReading.sentEssayReading);
  }

  menuButton = (sentEssayStatus: IEssayStatus): any => {

    return[
      {
        buttonImage: ImageResources.iconLexis,
        buttonText: l.common.lexis,
        onPress: this.dispatchProps.navigateToVocabularyInstr,
      },
      {
        buttonImage: ImageResources.iconListening,
        buttonText: l.common.listening,
        onPress: this.dispatchProps.navigateToAuditionInstruction,
      },
      {
        buttonImage: ImageResources.iconPhonetics,
        buttonText: l.common.phonetics,
        onPress: this.dispatchProps.navigateToPhoneticsInstr,
      },
      {
        buttonImage: ImageResources.iconGrammar,
        buttonText: l.common.grammar,
        onPress: this.dispatchProps.navigateToGrammarInstruction,
      },
      {
        buttonImage: ImageResources.iconReading,
        buttonText: l.common.reading,
        onPress: this.navigateToReadingEssayInstr.bind(this, sentEssayStatus)
      },
      {
        buttonImage: ImageResources.iconWriting,
        buttonText: l.common.writing,
        onPress: this.dispatchProps.navigateToWritingMain
      }
    ];
}

  keyExtractor = (item: IMenuButtonListItem): string => item.buttonText;

  renderItem = ({ item, index }: IListItem): JSX.Element => {
    const { buttonImage, buttonText, onPress } = item;

    return (
      <MenuBlockButton
        buttonText={buttonText}
        buttonImage={buttonImage}
        onPress={onPress}
      />
    );
  }

  render(): JSX.Element {
    const { sentEssayStatus } = this.stateProps;
    this.array = this.menuButton(sentEssayStatus);

    return (
      <ScrollView bounces={false} contentContainerStyle={styles.container}>
        <View style={styles.container}>
          <Image
            style={styles.imageBackground}
            source={ImageResources.backgroundApp}
          />
          <View style={styles.buttonsContainer}>
            <FlatList
              bounces={false}
              numColumns={2}
              data={this.array}
              renderItem={this.renderItem}
              keyExtractor={this.keyExtractor}
            />
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = styleSheetCreate({
  container: {
    flexGrow: 1,
    justifyContent: "center",
    zIndex: 3,
    backgroundColor: Colors.greyE5,
  } as ViewStyle,
  buttonsContainer: {
    zIndex: 2,
    justifyContent: "center",
    alignItems: "center",
  } as ViewStyle,
  imageBackground: {
    height: windowWidth * 0.5,
    width: windowWidth * 1.05,
    position: "absolute",
    alignSelf: "center",
    bottom: 0,
    zIndex: -1,
  } as ImageStyle,
});