import React from "react";
import {
  View,
  ViewStyle,
  Text,
  TextStyle,
  TouchableOpacity,
  //Platform,
  //Animated,
  //Easing,
  Image,
  ImageStyle,
} from "react-native";
import { Dispatch } from "redux";
import { BaseReduxComponent } from "../../core/BaseComponent";
import { connectAdv } from "../../core/store/connectAdv";
import { Colors, windowWidth } from "../../core/theme";
import { styleSheetCreate } from "../../common/utils";
import { NavigationActions } from "../../navigation/navigation";
import { ImageResources } from "../../common/ImageResources.g";
import { PlainHeader } from "../../common/components/Headers";
import { ButtonNoBg } from "../../common/components/ButtonNoBg";
//import LinearGradient from 'react-native-linear-gradient'

interface IStateProps {

}

interface IDispatchProps {
  navigateToAssumptionInstr (): void;
}

interface IState {

}

interface IProps {
  category: string;
  title: string;
}

@connectAdv(
  null,
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToAssumptionInstr(): void {
      dispatch(NavigationActions.navigateToAssumptionInstr(1));
    },
  })
)
export class TestingSuccessfulFinish extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  static navigationOptions = PlainHeader("Успешный финиш", true, false);

  render(): JSX.Element {
//TODO: перенести в компоненты
    return (
      <View  style={styles.container}>
        <Image
          // tslint:disable-next-line: jsx-alignment
          style={styles.cup}
          source={ImageResources.finishCup}
        />
        <View style={styles.contentText}>
          <Text style={styles.title}>Поздравляем!</Text>
          <Text  style={styles.text}>Вы успешно успешно прошли тестирование без ошибок</Text>
        </View>
        <TouchableOpacity>
          <ButtonNoBg titleButton={"Продолжить обучение"} width={windowWidth * .9}/>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = styleSheetCreate({
  container: {
    flex: 1,
    justifyContent: "space-around",
    alignItems: "center",
    backgroundColor: Colors.blueEC,
    paddingTop: windowWidth * .1,
  } as ViewStyle,
  cup: {
    height: windowWidth * .52,
    width: windowWidth * .54,
  } as ImageStyle,
  contentText: {
    alignItems: "center",
    paddingHorizontal: windowWidth * .1,
  } as ViewStyle,
  title: {
    fontSize: windowWidth * .063,
    fontWeight: "bold",
    color: Colors.white,
  } as TextStyle,
  text: {
    paddingTop: windowWidth * .047,
    paddingHorizontal: windowWidth * .1,
    fontSize: windowWidth * .044,
    textAlign: "center",
    color: Colors.white,
  } as TextStyle,
});
