import { Failure, Success } from "typescript-fsa";
import { reducerWithInitialState } from "typescript-fsa-reducers";
import { newState } from "../../common/newState";
//import {CoreActions} from "../../core/store";
import { TrainingActions } from "./trainingActions";
import { TrainingInitialState, ITrainingState } from "./trainingState";
// import {CommonState} from "../../modules/commonState"

// import { CommonActions } from '../commonActions'

function clearState(state: ITrainingState): ITrainingState {
  return newState(state, {isLoading: false, error: null, selectAssumption: null});
}

function selectAssumption(state: ITrainingState, payload: any): ITrainingState {
  return newState(state, {selectAssumption: payload});
}

function updateGrammar(state: ITrainingState, payload: any): ITrainingState {
  return newState(state, {grammarResult: payload});
}

function updateAudition(state: ITrainingState, payload: any): ITrainingState {
  return newState(state, {auditionResult: payload});
}

function updateReading(state: ITrainingState, payload: any): ITrainingState {
  return newState(state, {readingResult: payload});
}

function selectLevel(state: ITrainingState, payload: any): ITrainingState {
  return newState(state, {selectLevel: payload});
}
// function addNewWordPost(state: CommonState, payload: any): CommonState {
//   //return newState(state, {addNewWordPost: payload});
// }

//ФОНЕТИКА
function getPhoneticStarted(state: ITrainingState): ITrainingState {
  return newState(state, {
    isLoading: true,
    error: null
  });
}
function getPhoneticDone(state: ITrainingState, payload: Success<any, any>): ITrainingState {
  return newState(state, {
    isLoading: false,
    phoneticsResult: payload.result,
    error: null
  });
}
function getPhoneticFailed(state: ITrainingState, failed: Failure<any, Error>): ITrainingState {
  return newState(state, {
    isLoading: false,
    phoneticsResult: null,
    error: failed.error,
  });
}
//ЛЕКСИКА
function getVocabularyStarted(state: ITrainingState): ITrainingState {
  return newState(state, {
    isLoading: true,
    error: null
  });
}
function getVocabularyDone(state: ITrainingState, payload: Success<any, any>): ITrainingState {
  return newState(state, {
    isLoading: false,
    vocabularyResult: payload.result,
    error: null
  });
}
function getVocabularyFailed(state: ITrainingState, failed: Failure<any, Error>): ITrainingState {
  return newState(state, {
    isLoading: false,
    vocabularyResult: null,
    error: failed.error,
  });
}

//ГРАММАТИКА
function getGrammarStarted(state: ITrainingState): ITrainingState {
  return newState(state, {
    isLoading: true,
    error: null
  });
}
function getGrammarDone(state: ITrainingState, payload: Success<any, any>): ITrainingState {
  return newState(state, {
    isLoading: false,
    grammarResult: payload.result,
    error: null
});
}

function getGrammarFailed(state: ITrainingState, failed: Failure<any, Error>): ITrainingState {
  return newState(state, {
    isLoading: false,
    grammarResult: null,
  });
}

//ЧТЕНИЕ
function getReadingStarted(state: ITrainingState): ITrainingState {
  return newState(state, {
    isLoading: true,
    error: null
  });
}
function getReadingDone(state: ITrainingState, payload: Success<any, any>): ITrainingState {
  return newState(state, {
    isLoading: false,
    readingResult: payload.result,
    error: null
  });
}
function getReadingFailed(state: ITrainingState, failed: Failure<any, Error>): ITrainingState {
  return newState(state, {
    isLoading: false,
    readingResult: null,
    error: failed.error,
  });
}

//АУДИРОВАНИЕ
function getAuditionStarted(state: ITrainingState): ITrainingState {
  return newState(state, {
    isLoading: true,
    error: null
  });
}
function getAuditionDone(state: ITrainingState, payload: Success<any, any>): ITrainingState {
  return newState(state, {
    isLoading: false,
    auditionResult: payload.result,
    error: null
  });
}
function getAuditionFailed(state: ITrainingState, failed: Failure<any, Error>): ITrainingState {
  return newState(state, {
    isLoading: false,
    auditionResult: null,
    error: failed.error,
  });
}


export const trainingReducer = reducerWithInitialState(TrainingInitialState)

  .case(TrainingActions.selectAssumption, selectAssumption)

  .case(TrainingActions.selectLevel, selectLevel)

  .case(TrainingActions.updateGrammar, updateGrammar)
  .case(TrainingActions.updateAudition, updateAudition)
  .case(TrainingActions.updateReading, updateReading)
 // .case(trainingActions.addNewWordPost, addNewWordPost)
  .case(TrainingActions.clearState, clearState)

  .case(TrainingActions.getPhonetics.started, getPhoneticStarted)
  .case(TrainingActions.getPhonetics.done, getPhoneticDone)
  .case(TrainingActions.getPhonetics.failed, getPhoneticFailed)

  .case(TrainingActions.getVocabularys.started, getVocabularyStarted)
  .case(TrainingActions.getVocabularys.done, getVocabularyDone)
  .case(TrainingActions.getVocabularys.failed, getVocabularyFailed)

  .case(TrainingActions.getAuditions.started, getAuditionStarted)
  .case(TrainingActions.getAuditions.done, getAuditionDone)
  .case(TrainingActions.getAuditions.failed, getAuditionFailed)

  .case(TrainingActions.getGrammars.started, getGrammarStarted)
  .case(TrainingActions.getGrammars.done, getGrammarDone)
  .case(TrainingActions.getGrammars.failed, getGrammarFailed)

  .case(TrainingActions.getReading.started, getReadingStarted)
  .case(TrainingActions.getReading.done, getReadingDone)
  .case(TrainingActions.getReading.failed, getReadingFailed);

  // .case(trainingActions.sendAssumption.started, sendAssumptionStarted)
  // .case(trainingActions.sendAssumption.done, sendAssumptionDone)
  // .case(trainingActions.sendAssumption.failed, sendAssumptionFailed)
