import React from "react";
import {
  View,
  ViewStyle,
  Text,
  TextStyle,
  Platform,
  FlatList,
  ScrollView,

} from "react-native";
import { Dispatch } from "redux";
import _ from "underscore";
import { BaseReduxComponent } from "../../../core/BaseComponent";
import { NavigationActions } from "../../../navigation/navigation";
import { localization as l } from '../../../common/localization/localization'
import { connectAdv } from "../../../core/store/connectAdv";
import { Colors, windowWidth } from "../../../core/theme";
import { styleSheetCreate } from "../../../common/utils";
import {IAppState} from "../../../core/store/appState";
import { TrainingActions } from "../trainingActions";
import { PlainHeader } from "../../../common/components/Headers";
import { LoadingView } from "../../../common/components/LoadingView";
import { LevelItem } from "../../../common/components/LevelItem";

interface IStateProps {
  phoneticsResult: any;
  error: {} | string | null;
  isLoading: boolean;  
}

interface IDispatchProps {
  navigateToPhoneticTopic (exercises: any, dialog: string): void;
  selectAssumption(item: any): void;
  clearState(): void;
}

interface IState {
  isClose: boolean;
}

interface IProps {

}

@connectAdv(
  (state: IAppState): IStateProps => ({
    phoneticsResult: state.training.phoneticsResult,
    error: state.training.error,
    isLoading: state.training.isLoading,
  }),
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToPhoneticTopic(item): void {
      dispatch(NavigationActions.navigateToPhoneticTopic(item));
    },
    selectAssumption(item): void {
      dispatch(TrainingActions.selectAssumption(item));
    },
    clearState(): void {
      dispatch(TrainingActions.clearState());
    },
  })
)
export class PhoneticLevels extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  static navigationOptions = PlainHeader(l.training.phonetics, true, false);

  keyExtractor = (item: any): string => item.theme;

  getStatus(item: number) {
    let status = "isInToDo";
    if (item === 1) {
      status = "isDone";
    } else if (item === 2) {
      status = "isSentReview";
    }

    return status;
  }

  renderItem = (item: any): JSX.Element => (
    <LevelItem
      category={item.item.theme}
      status={this.getStatus(item.item.state)}
      data={item}
      //@ts-ignore
      onPress={this.dispatchProps.navigateToPhoneticTopic}
      progress={[
        {lesson: true},
        {lesson: true},
        {lesson: false},
        {lesson: false},
      ]}
    />
  )

  componentWillUnmount() {
    this.dispatchProps.clearState();
  }

  render(): JSX.Element {
    const {isLoading, phoneticsResult} = this.stateProps;
    console.log(66, phoneticsResult);

    return (
      <ScrollView style={styles.container} >
        <LoadingView isLoading={isLoading} />
        <View style={styles.content}>
          {
            phoneticsResult ?
              <FlatList
                scrollEnabled={false}
                data={phoneticsResult}
                renderItem={this.renderItem}
                keyExtractor={this.keyExtractor}
              />

            :
              phoneticsResult &&  phoneticsResult.length == 0 ?
                <View style={styles.noSatutic}>
                  <Text>l.training.notExercises</Text>
                </View>
              : null
          }
        </View>
      </ScrollView>
    );
  }
}

const styles = styleSheetCreate({
  container: {
    flex: 1,
    backgroundColor: Colors.greyE5,
  } as ViewStyle,
  noSatutic: {
    padding: 15,
    flex: 1,
    justifyContent: "center",
    alignItems:  "center",
  } as ViewStyle,
  content: {
    borderRadius: windowWidth * .03,
    overflow: "hidden",
    alignItems: "flex-start",
    backgroundColor: Colors.white,
    marginVertical: windowWidth * .07,
    marginHorizontal: windowWidth * .04,
    zIndex: -2,
    ...Platform.select({
      ios: {
          shadowRadius: 8,
          shadowOpacity: 0.1,
          shadowOffset: {width: 1, height: 4},
      },
      android: {
        elevation: 8
    }}),
  } as ViewStyle,
  levelsContainer: {
    padding: windowWidth * .05,
    justifyContent: "space-around",
  } as ViewStyle,
  activeTitle: {
    color: Colors.dark5E,
    fontSize: windowWidth * .039,
  } as TextStyle,
  notActiveTitile: {
    color: Colors.greyCE,
    fontSize: windowWidth * .039,
  } as TextStyle,
});
