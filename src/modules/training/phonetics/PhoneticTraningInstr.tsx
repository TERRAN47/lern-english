import React from "react"
import { BaseReduxComponent } from '../../../core/BaseComponent'
import {
  View,
  Text,
  TextStyle,
  TouchableOpacity,
  Image,
  ImageStyle,
  Platform,
  ViewStyle,
} from 'react-native'
import { Colors, windowWidth } from '../../../core/theme'
import { PlainHeader } from '../../../common/components/Headers'
import { styleSheetCreate } from '../../../common/utils'
import { localization as l } from '../../../common/localization/localization'
import { Button } from '../../../common/components/Button'
import { ImageResources } from '../../../common/ImageResources.g'
import { Dispatch } from 'redux'
import { NavigationActions } from '../../../navigation/navigation'
import { connectAdv } from '../../../core/store/connectAdv'
import {
  NavigationRoute,
  NavigationScreenProp,
} from 'react-navigation'

interface IStateProps {

}

interface IDispatchProps {
  navigateToPhoneticAssumption(): void
}

interface IState {

}

interface IProps {
  navigation: NavigationScreenProp<NavigationRoute<any>>
}

@connectAdv(
  null,
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToPhoneticAssumption(): void {
      dispatch(NavigationActions.navigateToPhoneticTesting())
    },
  })
)
export class PhoneticTraningInstr extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  static navigationOptions = PlainHeader(l.training.testing, true, false)

  render(): JSX.Element {
    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <Text style={styles.contentTitle}>{l.training.exerciseInstruction}</Text>
          <View style={styles.taskBlock}>
            <View style={styles.taskChild}>
              <Image
                style={styles.imageIcon}
                source={ImageResources.volumeIcon}
              />
              <Text style={styles.contentText}>{l.training.listenRecording}</Text>
            </View>
          </View>
          <TouchableOpacity  onPress={()=>this.dispatchProps.navigateToPhoneticAssumption()}>
            <Button titleButton={l.common.understandable} />
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = styleSheetCreate({
  container: {
    flexGrow: 1,
    justifyContent:'center',
    padding: windowWidth * .05,
    backgroundColor: Colors.greyE5,
  } as ViewStyle,
  taskBlock:{
    paddingVertical: windowWidth * .02,
  } as ViewStyle,
  taskChild:{
    flexDirection:'row',
    justifyContent:'flex-start',
    alignItems:'center',
    paddingVertical: windowWidth * .03,
  } as ViewStyle,
  content:{
    flex: 1,
    padding: windowWidth * .05,
    backgroundColor: Colors.white,
    alignItems:'center',
    justifyContent:'space-between',
    borderRadius: windowWidth * .05,
    ...Platform.select({
      ios: {
          shadowRadius: 8,
          shadowOpacity: 0.1,
          shadowOffset: {width: 1, height: 4},
      },
      android: {
        elevation: 8
    }}),
    } as ViewStyle,
    imageIcon:{
      width: windowWidth * .08,
      height: windowWidth * .08,
      marginRight: windowWidth * .05,
      resizeMode: 'contain',
    } as ImageStyle,
    contentText:{
    color: Colors.dark5E,
    fontSize: windowWidth * .039,
    lineHeight: windowWidth * .075,
  } as TextStyle,
  contentTitle:{
    fontWeight:'500',
    color: Colors.dark5E,
    fontSize: windowWidth * .046,
    padding: windowWidth * .07,
  } as TextStyle,
})
