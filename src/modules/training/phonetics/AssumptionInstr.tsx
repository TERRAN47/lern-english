import React from "react"
import { BaseReduxComponent } from '../../../core/BaseComponent'
import {
  View,
  Text,
  TextStyle,
  TouchableOpacity,
  Image,
  ImageStyle,
  Platform,
  ViewStyle,
  ScrollView,
} from 'react-native'
import { Colors, windowWidth } from '../../../core/theme'
import { PlainHeader } from '../../../common/components/Headers'
import { styleSheetCreate } from '../../../common/utils'
import { localization as l } from '../../../common/localization/localization'
import { Button } from '../../../common/components/Button'
import { ImageResources } from '../../../common/ImageResources.g'
import { Dispatch } from 'redux'
import { NavigationActions } from '../../../navigation/navigation'
import { connectAdv } from '../../../core/store/connectAdv'
import {
  NavigationRoute,
  NavigationScreenProp,
  NavigationScreenConfigProps,
  NavigationStackScreenOptions
} from 'react-navigation'

interface IStateProps {

}

interface IDispatchProps {
  navigateToPhoneticAssumption(assumptionNum:number): void
}

interface IState {

}

interface IProps {
  navigation: NavigationScreenProp<NavigationRoute<any>>
}

@connectAdv(
  null,
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToPhoneticAssumption(assumptionNum): void {
      if(assumptionNum === 1){
        dispatch(NavigationActions.navigateToPhoneticAssumption1())
      }else{
        dispatch(NavigationActions.navigateToPhoneticAssumption2())
      }
    },
  })
)
export class AssumptionInstr extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  static navigationOptions = ({ navigation }: NavigationScreenConfigProps): NavigationStackScreenOptions =>{
    return PlainHeader(`${l.training.exercises} ${navigation.state.params}`, true, false)
  }

  render(): JSX.Element {
    
    let {params} = this.props.navigation.state

    return (
      <ScrollView contentContainerStyle={styles.bgStyle}>
        <View style={styles.container}>
          <View style={styles.content}>
            <Text style={styles.contentTitle}>{l.training.exerciseInstruction}</Text>
            <View style={styles.taskBlock}>
              <View style={styles.taskChild}>
                <Image
                  style={styles.imageIcon}
                  source={ImageResources.volumeIcon}
                />
                <Text style={styles.contentText}>1. {l.training.listenSpeaker}</Text>
              </View>
              <View style={styles.taskChild}>
                <Image
                  style={styles.imageIcon}
                  source={ImageResources.microIcon}
                />
                <Text style={styles.contentText}>2. {l.training.pressRecord}</Text>
              </View>
              <View style={styles.taskChild}>
                <Image
                  style={styles.imageIcon}
                  source={ImageResources.volumeIcon}
                />
                <Text style={styles.contentText}>3. {l.training.listenYourRecording}</Text>
              </View>
            </View>
            <TouchableOpacity  onPress={()=>this.dispatchProps.navigateToPhoneticAssumption(params)}>
              <Button titleButton={l.common.understandable} />
            </TouchableOpacity>
          </View>          
        </View>
      </ScrollView>
    )
  }
}

const styles = styleSheetCreate({
  bgStyle: {
    flexGrow: 1,
    justifyContent: 'center',
    backgroundColor: Colors.greyE5,
  } as ViewStyle,
  container: {
    flexGrow: 1,
    justifyContent:'center',
    padding: windowWidth * .05,
  } as ViewStyle,
  taskBlock:{
    paddingVertical: windowWidth * .02,
  } as ViewStyle,
  taskChild:{
    flexDirection:'row',
    justifyContent:'flex-start',
    alignItems:'center',
    paddingVertical: windowWidth * .03,
  } as ViewStyle,
  imageIcon:{
    width: windowWidth * .08,
    height: windowWidth * .08,
    marginRight: windowWidth * .05,
    resizeMode: 'contain',
  } as ImageStyle,
  contentText:{
    color: Colors.dark5E,
    fontSize: windowWidth * .039,
    lineHeight: windowWidth * .075,
  } as TextStyle,
  contentTitle:{
    fontWeight:'500',
    color: Colors.dark5E,
    fontSize: windowWidth * .046,
    padding: windowWidth * .07,
  } as TextStyle,
  content:{
    padding: windowWidth * .05,
    backgroundColor: Colors.white,
    alignItems:'center',
    justifyContent:'space-between',
    borderRadius: windowWidth * .05,
    ...Platform.select({
      ios: {
          shadowRadius: 8,
          shadowOpacity: 0.1,
          shadowOffset: {width: 1, height: 4},
      },
      android: {
        elevation: 8
    }}),
  } as ViewStyle,
})
