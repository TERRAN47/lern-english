import React from "react"
import { BaseReduxComponent } from "../../../core/BaseComponent"
import {
  View,
  ViewStyle,
  Alert,
  FlatList,

} from "react-native"
import { Dispatch } from "redux"
import { localization as l } from '../../../common/localization/localization'
import { connectAdv } from "../../../core/store/connectAdv"
import { NavigationActions } from "../../../navigation/navigation"
import { styleSheetCreate } from "../../../common/utils"
import { Colors, windowWidth } from "../../../core/theme"
import { TopicItem } from "../../../common/components/topicItem"
import { PlainHeader } from "../../../common/components/Headers"
import {
  NavigationRoute,
  NavigationScreenProp,
  NavigationScreenConfigProps,
  NavigationStackScreenOptions
} from "react-navigation"
import {IAppState} from "../../../core/store/appState"
import { TrainingActions } from "../trainingActions"

interface IStateProps {
  selectAssumption: any;
}

interface IDispatchProps {
  navigateToPhoneticTheory (data: any): void;
  navigateToPhoneticAssumption1 (assumption: any): void;
  navigateToPhoneticAssumption2 (data: any): void;
  navigateToPhoneticTraining (data: any): void;
}

interface ILevelTopic {
  category: string;
  title: string;
  status: string;
  onPress(): void;
}

interface IState {

}

interface IProps {
  navigation: NavigationScreenProp<NavigationRoute<any>>;
}

let array = [];

@connectAdv(
  (state: IAppState): IStateProps => ({
    selectAssumption: state.training.selectAssumption,
  }),
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToPhoneticTheory(data: any): void {
      if (data && data.exercises && data.exercises.length > 0) {
        const assumption = data.exercises[0];
        dispatch(TrainingActions.selectAssumption(assumption));
        dispatch(NavigationActions.navigateToPhoneticTheory({data}));
      } else {
        Alert.alert(l.training.attention, l.training.noExercisesInThisSection);
      }
    },
    navigateToPhoneticAssumption1(data): void {
      if (data && data.exercises && data.exercises.length > 0) {
        const assumption = data.exercises[0];
        dispatch(TrainingActions.selectAssumption(assumption));
        dispatch(NavigationActions.navigateToAssumptionInstr(1));
      } else {
        Alert.alert(l.training.attention, l.training.noExercisesInThisSection);
      }
    },
    navigateToPhoneticAssumption2(data: any): void {
      if (data.exercises && data.exercises.length > 0 && data.dialog) {
        const assumption = {assumption: data.exercises[1], dialog: data.dialog};
        dispatch(TrainingActions.selectAssumption(assumption));
        dispatch(NavigationActions.navigateToAssumptionInstr(2));
      } else {
        Alert.alert(l.training.attention, l.training.noExercisesInThisSection);
      }
    },
    navigateToPhoneticTraining(data): void {
      if (data.exercises && data.exercises.length > 0 && data.dialog) {
        const assumption = data.exercises[2];
        dispatch(TrainingActions.selectAssumption(assumption));
        dispatch(NavigationActions.navigateToTrainingInstr());
      } else {
        Alert.alert(l.training.attention, l.training.noExercisesInThisSection);
      }
    },
  })
)
export class PhoneticTopic extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {
  static navigationOptions = ({ navigation }: NavigationScreenConfigProps): NavigationStackScreenOptions => {
    return PlainHeader(`${navigation.state.params ? navigation.state.params.item.theme : ""}`, true, false);
  }

	topicItems(data: any) {
    return[
      {
        category: l.training.soundTheory,
        title: data.theme,
        status: "isDone",
        onPress: () => { this.dispatchProps.navigateToPhoneticTheory({...data, assemtionNum: 1}); }
      },
      {
        category: l.training.exerciseOne,
        status: 'isSentReview',
        title: '',
        onPress: () => { this.dispatchProps.navigateToPhoneticAssumption1(data) }
      },
      {
        category: l.training.exerciseTwo,
        status: 'isInToDo',
        title: '',
        onPress: () => { this.dispatchProps.navigateToPhoneticAssumption2(data) }
      },
      {
        category: l.training.testing,
        status: '',
        title: '',
        onPress: () => { this.dispatchProps.navigateToPhoneticTraining(data) }
      },
    ];
  }

  keyExtractor = (item: ILevelTopic): string => item.category;

  renderItem = ({ item: { category, title, status, onPress }, index }: any): JSX.Element => (
    <TopicItem
      access={true}
      category={category}
      title={title}
      status={status}
      onPress={onPress}
      isFirstItem={index}
      isLastItem={index === array.length - 1}
    />
  )
  render(): JSX.Element {
    const {item} = this.props.navigation.state.params;

    array = this.topicItems(item);

    return (
      <View style={styles.container}>
        <FlatList style={styles.content}
					data={array}
					renderItem={this.renderItem}
					keyExtractor={this.keyExtractor}
				/>
      </View>
    );
  }
}

const styles = styleSheetCreate({
  container: {
    flex: 1,
    backgroundColor: Colors.greyE5,
	} as ViewStyle,
	content: {
    paddingTop: windowWidth * .04,
	} as ViewStyle,
});
