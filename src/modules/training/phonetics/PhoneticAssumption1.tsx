import React from "react";
import {
  View,
  ViewStyle,
  Text,
  Alert,
  Platform,
  TextStyle,
  ScrollView,

} from "react-native";
import { BaseReduxComponent } from "../../../core/BaseComponent";
import { Colors, windowWidth } from "../../../core/theme";
import { styleSheetCreate } from "../../../common/utils";
import Sound from "react-native-sound";
import { PlainHeader } from "../../../common/components/Headers";
import { Assemtion1 } from "../../../common/components/Assemtion1";
import { localization as l } from '../../../common/localization/localization'
import {IAppState} from "../../../core/store/appState";
import { Dispatch } from "redux";
import { connectAdv } from "../../../core/store/connectAdv";
import Carousel from "react-native-snap-carousel";
import { NavigationActions } from "../../../navigation/navigation";
import {AudioRecorder, AudioUtils} from "react-native-audio";

interface IStateProps {
  selectAssumption: {items: []} | null;

}

interface IDispatchProps {
  navigateToSuccessfulFinish(): void;
}

interface IState {
  filterAssump: any;
  scrollStatus: boolean;
  paused: boolean;
  count: number;
  activeItem: number;
  index: number;
  isModalVisible: boolean;
  assumptionList: [];
  listAssump: any;
  recording: boolean;
  stoppedRecording: boolean;
  finished: boolean;
  audioPath: string;
  hasPermission: any;
}

interface IProps {
 
}

@connectAdv(
  (state: IAppState): IStateProps => ({
    selectAssumption: state.training.selectAssumption,
  }),
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToSuccessfulFinish(): void {
      dispatch(NavigationActions.navigateToSuccessfulFinish());
    },
  })
)
export class PhoneticAssumption1 extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {


  static navigationOptions = PlainHeader(l.training.exerciseOne, true, false);
  _carousel: any;

  constructor(props: any) {
    super(props);
    this.state = {
      filterAssump: [],
      scrollStatus: false,
      assumptionList: [],
      listAssump: [],
      count: 25,
      index: 0,
      activeItem: 0,
      recording: false,
      isModalVisible: false,
      paused: false,
      stoppedRecording: false,
      finished: false,
      audioPath: AudioUtils.DocumentDirectoryPath + "/sound0.aac",
      hasPermission: undefined,

    };
  }

  newSound: any;

  modal() {
    this.stop(this.state.filterAssump.length - 1);
    this.dispatchProps.navigateToSuccessfulFinish();
  }
  prepareRecordingPath(audioPath: string) {
    AudioRecorder.prepareRecordingAtPath(audioPath, {
      SampleRate: 22050,
      Channels: 1,
      AudioQuality: "Low",
      AudioEncoding: "aac",
      AudioEncodingBitRate: 32000
    });
  }

  componentWillUnmount() {
    
    if (this.state.recording) {
      try {
        AudioRecorder.stopRecording();
      }catch (err) {
        console.log(err);
      }
    }
    
    if (this.newSound) {

      this.newSound.stop();
    }
  }

  componentDidMount() {
    let { selectAssumption } = this.stateProps;

    if (selectAssumption) {
      let filterAssump = selectAssumption.items.filter((elem): any => {
        if (elem[0]) {
          return elem;
        }
      });
      let listAssump = [filterAssump[0], filterAssump[1]];

      this.setState({filterAssump, listAssump});
    }

    AudioRecorder.requestAuthorization().then((isAuthorised: any) => {
      this.setState({ hasPermission: isAuthorised });

      if (!isAuthorised) return;

      this.prepareRecordingPath(this.state.audioPath);

      // AudioRecorder.onProgress = (data) => {
      //   this.setState({currentTime: Math.floor(data.currentTime)});
      // };

      AudioRecorder.onFinished = (data: any) => {
        // Android callback comes in the form of a promise instead.
        if (Platform.OS === "ios") {
          this.finishRecording(data.status === "OK");
        }
      };
    });
  }

  stop = async (index: number): Promise<any> => {
    this.recordStatus(true);
    if (!this.state.recording) {
      console.warn("Can't stop, not recording!");
      return;
    }
    if (this.newSound) {
      this.newSound.stop();
    }
    let {filterAssump} = this.state;
    if (filterAssump[index]) {
      filterAssump[index].clickStop = true;
    }  

    this.checkNextSlide(index);

    this.setState({finished: true, stoppedRecording: true, index, recording: false, paused: false});

    try {
      const filePath = await AudioRecorder.stopRecording();

      if (Platform.OS === "android") {
        this.finishRecording(true);
      }

      return filePath;
    } catch (error) {
      console.error(error);
    }
  }

  play = async (index: number): Promise<any> => {
    if (this.newSound) {
      await this.newSound.stop();
    }

    this.checkNextSlide(index);

    let audioPath =  "";

    setTimeout(() => {
      let {filterAssump} = this.state;

      if (filterAssump[index].soudPath) {
        audioPath = filterAssump[index].soudPath;
      }

      if (filterAssump[index]) {
        filterAssump[index].clickPlay = true;
      }   

      this.newSound = new Sound(audioPath, "", (error) => {
        if (error) {
          console.log("failed to load the sound", error);
        }
      });
      this.setState({index});
      setTimeout(() => {
        this.newSound.play((success: any) => {
          if (success) {
            console.log("successfully finished playing");
          } else {
            console.log("playback failed due to audio decoding errors");
          }
        });
      }, 100);
    }, 100);
  }

  finishRecording(didSucceed: boolean) {
    this.setState({ finished: didSucceed });
  }

  async record(index: number) {
    let {filterAssump} = this.state;
    this.recordStatus(false);
    if (this.state.recording) {
      console.warn("Already recording!");
      Alert.alert(l.training.attention, l.training.voiceRecording);
      return;
    }
    this.setState({index});
    if (!this.state.hasPermission) {

      console.warn("Can't record, no permission granted!");
      Alert.alert(l.training.attention, l.training.notProvideVoiceRecording);
      return;
    }

    let audioPath = `${AudioUtils.DocumentDirectoryPath}/sound${index}.aac`;

    filterAssump[index].soudPath = audioPath;

    if (this.state.stoppedRecording) {

      this.prepareRecordingPath(audioPath);
    }

    this.setState({recording: true, audioPath, paused: false});

    try {
      await AudioRecorder.startRecording();
    } catch (error) {
      console.error(error);
    }
  }
  
  async listen(word: string, index: number) {
    if (this.state.recording) {
      await this.stop(index);
    }

    let {filterAssump} = this.state;

    if (filterAssump[index]) {
      filterAssump[index].clickListen = true;
    }

    this.checkNextSlide(index);

    setTimeout(() => {
      let sound = new Sound(`sound_${word.toLowerCase().replace(/\s/g, "")}.mp3`, Sound.MAIN_BUNDLE, (error) => {
    
        if (error) {
          console.log("failed to load the sound", error);
        }
      });
      this.setState({index});
      setTimeout(() => {
        sound.play((success) => {
          if (success) {
            console.log("successfully finished playing");
          } else {
            console.log("playback failed due to audio decoding errors");
          }
        });
      }, 100);
    }, 100);
  }

  async pause() {
    if (!this.state.recording) {
      console.warn("Can't pause, not recording!");
      return;
    }

    try {
      await AudioRecorder.pauseRecording();
      this.setState({paused: true});
    } catch (error) {
      console.error(error);
    }
  }
  
  //@ts-ignore
  renderSlide({item, index}) {
    let {filterAssump} = this.state;
    if (item[0]) {
      return <Assemtion1 
        finishSreen={this.modal.bind(this)} 
        stop={this.stop.bind(this)}
        play={this.play.bind(this)} 
        record={this.record.bind(this)}   
        recordStatus={this.recordStatus.bind(this)}
        recording={this.state.recording}
        listen={this.listen.bind(this)}
        item={item} 
        index={index} 
        items={filterAssump} 
      />;
    }
  }
  
  recordStatus(status: boolean) {
    this.setState({scrollStatus: status});
  }

  checkNextSlide(index: number) {
    let {listAssump, activeItem, filterAssump} = this.state;
    if (filterAssump[index].clickListen && 
      filterAssump[index].clickPlay &&
      activeItem !== index &&
      filterAssump[index].clickStop) {
        listAssump.push(filterAssump[index]);
        this.setState({activeItem: index});
    }
  }
  render() {
    let {listAssump, filterAssump, index, activeItem} = this.state;
    
    return (
      <View style={styles.container}>
        <ScrollView>
        {
          listAssump.length > 0 ?
            <Carousel
              ref={( c: any ) => { c && c._activeItem ? this.checkNextSlide(c._activeItem) : c; }}
              data={listAssump}
              onScrollBeginDrag={() => this.newSound ? this.newSound.stop() : null}
              scrollEnabled={ 
                index > -1
                && activeItem === index
                && filterAssump[activeItem]
                && filterAssump[activeItem].clickListen 
                && filterAssump[activeItem].clickPlay
                && filterAssump[activeItem].clickStop
                ? true  
                : false
              }
              renderItem={this.renderSlide.bind(this)}
              sliderWidth={windowWidth}
              itemWidth={windowWidth}
              layout={"default"}
              firstItem={0}
            />
          :
            <Text style={styles.notAssumptionText}>{l.training.noExercise}</Text>
        }
        </ScrollView>
      </View>
    );
  }
}

const styles = styleSheetCreate({
  container: {
    flexGrow: 1,
    backgroundColor: Colors.greyE5,
    justifyContent: "space-around",
  } as ViewStyle,
  notAssumptionText: {
    textAlign: "center"
  } as TextStyle,
  contentText: {
    color: Colors.black60,
    fontSize: windowWidth * .039,
    lineHeight: windowWidth * .075,
  } as TextStyle,
});