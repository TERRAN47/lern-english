import React from 'react'
import {
  View,
  ViewStyle,
  Text,
  TextStyle,
 // WebView,
  TouchableOpacity,
  Platform,
  ScrollView,
} from 'react-native'
import { Dispatch } from 'redux'
import { connectAdv } from '../../../core/store/connectAdv'
import { NavigationActions } from '../../../navigation/navigation'
import { BaseReduxComponent } from '../../../core/BaseComponent'
import { Colors, windowWidth } from '../../../core/theme'
import { localization as l } from '../../../common/localization/localization'
import { styleSheetCreate } from '../../../common/utils'
import { Button } from '../../../common/components/Button'
import { PlainHeader } from '../../../common/components/Headers'
import {
  NavigationRoute,
  NavigationScreenProp,
} from 'react-navigation'

interface IStateProps {

}

interface IDispatchProps {
  navigateToAssumptionInstr (assemtionNum:number): void,
}

interface IState {
  lang: string,
  titleButton: string,
}

interface IProps {
  category: string,
  title: string,
  navigation: NavigationScreenProp<NavigationRoute<any>>
}

@connectAdv(
  null,
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToAssumptionInstr(assemtionNum): void {
      dispatch(NavigationActions.navigateToAssumptionInstr(assemtionNum))
    },
  })
)
export class PhoneticTheory extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  static navigationOptions = PlainHeader(l.training.soundTheory, true, false)

  state = {
    lang: 'ru',
    titleButton: l.training.goToExercises,
  }

  render(): JSX.Element {
    
    let {data} = this.props.navigation.state.params

    const changeContentRu = (): void => {
      this.setState({
        lang: 'ru',
        titleButton: l.training.goToExercises,
      })
    }

    const changeContentEn = (): void => {
      this.setState({
        lang: 'en',
        titleButton: l.training.goToExercisesEn,
      })
    }

    const styleRu = 
      this.state.lang === 'ru'
        ? styles.langFocus
        : styles.langUnFocus

    const styleEn = 
      this.state.lang === 'en'
        ? styles.langFocus
        : styles.langUnFocus
        const regex = /(<([^>]+)>)/ig;
        
    return (
      // <WebView 
      //   originWhitelist={['*']}
      //   source={{html: `${data.rule}`}}
      // />
      <ScrollView>
        <View  style={styles.container}>	
          <View style={styles.lang}>
            <TouchableOpacity style={styleRu} onPress={changeContentRu}>
              <Text style={styles.langTitle}>{l.training.russian}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styleEn}>
              <Text style={styles.langTitle} onPress={changeContentEn}>{l.training.english}</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.content}>
            {
              this.state.lang === 'en' ? 
                <Text style={styles.contentText}>{data.rule.replace(/&bull;/gi, '').replace(/(&bull;) | &ndash;/gi, '').replace(/(&nbsp;)/ig, '').replace(regex, '')}</Text>
              : 
                <Text style={styles.contentText}>{data.rule.replace(/&bull;/gi, '').replace(/&ndash;/gi, '').replace(/(&nbsp;)/ig, '').replace(regex, '')}</Text>
            }
          </View>
          <TouchableOpacity  onPress={()=>this.dispatchProps.navigateToAssumptionInstr(data.assemtionNum)}>
            <Button titleButton={this.state.titleButton}/>
          </TouchableOpacity>
        </View>
      </ScrollView>
    )
  }
}

const styles = styleSheetCreate({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    alignItems: 'center',
    paddingBottom: windowWidth * .07,
  } as ViewStyle,
  
  //-- menu leng --//
  contentText: {
    fontSize: windowWidth * .04,
  } as TextStyle,

  lang: {
    height: windowWidth * .12,
    flexDirection: 'row',
    backgroundColor: Colors.blueEC,
    ...Platform.select({
      ios: {
        shadowRadius: 15,
        shadowOpacity: 0.3,
        shadowOffset: { width: 1, height: 4 },
      },
      android: {
        elevation: 8,
    }}),
  } as ViewStyle,

  langTitle: {
    color: Colors.white,
    fontWeight: 'bold',
    fontSize: windowWidth * .037,
  } as TextStyle,

  langFocus: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: Colors.white,
    borderBottomWidth: windowWidth * .007,
    borderStyle: 'solid',
  } as ViewStyle,
  langUnFocus: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  } as ViewStyle,

  //-- content --//

	content: {
    flex: 1,
    padding: windowWidth * .06,
  } as ViewStyle,
  title:{
    color: Colors.black59,
    fontWeight: 'bold',
    fontSize: windowWidth * .037,
  } as TextStyle,
})
