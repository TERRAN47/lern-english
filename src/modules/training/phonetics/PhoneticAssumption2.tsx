import React from "react";
import {
  View,
  ViewStyle,
  Text,
  TextStyle,
  Image,
  ImageStyle,
  Platform,
  TouchableOpacity,

} from "react-native";
import { Dispatch } from "redux";
import LinearGradient from "react-native-linear-gradient";
import {AudioRecorder, AudioUtils} from "react-native-audio";
import { BaseReduxComponent } from "../../../core/BaseComponent";
import { Colors, windowWidth } from "../../../core/theme";
import { ImageResources } from "../../../common/ImageResources.g";
import { styleSheetCreate } from "../../../common/utils";
//@ts-ignore
import { localization as l } from '../../../common/localization/localization';
//import TrackPlayer from 'react-native-track-player';
import { AudioPlayer } from "../../../common/components/AudioPlayer";
import { ScrollView } from "react-native-gesture-handler";
import { Button } from "../../../common/components/Button";
import { NavigationActions } from "../../../navigation/navigation";
import {IAppState} from "../../../core/store/appState";
import { connectAdv } from "../../../core/store/connectAdv";
import { PlainHeader } from "../../../common/components/Headers";

interface IStateProps {
  selectAssumption: any;
}

  interface IDispatchProps {
  navigateToSuccessfulFinish(): void;
}

interface IState {
  paused: boolean;
  isModalVisible: boolean;
  duration: any;
  assumptionList: [];
  recording: boolean;
  finished: boolean;
  audioPath: string;
  hasPermission: any;
  stoppedRecording: boolean;
}

interface IProps {

}
@connectAdv(
  (state: IAppState): IStateProps => ({
    selectAssumption: state.training.selectAssumption,
  }),
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToSuccessfulFinish(): void {
      dispatch(NavigationActions.navigateToSuccessfulFinish());
    },
  })
)
export class PhoneticAssumption2 extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps>{

  constructor(props: any) {
    super(props);
    this.state = {
      duration: 0,
      assumptionList: [],
      stoppedRecording: false,
      recording: false,
      isModalVisible: false,
      paused: false,
      finished: false,
      audioPath: AudioUtils.DocumentDirectoryPath + "/test.aac",
      hasPermission: undefined,
    };
  }


  static navigationOptions = PlainHeader(l.training.exerciseTwo, true, false);

  prepareRecordingPath(audioPath: string) {
    AudioRecorder.prepareRecordingAtPath(audioPath, {
      SampleRate: 22050,
      Channels: 1,
      AudioQuality: "Low",
      AudioEncoding: "aac",
      AudioEncodingBitRate: 32000
    });
  }

  componentDidMount() {

    AudioRecorder.requestAuthorization().then((isAuthorised: any) => {
      this.setState({ hasPermission: isAuthorised });

      if (!isAuthorised) return;

      this.prepareRecordingPath(this.state.audioPath);

      AudioRecorder.onFinished = (data: any) => {
        // Android callback comes in the form of a promise instead.
        if (Platform.OS === "ios") {
          this.finishRecording(data.status === "OK");
        }
      };
    });
  }

  componentWillUnmount() {

    if (this.state.recording) {
      try {
        AudioRecorder.stopRecording();
      } catch (err) {
        console.log(err);
      }
    }
  }

  renderButton(title: string, onPress: () => void) {
    return (
      <TouchableOpacity style={styles.button} onPress={onPress}>
        <Button titleButton={title} />
      </TouchableOpacity>
    );
  }

  private _stop = async(): Promise<any> => {
    if (!this.state.recording) {
      console.warn("Can't stop, not recording!");

      return;
    }
    this.setState({finished: true, stoppedRecording: true, recording: false, paused: false});

    try {
      const filePath = await AudioRecorder.stopRecording();

      if (Platform.OS === "android") {
        this.finishRecording(true);
      }

      return filePath;
    } catch (error) {
      console.error(error);
    }
  }

  private _record = async(): Promise<void> => {
    if (this.state.recording) {
      console.warn("Already recording!");

      return;
    }

    if (!this.state.hasPermission) {
      console.warn("Can't record, no permission granted!");

      return;
    }

    if (this.state.finished) {
      this.prepareRecordingPath(this.state.audioPath);
    }

    this.setState({recording: true, paused: false});

    try {
      await AudioRecorder.startRecording();
    } catch (error) {
      console.error(error);
    }
  }

  private finishRecording(didSucceed: boolean) {
    this.setState({ finished: didSucceed });
  }

  render() {
		let { finished, audioPath, recording} = this.state;
    let {assumption, dialog} = this.stateProps.selectAssumption;

    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.content}> 
            <Text style={styles.contentTitle}>{assumption.items[0][0]}</Text>
            <AudioPlayer audioPath={`sound_${dialog.slice(14)}`} record={false} />
          </View>
  
          {
            finished ?
              <View style={styles.recordContainer}>
                <Text style={styles.contentTitle}>{l.training.listenYourVoice}:</Text>
                <AudioPlayer audioPath={audioPath} record={true} />
                <View style={styles.compitButtonBlock}>
                  {this.renderButton(l.training.completeExercise, this.dispatchProps.navigateToSuccessfulFinish)}
                </View>
              </View>
            :
              <View style={styles.recordContainer}>
                <Text style={styles.contentTitle}>{l.training.clickRecordYourVoice}:</Text>
                {
                  recording ?
                    <View style={styles.controls}>
                      <TouchableOpacity onPress={this._stop}>			
                        <LinearGradient
                          style={styles.player}
                            start={{ x: 0, y: 0 }}
                            end={{ x: 1, y: 0 }}
                            colors={["#4E71EC", "#4EAAEC"]}
                          >
                            <View style={styles.playButton}>
                              <Image 
                                style={styles.icon}
                                source={ImageResources.iconMicrophone}
                              />
                            </View>
                            <Text style={styles.buttonText}>{l.training.isRecording}</Text>
                        </LinearGradient>
                      </TouchableOpacity>
                    </View>
                  :		
                    <View style={styles.controls}>
                      <TouchableOpacity onPress={this._record}>
                        <LinearGradient
                          style={styles.player}
                            start={{ x: 0, y: 0 }}
                            end={{ x: 1, y: 0 }}
                            colors={["#4E71EC", "#4EAAEC"]}
                          >
                            <View style={styles.playButton}>
                              <Image 
                                style={styles.icon}
                                source={ImageResources.iconMicrophone}
                              />
                            </View>
                        </LinearGradient>
                      </TouchableOpacity>
                    </View>
                }
              </View>
            }
        </ScrollView>
      </View>
    );
  }
}

const styles = styleSheetCreate({
  container: {
    flex: 1,
		backgroundColor: "#eff1f4",
		alignItems: "center",
  } as ViewStyle,
  controls: {
    alignItems: "center",
  } as ViewStyle,
  compitButtonBlock: {
    marginTop: 20,
    alignItems: "center"
  }  as ViewStyle,
  contentText: {
    color: Colors.black60,
    fontSize: windowWidth * .039,
  } as TextStyle,
  countPageText: {
    fontSize:  windowWidth * .039,
    color: Colors.blueEC,
    marginTop: 20,
    textAlign: "center"
  } as TextStyle,
  contentTitle: {
    color: Colors.dark5E,
		fontSize: windowWidth * .045,
		lineHeight: windowWidth * .08,
		paddingBottom: windowWidth * .034, 
  } as TextStyle,
  content: {
    backgroundColor: Colors.white,
    alignItems: "center",
		justifyContent: "space-between",
		padding: windowWidth * .05,
    ...Platform.select({
      ios: {
          shadowRadius: 1,
          shadowOpacity: 0.04,
          shadowOffset: {width: 0, height: 2},
      },
      android: {
        elevation: 8
    }}),
  } as ViewStyle,
  button: {
    padding: 10
  } as ViewStyle,
  activeButtonText: {
    fontSize: windowWidth * .039,
    color: "#B81F00"
  } as TextStyle,
  player: {
		flexDirection: "row",
    backgroundColor: Colors.blueEC, 
    width: windowWidth * .9,
    borderRadius: windowWidth * .03, 
		overflow: "hidden",
		alignItems: "center",
		padding: windowWidth * .034, 
  } as ViewStyle,
  playButton: {
		height: windowWidth * .1,
		width: windowWidth * .1,
    backgroundColor: "white", 
    borderRadius: windowWidth * .05, 
		padding: windowWidth * .024, 
		justifyContent: "center",
		alignItems: "center",
	} as ViewStyle,
	icon: {
		height: windowWidth * .05,
		width: windowWidth * .034,
	} as ImageStyle,
	buttonText: {
		paddingLeft: windowWidth * .1,
		color: Colors.white,
	} as TextStyle,
	recordContainer: {
    paddingTop: windowWidth * .06,
    paddingHorizontal: windowWidth * .05,
	} as ViewStyle,
});
