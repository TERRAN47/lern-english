import React from "react";
import {
	View,
  Text,
  ViewStyle,
  TextStyle,
  ScrollView,

} from "react-native";
import { BaseReduxComponent } from "../../../core/BaseComponent";
import { NavigationActions } from "../../../navigation/navigation";
import { Colors, windowWidth } from "../../../core/theme";
import { styleSheetCreate } from "../../../common/utils";
import { localization as l } from '../../../common/localization/localization';
import { PlainHeader } from "../../../common/components/Headers";
import {IAppState} from "../../../core/store/appState";
import { Dispatch } from "redux";
import { connectAdv } from "../../../core/store/connectAdv";
import Carousel from "react-native-snap-carousel";
import { PhoneticTraning } from "../../../common/components/PhoneticTraning";

interface IStateProps {
  selectAssumption: {items: []} | null;
}

interface IDispatchProps {
	navigateToTestingSuccessfulFinish(): void;
  navigateToFinishWithErrors(seeErrors: any, testingAgain: any): void;
  goBack(): void;
  restart(): void;
}

interface IState {
  testFinesh: boolean;
  scrollStatus: boolean;
  pageNum: number;
}

interface IProps {

}

@connectAdv(
  (state: IAppState): IStateProps => ({
    selectAssumption: state.training.selectAssumption
  }),
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToTestingSuccessfulFinish(): void {
      dispatch(NavigationActions.navigateToTestingSuccessfulFinish(3));
    },
    goBack(): void {
      dispatch(NavigationActions.navReToBack());
    },
    restart(): void {
      dispatch(NavigationActions.navReToBack());
      dispatch(NavigationActions.navReToBack());
    },
    navigateToFinishWithErrors(seeErrors, testingAgain): void {
      dispatch(NavigationActions.navigateToFinishWithErrors({seeErrors, testingAgain}));
		},

  })
)
export class PhoneticTesting extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  static navigationOptions = PlainHeader(l.training.testing, true, false);
  
  constructor(props: any) {
    super(props);
    this.state = {
      testFinesh: false,
      scrollStatus: false,
      pageNum: 0
    };
  }

  _carousel: any;

  modal() {
		const {selectAssumption} = this.stateProps;
		console.log(123, selectAssumption);
		this.setState({testFinesh: true});

		let noError = true;
		//@ts-ignore
		selectAssumption.items.forEach((elem: elem[number]) => {
			if (elem[3] === undefined || !elem[3]) {
				return noError = false;
			}
    });

		if (noError) {
			this.dispatchProps.navigateToTestingSuccessfulFinish();
		} else {
			this.dispatchProps.navigateToFinishWithErrors(this.seeErrors, this.testingAgain);
		}
  }

  seeErrors = () => {
    this._carousel.snapToItem(0);
    this.setState({scrollStatus: true});
    this.dispatchProps.goBack();
  }

  testingAgain = () => {
    this.dispatchProps.restart();
  }
  //@ts-ignore
  renderSlide({item, index}) {
		const {selectAssumption} = this.stateProps;
		const {testFinesh} = this.state;

		return <PhoneticTraning
      finishSreen={this.modal.bind(this)}
      nextSlide={this.nextSlide}
      testFinesh={testFinesh}
      item={item}
      id={index + 1}
      items={selectAssumption ? selectAssumption.items : []}
    />;
  }

	nextSlide = () => {
    this._carousel.snapToNext();
  }

  render(): JSX.Element {
    const {selectAssumption} = this.stateProps;
    const {scrollStatus} = this.state;

    return (
      <View style={styles.container} >
        <ScrollView>
        {
          selectAssumption ?
            <Carousel
              ref={( c: any ) => { this._carousel = c; }}
              data={selectAssumption.items}
              renderItem={this.renderSlide.bind(this)}
              sliderWidth={windowWidth}
              itemWidth={windowWidth}
              layout={"tinder"}
              scrollEnabled={scrollStatus}
							layoutCardOffset={9}
              firstItem={0}
            />
          :
            <Text style={styles.notAssumptionText}>{l.training.noExercise}</Text>
        }
        </ScrollView>
      </View>
    );
  }
}

const styles = styleSheetCreate({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: Colors.blueEC,
		justifyContent: "space-around",
  } as ViewStyle,
  notAssumptionText: {
    textAlign: "center"
  } as TextStyle,
});
