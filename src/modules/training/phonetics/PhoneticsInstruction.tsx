import React from "react"
import { BaseReduxComponent } from '../../../core/BaseComponent'
import {
  View,
  Text,
  TextStyle,
  Platform,
  TouchableOpacity,
  ViewStyle,
} from 'react-native'
import { Dispatch } from 'redux'
import { connectAdv } from '../../../core/store/connectAdv'
import { NavigationActions } from '../../../navigation/navigation'
import { Colors, windowHeight, windowWidth } from '../../../core/theme'
import { styleSheetCreate } from '../../../common/utils'
import { localization as l } from '../../../common/localization/localization'
import { Button } from '../../../common/components/Button'
import { PlainHeader } from '../../../common/components/Headers'
import { trainingActionsAsync } from '../../training/trainingActionsAsync'
import {IAppState} from "../../../core/store/appState"
import {
  NavigationRoute,
  NavigationScreenProp,
} from 'react-navigation'

interface IStateProps {
  phoneticsResult: any,
  error: {} | string | null,
  isLoading: boolean,
  netStatus: boolean
}

interface IDispatchProps {
  navigateToPhoneticLevels (): void
  getPhonetics (netStatus:boolean): void
}

interface IState {

}

interface IProps {
  navigation: NavigationScreenProp<NavigationRoute<any>>
}
@connectAdv(
  (state: IAppState): IStateProps => ({
    phoneticsResult: state.training.phoneticsResult,
    error: state.homeWork.error,
    netStatus: state.common.netStatus,
    isLoading: state.homeWork.isLoading,
  }),
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToPhoneticLevels(): void {
      dispatch(NavigationActions.navigateToPhoneticLevels())
    },
    getPhonetics(netStatus): void {
      //@ts-ignore
      dispatch(trainingActionsAsync.getPhonetics(netStatus))
    }
  })
)

export class PhoneticsInstruction extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  static navigationOptions = PlainHeader(l.training.phonetics, true, false)

  componentDidMount() {
    let {netStatus} = this.stateProps 

    this.dispatchProps.getPhonetics(netStatus)
  }

  navToLeavels(){
   // this.props.navigation.goBack()
    this.dispatchProps.navigateToPhoneticLevels()
  }

  render(): JSX.Element {
    let {phoneticsResult, error} = this.stateProps
    console.log(33, phoneticsResult, error)
    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <View>
            <Text style={styles.contentTitle}>{l.training.instruction}</Text>
            <Text style={styles.contentText}>{l.training.phoneticInstr}</Text>
          </View>
          <TouchableOpacity  onPress={this.navToLeavels.bind(this)}>
            <Button titleButton={l.common.understandable} />
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = styleSheetCreate({
  container: {
    flex: 1,
    backgroundColor: Colors.greyE5,
    justifyContent:'center',
    padding: windowWidth * .05,
  } as ViewStyle,
  contentText:{
    color:Colors.dark5E,
    fontSize: windowWidth * .039,
    lineHeight: windowWidth * .075,
  } as TextStyle,
  contentTitle:{
    paddingBottom: windowWidth * .07,
    fontWeight:'500',
    color: Colors.dark5E,
    fontSize: windowWidth * .042,
    textAlign: 'center',
  } as TextStyle,
  content:{
    padding: windowHeight * .03,
    height: windowHeight * .8,
    backgroundColor:Colors.white,
    alignItems:'center',
    justifyContent:'space-between',
    borderRadius:15,
    ...Platform.select({
      ios: {
          shadowRadius: 6,
          shadowOpacity: 0.1,
          shadowOffset: {width: 0, height: 3},
      },
      android: {
        elevation: 8
    }}),
  } as ViewStyle,
})
