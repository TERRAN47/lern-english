import React from "react";
import { BaseReduxComponent } from "../../../core/BaseComponent";
import {
  View,
  Text,
  TextStyle,
  TouchableOpacity,
  //Image,
  ImageStyle,
  Platform,
  ViewStyle,
  ScrollView,
} from "react-native";
import {
  NavigationRoute,
  NavigationScreenProp,
  NavigationScreenConfigProps,
  NavigationStackScreenOptions,
} from "react-navigation";
import { connectAdv } from "../../../core/store/connectAdv";
import { NavigationActions } from "../../../navigation/navigation";
import { styleSheetCreate } from "../../../common/utils";
import { localization as l } from "../../../common/localization/localization";
import { Colors, windowWidth } from "../../../core/theme";
//import { ImageResources } from '../../../common/ImageResources.g'
import { Dispatch } from "redux";
import { Button } from "../../../common/components/Button";
import { PlainHeader } from "../../../common/components/Headers";
import {IAppState} from "../../../core/store/appState";

interface IStateProps {
  selectAssumption: any;
}

interface IDispatchProps {
  navigateToGrammarAssumption(assumptionNum: number): void;
}

interface IState {

}

interface IProps {
  navigation: NavigationScreenProp<NavigationRoute<any>>;
}

@connectAdv(
  (state: IAppState): IStateProps => ({
    selectAssumption: state.training.selectAssumption,
  }),
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToGrammarAssumption(assumptionNum: number): void {
      switch (assumptionNum) {
        case 1: dispatch(NavigationActions.navigateToGrammarAssumption1());
        break;

        case 2: dispatch(NavigationActions.navigateToGrammarAssumption2());
        break;

        case 3: dispatch(NavigationActions.navigateToGrammarAssumption3());
        break;

        case 4: dispatch(NavigationActions.navigateToGrammarAssumption4());
        break;

        case 5: dispatch(NavigationActions.navigateToGrammarAssumption5());
        break;

        default: dispatch(NavigationActions.navigateToGrammarAssumption1());
        break;
      }
    },
  })
)
export class GrammarAssumptionInstr extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {
  static navigationOptions = ({ navigation }: NavigationScreenConfigProps): NavigationStackScreenOptions => {
    return PlainHeader(`Упражнения ${navigation.state.params}`, true, false);
  }
  navigateToGrammarAssumption = (): void => {
    const {params} = this.props.navigation.state;
    this.dispatchProps.navigateToGrammarAssumption(params);
  }
  render(): JSX.Element {
    const {selectAssumption} = this.stateProps;

    return (
      <ScrollView contentContainerStyle={styles.bgStyle}>
        <View style={styles.container}>
          <View style={styles.content}>
            <Text style={styles.contentTitle}>Инструкция к упражнению</Text>
            <View style={styles.taskBlock}>
              <View style={styles.taskChild}>
                <Text style={styles.contentText}>{`${selectAssumption.selAssumption.task}`}</Text>
              </View>
            </View>
            <TouchableOpacity  onPress={this.navigateToGrammarAssumption}>
              <Button titleButton={l.common.understandable}/>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = styleSheetCreate({
  bgStyle: {
    flexGrow: 1,
    justifyContent: "center",
    backgroundColor: Colors.greyE5,
  } as ViewStyle,
  container: {
    flexGrow: 1,
    justifyContent: "center",
    padding: windowWidth * .05,
  } as ViewStyle,
  content: {
    flex: 1,
    padding: windowWidth * .05,
    backgroundColor: Colors.white,
    alignItems: "center",
    justifyContent: "space-between",
    borderRadius: windowWidth * .05,
    ...Platform.select({
      ios: {
          shadowRadius: 8,
          shadowOpacity: 0.1,
          shadowOffset: {width: 1, height: 4},
      },
      android: {
        elevation: 8
    }}),
  } as ViewStyle,
  taskBlock: {
    paddingVertical: windowWidth * .02,
  } as ViewStyle,
  taskChild: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    paddingVertical: windowWidth * .03,
  } as ViewStyle,
  imageIcon: {
    width: windowWidth * .08,
    height: windowWidth * .08,
    marginRight: windowWidth * .05,
    resizeMode: "contain",
  } as ImageStyle,
  contentText: {
    color: Colors.dark5E,
    fontSize: windowWidth * .039,
    textAlign: "center",
    lineHeight: windowWidth * .075,
  } as TextStyle,
  contentTitle: {
    fontWeight: "500",
    color: Colors.dark5E,
    fontSize: windowWidth * .046,
    padding: windowWidth * .07,
  } as TextStyle,
});
