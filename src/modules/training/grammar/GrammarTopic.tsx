import React from "react";
import { BaseReduxComponent } from "../../../core/BaseComponent";
import {
  View,
  Alert,
  ViewStyle,
  FlatList,
  ListRenderItemInfo,

} from "react-native";
import { Dispatch } from "redux";
import { localization as l } from '../../../common/localization/localization'
import { connectAdv } from "../../../core/store/connectAdv";
import { NavigationActions } from "../../../navigation/navigation";
import { styleSheetCreate } from "../../../common/utils";
import { Colors, windowWidth } from "../../../core/theme";
import { TopicItem } from "../../../common/components/topicItem";
import { PlainHeader } from "../../../common/components/Headers";
import {
  NavigationRoute,
  NavigationScreenProp,
  NavigationScreenConfigProps,
  NavigationStackScreenOptions
} from "react-navigation";
import { TrainingActions } from "../trainingActions";
import {IAppState} from "../../../core/store/appState";

interface IStateProps {
  selectLevel: any;
}

interface IDispatchProps {
  navigateToGrammarTheory(data: any): void;
  navigateToGrammarAssumptionOne (data: any): void;
  navigateToGrammarAssumptionTwo (data: any): void;
  navigateToGrammarAssumptionThree (data: any): void;
  navigateToGrammarAssumption4 (data: any): void;
  navigateToGrammarAssumptionFive (data: any): void;
}

interface ILevelTopic {
  category: string;
  title: string;
  status: string;
  onPress(): void;
  access: boolean;
}

interface IListItem extends ListRenderItemInfo<ILevelTopic> {
  item: ILevelTopic;
}

interface IState {

}

interface IProps {
  navigation: NavigationScreenProp<NavigationRoute<any>>;
}

@connectAdv(
  (state: IAppState): IStateProps => ({
    selectLevel: state.training.selectLevel
  }),
  (dispatch: Dispatch): IDispatchProps => ({
// <<<<<<< HEAD
//     navigateToGrammarTheory(data): void {
//       if(data && data.exercises && data.exercises.length > 0){
//         let assumption = {...data, section:30, assumption:1, selAssumption:data.exercises[0]}
//         dispatch(trainingActions.selectAssumption(assumption))
//         dispatch(NavigationActions.navigateToGrammarTheory(assumption))
//       }else{
//         Alert.alert(l.training.attention, l.training.noExercisesInThisSection)
//       }
//     },
//     navigateToGrammarAssumption1(data): void {
//       if(data && data.exercises && data.exercises.length > 0){
//         let assumption = {...data, section:30, assumption:2, selAssumption:data.exercises[0]}
//         dispatch(trainingActions.selectAssumption(assumption))
//         dispatch(NavigationActions.navigateToGrammarAssumptionInstr(1))
//       }else{
//         Alert.alert(l.training.attention, l.training.noExercisesInThisSection)
//       }
//     },
//     navigateToGrammarAssumption2(data): void {
//       if(data.exercises && data.exercises.length > 0 && data.exercises[1]){
//         let assumption = {...data, section:30, assumption:3, selAssumption:data.exercises[1]}
//         dispatch(trainingActions.selectAssumption(assumption))
//         dispatch(NavigationActions.navigateToGrammarAssumptionInstr(2))
//       }else{
//         Alert.alert(l.training.attention, l.training.noExercisesInThisSection)
//       }
//     },
//     navigateToGrammarAssumption3(data): void {
//       if(data.exercises && data.exercises.length > 0 && data.exercises[2]){
//         let assumption = {...data, section:30, assumption:4, selAssumption:data.exercises[2]}
//         dispatch(trainingActions.selectAssumption(assumption))
//         dispatch(NavigationActions.navigateToGrammarAssumptionInstr(3))
//       }else{
//         Alert.alert(l.training.attention, l.training.noExercisesInThisSection)
//       }
//     },
//     navigateToGrammarAssumption4(data): void {
//       if(data.exercises && data.exercises.length > 0 && data.exercises[3]){
//         let assumption = {...data, section:30, assumption:5, selAssumption:data.exercises[3]}
//         dispatch(trainingActions.selectAssumption(assumption))
//         dispatch(NavigationActions.navigateToGrammarAssumptionInstr(4))
//       }else{
//         Alert.alert(l.training.attention, l.training.noExercisesInThisSection)
//       }
//     },
//     navigateToGrammarAssumption5(data): void {
//       if(data.exercises && data.exercises.length > 0 && data.exercises[4]){
//         let assumption = {...data, section:30, assumption:6, selAssumption:data.exercises[4]}
//         dispatch(trainingActions.selectAssumption(assumption))
//         dispatch(NavigationActions.navigateToGrammarAssumptionInstr(5))
//       }else{
//         Alert.alert(l.training.attention, l.training.noExercisesInThisSection)
// =======
    navigateToGrammarTheory(data: any): void {
      if (data && data.exercises && data.exercises.length > 0) {
        const assumption = {...data, section: 30, assumption: 1, selAssumption: data.exercises[0]};
        dispatch(TrainingActions.selectAssumption(assumption));
        dispatch(NavigationActions.navigateToGrammarTheory(assumption));
      } else {
        Alert.alert(l.training.attention, l.training.noExercisesInThisSection);
      }
    },
    navigateToGrammarAssumptionOne(data: any): void {
      if (data && data.exercises && data.exercises.length > 0) {
        const assumption = {...data, section: 30, assumption: 2, selAssumption: data.exercises[0]};
        dispatch(TrainingActions.selectAssumption(assumption));
        dispatch(NavigationActions.navigateToGrammarAssumptionInstr(1));
      } else {
        Alert.alert(l.training.attention, l.training.noExercisesInThisSection);
      }
    },
    navigateToGrammarAssumptionTwo(data: any): void {
      if (data.exercises && data.exercises.length > 0 && data.exercises[1]) {
        const assumption = {...data, section: 30, assumption: 3, selAssumption: data.exercises[1]};
        dispatch(TrainingActions.selectAssumption(assumption));
        dispatch(NavigationActions.navigateToGrammarAssumptionInstr(2));
      } else {
        Alert.alert(l.training.attention, l.training.noExercisesInThisSection);
      }
    },
    navigateToGrammarAssumptionThree(data: any): void {
      if (data.exercises && data.exercises.length > 0 && data.exercises[2]) {
        const assumption = {...data, section: 30, assumption: 4, selAssumption: data.exercises[2]};
        dispatch(TrainingActions.selectAssumption(assumption));
        dispatch(NavigationActions.navigateToGrammarAssumptionInstr(3));
      } else {
        Alert.alert(l.training.attention, l.training.noExercisesInThisSection);
      }
    },
    navigateToGrammarAssumption4(data: any): void {
      if (data.exercises && data.exercises.length > 0 && data.exercises[3]) {
        const assumption = {...data, section: 30, assumption: 5, selAssumption: data.exercises[3]};
        dispatch(TrainingActions.selectAssumption(assumption));
        dispatch(NavigationActions.navigateToGrammarAssumptionInstr(4));
      } else {
        Alert.alert(l.training.attention, l.training.noExercisesInThisSection);
      }
    },
    navigateToGrammarAssumptionFive(data: any): void {
      if (data.exercises && data.exercises.length > 0 && data.exercises[4]) {
        const assumption = {...data, section: 30, assumption: 6, selAssumption: data.exercises[4]};
        dispatch(TrainingActions.selectAssumption(assumption));
        dispatch(NavigationActions.navigateToGrammarAssumptionInstr(5));
      } else {
        Alert.alert(l.training.attention, l.training.noExercisesInThisSection);
      }
    },
  })
)
export class GrammarTopic extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  static navigationOptions = ({ navigation }: NavigationScreenConfigProps): NavigationStackScreenOptions => {
    return PlainHeader(`${navigation.state.params !== undefined ? navigation.state.params.theme : ""}`, true, false);
  }
  array: any = [];

  navigateToGrammarTheory = (): any => {
    const { selectLevel } = this.stateProps;
    this.dispatchProps.navigateToGrammarTheory(selectLevel);
  }

  navigateToGrammarAssumptionOne = (): any => {
    const { selectLevel } = this.stateProps;
    this.dispatchProps.navigateToGrammarAssumptionOne(selectLevel);
  }

  navigateToGrammarAssumptionTwo = (): any => {
    const { selectLevel } = this.stateProps;
    this.dispatchProps.navigateToGrammarAssumptionTwo(selectLevel);
  }

  navigateToGrammarAssumptionThree = (): any => {
    const { selectLevel } = this.stateProps;
    this.dispatchProps.navigateToGrammarAssumptionThree(selectLevel);
  }

  navigateToGrammarAssumptionFour = (): any => {
    const { selectLevel } = this.stateProps;
    this.dispatchProps.navigateToGrammarAssumptionThree(selectLevel);
  }

  navigateToGrammarAssumptionFive = (): any => {
    const { selectLevel } = this.stateProps;
    this.dispatchProps.navigateToGrammarAssumptionFive(selectLevel);
  }

  // tslint:disable-next-line: cyclomatic-complexity
  topicItems = (data: any): any => {
    return[
      {
        category: "Теория",
        title: data.theme,
        access:  true,
        status: data.state_ex && data.state_ex[1] || data.state == 1 ? "isDone" : "isInToDo",
        onPress: this.navigateToGrammarTheory,
      },
      {
        category: "Упражнение 1",
        access: data.state_ex && data.state_ex[1] || data.state == 1 ? true : false,
        status: data.state_ex && data.state_ex[2] || data.state == 1 ? "isDone" : "isInToDo",
        title: "",
        onPress: this.navigateToGrammarAssumptionOne,
      },
      {
        category: "Упражнение 2",
        access: data.state_ex && data.state_ex[2] || data.state == 1 ? true : false,
        status: data.state_ex && data.state_ex[3] || data.state == 1 ? "isDone" : "isInToDo",
        title: "",
        onPress: this.navigateToGrammarAssumptionTwo
      },
      {
        category: "Упражнение 3",
        access: data.state_ex && data.state_ex[3] || data.state == 1 ? true : false,
        status: data.state_ex && data.state_ex[4] || data.state == 1 ? "isDone" : "isInToDo",
        title: "",
        onPress: this.navigateToGrammarAssumptionThree
      },
      {
        category: "Упражнение 4",
        access: data.state_ex && data.state_ex[4] || data.state == 1 ? true : false,
        status: data.state_ex && data.state_ex[5] || data.state == 1 ? "isDone" : "isInToDo",
        title: "",
        onPress: this.navigateToGrammarAssumptionFour
      },
      {
        category: "Упражнение 5",
        access: data.state_ex && data.state_ex[5] || data.state == 1 ? true : false,
        status: data.state_ex && data.state_ex[6] || data.state == 1 ? "isDone" : "isInToDo",
        title: "",
        onPress: this.navigateToGrammarAssumptionFive
      }
    ];
  }

  keyExtractor = (item: ILevelTopic, index: number): string => `${index}`;

  renderItem = ({ item: { category, access, title, status, onPress }, index }: IListItem): JSX.Element => (
    <TopicItem
      category={category}
      title={title}
      access={access}
      status={status}
      onPress={onPress}
      isFirstItem={index}
      isLastItem={index === this.array.length - 1}
    />
  )

  render(): JSX.Element {
    const {selectLevel} = this.stateProps;
    console.log("item", selectLevel);
    if (selectLevel) { this.array = this.topicItems(selectLevel); }

    return (
      <View style={styles.container}>
        <FlatList
          style={styles.content}
          extraData={selectLevel}
          data={this.array}
          renderItem={this.renderItem}
          keyExtractor={this.keyExtractor}
        />
      </View>
    );
  }
}

const styles = styleSheetCreate({
  container: {
    flex: 1,
    backgroundColor: Colors.greyE5,
  } as ViewStyle,
  content: {
    paddingTop: windowWidth * .04,
  } as ViewStyle,
});