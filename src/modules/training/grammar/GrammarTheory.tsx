import React from "react";
import {
  View,
  ViewStyle,
  Text,
  TextStyle,
  TouchableOpacity,
  Platform,
  ScrollView,
} from "react-native";
import { Dispatch } from "redux";
import { connectAdv } from "../../../core/store/connectAdv";
import { NavigationActions } from "../../../navigation/navigation";
import { BaseReduxComponent } from "../../../core/BaseComponent";
import { Colors, windowWidth } from "../../../core/theme";
import { styleSheetCreate } from "../../../common/utils";
import { Button } from "../../../common/components/Button";
import { PlainHeader } from "../../../common/components/Headers";
import {
  NavigationRoute,
  NavigationScreenProp,
} from "react-navigation";
//import { relativeTimeThreshold } from 'moment';
//import { WebView } from 'react-native-webview';
import {requestsRepository} from "../../../core/api/requestsRepository";
import { TrainingActions } from "../trainingActions";
import {IAppState} from "../../../core/store/appState";

interface IStateProps {
  grammarResult: any;
}

interface IDispatchProps {
  navigateToGrammarAssumptionInstr (data: any, grammarResult: any): void;
}

interface IState {
  lang: string;
  titleButton: string;
}

interface IProps {
  category: string;
  title: string;
  navigation: NavigationScreenProp<NavigationRoute<any>>;
}

@connectAdv(
  (state: IAppState): IStateProps => ({
    grammarResult: state.training.grammarResult
  }),
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToGrammarAssumptionInstr(data: any, grammarResult: any): void {

      const selectAssumption = {
        section: 30,
        ex_id: data.ex_id,
        ex_item_id: data.assumption,
      };

      let grammars = [];

      grammars = grammarResult.filter((item: any) => {
        if (item.level === data.level) {
          return item;
        }
      });

      if ( grammars[data.index].state_ex) {
        grammars[data.index].state_ex[1] = 1;
        data.state_ex[1] = 1;
      } else {
        grammars[data.index].state_ex = {1: 1};
        data.state_ex = {1: 1};
      }
      console.log("grammars", grammars);
      dispatch(TrainingActions.updateGrammar([...grammarResult]));
      dispatch(TrainingActions.selectLevel(data));
      dispatch(NavigationActions.navigateToGrammarAssumptionInstr(1));
      requestsRepository.trainingApiRequest.assumptReady(selectAssumption);
    },
  })
)
export class GrammarTheory extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  static navigationOptions = PlainHeader("Теория", true, false);

  state = {
    lang: "ru",
    titleButton: "Перейти к упражнениям",
  };

  wordProcessing = (text: string): string => {
    const regex = /(<([^>]+)>)/ig;

    return text.replace(/&bull;/gi, "")
    .replace(/(&bull;) | &ndash;/gi, "")
    .replace(/(&nbsp;)/ig, "")
    .replace(regex, "")
    .replace(/[+]/gi, " ");
  }

  navigateToGrammarAssumptionInstr = (): any => {
    const {grammarResult} = this.stateProps;
    const {params} = this.props.navigation.state;
    this.dispatchProps.navigateToGrammarAssumptionInstr(params, grammarResult);
  }

  render(): JSX.Element {
    const {params} = this.props.navigation.state;
    const changeContentRu = (): void => {
      this.setState({
        lang: "ru",
        titleButton: "Перейти к упражнениям"
      });
    };

    const changeContentEn = (): void => {
      this.setState({
        lang: "en",
        titleButton: "Go to exercises"
      });
    };

    const styleRu = this.state.lang === "ru" ? styles.langFocus : styles.langUnFocus;
    const styleEn = this.state.lang === "en" ? styles.langFocus : styles.langUnFocus;

    return (
      <ScrollView>
        <View  style={styles.container}>
          <View style={styles.lang}>
            <TouchableOpacity style={styleRu} onPress={changeContentRu}>
              <Text style={styles.langTitle}>РУССКИЙ</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styleEn}>
              <Text style={styles.langTitle} onPress={changeContentEn}>ENGLISH</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.content}>
            <Text style={styles.contentText}>{this.wordProcessing(this.state.lang === "en" ? params.rule_eng : params.rule)}</Text>
          </View>
          <TouchableOpacity  onPress={this.navigateToGrammarAssumptionInstr}>
            <Button titleButton={this.state.titleButton}/>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

const styles = styleSheetCreate({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    alignItems: "center",
    paddingBottom: windowWidth * .07,
  } as ViewStyle,

  //-- menu leng --//
  contentText: {
    fontSize: windowWidth * .04,
  } as TextStyle,

  lang: {
    height: windowWidth * .12,
    flexDirection: "row",
    backgroundColor: Colors.blueEC,
    ...Platform.select({
      ios: {
        shadowRadius: 15,
        shadowOpacity: 0.3,
        shadowOffset: { width: 1, height: 4 },
      },
      android: {
        elevation: 8,
    }}),
  } as ViewStyle,

  langTitle: {
    color: Colors.white,
    fontWeight: "bold",
    fontSize: windowWidth * .037,
  } as TextStyle,

  langFocus: {
    flex: 2,
    justifyContent: "center",
    alignItems: "center",
    borderBottomColor: Colors.white,
    borderBottomWidth: windowWidth * .007,
    borderStyle: "solid",
  } as ViewStyle,
  langUnFocus: {
    flex: 2,
    justifyContent: "center",
    alignItems: "center",
  } as ViewStyle,

  //-- content --//
  content: {
    flex: 1,
    padding: windowWidth * .06,
  } as ViewStyle,
  title: {
    color: Colors.black59,
    fontWeight: "bold",
    fontSize: windowWidth * .037,
  } as TextStyle,
});
