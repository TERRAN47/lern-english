import React from "react";
import {
  ViewStyle,
  Text,
  TouchableWithoutFeedback,
  Keyboard,
  TextStyle,
  ScrollView,
} from "react-native";
import { BaseReduxComponent } from "../../../core/BaseComponent";
import { NavigationActions } from "../../../navigation/navigation";
import { Dispatch } from "redux";
import { connectAdv } from "../../../core/store/connectAdv";
import { Colors, windowWidth } from "../../../core/theme";
import { styleSheetCreate } from "../../../common/utils";
//import { localization as l } from '../../../common/localization/localization'
import { PlainHeader } from "../../../common/components/Headers";
import { MainModal } from "../../../common/components/MainModal";
import { IAppState } from "../../../core/store/appState";
import Carousel from "react-native-snap-carousel";
import { GrammarSlide } from "../../../common/components/GrammarSlide";
import { TransparentButton } from "../../../common/components/TransparentButton";
import { ButtonType } from "../../../common/enums/buttonType";
import {requestsRepository} from "../../../core/api/requestsRepository";
import { TrainingActions } from "../trainingActions";

interface IStateProps {
  selectAssumption: any;
  selectLevel: any;
  grammarResult: any;

}

interface IDispatchProps {
  navigateToTestingSuccessfulFinish(data: any, grammarResult: any, result: any): void;
  navigateToFinishWithErrors(seeErrors: any, testingAgain: any): void;
  goBack(): void;
  restart(): void;
}

interface IState {
  testFinesh: boolean;
  scrollStatus: boolean;
  pageNum: number;
  text: string;
  isModalVisible: boolean;
  body: string;
}

interface IProps {

}

@connectAdv(
  (state: IAppState): IStateProps => ({
    selectAssumption: state.training.selectAssumption,
    selectLevel: state.training.selectAssumption,
    grammarResult: state.training.grammarResult,
  }),
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToTestingSuccessfulFinish(data: any, grammarResult: any, result: any): void {

      dispatch(TrainingActions.updateGrammar([...grammarResult]));
      dispatch(TrainingActions.selectLevel(data));
      dispatch(NavigationActions.navigateToTestingSuccessfulFinish(3));

      requestsRepository.trainingApiRequest.assumptReady(result);
    },
    navigateToFinishWithErrors(seeErrors: any, testingAgain: any): void {
      dispatch(NavigationActions.navigateToFinishWithErrors({seeErrors, testingAgain}));
    },
    goBack(): void {
      dispatch(NavigationActions.navReToBack());
    },
    restart(): void {
      dispatch(NavigationActions.navReToBack());
      dispatch(NavigationActions.navReToBack());
    },
  })
)
export class GrammarAssumption extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  static navigationOptions = PlainHeader("Упражнение", true, false);

  _carousel: any;

  constructor(props: any) {
    super(props);
    this.state = {
      testFinesh: false,
      scrollStatus: false,
      isModalVisible: false,
      pageNum: 0,
      text: "",
      body: ""
    };
  }

  modal = (): void => {
    const {selectAssumption, selectLevel, grammarResult} = this.stateProps;
    console.log("selectAssumption", selectAssumption);
    this.setState({testFinesh: true});

    let noError = true;
    let countError = 0;
    //@ts-ignore
    selectAssumption.selAssumption.items.forEach((elem: elem[number]) => {
      if (elem[2] === undefined || !elem[2]) {
        if (countError > 2) {
          return noError = false;
        }
        countError += 1;
      }
    });

    console.log("selectLevel", selectLevel);

    if (noError) {
      let allComplit = false;
      selectAssumption.exercises.forEach((el: any, index: number): any => {
        if (selectAssumption.state_ex[index + 2] == 1) {
          allComplit = true;
        } else {
          return allComplit = false;
        }
      });

      const data = {
        section: selectAssumption.section,
        ex_id: selectAssumption.ex_id,
        ex_done: allComplit ? 1 : 0,
        ex_item_id: selectAssumption.assumption,
      };

      if (allComplit) {
        grammarResult[selectLevel.index].state = 1;
      }

      let grammars = [];

      grammars = grammarResult.filter((item: any) => {
        if (item.level === selectLevel.level) {
          return item;
        }
      });

      if ( grammars[selectLevel.index].state_ex) {
        grammars[selectLevel.index].state_ex[data.ex_item_id] = 1;
        selectLevel.state_ex[data.ex_item_id] = 1;
      } else {
        grammars[selectLevel.index].state_ex = {[data.ex_item_id]: 1};
        selectLevel.state_ex = {[data.ex_item_id]: 1};
      }

      this.dispatchProps.navigateToTestingSuccessfulFinish(selectLevel, grammarResult, data);
    } else {
      this.dispatchProps.navigateToFinishWithErrors(this.seeErrors, this.testingAgain);
    }
  }

  seeErrors = (): any => {
    this._carousel.snapToItem(0);
    this.setState({scrollStatus: true});
    this.dispatchProps.goBack();
  }

  private openModal = (): void => {
    const {selectAssumption} = this.stateProps;
    this.setState({ isModalVisible: true, body: selectAssumption.selAssumption.task});
  };

  private closeModal = (): void => {
    this.setState({ isModalVisible: false, body: ""});
  }

  testingAgain = (): void => {
    this.dispatchProps.restart();
  }

  nextSlide = (): void => {
    this._carousel.snapToNext();
  }

  renderSlide = ({item, index}: any): any => {
    const {selectAssumption} = this.stateProps;
    const {testFinesh} = this.state;

    return (
      <GrammarSlide
        finishSreen={this.modal}
        nextSlide={this.nextSlide}
        openModal={this.openModal}
        testFinesh={testFinesh}
        item={item}
        id={+(index) + 1}
        items={selectAssumption ? selectAssumption.selAssumption.items : []}
      />
    );
  }

  getContent = (): JSX.Element => {
    const {selectAssumption, selectLevel} = this.stateProps;
    const {scrollStatus} = this.state;
    console.log(22, selectLevel);

    if (selectAssumption && selectAssumption.selAssumption) {
      return (
        <Carousel
          ref={( c: any ): any => { this._carousel = c; }}
          data={selectAssumption.selAssumption.items}
          renderItem={this.renderSlide}
          sliderWidth={windowWidth}
          itemWidth={windowWidth}
          keyboardShouldPersistTaps="always"
          layout={"tinder"}
          scrollEnabled={scrollStatus}
          layoutCardOffset={9}
          firstItem={0}
        />
      );
    } else {
      return <Text style={styles.notAssumptionText}>Упражнения нет</Text>;
    }
  }

  render(): JSX.Element {
    const {isModalVisible, body} = this.state;

    return (
      <ScrollView contentContainerStyle={styles.bgStyle}>
        <TouchableWithoutFeedback  onPress={Keyboard.dismiss} accessible={false}>
          {this.getContent()}
        </TouchableWithoutFeedback>
        <MainModal
          title="Инструкция к упражнению"
          body={body}
          isVisible={isModalVisible}
          closeModal={this.closeModal}
        >
          <TransparentButton
            title="OK"
            buttonType={ButtonType.positive}
            onPress={this.closeModal}
          />
        </MainModal>

      </ScrollView>
    );
  }
}

const styles = styleSheetCreate({
  bgStyle: {
    flexGrow: 1,
    justifyContent: "center",
    backgroundColor: Colors.blueEC,
  } as ViewStyle,
  notAssumptionText: {
    textAlign: "center"
  } as TextStyle,
});
