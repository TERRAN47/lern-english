import {actionCreator} from "../../core/store";
//import {ErrorSource} from "./authState";
//import {SignInRequestDto} from "../../core/api/generated/dto/SignInRequest.g";
//import {Profile} from "../../core/api/generated/dto/Profile.g";


export class TrainingActions {
  static getPhonetics = actionCreator.async<IEmpty, any, Error>("Training/UPDATE_PHONETICS");
  static getVocabularys = actionCreator.async<IEmpty, any, Error>("Training/UPDATE_VOCABULARYS");
  static getGrammars = actionCreator.async<IEmpty, any, Error>("Training/UPDATE_GRAMMARS");
  static addNewWordPost = actionCreator.async<IEmpty, any, Error>("Training/ADD_NEW_WORD");
  static getReading = actionCreator.async<IEmpty, any, Error>("Training/UPDATE_READING");
  static sendAssumption = actionCreator.async<IEmpty, any, Error>("Training/SEND_ASSUMPTION");
  static selectAssumption = actionCreator<any>("Training/SELECT_ASSEMTION");
  static clearState = actionCreator("Training/CLEAR_STATE");
  static getAuditions = actionCreator.async<IEmpty, any, Error>("Training/UPDATE_AUDITIONS");
  static updateGrammar = actionCreator<any>("Training/UP_GRAMMAR");
  static updateAudition = actionCreator<any>("Training/UP_AUDITION");
  static updateReading = actionCreator<any>("Training/UP_READING");
  static selectLevel = actionCreator<any>("Training/SELECT_LEVEL");
}