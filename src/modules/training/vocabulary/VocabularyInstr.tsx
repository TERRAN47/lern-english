import React from 'react';
import {
  View,
  Text,
  TextStyle,
  Platform,
  ViewStyle,
  TouchableOpacity,
} from 'react-native';
import DevMenu from 'react-native-dev-menu';
import { connectAdv } from '../../../core/store/connectAdv';
import { Dispatch } from 'redux';
import { NavigationActions } from '../../../navigation/navigation';
import { Colors, windowHeight, windowWidth } from '../../../core/theme';
import { BaseReduxComponent } from '../../../core/BaseComponent';
import { styleSheetCreate } from '../../../common/utils';
import { localization as l } from '../../../common/localization/localization';
import { PlainHeader } from '../../../common/components/Headers';
import { Button } from '../../../common/components/Button';
import {IAppState} from "../../../core/store/appState";
import { trainingActionsAsync } from '../../training/trainingActionsAsync';

interface IStateProps {
  netStatus:boolean;
}

interface IDispatchProps {
  navigateToVocabularyEssay(): void;
  navigateToVocabularyLevels(): void;
  navigateToTestingSuccessfulFinish(): void;
  navigateToDictionary(): void;
  navigateToDictionaryAddWord(): void;
  getVocabulary (netStatus:boolean): void;
}

interface IState {

}

interface IProps {

}
@connectAdv(
  (state: IAppState): IStateProps => ({
    netStatus: state.common.netStatus,
  }),
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToVocabularyEssay(): void {
      dispatch(NavigationActions.navigateToVocabularyEssay());
    },
    navigateToTestingSuccessfulFinish(): void {
      dispatch(NavigationActions.navigateToTestingSuccessfulFinish(3));
    },
    navigateToVocabularyLevels(): void {
      dispatch(NavigationActions.navigateToVocabularyLevels());
    },
    navigateToDictionary(): void {
      dispatch(NavigationActions.navigateToDictionary());
    },
    navigateToDictionaryAddWord(): void {
      dispatch(NavigationActions.navigateToDictionaryAddWord());
    },
    getVocabulary(netStatus): void {
      //@ts-ignore
      dispatch(trainingActionsAsync.getVocabulary(netStatus));
    }
  })
)
export class VocabularyInstr extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {


  static navigationOptions = PlainHeader(l.training.vocabulary, true);

  componentDidMount() {
    if (__DEV__) {
      DevMenu.addItem('>>> Dictionary', this.dispatchProps.navigateToDictionary);
      DevMenu.addItem('>>> Add Word', this.dispatchProps.navigateToDictionaryAddWord);
      DevMenu.addItem('>>> Testing Finish', this.dispatchProps.navigateToTestingSuccessfulFinish);
    }
    let {netStatus} = this.stateProps; 

    this.dispatchProps.getVocabulary(netStatus);
  }

  render(): JSX.Element {
    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <View>
            <Text style={styles.contentTitle}>{l.training.instruction}</Text>
            <Text style={styles.contentText}>{l.training.instructionAllDetails}</Text>
          </View>
          <TouchableOpacity onPress={this.dispatchProps.navigateToVocabularyLevels}>
            <Button titleButton={l.common.understandable} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = styleSheetCreate({
  container: {
    flexGrow: 1,
    backgroundColor: Colors.greyE5,
    justifyContent:'center',
    padding: windowWidth * .05,
  } as ViewStyle,
  contentText:{
    color:Colors.dark5E,
    fontSize: windowWidth * .039,
    lineHeight: windowWidth * .075,
  } as TextStyle,
  contentTitle:{
    paddingBottom: windowWidth * .07,
    fontWeight:'500',
    color: Colors.dark5E,
    fontSize: windowWidth * .042,
    textAlign: 'center',
  } as TextStyle,
  content:{
    padding: windowHeight * .03,
    height: windowHeight * .8,
    backgroundColor:Colors.white,
    alignItems:'center',
    justifyContent:'space-between',
    borderRadius:15,
    ...Platform.select({
      ios: {
          shadowRadius: 6,
          shadowOpacity: 0.1,
          shadowOffset: {width: 0, height: 3},
      },
      android: {
        elevation: 8
    }}),
  } as ViewStyle,
});
