import React from 'react'
import { BaseReduxComponent } from '../../../core/BaseComponent'
import {
  View,
  ViewStyle,
  FlatList,
  ListRenderItemInfo,
} from 'react-native'
import {
  NavigationRoute,
  NavigationScreenProp,
  NavigationScreenConfigProps,
  NavigationStackScreenOptions
} from "react-navigation";
import { Dispatch } from 'redux'
import {IAppState} from "../../../core/store/appState";
import { connectAdv } from '../../../core/store/connectAdv'
import { NavigationActions } from '../../../navigation/navigation'
import { styleSheetCreate } from '../../../common/utils'
import { Colors, windowWidth } from '../../../core/theme'
import { localization as l } from '../../../common/localization/localization'
import { TopicItem } from '../../../common/components/topicItem'
import { PlainHeader } from '../../../common/components/Headers'

interface IStateProps {
  selectLevel: any
}

interface IDispatchProps {
  navigateToAssumptionInstr (): void
  navigateToVocabularyAssumptionInstr (): void
  navigateToVocabularyEssay (): void
  navigateToVocabularyTesting (): void
}

interface ILevelTopic {
  category: string,
  title: string,
  status: string,
  onPress(): void,
}

interface IListItem extends ListRenderItemInfo<ILevelTopic> {
  item: ILevelTopic;
}

interface IState {

}

interface IProps {
  navigation: NavigationScreenProp<NavigationRoute<any>>
}

@connectAdv(
  (state: IAppState): IStateProps => ({
    selectLevel: state.training.selectLevel
  }),
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToAssumptionInstr(): void {
      dispatch(NavigationActions.navigateToAssumptionInstr(1))
    },
    navigateToVocabularyAssumptionInstr(): void {
      dispatch(NavigationActions.navigateToVocabularyAssumptionInstr())
    },
    navigateToVocabularyEssay(): void {
      dispatch(NavigationActions.navigateToVocabularyEssay())
    },
    navigateToVocabularyTesting(): void {
      dispatch(NavigationActions.navigateToVocabularyTesting())
    },
  })
)
export class VocabularyTopic extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  static navigationOptions = ({ navigation }: NavigationScreenConfigProps): NavigationStackScreenOptions => {
    return PlainHeader(`${navigation.state.params !== undefined ? navigation.state.params.theme : ""}`, true, false);
  }

  navigateToGrammarAssumptionOne = (): any => {
    //const { selectLevel } = this.stateProps;
    this.dispatchProps.navigateToVocabularyAssumptionInstr();
  }

  array: any = [];

	topicItems = (data: any): any  => {
    return [
      {
        category: l.training.learningWords,
        title: '',
        status: data.state_ex || data.state == 1 ? "isDone" : "isInToDo",
        onPress: this.dispatchProps.navigateToVocabularyAssumptionInstr,
      },
      {
        category: 'Слово-перевод',
        title: '',
        status: data.state_ex || data.state == 1 ? "isDone" : "isInToDo",
        onPress: this.dispatchProps.navigateToVocabularyTesting,
      },
      {
        category: 'Перевод-слово',
        title: '',
        status: 'isSentReview',
        onPress: this.dispatchProps.navigateToVocabularyTesting,
      },
      {
        category: l.training.testing,
        title: '',
        status: 'isInToDo',
        onPress: this.dispatchProps.navigateToVocabularyEssay,
      },
    ]
  }

  keyExtractor = (item: ILevelTopic, index: number): string => `${index}`;

  renderItem = ({ item: { category, title, status, onPress }, index }: IListItem): JSX.Element => (
    <TopicItem
      category={category}
      title={title}
      access={true}
      status={status}
      onPress={onPress}
      isFirstItem={index}
      isLastItem={index === this.array.length - 1}
    />
  )
  render(): JSX.Element {

    const {selectLevel} = this.stateProps;
    console.log("item", selectLevel);
    if (selectLevel) { this.array = this.topicItems(selectLevel); }

    return (
      <View style={styles.container}>
        <FlatList style={styles.content}
					data={this.array}
					renderItem={this.renderItem}
					keyExtractor={this.keyExtractor}
				/>
      </View>
    )
  }
}

const styles = styleSheetCreate({
  container: {
    flex: 1,
    backgroundColor: Colors.greyE5,
	} as ViewStyle,
	content: {
		paddingTop: windowWidth * .04,
	} as ViewStyle,
})
