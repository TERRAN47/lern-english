import React from 'react'
import {
  View,
  ViewStyle,
  TextStyle,
} from 'react-native'
import { BaseReduxComponent } from '../../../core/BaseComponent'
import { Colors, windowWidth } from '../../../core/theme'
import { styleSheetCreate } from '../../../common/utils'
import { PlainHeader } from '../../../common/components/Headers'
import { Assemtion1 } from '../../../common/components/Assemtion1'
import { localization as l } from '../../../common/localization/localization'
import {IAppState} from "../../../core/store/appState";
import { Dispatch } from 'redux'
import { connectAdv } from '../../../core/store/connectAdv'
import {
  NavigationRoute,
  NavigationScreenProp,
} from 'react-navigation'
import Carousel from 'react-native-snap-carousel';
import { NavigationActions } from '../../../navigation/navigation'

interface IStateProps {
  error: {} | null;
  isLoading: boolean
}

interface IDispatchProps {
  navigateToSuccessfulFinish():void
}

interface IState {

}

interface IProps {
  navigation: NavigationScreenProp<NavigationRoute<any>>
}

@connectAdv(
  (state: IAppState): IStateProps => ({
    error: state.homeWork.error,
    isLoading: state.homeWork.isLoading,
  }),
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToSuccessfulFinish(): void {
      dispatch(NavigationActions.navigateToSuccessfulFinish())
    },
  })
)

export class VocabularyAssumption1 extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  static navigationOptions = PlainHeader(l.training.exerciseOne, true, false)

  //@ts-ignore
  renderSlide({item, index}){
    //let {items} = this.props.navigation.state.params
    //@ts-ignore
    return <Assemtion1 finishSreen={this.dispatchProps.navigateToSuccessfulFinish} item={item} index={index} items={[['ferry']]} />
  }

  render() {
    //let { error} = this.stateProps
    //let {items} = this.props.navigation.state.params
    return (
      <View style={styles.container}> 
        <Carousel
          data={[['ferry']]}
          renderItem={this.renderSlide.bind(this)}
          sliderWidth={windowWidth}
          itemWidth={windowWidth}
          layout={'default'}
          firstItem={0}
        />
      </View>
    )
  }
}

const styles = styleSheetCreate({
  container: {
    flex: 1,
    backgroundColor: "#eff1f4",
    justifyContent: 'center',
  } as ViewStyle,
  contentText:{
    color: Colors.black60,
    fontSize: windowWidth * .039,
    lineHeight: windowWidth * .075,
  } as TextStyle,
});