import React from 'react'
import { 
  View,
  ViewStyle,
  Text,
  TextStyle,
  TextInput,
  ScrollView,
  TouchableOpacity,
 } from 'react-native'
 import { Dispatch } from 'redux'
 import { NavigationRoute, NavigationScreenProp } from 'react-navigation'
 import { BaseReduxComponent } from '../../../core/BaseComponent'
 import { NavigationActions } from '../../../navigation/navigation'
 import { connectAdv } from '../../../core/store'
 import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { styleSheetCreate } from '../../../common/utils'
import { localization as l } from '../../../common/localization/localization'
import { 
  Colors,
  windowWidth,
  windowHeight,
 } from '../../../core/theme'
import { PlainHeader } from '../../../common/components/Headers' 
import { Button } from '../../../common/components/Button'
 
interface IState {

}

interface IStateProps {
  
}

interface IProps {
  navigation: NavigationScreenProp<NavigationRoute<any>>
}

interface IDispatchProps {
  navigateToEssayFinish(data: any): void
}

@connectAdv(
  null,
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToEssayFinish(data): void {
      dispatch(NavigationActions.navigateToEssayFinish(data))
    },
  })
)
export class VocabularyEssay extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {
  static navigationOptions = PlainHeader(l.training.essay, true)  
  
  render(): JSX.Element {
    console.log(this.props.navigation.state.params)
    return(
      <ScrollView contentContainerStyle={styles.bgStyle}>
        <View>
          <KeyboardAwareScrollView>
              <View style={styles.content}>
                <View>
                  <View style={styles.titleContainer}>
                    <Text><Text style={styles.title}>{l.common.theme}: </Text><Text style={styles.description}>Как я провел каникулы</Text></Text>
                    <View style={styles.underline} />
                  </View>
                </View>
                <View style={styles.inputContainer}>
                  <TextInput 
                    editable={true}
                    multiline={true}
                    placeholder={l.training.writeHereYourEssay}
                  />
                </View>
                <TouchableOpacity onPress={this.dispatchProps.navigateToEssayFinish}>
                  <Button
                    titleButton={l.common.send}
                  />
                </TouchableOpacity>
              </View>
          </KeyboardAwareScrollView>
        </View>
      </ScrollView>
    )
  }
}

const styles = styleSheetCreate({
  bgStyle: {
    flexGrow: 1,
    justifyContent: 'center',
    backgroundColor: Colors.greyE5,
  } as ViewStyle,
  content: {
    alignItems: 'center',
    justifyContent: 'space-around',
  } as ViewStyle,
  titleContainer: {
    paddingTop: windowWidth * .02,
    alignItems: 'flex-start',
    width: windowWidth * .8,
  } as ViewStyle,
  inputContainer: {
    marginVertical: windowWidth * .04,
    height: windowHeight * .6,
    padding: windowWidth * .02,
    backgroundColor: Colors.white,
    borderWidth: windowWidth * .005,
    borderColor: Colors.greyDB,
    borderStyle: 'solid',
    borderRadius: windowWidth * .04,
    width: windowWidth * .8,
  } as ViewStyle,
  title: {
    color: Colors.darkGrey9F,
		fontSize: windowWidth * .045,
  } as TextStyle,
  description: {
    paddingLeft: windowWidth * .04,
    color: Colors.dark5E,
		fontSize: windowWidth * .045,
  } as TextStyle,
  underline: {
    backgroundColor: Colors.greyDB,
    marginTop: windowWidth * .02,
    height: windowWidth * .005,
    width: windowWidth * .8,
  } as ViewStyle,
})
