import React from 'react'
import {
  View,
  ViewStyle,
  Text,
  TextStyle,
  ListRenderItemInfo,
  Platform,
  FlatList,
  ScrollView,
} from 'react-native'
import { Dispatch } from 'redux'
import _ from 'underscore'
import { BaseReduxComponent } from '../../../core/BaseComponent'
import { NavigationActions } from '../../../navigation/navigation'
import { connectAdv } from '../../../core/store/connectAdv'
import {IAppState} from '../../../core/store/appState'
import { styleSheetCreate, progressStatus, fillterLevel, getStatus } from "../../../common/utils";
import { Colors, windowWidth } from '../../../core/theme'
import { localization as l } from '../../../common/localization/localization'
import { PlainHeader } from '../../../common/components/Headers'
import { LevelSelect } from '../../../common/components/LevelSelect'
import { LevelItem } from '../../../common/components/LevelItem'
import { LoadingView } from '../../../common/components/LoadingView'
import { Profile } from '../../../core/api/generated/dto/CommonResponse.g'
import { TrainingActions } from '../trainingActions'

interface IStateProps {
  vocabularyResult: any
  error: {} | string | null
  isLoading: boolean
  user:Profile
}

interface IDispatchProps {
  navigateToAssumptionInstr (): void
  navigateToVocabularyTopic (item: any): void
}

interface IState {
  levelTitle:string
  level:string
}

interface IProps {
  
}
interface IListItem extends ListRenderItemInfo<ITrainingVocabulatyLevelThemeItem> {
  item: ITrainingVocabulatyLevelThemeItem
}

interface ITrainingVocabulatyLevelThemeItem {
  theme: string
  state: number
  onPress(): void
  exercises: any  
  state_ex: any
}

@connectAdv(
  (state: IAppState): IStateProps => ({
    vocabularyResult: state.training.vocabularyResult,
    error: state.training.error,
    user: state.common.user,
    isLoading: state.training.isLoading,
  }),
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToAssumptionInstr(): void {
      dispatch(NavigationActions.navigateToAssumptionInstr(1))
    },    
    navigateToVocabularyTopic(item): void {
      dispatch(TrainingActions.selectLevel(item));
      dispatch(NavigationActions.navigateToVocabularyTopic(item))
    },
  })
)
export class VocabularyLevels extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  static navigationOptions = PlainHeader(l.training.vocabulary, true, false)

  state = {
    levelTitle:'',
    level:'0',
  }

  keyExtractor = (item: ITrainingVocabulatyLevelThemeItem, index: number): string => `${index}`;

  renderItem = ({item, index}: IListItem): JSX.Element => (
    <LevelItem
      category={item.theme}
      data={item}
      index={index}
      onPress={this.dispatchProps.navigateToVocabularyTopic}
      theory={item.state_ex && item.state_ex[1] ? true : false}
      progress={progressStatus(item)}
      status={item.state != 3 ? getStatus(item.state) : ""}
    />
  )

  changeLevel = (level: string, levelTitle: string): void => {

    this.setState({levelTitle, level});
  }

  getContent = (): any => {
    const { vocabularyResult } = this.stateProps;

    return (
      vocabularyResult ?
      vocabularyResult.length > 0 ?
          this.listContent()
        : <View style={styles.noSatutic}><Text>Упражнений нет</Text></View>
      : null
    );
  }

  listContent = (): JSX.Element => {
    const { vocabularyResult, user } = this.stateProps;
    const statusAssuumtion = true;
    const {levelTitle} = this.state;
    let { level } = this.state;

    if (level == "0") {
      level = user.userLevel;
    }

    return(
      <ScrollView>
        <View style={styles.content}>
          <FlatList
            extraData={[vocabularyResult, level]}
            data={fillterLevel(vocabularyResult, statusAssuumtion, level)}
            renderItem={this.renderItem}
            scrollEnabled={false}
            ListEmptyComponent={
              <View style={styles.noSatutic}>
                <Text style={{textAlign: "center"}}>На {levelTitle != "" ? levelTitle : "данном"} уровне упражнений нет</Text>
              </View>
            }
            keyExtractor={this.keyExtractor}
          />
        </View>
      </ScrollView>
    );
  }

  render(): JSX.Element {
    const { isLoading, user } = this.stateProps;

    return (
      <View style={styles.container} >
        <LoadingView isLoading={isLoading} />
        <LevelSelect level={user.userLevel}  changeLevel={this.changeLevel} />
        {this.getContent()}
      </View>
    );
  }
}

const styles = styleSheetCreate({
  container: {
    flex: 1,
    backgroundColor: Colors.greyE5,
  } as ViewStyle,
  noSatutic: {
    padding:15,
    flex:1,
    justifyContent: 'center',
    alignItems:  'center',
  } as ViewStyle,
  content:{
    borderRadius: windowWidth * .03,
    overflow: 'hidden',
    alignItems: 'flex-start',
    backgroundColor: Colors.white,
    marginBottom: windowWidth * .07,
    marginHorizontal: windowWidth * .04,
    zIndex: -2,
    ...Platform.select({
      ios: {
        shadowRadius: 15,
        shadowOpacity: 0.3,
        shadowOffset: { width: 1, height: 4 },
      },
      android: {
        elevation: 8
    }}),
  } as ViewStyle,
  levelsContainer: {
    padding: windowWidth * .05,
    justifyContent: 'space-around',
  } as ViewStyle,
  activeTitle: {
    color: Colors.dark5E,
    fontSize: windowWidth * .039,
  } as TextStyle,
  notActiveTitile: {
    color: Colors.greyCE,
    fontSize: windowWidth * .039,
  } as TextStyle,
})
