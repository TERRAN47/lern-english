import React from 'react'
import {
	View,
	ViewStyle,
	Text,
	TextStyle,
	TouchableOpacity,
	Platform,
	ScrollView,
} from 'react-native'
import { BaseReduxComponent } from '../../../core/BaseComponent'
import { NavigationActions } from '../../../navigation/navigation'
import { Dispatch } from 'redux'
import { connectAdv } from '../../../core/store/connectAdv'
import { Colors, windowWidth } from '../../../core/theme'
import { styleSheetCreate } from '../../../common/utils'
import { localization as l } from '../../../common/localization/localization'
import { PlainHeader } from '../../../common/components/Headers'
import { Answer } from '../../../common/components/Answer'

interface IStateProps {

}

interface IDispatchProps {
  navigateToAssumptionInstr (): void,
}

interface IState {
  
}

interface IProps {
  
}

@connectAdv(
  null,
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToAssumptionInstr(): void {
      dispatch(NavigationActions.navigateToAssumptionInstr(1))
    },
  })
)
export class VocabularyTesting extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {
 
	state = {
		
	}
    
	static navigationOptions = PlainHeader(l.training.testing, true, false)
	
  render(): JSX.Element {
    return (
			<ScrollView contentContainerStyle={styles.bgStyle}>
				<View style={styles.container} >
					<View style={styles.content} >
						<Text style={styles.description}>... is a large, round fruit with hard yellow or green skin, sweet flesh, and a lot of seeds</Text>
						<View>
							<TouchableOpacity>
								{/* answerState - isSelected, isNotSelected, isError, isCorrect, isNotSelectedNoCircle */}
								<Answer title={'mango'} answerState={'isSelected'}/>
							</TouchableOpacity>
							<TouchableOpacity>
								<Answer title={'wine'} answerState={'isNotSelected'}/>
							</TouchableOpacity>
							<TouchableOpacity>
								<Answer title={'onion'} answerState={'isNotSelected'}/>
							</TouchableOpacity>
						</View>
					</View>  
					<View>
						<Text style={styles.numberSlide}><Text>2</Text>/<Text>25</Text></Text>
					</View>
				</View> 
			</ScrollView>
    )
  }
}

const styles = styleSheetCreate({
	bgStyle: {
    flexGrow: 1,
    justifyContent: 'center',
    backgroundColor: Colors.blueEC,
	} as ViewStyle,
  container: {
		flexGrow: 1,
		marginTop: windowWidth * .04,
    alignItems: 'center',
		justifyContent: 'space-around',
  } as ViewStyle,
  content: {
    width: windowWidth * .8,
    height: windowWidth * 1.12,
    backgroundColor: Colors.white,
		borderRadius: windowWidth * .03,    
		alignItems: 'center',
		justifyContent: 'space-between',
		paddingHorizontal: windowWidth * .07,
		paddingTop: windowWidth * .12,
		paddingBottom: windowWidth * .07,
		...Platform.select({
      ios: {
          shadowRadius: 8,
          shadowOpacity: 0.1,
          shadowOffset: {width: 1, height: 4},
      },
      android: {
        elevation: 8
    }}),
	} as ViewStyle,
	description: {
		fontSize: windowWidth * .045,
		color: Colors.darkGrey9F,
	} as TextStyle,
	buttonStyle: {
		paddingHorizontal: windowWidth * .05,
	} as ViewStyle,	
	numberSlide: {
		marginTop: windowWidth * .04,
		color: Colors.white,
		fontSize: windowWidth * .045,
	} as TextStyle,	
})
