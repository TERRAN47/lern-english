import React from 'react'
import {
  View,
  Text,
  TextStyle,
  TouchableOpacity,
  Image,
  ImageStyle,
  Platform,
  ViewStyle,
  ScrollView,
} from 'react-native'
import { Dispatch } from 'redux'
import { BaseReduxComponent } from '../../../core/BaseComponent'
import { Colors, windowWidth } from '../../../core/theme'
import { connectAdv } from '../../../core/store/connectAdv'
import { NavigationActions } from '../../../navigation/navigation'
import { styleSheetCreate } from '../../../common/utils'
import { localization as l } from '../../../common/localization/localization'
import { ImageResources } from '../../../common/ImageResources.g'
import { Button } from '../../../common/components/Button'
import { PlainHeader } from '../../../common/components/Headers'

interface IStateProps {

}

interface IDispatchProps {
  navigateToVocabularyAssumption1(): void
}

interface IState {

}

interface IProps {

}
@connectAdv(
  null,
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToVocabularyAssumption1(): void {
      dispatch(NavigationActions.navigateToVocabularyAssumption1())
    },
  })
)
export class VocabularyAssumptionInstr extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  static navigationOptions = PlainHeader(l.training.learningWords, true, false)

  render(): JSX.Element {
    return (
      <ScrollView contentContainerStyle={styles.bgStyle}>
        <View style={styles.container}>
          <View style={styles.content}>
            <Text style={styles.contentTitle}>{l.training.exerciseInstruction}</Text>
            <View style={styles.taskBlock}>
              <View style={styles.taskChild}>
                <Image
                  style={styles.imageIcon}
                  source={ImageResources.volumeIcon}
                />
                <Text style={styles.contentText}>1. {l.training.listenSpeaker}</Text>
              </View>
              <View style={styles.taskChild}>
                <Image
                  style={styles.imageIcon}
                  source={ImageResources.microIcon}
                />
                <Text style={styles.contentText}>2. {l.training.pressRecord}</Text>
              </View>
              <View style={styles.taskChild}>
                <Image
                  style={styles.imageIcon}
                  source={ImageResources.volumeIcon}
                />
                <Text style={styles.contentText}>3. {l.training.listenYourRecording}</Text>
              </View>
            </View>
            <TouchableOpacity  onPress={this.dispatchProps.navigateToVocabularyAssumption1}>
              <Button titleButton={l.common.understandable} />
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    )
  }
}

const styles = styleSheetCreate({
  bgStyle: {
    flexGrow: 1,
    justifyContent: 'center',
    backgroundColor: Colors.greyE5,
  } as ViewStyle,
  container: {
    flexGrow: 1,
    justifyContent:'center',
    padding: windowWidth * .05,
  } as ViewStyle,
  taskBlock:{
    paddingVertical: windowWidth * .02,
  } as ViewStyle,
  taskChild:{
    flexDirection:'row',
    justifyContent:'flex-start',
    alignItems:'center',
    paddingVertical: windowWidth * .03,
  } as ViewStyle,
  imageIcon:{
    width: windowWidth * .08,
    height: windowWidth * .08,
    marginRight: windowWidth * .05,
    resizeMode: 'contain',
  } as ImageStyle,
  contentText:{
    color:Colors.black60,
    fontSize: windowWidth * .042,
    lineHeight: windowWidth * .075,
  } as TextStyle,
  contentTitle:{
    fontWeight:'bold',
    color:Colors.black60,
    fontSize: windowWidth * .046,
    padding: windowWidth * .07,
  } as TextStyle,
  content:{
    padding: windowWidth * .05,
    backgroundColor:Colors.white,
    alignItems:'center',
    justifyContent:'space-between',
    borderRadius: windowWidth * .05,
    ...Platform.select({
      ios: {
          shadowRadius: 6,
          shadowOpacity: 0.1,
          shadowOffset: {width: 0, height: 3},
      },
      android: {
        elevation: 8
    }}),
  } as ViewStyle,
})