import React from "react";
import { BaseReduxComponent } from "../../../core/BaseComponent";
import {
  View,
  Text,
  TextStyle,
  Platform,
  TouchableOpacity,
  ViewStyle,
} from "react-native";
import { Dispatch } from "redux";
import { connectAdv } from "../../../core/store/connectAdv";
import { NavigationActions } from "../../../navigation/navigation";
import { Colors, windowHeight, windowWidth } from "../../../core/theme";
import { styleSheetCreate } from "../../../common/utils";
import { localization as l } from "../../../common/localization/localization";
import { Button } from "../../../common/components/Button";
import { PlainHeader } from "../../../common/components/Headers";
import { trainingActionsAsync } from "../trainingActionsAsync";
import {IAppState} from "../../../core/store/appState";
import {
  NavigationRoute,
  NavigationScreenProp,
} from "react-navigation";

interface IStateProps {
  auditionResult: any;
  error: {} | string | null;
  netStatus: boolean;
}

interface IDispatchProps {
  navigateToAuditionLevels (): void;
  getAudition (netStatus: boolean): void;
}

interface IState {

}

interface IProps {
  navigation: NavigationScreenProp<NavigationRoute<any>>;
}
@connectAdv(
  (state: IAppState): IStateProps => ({
    auditionResult: state.training.auditionResult,
    error: state.training.error,
    netStatus: state.common.netStatus,
  }),
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToAuditionLevels(): void {
      dispatch(NavigationActions.navReToBack());
      dispatch(NavigationActions.navigateToAuditionLevels());
    },
    getAudition(netStatus: boolean): void {
      //@ts-ignore
      dispatch(trainingActionsAsync.getAudition(netStatus));
    }
  })
)

export class AuditionInstruction extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  static navigationOptions = PlainHeader("Аудирование", true, false);

  componentDidMount = (): any => {
    const {netStatus} = this.stateProps;

    this.dispatchProps.getAudition(netStatus);
  }

  render(): JSX.Element {
    const {auditionResult, error} = this.stateProps;
    console.log("Аудирование", auditionResult, error);

    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <View>
            <Text style={styles.contentTitle}>Инструкция</Text>
            <Text style={styles.contentText}>
              По итогам тестирования Вам подключен соответствующий уровень. Начните обучение с изучения тем по порядку.
              В каждой теме Вы изучаете и прослушиваете новые слова и затем тренируете их в упражнениях.
              Если Вы хотите повторить/изучить предыдущие уровни, выберите их из списка.
            </Text>
          </View>
          <TouchableOpacity  onPress={this.dispatchProps.navigateToAuditionLevels}>
            <Button titleButton={l.common.understandable} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = styleSheetCreate({
  container: {
    flex: 1,
    backgroundColor: Colors.greyE5,
    justifyContent: "center",
    padding: windowWidth * .05,
  } as ViewStyle,
  contentText: {
    color: Colors.dark5E,
    fontSize: windowWidth * .039,
    lineHeight: windowWidth * .075,
  } as TextStyle,
  contentTitle: {
    paddingBottom: windowWidth * .07,
    fontWeight: "500",
    color: Colors.dark5E,
    fontSize: windowWidth * .042,
    textAlign: "center",
  } as TextStyle,
  content: {
    padding: windowHeight * .03,
    height: windowHeight * .8,
    backgroundColor: Colors.white,
    alignItems: "center",
    justifyContent: "space-between",
    borderRadius: 15,
    ...Platform.select({
      ios: {
          shadowRadius: 8,
          shadowOpacity: 0.1,
          shadowOffset: {width: 1, height: 4},
      },
      android: {
        elevation: 8
    }}),
  } as ViewStyle,
});
