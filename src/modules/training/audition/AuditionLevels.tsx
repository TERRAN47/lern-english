import React from "react";
import {
  View,
  ViewStyle,
  Text,
  Platform,
  FlatList,
  ListRenderItemInfo,
  ScrollView,
} from "react-native";
import { Dispatch } from "redux";
import _ from "underscore";
import { BaseReduxComponent } from "../../../core/BaseComponent";
import { NavigationActions } from "../../../navigation/navigation";
import { connectAdv } from "../../../core/store/connectAdv";
import {IAppState} from "../../../core/store/appState";
import { Colors, windowWidth } from "../../../core/theme";
//import { localization as l } from '../../../common/localization/localization'
import { styleSheetCreate, progressStatus, fillterLevel, getStatus } from "../../../common/utils";
import { PlainHeader } from "../../../common/components/Headers";
import { LevelItem } from "../../../common/components/LevelItem";
import { LoadingView } from "../../../common/components/LoadingView";
import { Profile } from "../../../core/api/generated/dto/CommonResponse.g";
import { LevelSelect } from "../../../common/components/LevelSelect";
import { TrainingActions } from "../trainingActions";
interface IStateProps {
  auditionResult: any;
  error: {} | string | null;
  isLoading: boolean;
  user: Profile;
}

interface IDispatchProps {
  navigateToAuditionTopic (item: any): void;
}

interface IListItem extends ListRenderItemInfo<ITrainingAuditionLevelThemeItem> {
  item: ITrainingAuditionLevelThemeItem;
}

interface ITrainingAuditionLevelThemeItem {
  theme: string;
  state: number;
  onPress(): void;
  exercises: any;
  state_ex: any;
}

interface IState {
  level: string;
  levelTitle: string;
}

interface IProps {

}

@connectAdv(
  (state: IAppState): IStateProps => ({
    auditionResult: state.training.auditionResult,
    error: state.training.error,
    isLoading: state.training.isLoading,
    user: state.common.user
  }),
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToAuditionTopic(item: any): void {
      dispatch(TrainingActions.selectLevel(item));
      dispatch(NavigationActions.navigateToAuditionTopic(item));
    },
  })
)
export class AuditionLevels extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  static navigationOptions = PlainHeader("Аудирование", true, false);

  constructor(props: any) {
    super(props);
    this.state = {
      level: "0",
      levelTitle: ""
    };
  }

  keyExtractor = (item: ITrainingAuditionLevelThemeItem, index: number): string => `${index}`;

  renderItem = ({item, index}: IListItem): JSX.Element => (
    <LevelItem
      category={item.theme}
      data={item}
      index={index}
      onPress={this.dispatchProps.navigateToAuditionTopic}
      theory={item.state_ex && item.state_ex[1] ? true : false}
      progress={progressStatus(item)}
      status={item.state != 3 ? getStatus(item.state) : ""}
    />
  )

  changeLevel = (level: string, levelTitle: string): void => {
    this.setState({levelTitle, level});
  }

  getContent = (): any => {
    const { auditionResult } = this.stateProps;

    return (
      auditionResult ?
        auditionResult.length > 0 ?
          this.listContent()
        : <View style={styles.noSatutic}><Text>Упражнений нет</Text></View>
      : null
    );
  }

  listContent = (): JSX.Element => {
    const { auditionResult, user } = this.stateProps;
    const statusAssuumtion = true;
    const { levelTitle } = this.state;
    let { level } = this.state;

    if (level == "0") {
      level = user.userLevel;
    }

    return(
      <ScrollView>
        <View style={styles.content}>
          <FlatList
            extraData={auditionResult}
            data={fillterLevel(auditionResult, statusAssuumtion, level)}
            renderItem={this.renderItem}
            scrollEnabled={false}
            ListEmptyComponent={
              <View style={styles.noSatutic}>
                <Text style={{textAlign: "center"}}>На уровне {levelTitle != "" ? levelTitle : "данном"} упражнений нет</Text>
              </View>
            }
            keyExtractor={this.keyExtractor}
          />
        </View>
      </ScrollView>
    );
  }

  render(): JSX.Element {
    const { user, isLoading, auditionResult } = this.stateProps;
    console.log("auditionResult", auditionResult);

    return (
      <View style={styles.container} >
        <LoadingView isLoading={isLoading} />
        <LevelSelect level={user.userLevel}  changeLevel={this.changeLevel} />
        {this.getContent()}
      </View>
    );
  }
}

const styles = styleSheetCreate({
  container: {
    flex: 1,
    backgroundColor: Colors.greyE5,
  } as ViewStyle,
  noSatutic: {
    padding: 15,
    flex: 1,
    zIndex: -1,
    justifyContent: "center",
    alignItems:  "center",
  } as ViewStyle,
  content: {
    borderRadius: windowWidth * .03,
    overflow: "hidden",
    alignItems: "flex-start",
    backgroundColor: Colors.white,
    marginBottom: windowWidth * .07,
    marginHorizontal: windowWidth * .04,
    zIndex: -2,
    ...Platform.select({
      ios: {
        shadowRadius: 15,
        shadowOpacity: 0.3,
        shadowOffset: { width: 1, height: 4 },
      },
      android: {
        elevation: 8
    }}),
  } as ViewStyle,
});