
import React from "react";
import { BaseReduxComponent } from "../../../core/BaseComponent";
import {
  View,
  Alert,
  Text,
  ViewStyle,
  FlatList,
  ListRenderItemInfo,
} from "react-native";
import { Dispatch } from "redux";
import { connectAdv } from "../../../core/store/connectAdv";
import { NavigationActions } from "../../../navigation/navigation";
import { styleSheetCreate } from "../../../common/utils";
import { Colors, windowWidth } from "../../../core/theme";
import { TopicItem } from "../../../common/components/topicItem";
import { PlainHeader } from "../../../common/components/Headers";
import {
  NavigationRoute,
  NavigationScreenProp,
  NavigationScreenConfigProps,
  NavigationStackScreenOptions
} from "react-navigation";
import { TrainingActions } from "../trainingActions";
import {IAppState} from "../../../core/store/appState";
import { writeAudioFile, getAudioFile } from "../../../common/helpers/appFiles";
import * as Progress from "react-native-progress";

interface IStateProps {
  selectLevel: any;
}

interface IDispatchProps {
  navigateToAuditionAssumptionInstr(data: any): void;
  navigateToAuditionTesting1(data: any): void;
  navigateToAuditionTesting2(data: any): void;
}

interface ILevelTopic {
  category: string;
  title: string;
  status: string;
  access: boolean;
  onPress(): void;
}

interface IListItem extends ListRenderItemInfo<ILevelTopic> {
  item: ILevelTopic;
}

interface IState {
  loadStatus: boolean;
  progressBar: number;
}

interface IProps {
  navigation: NavigationScreenProp<NavigationRoute<any>>;
}

@connectAdv(
  (state: IAppState): IStateProps => ({
    selectLevel: state.training.selectLevel
  }),
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToAuditionAssumptionInstr(data: any): void {

      if (data && data.exercises && data.exercises.length > 0 && data.exercises[0]) {
        const assumption = {...data, section: 28, assumption: 1, selAssumption: data.exercises[0], audioPath: data.audioPath};
        dispatch(TrainingActions.selectAssumption(assumption));
        dispatch(NavigationActions.navigateToAuditionAssumptionInstr());
      } else {
        Alert.alert("Внимание!", "В данном разделе нет упражнений");
      }
    },
    navigateToAuditionTesting1(data: any): void {
      if (data && data.exercises && data.exercises.length > 0) {
        const assumption = {...data, section: 28, assumption: 2, selAssumption: data.exercises[0], audioPath: data.audioPath};
        dispatch(TrainingActions.selectAssumption(assumption));
        dispatch(NavigationActions.navigateToAuditionTesting1());
      } else {
        Alert.alert("Внимание!", "В данном разделе нет упражнений");
      }
    },
    navigateToAuditionTesting2(data: any): void {
      if (data && data.exercises && data.exercises.length > 0) {
        const assumption = {...data, section: 28, assumption: 3, selAssumption: data.exercises[1], audioPath: data.audioPath};
        dispatch(TrainingActions.selectAssumption(assumption));
        dispatch(NavigationActions.navigateToAuditionTesting2());
      } else {
        Alert.alert("Внимание!", "В данном разделе нет упражнений");
      }
    },
  })
)
export class AuditionTopic extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  static navigationOptions = ({ navigation }: NavigationScreenConfigProps): NavigationStackScreenOptions => {
    return PlainHeader(`${navigation.state.params !== undefined ? navigation.state.params.theme : ""}`, true, false);
  }

  constructor(props: any) {
    super(props);
    this.state = {
      loadStatus: false,
      progressBar: 0.1,
    };
  }

  array: any = [];
  loadInterval: any = null;

  componentDidMount = async (): Promise<any> => {
    const {selectLevel} = this.stateProps;

    const findAudio = await this.getAudio(selectLevel);

    if (findAudio) {
      selectLevel.audioPath = findAudio;
    } else {
      this.setState({loadStatus: true});
      this.uploudAudio(selectLevel);
    }
  }

  getAudio = async (item: any): Promise<any> => {
    const findAudio = await getAudioFile(item.audio.slice(14));

    return findAudio;
  }

  uploudAudio = async (item: any): Promise<any> => {
    this.loadInterval = setInterval(async () => {
      let { progressBar } = this.state;
      const { loadStatus } = this.state;
      const {selectLevel} = this.stateProps;

      if (progressBar <= 1 && loadStatus) {
        progressBar += 0.025;
        this.setState({progressBar});
      } else {
        clearInterval(this.loadInterval);
        setInterval(async(): Promise<any> => {
          const findAudio = await this.getAudio(selectLevel);
          selectLevel.audioPath = findAudio;
          this.setState({loadStatus: false});
        }, 7000);
      }
    }, 400);

    const loadAudio = await writeAudioFile(item);

    if (loadAudio !== null) {
      item.audioPath = await this.getAudio(item);
      this.setState({progressBar: 1});
    }
    console.log("loadAudio", loadAudio);
  }

  navigateToAuditionAssumptionInstr = (): any => {
    const {selectLevel} = this.stateProps;
    this.dispatchProps.navigateToAuditionAssumptionInstr(selectLevel);
  }

  navigateToAuditionTestingOne = (): any => {
    const {selectLevel} = this.stateProps;
    this.dispatchProps.navigateToAuditionTesting1(selectLevel);
  }

  navigateToAuditionTestingTwo = (): any => {
    const {selectLevel} = this.stateProps;
    this.dispatchProps.navigateToAuditionTesting2(selectLevel);
  }

  topicItems = (data: any): any => {
    return[
      {
        category: "Аудиозапись",
        title: "",
        access: true,
        status: data.state_ex && data.state_ex[1] || data.state == 1 ? "isDone" : "isInToDo",
        onPress: this.navigateToAuditionAssumptionInstr,
      },
      {
        category: "Упражнение 1",
        title: "",
        access: data.state_ex && data.state_ex[1] || data.state == 1 ? true : false,
        status: data.state_ex && data.state_ex[2] || data.state == 1 ? "isDone" : "isInToDo",
        onPress: this.navigateToAuditionTestingOne,
      },
      {
        category: "Упражнение 2",
        title: "",
        access: data.state_ex && data.state_ex[2] || data.state == 1 ? true : false,
        status: data.state_ex && data.state_ex[3] || data.state == 1 ? "isDone" : "isInToDo",
        onPress: this.navigateToAuditionTestingTwo,
      }
    ];
  }

  keyExtractor = (item: ILevelTopic): string => item.category;

  renderItem = ({ item: { category, access, title, status, onPress }, index }: IListItem): JSX.Element => (
    <TopicItem
      category={category}
      title={title}
      access={access}
      status={status}
      onPress={onPress}
      isFirstItem={index}
      isLastItem={index === this.array.length - 1}
    />
  )

  getContent = (): JSX.Element => {
    const {loadStatus, progressBar} = this.state;
    const {selectLevel} = this.stateProps;
    if (selectLevel) { this.array = this.topicItems(selectLevel); }
    console.log("selectLevel", selectLevel);

    if (!loadStatus) {
      return (
        <FlatList
          style={styles.content}
          extraData={selectLevel}
          data={this.array}
          renderItem={this.renderItem}
          keyExtractor={this.keyExtractor}
        />
      );
    } else {
      return(
        <View style={styles.progressBar}>
          <Text>Загрузка аудио записи</Text>
          <Progress.Bar
            borderRadius={1}
            height={14}
            progress={progressBar}
            color={Colors.blueEC}
            width={windowWidth * .8}
          />
        </View>
      );
    }
  }

  render(): JSX.Element {

    return (
      <View style={styles.container}>
        {this.getContent()}
      </View>
    );
  }
}

const styles = styleSheetCreate({
  progressBar: {
    flex: 1,
    width: windowWidth,
    justifyContent: "center",
    alignItems: "center"
  } as ViewStyle,

  container: {
    flex: 1,
    backgroundColor: Colors.greyE5,
  } as ViewStyle,
  content: {
    paddingTop: windowWidth * .04,
  } as ViewStyle,
});