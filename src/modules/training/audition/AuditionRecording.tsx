import React from "react";
import {
  View,
  ViewStyle,
  Text,
  TextStyle,
  TouchableOpacity,
  Platform,
} from "react-native";
import { Dispatch } from "redux";
import { BaseReduxComponent } from "../../../core/BaseComponent";
import { styleSheetCreate } from "../../../common/utils";
import {
  Colors,
  windowWidth,
 } from "../../../core/theme";
import { localization as l } from "../../../common/localization/localization";
import { NavigationActions } from "../../../navigation/navigation";
import {IAppState} from "../../../core/store/appState";
import { connectAdv } from "../../../core/store/connectAdv";
import { ButtonNoBg } from "../../../common/components/ButtonNoBg";
import { AudioPlayer } from "../../../common/components/AudioPlayer";
import { PlainHeader } from "../../../common/components/Headers";
import { TrainingActions } from "../trainingActions";
import {requestsRepository} from "../../../core/api/requestsRepository";

interface IStateProps {
  selectAssumption: any;
  auditionResult: any;
}

interface IProps {

}

interface IDispatchProps {
  navReToBack(data: any, auditionResult: any): void;
}

interface IState {

}

@connectAdv(
  (state: IAppState): IStateProps => ({
    selectAssumption: state.training.selectAssumption,
    auditionResult: state.training.auditionResult
  }),
  (dispatch: Dispatch): IDispatchProps => ({
    navReToBack(data: any, auditionResult: any): void {
      const result = {
        section: data.section,
        ex_done: 0,
        ex_id: data.ex_id,
        ex_item_id: data.assumption,
      };

      let auditions = [];

      auditions = auditionResult.filter((item: any) => {
        if (item.level === data.level) {
          return item;
        }
      });

      if ( auditions[data.index].state_ex) {
        auditions[data.index].state_ex[1] = 1;
        data.state_ex[1] = 1;
      } else {
        auditions[data.index].state_ex = {1: 1};
        data.state_ex = {1: 1};
      }

      dispatch(TrainingActions.updateAudition([...auditionResult]));
      dispatch(TrainingActions.selectLevel(data));

      dispatch(NavigationActions.navReToBack());
      dispatch(NavigationActions.navReToBack());
      requestsRepository.trainingApiRequest.assumptReady(result);
    },
  })
)
export class AuditionRecording extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {
  static navigationOptions = PlainHeader("Аудирование", true, false);
  navReToBack = (): any => {
    const {selectAssumption, auditionResult} = this.stateProps;
    this.dispatchProps.navReToBack(selectAssumption, auditionResult);
  }
  render(): JSX.Element {
    const {selectAssumption} = this.stateProps;

    return(
      <View style={styles.container}>
        <View>
          <Text style={styles.text}>Прослушайте аудиозапись затем нажмите «Продолжить»</Text>
          <AudioPlayer audioPath={selectAssumption.audioPath} record={true} white={true}/>
        </View>
        <TouchableOpacity
          style={styles.button}
          onPress={this.navReToBack}
        >
          <ButtonNoBg
            titleButton={l.common.continue}
            width={windowWidth * .9}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = styleSheetCreate({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: Colors.blueEC,
    paddingHorizontal: windowWidth * .05,
    paddingVertical: windowWidth * .1,
  } as ViewStyle,
  text: {
    fontSize: windowWidth * .042,
    fontWeight: "500",
    lineHeight: windowWidth * .08,
    color: Colors.white,
    textAlign: "center",
    paddingBottom: windowWidth * .1,
  } as TextStyle,
  button: {
    borderRadius: windowWidth * .03,
    backgroundColor: Colors.blueEC,
    ...Platform.select({
      ios: {
        shadowRadius: 15,
        shadowOpacity: 0.3,
        shadowOffset: {width: 1, height: 4},
      },
      android: {
        elevation: 4
    }}),
  } as ViewStyle,
});