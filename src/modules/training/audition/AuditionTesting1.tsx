import React from "react";
import {
  View,
  Text,
  ViewStyle,
  TextStyle,
  ScrollView,
} from "react-native";
import { BaseReduxComponent } from "../../../core/BaseComponent";
import { NavigationActions } from "../../../navigation/navigation";
import { Colors, windowWidth } from "../../../core/theme";
import { styleSheetCreate } from "../../../common/utils";
//import { localization as l } from '../../../common/localization/localization'
import { PlainHeader } from "../../../common/components/Headers";
import {IAppState} from "../../../core/store/appState";
import { Dispatch } from "redux";
import { connectAdv } from "../../../core/store/connectAdv";
import Carousel from "react-native-snap-carousel";
import { AuditionAssemtOne } from "../../../common/components/AuditionAssemtOne";
import { AudioPlayer } from "../../../common/components/AudioPlayer";
import { TrainingActions } from "../trainingActions";
import {requestsRepository} from "../../../core/api/requestsRepository";

interface IStateProps {
  selectAssumption: any;
  selectLevel: any;
  auditionResult: any;
}

interface IDispatchProps {
  navigateToTestingSuccessfulFinish(data: any, grammarResult: any, result: any): void;
  navigateToFinishWithErrors(seeErrors: any, testingAgain: any): void;
  goBack(): void;
  restart(): void;
}

interface IState {
  testFinesh: boolean;
  scrollStatus: boolean;
  pageNum: number;
}

interface IProps {

}

@connectAdv(
  (state: IAppState): IStateProps => ({
    selectAssumption: state.training.selectAssumption,
    selectLevel: state.training.selectAssumption,
    auditionResult: state.training.auditionResult
  }),
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToTestingSuccessfulFinish(data: any, auditionResult: any, result: any): void {

      dispatch(TrainingActions.updateAudition([...auditionResult]));
      dispatch(TrainingActions.selectLevel(data));
      dispatch(NavigationActions.navigateToTestingSuccessfulFinish(2));

      requestsRepository.trainingApiRequest.assumptReady(result);
    },
    goBack(): void {
      dispatch(NavigationActions.navReToBack());
    },
    restart(): void {
      dispatch(NavigationActions.navReToBack());
      dispatch(NavigationActions.navReToBack());
    },
    navigateToFinishWithErrors(seeErrors: any, testingAgain: any): void {
      dispatch(NavigationActions.navigateToFinishWithErrors({seeErrors, testingAgain}));
    },
  })
)
export class AuditionTesting1 extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {
  static navigationOptions = PlainHeader("Упражнения 1", true, false);

  constructor(props: any) {
    super(props);
    this.state = {
      testFinesh: false,
      scrollStatus: false,
      pageNum: 0
    };
  }

  _carousel: any;

  modal = (): void => {
    const {selectAssumption, selectLevel, auditionResult} = this.stateProps;
    console.log("selectAssumption", selectAssumption);
    this.setState({testFinesh: true});

    let noError = true;

    selectAssumption.selAssumption.items.forEach((elem: any): any => {
      if (!elem[elem.length - 1]) {
        return noError = false;
      }
    });

    if (noError) {
      let allComplit = false;
      if (selectAssumption.state_ex === null) { selectAssumption.state_ex = {}; }

      selectAssumption.exercises.forEach((el: any, index: number): any => {
        if (selectAssumption.state_ex[index + 2] == 1) {
          allComplit = true;
        } else {
          return allComplit = false;
        }
      });

      const data = {
        section: selectAssumption.section,
        ex_id: selectAssumption.ex_id,
        ex_done: allComplit ? 1 : 0,
        ex_item_id: selectAssumption.assumption,
      };

      if (allComplit) {
        auditionResult[selectLevel.index].state = 1;
      }
      let auditions = [];

      auditions = auditionResult.filter((item: any) => {
        if (item.level === selectLevel.level) {
          return item;
        }
      });

      if (auditions[selectLevel.index].state_ex) {
        auditions[selectLevel.index].state_ex[data.ex_item_id] = 1;
        selectLevel.state_ex[data.ex_item_id] = 1;
      } else {
        auditions[selectLevel.index].state_ex = {[data.ex_item_id]: 1};
        selectLevel.state_ex = {[data.ex_item_id]: 1};
      }

      this.dispatchProps.navigateToTestingSuccessfulFinish(selectLevel, auditionResult, data);
    } else {
      this.dispatchProps.navigateToFinishWithErrors(this.seeErrors, this.testingAgain);
    }
  }

  seeErrors = (): void => {
    this._carousel.snapToItem(0);
    this.setState({scrollStatus: true});
    this.dispatchProps.goBack();
  }

  testingAgain = (): void => {
    this.dispatchProps.restart();
  }

  renderSlide = ({item, index}: any): any => {
    const {selectAssumption} = this.stateProps;
    const {testFinesh} = this.state;

    return (
      <AuditionAssemtOne
        finishSreen={this.modal}
        nextSlide={this.nextSlide}
        testFinesh={testFinesh}
        item={item}
        id={+(index) + 1}
        items={selectAssumption ? selectAssumption.selAssumption.items : []}
      />
    );
  }

  nextSlide = (): void => {
    this._carousel.snapToNext();
  }

  getContent = (): JSX.Element => {
    const {selectAssumption} = this.stateProps;
    const {scrollStatus} = this.state;
    console.log("selectAssumption", selectAssumption);

    if (selectAssumption && selectAssumption.selAssumption) {
      return (
        <View style={styles.contant}>
          <Carousel
            ref={( c: any ): any => { this._carousel = c; }}
            data={selectAssumption.selAssumption.items}
            renderItem={this.renderSlide}
            sliderWidth={windowWidth}
            itemWidth={windowWidth}
            layout={"tinder"}
            scrollEnabled={scrollStatus}
            layoutCardOffset={9}
            firstItem={0}
          />
          <AudioPlayer audioPath={selectAssumption.audioPath} record={true} white={true}/>
        </View>
      );
    } else {
      return <Text style={styles.notAssumptionText}>Упражнения нет</Text>;
    }
  }

  render(): JSX.Element {
    return (
      <View style={styles.container} >
        <ScrollView>
        {this.getContent()}
        </ScrollView>
      </View>
    );
  }
}

const styles = styleSheetCreate({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: Colors.blueEC,
    justifyContent: "space-around",
  } as ViewStyle,
  contant: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  } as ViewStyle,
  notAssumptionText: {
    textAlign: "center"
  } as TextStyle,
});