import React from "react";
import { BaseReduxComponent } from "../../../core/BaseComponent";
import {
  View,
  Text,
  TextStyle,
  TouchableOpacity,
  Image,
  ImageStyle,
  Platform,
  ViewStyle,
  ScrollView,
} from "react-native";
import {
  NavigationRoute,
  NavigationScreenProp,
} from "react-navigation";
import { connectAdv } from "../../../core/store/connectAdv";
import { NavigationActions } from "../../../navigation/navigation";
import { styleSheetCreate } from "../../../common/utils";
import { localization as l } from "../../../common/localization/localization";
import { Colors, windowWidth } from "../../../core/theme";
import { ImageResources } from "../../../common/ImageResources.g";
import { Dispatch } from "redux";
import { Button } from "../../../common/components/Button";
import { PlainHeader } from "../../../common/components/Headers";

interface IStateProps {

}

interface IDispatchProps {
  navigateToAuditionRecording(): void;
}

interface IState {

}

interface IProps {
  navigation: NavigationScreenProp<NavigationRoute<any>>;
}

@connectAdv(
  null,
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToAuditionRecording(): void {
      dispatch(NavigationActions.navigateToAuditionRecording());
    },
  })
)
export class AuditionAssumptionInstr extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  static navigationOptions = PlainHeader("Аудирование", true, false);

  navigateToAuditionRecording = (): any => {
    this.dispatchProps.navigateToAuditionRecording();
  }

  render(): JSX.Element {

    return (
      <ScrollView contentContainerStyle={styles.bgStyle}>
        <View style={styles.container}>
          <View style={styles.content}>
            <Text style={styles.contentTitle}>{`Инструкция к \n аудиозаписи и упражнениям`}</Text>
            <View style={styles.taskBlock}>
              <View style={styles.taskChild}>
                <Image
                  style={styles.imageIcon}
                  source={ImageResources.volumeIcon}
                />
                <Text style={styles.contentText}>{`1. Прослушайте \n аудиозапись`}</Text>
              </View>
              <View style={styles.taskChild}>
                <Image
                  style={styles.imageIcon}
                  source={{}}
                />
                <Text style={styles.contentText}>{`2. Перейдите к \n упражнениям `}</Text>
              </View>
              <View style={styles.taskChild}>
                <Image
                  style={styles.imageIcon}
                  source={{}}
                />
                <Text style={styles.contentText}>{`3. Выберите \n правильный ответ`}</Text>
              </View>
            </View>
            <TouchableOpacity  onPress={this.navigateToAuditionRecording}>
              <Button titleButton={l.common.understandable}/>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = styleSheetCreate({
  bgStyle: {
    flexGrow: 1,
    justifyContent: "center",
    backgroundColor: Colors.greyE5,
  } as ViewStyle,
  container: {
    flexGrow: 1,
    justifyContent: "center",
    padding: windowWidth * .05,
  } as ViewStyle,
  content: {
    flex: 1,
    padding: windowWidth * .05,
    backgroundColor: Colors.white,
    alignItems: "center",
    justifyContent: "space-between",
    borderRadius: windowWidth * .05,
    ...Platform.select({
      ios: {
          shadowRadius: 8,
          shadowOpacity: 0.1,
          shadowOffset: {width: 1, height: 4},
      },
      android: {
        elevation: 8
    }}),
  } as ViewStyle,
  taskBlock: {
    paddingVertical: windowWidth * .02,
  } as ViewStyle,
  taskChild: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    paddingVertical: windowWidth * .03,
  } as ViewStyle,
  imageIcon: {
    width: windowWidth * .08,
    height: windowWidth * .08,
    marginRight: windowWidth * .05,
    resizeMode: "contain",
  } as ImageStyle,
  contentText: {
    color: Colors.dark5E,
    fontSize: windowWidth * .039,
    lineHeight: windowWidth * .075,

  } as TextStyle,
  contentTitle: {
    fontWeight: "500",
    color: Colors.dark5E,
    textAlign: "center",
    fontSize: windowWidth * .046,
    padding: windowWidth * .07,
  } as TextStyle,
});
