import React from 'react'
import { 
  View,
  ViewStyle,
  TextInput,
  ScrollView,
  TouchableOpacity,
  KeyboardAvoidingView,
 } from 'react-native'
 import { Dispatch } from 'redux'
 import _ from "underscore"
 import { IAppState } from '../../../core/store/appState'
 import { NavigationRoute, NavigationScreenProp } from 'react-navigation' 
 import { trainingActionsAsync } from '../trainingActionsAsync'
 import { TrainingActions } from '../trainingActions'
 import { CommonActions } from '../../commonActions'
 import { BaseReduxComponent } from '../../../core/BaseComponent'
 import { NavigationActions } from '../../../navigation/navigation'
 import { connectAdv } from '../../../core/store'
import { styleSheetCreate } from '../../../common/utils'
import { localization as l } from '../../../common/localization/localization'
import { 
  Colors,
  windowWidth,
  windowHeight,
 } from '../../../core/theme'
import { PlainHeader } from '../../../common/components/Headers' 
import { Button } from '../../../common/components/Button'

interface IStateProps {
  netStatus: boolean
  readingResult: any
  sentEssayStatus: any
}

interface IDispatchProps {
  sendAssumption(data: any): any
}

interface IState {

}

interface IProps {
  navigation: NavigationScreenProp<NavigationRoute<any>>
}

@connectAdv(
  (state: IAppState): IStateProps => ({
    netStatus: state.common.netStatus,
    sentEssayStatus: state.common.sentEssayStatus,
    readingResult: state.training.readingResult,
  }),
  (dispatch: Dispatch): IDispatchProps => ({
    sendAssumption(data): void {
      dispatch(TrainingActions.updateReading([...data.readingResult]))
      dispatch(CommonActions.updateEssayStatus(data.sentEssayStatus))
      //@ts-ignore
      dispatch(trainingActionsAsync.sendAssumption(data))
      dispatch(NavigationActions.navigateToEssayFinish(data))
    },
  })
)
export class ReadingEssay extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {
  static navigationOptions = PlainHeader(l.training.essay, true)
  
  state = {
    textEssay: '',
    item: this.props.navigation.state.params
  }

  updateEssayStatus = () => {
    
  }

  navigateToEssayFinish(){
		let { readingResult } = this.stateProps
    let { textEssay, item } = this.state
  
    if(this.stateProps.netStatus) {
      let data = { item, textEssay }

      let findEx_id = _.findWhere(readingResult, {
        ex_id: item.ex_id
      })
      //@ts-ignore
      if(findEx_id) findEx_id.state > 0

      let { sentEssayStatus } = this.stateProps

      sentEssayStatus.sentEssayReading = true

      this.dispatchProps.sendAssumption({data, readingResult, sentEssayStatus})
    } else {
      alert(l.common.noConnect)
    }
  }
    
  handleTextChange = (newText: string): void => this.setState({ textEssay: newText })
    
  render(): JSX.Element {

    let { textEssay } = this.state

    console.log('data>>', textEssay)

    return(
      <ScrollView contentContainerStyle={styles.bgStyle}>
        <View style={styles.content}>
          <View style={styles.inputContainer}>
            <TextInput
              style={styles.input}
              editable={true}
              multiline={true}
              value={textEssay}
              onChangeText={this.handleTextChange}
              placeholder={l.training.essayInstr}
            />
          </View>
          <KeyboardAvoidingView 
            behavior="padding" 
            keyboardVerticalOffset={windowWidth * .2}
          >
            <TouchableOpacity onPress={this.navigateToEssayFinish.bind(this)}>
              <Button
                width={windowWidth * .9}
                titleButton={l.common.send}
              />
            </TouchableOpacity>
          </KeyboardAvoidingView>
        </View>
      </ScrollView>
    )
  }
}

const styles = styleSheetCreate({
  bgStyle: {
    flexGrow: 1,
    backgroundColor: Colors.greyE5,
  } as ViewStyle,
  content: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
  } as ViewStyle,
  inputContainer: {
    paddingVertical: windowWidth * .04,
    marginVertical: windowWidth * .02,
    minHeight: windowHeight * .3,
    maxHeight: windowHeight * .75,
    flex: 1,
    padding: windowWidth * .02,
    backgroundColor: Colors.white,
    borderWidth: windowWidth * .005,
    borderColor: Colors.greyDB,
    borderStyle: 'solid',
    borderRadius: windowWidth * .04,
    width: windowWidth * .9,
  } as ViewStyle,
  input: {
    flex: 1,
    textAlignVertical: "top",
  } as ViewStyle,
})
