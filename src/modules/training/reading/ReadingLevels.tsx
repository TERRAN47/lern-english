import React from 'react'
import {
  View,
  ViewStyle,
  Text,
  TextStyle,
  FlatList,
  ScrollView,
} from 'react-native'
import { Dispatch } from 'redux'
import _ from 'underscore'
import { BaseReduxComponent } from '../../../core/BaseComponent'
import { NavigationActions } from '../../../navigation/navigation'
import { connectAdv } from '../../../core/store/connectAdv'
import { Colors, windowWidth } from '../../../core/theme'
import {IAppState} from "../../../core/store/appState"
import { trainingActionsAsync } from '../../training/trainingActionsAsync'
import { styleSheetCreate } from '../../../common/utils'
import { Profile } from '../../../core/api/generated/dto/CommonResponse.g'
import { localization as l } from "../../../common/localization/localization"
import { PlainHeader } from '../../../common/components/Headers'
import { ReadingItem } from '../../../common/components/ReadingItem'
import { LevelSelect } from '../../../common/components/LevelSelect'
import { LoadingView } from '../../../common/components/LoadingView'


interface IStateProps {
  user: Profile
  readingResult: any
  error: {} | string | null
  isLoading: boolean
  netStatus: boolean
}

interface IDispatchProps {
  navigateToReadingText(data: any): void
  getReading (netStatus:boolean): void
}

interface IState {
  level: string
  levelTitle: string
}

interface IProps {
  
}

@connectAdv(
  (state: IAppState): IStateProps => ({
    readingResult: state.training.readingResult,
    error: state.training.error,
    netStatus: state.common.netStatus,
    isLoading: state.training.isLoading,
    user: state.common.user,
  }),
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToReadingText(data): void {
      dispatch(NavigationActions.navigateToReadingText(data))
    },
    getReading(netStatus): void {
      //@ts-ignore
      dispatch(trainingActionsAsync.getReading(netStatus))
    }
  })
)
export class ReadingLevels extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  static navigationOptions = PlainHeader(l.training.reading, true, false)

  componentDidMount() {
    let { netStatus } = this.stateProps 
    this.dispatchProps.getReading(netStatus)
  }

  state = {
    level: this.stateProps.user.userLevel,
    levelTitle: '',
  }  

  keyExtractor = (item: any): string => item.theme
  
  renderItem = ({ item }: any): JSX.Element => (
    <ReadingItem
      item={item}
      onPress={this.navToTextScreen.bind(this, item)}  
    />
  )

  navToTextScreen(item: any) {
    this.dispatchProps.navigateToReadingText(item)
  }

  changeLevel = (level:string, levelTitle:string):void =>{
    this.setState({levelTitle, level})
  }

  render(): JSX.Element {
    const { isLoading, user, readingResult } = this.stateProps
    //console.log('reading', readingResult.map((item: any) => item.ex_ID))
    let { level, levelTitle } = this.state 
    if(level == '0') levelTitle = user.userLevel

    console.log('level >>>>>' , levelTitle)


    return (
      <View style={styles.container} >
        <ScrollView showsVerticalScrollIndicator={false}>
        <LoadingView isLoading={isLoading} />
          <LevelSelect 
            level={user.userLevel}  
            changeLevel={this.changeLevel}
          />
          {
            readingResult
            ? readingResult.length > 0 
              ? <View style={styles.itemsContainer}>
                  <FlatList 
                    data={readingResult.filter((item:any)=>{
                      if(item.level == level){
                        return item
                      }
                    })}
                    renderItem={this.renderItem}
                    keyExtractor={this.keyExtractor}
                  />
                </View>
              : <View style={styles.noLevels}>
                  <Text>{l.training.on} {levelTitle ? levelTitle : l.training.this} {l.training.noExercises}</Text>
                </View>
            : null
          }
        </ScrollView>
      </View>
    )
  }
}

const styles = styleSheetCreate({
  container: {
    flex: 1,
    backgroundColor: Colors.greyE5,
  } as ViewStyle,
  levelsContainer: {
    padding: windowWidth * .05,
    justifyContent: 'space-around',
  } as ViewStyle,
  activeTitle: {
    color: Colors.dark5E,
    fontSize: windowWidth * .039,
  } as TextStyle,
  notActiveTitile: {
    color: Colors.greyCE,
    fontSize: windowWidth * .039,
  } as TextStyle,
  itemsContainer: {
    flex: 1,
    alignItems: 'center',
  } as ViewStyle,
  noLevels: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  } as ViewStyle,
})
