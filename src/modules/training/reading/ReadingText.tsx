import React from 'react'
import {
  View,
  ViewStyle,
  Text,
  TextStyle,
  TouchableOpacity,
  ScrollView,
} from 'react-native'
import { Dispatch } from 'redux'
import { NavigationRoute, NavigationScreenProp } from 'react-navigation'
import { connectAdv } from '../../../core/store/connectAdv'
import { NavigationActions } from '../../../navigation/navigation'
import { IAppState } from '../../../core/store/appState'
import { Profile } from '../../../core/api/generated/dto/CommonResponse.g'
import { BaseReduxComponent } from '../../../core/BaseComponent'
import { localization as l } from '../../../common/localization/localization'
import { Colors, windowWidth } from '../../../core/theme'
import { styleSheetCreate } from '../../../common/utils'
import { Button } from '../../../common/components/Button'
import { PlainHeader } from '../../../common/components/Headers'

interface IStateProps {
  user: Profile
}

interface IDispatchProps {
  navigateToReadingEssay(data: any): void
  navigateToReadingEssayFeedback(data: any): void
  navigateToReadingLevels(): void
}

interface IState {
  lang: string
  titleText: string
  titleButton: string
}

interface IProps {
  category: string
  title: string
  data: any
  navigation: NavigationScreenProp<NavigationRoute<any>>
}

@connectAdv(
  (state: IAppState): IStateProps => ({
    user: state.common.user,
  }),
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToReadingEssay(data): void {
      dispatch(NavigationActions.navigateToReadingEssay(data))
    },
    navigateToReadingEssayFeedback(data): void {
      dispatch(NavigationActions.navigateToReadingEssayFeedback(data))
    },
    navigateToReadingLevels(): void {
      dispatch(NavigationActions.navigateToReadingLevels())
    },
  })
)
export class ReadingText extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  static navigationOptions = PlainHeader(l.common.theory, true, false)

  navToEssaytScreen(item: any) {
    this.dispatchProps.navigateToReadingEssay(this.props.navigation.state.params)
  }
  
  navTofeedbackScreen(item: any) {
    this.dispatchProps.navigateToReadingEssayFeedback(this.props.navigation.state.params)
  }
  
  render(): JSX.Element {
    
    let { theme, text, state } = this.props.navigation.state.params
    console.log('ex_id', this.props.navigation.state.params.ex_id)
    let { userTariff } = this.stateProps.user
    const regex = /(<([^>]+)>)/ig
    // console.log('Number(userTariff)', Number(userTariff))
    // console.log('userTariff=', userTariff)
    // console.log('user=', this.stateProps.user)

    return (
      <ScrollView>
        <View  style={styles.container}>
          <View>
            <Text style={styles.title}>{theme}</Text>
            <Text style={styles.text}>
              {text.replace(/&bull;/gi, '').replace(/(&bull;) | &ndash;/gi, '').replace(/(&nbsp;)/ig, '').replace(regex, '').replace(/&#39;/gi, '\'').replace(/&ldquo;/gi, '"').replace(/&rsquo;/gi, '\'').replace(/&rdquo;/gi, '"').replace(/&raquo;/gi, '»').replace(/&laquo;/gi, '«').replace(/&mdash;/gi, '—').replace(/&ndash;/gi, '–').replace(/&quot;/gi, '"')}
            </Text>
          </View>
          {
            (Number(userTariff) < 5) 
            ? <TouchableOpacity onPress={this.dispatchProps.navigateToReadingLevels}>
                <Button 
                  titleButton={l.training.complete}
                  width={windowWidth * .9}
                />
              </TouchableOpacity>
            :
              state
              ? <TouchableOpacity onPress={this.navTofeedbackScreen.bind(this, this.props.navigation.state.params)}>
                  <Button 
                    titleButton={l.training.viewGrade}
                    width={windowWidth * .9}
                  />
                </TouchableOpacity>
              : <TouchableOpacity onPress={this.navToEssaytScreen.bind(this, this.props.navigation.state.params)}>
                  <Button 
                    titleButton={l.common.continue}
                    width={windowWidth * .9}
                  />
                </TouchableOpacity>
          }
        </View>
      </ScrollView>
    )
  }
}

const styles = styleSheetCreate({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    alignItems: 'center',
    paddingVertical: windowWidth * .07,
    paddingHorizontal: windowWidth * .05,
  } as ViewStyle,
  title:{
    color: Colors.dark5E,
    fontWeight: '500',
    fontSize: windowWidth * .042,
    lineHeight: windowWidth * .082,
    textAlign: 'center',
  } as TextStyle,
	text: {
    color: Colors.dark5E,
    paddingVertical: windowWidth * .05,
    fontSize: windowWidth * .039,
    lineHeight: windowWidth * .075,
  } as TextStyle,
})
