import React from "react"
import {
  View,
  Text,
  TextStyle,
  TouchableOpacity,
  Platform,
  ViewStyle,
  ScrollView,
} from 'react-native'
import {
  NavigationRoute,
  NavigationScreenProp,
  NavigationScreenConfigProps,
  NavigationStackScreenOptions,
} from 'react-navigation'
import { Dispatch } from 'redux'
import { BaseReduxComponent } from '../../../core/BaseComponent'
import { connectAdv } from '../../../core/store/connectAdv'
import { NavigationActions } from '../../../navigation/navigation'
import { styleSheetCreate } from '../../../common/utils'
import { localization as l } from '../../../common/localization/localization'
import { Colors, windowWidth } from '../../../core/theme'
import { Button } from '../../../common/components/Button'
import { PlainHeader } from '../../../common/components/Headers'

interface IStateProps {

}

interface IDispatchProps {
  navigateToReadingLevels(): void
}

interface IState {

}

interface IProps {
  navigation: NavigationScreenProp<NavigationRoute<any>>
}

@connectAdv(
  null,
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToReadingLevels(): void {
        dispatch(NavigationActions.navigateToReadingLevels())
    },
  })
)
export class ReadingEssayInstr extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  static navigationOptions = ({ navigation }: NavigationScreenConfigProps): NavigationStackScreenOptions =>{
    return PlainHeader(l.training.essay, true, false)
  }

  render(): JSX.Element {
    
    //let {params} = this.props.navigation.state

    return (
      <ScrollView contentContainerStyle={styles.bgStyle}>
        <View style={styles.container}>
          <View style={styles.content}>
            <Text style={styles.contentTitle}>{l.training.instrWritingEssay}</Text>
            <View style={styles.taskBlock}>
              <View style={styles.taskChild}>
                <Text style={styles.contentText}>
                  {l.training.detailInstrWritingEssay}
                </Text>
              </View>
            </View>
            <TouchableOpacity  onPress={()=>this.dispatchProps.navigateToReadingLevels()}>
              <Button titleButton={l.common.understandable}/>
            </TouchableOpacity>
          </View>             
        </View>
      </ScrollView>
    )
  }
}

const styles = styleSheetCreate({
  bgStyle: {
    flexGrow: 1,
    justifyContent: 'center',
    backgroundColor: Colors.greyE5,
  } as ViewStyle,
  container: {
    flexGrow: 1,
    justifyContent:'center',
    padding: windowWidth * .05,
  } as ViewStyle,
  content:{
    flex: 1,
    padding: windowWidth * .05,
    backgroundColor: Colors.white,
    alignItems:'center',
    justifyContent:'space-between',
    borderRadius: windowWidth * .05,
    ...Platform.select({
      ios: {
          shadowRadius: 8,
          shadowOpacity: 0.1,
          shadowOffset: {width: 1, height: 4},
      },
      android: {
        elevation: 8
    }}),
  } as ViewStyle,
  taskBlock:{
    paddingVertical: windowWidth * .02,
  } as ViewStyle,
  taskChild:{
    flexDirection:'row',
    justifyContent:'flex-start',
    alignItems:'center',
    paddingVertical: windowWidth * .03,
  } as ViewStyle,
  contentText:{
    color: Colors.dark5E,
    fontSize: windowWidth * .039,
    lineHeight: windowWidth * .075,
    textAlign: 'center',
  } as TextStyle,
  contentTitle:{
    fontWeight:'500',
    color: Colors.dark5E,
    fontSize: windowWidth * .046,
    padding: windowWidth * .07,
    textAlign: 'center',
  } as TextStyle,  
})
