import React from 'react'
import { 
  View,
  ViewStyle,
  Text,
  TextStyle,
  ScrollView,
 } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { NavigationRoute, NavigationScreenProp } from 'react-navigation'
import { BaseReduxComponent } from '../../../core/BaseComponent'
import { localization as l } from "../../../common/localization/localization";
import { styleSheetCreate } from '../../../common/utils'
import { 
  Colors,
  windowWidth,
  windowHeight,
 } from '../../../core/theme'
import { PlainHeader } from '../../../common/components/Headers'
import { Stars } from '../../../common/components/Stars'
 
interface IState {

}

interface IStateProps {
  
}

interface IProps {
  theme: any
  navigation: NavigationScreenProp<NavigationRoute<any>>
}

interface IDispatchProps {
  navigateToEssayFinish(): void
}

// @connectAdv(
//   null,
//   (dispatch: Dispatch): IDispatchProps => ({
//     navigateToEssayFinish(): void {
//       dispatch(NavigationActions.navigateToEssayFinish())
//     },
//   })
// )
export class ReadingEssayFeedback extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {
  static navigationOptions = PlainHeader(l.training.essay, true)
  
  render(): JSX.Element {

    const { theme, curator_comment, essay_grade } = this.props.navigation.state.params
    const { essy_text } = this.props.navigation.state.params.essay[0]
    //console.log('text >>', this.props.navigation.state.params.essay[0].essy_text)

    return(
      <View style={styles.container}>
        <ScrollView contentContainerStyle={styles.bgStyle}>
          <View>
            <KeyboardAwareScrollView>
              <View style={styles.content}>
                <View>
                  <View style={styles.titleContainer}>
                    <Text style={styles.title}>{theme}</Text>
                    <View style={styles.underline} />
                  </View>
                </View>
                <View style={styles.feedbackContainer}>
                  <Text style={styles.description}>{l.training.yourGrade}:</Text>
                  <Stars stars={essay_grade}/>
                </View>    
                <View>
                  <Text>{l.training.teacherComment}:</Text>
                  <View style={styles.textContainer}>
                    <Text style={styles.text}>{curator_comment}</Text>
                  </View>
                </View>     
                <View>
                  <Text>{l.training.yourEssay}</Text>
                  <View style={styles.textContainer}>
                    <Text style={styles.textEssay}>
                      {essy_text}
                    </Text>
                  </View>
                </View>   
              </View>
            </KeyboardAwareScrollView>
          </View>
        </ScrollView>
      </View>
    )
  }
}

const styles = styleSheetCreate({
  container: {
    flex: 1,
    backgroundColor: Colors.greyE5,
  } as ViewStyle,
  bgStyle: {
    flexGrow: 1,
    paddingHorizontal: windowWidth * .05,
    backgroundColor: Colors.greyE5,
  } as ViewStyle,
  content: {
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: Colors.greyE5,
    paddingVertical: windowWidth * .05,
  } as ViewStyle,
  titleContainer: {
    paddingVertical: windowWidth * .02,
    width: windowWidth * .9,
    alignItems: 'flex-start',
  } as ViewStyle,
  title: {
    color: Colors.darkGrey9F,
		fontSize: windowWidth * .039,
  } as TextStyle, 
  description: {
    color: Colors.white,
    fontSize: windowWidth * .039,
    paddingBottom: windowWidth * .04,
  } as TextStyle,
  underline: {
    backgroundColor: Colors.greyDB,
    marginTop: windowWidth * .02,
    height: windowWidth * .005,
    width: windowWidth * .9,
  } as ViewStyle,
  textContainer: {
    marginVertical: windowWidth * .04,
    minHeight: windowHeight * .24,
    padding: windowWidth * .05,
    backgroundColor: Colors.white,
    borderWidth: windowWidth * .005,
    borderColor: Colors.greyDB,
    borderStyle: 'solid',
    borderRadius: windowWidth * .04,
    width: windowWidth * .9,
  } as ViewStyle,
  feedbackContainer: {
    width: windowWidth * .9,
    padding: windowWidth * .05,
    borderRadius: windowWidth * .025,
    backgroundColor: Colors.blueEC,
    marginBottom: windowWidth * .08,
  } as ViewStyle,
  text: {
    color: Colors.black59,
    fontSize: windowWidth * .039,
    width: windowWidth * .76,
  } as TextStyle,
  textEssay: {
    color: Colors.darkGrey9F,
    fontSize: windowWidth * .039,
    width: windowWidth * .76,
  } as TextStyle,
})
