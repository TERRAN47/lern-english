import React from 'react'
import {
	View,
	ViewStyle,
	TouchableOpacity,
	Platform,
	//ScrollView,
	TextInput,
	Text,
	TextStyle,
} from 'react-native'
import { Dispatch } from 'redux'
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { BaseReduxComponent } from '../../../core/BaseComponent'
import { NavigationActions } from '../../../navigation/navigation'
import { connectAdv } from '../../../core/store/connectAdv'
import { Colors, windowWidth } from '../../../core/theme'
import { styleSheetCreate } from '../../../common/utils'
import { trainingActionsAsync } from '../trainingActionsAsync'
import { localization as l } from '../../../common/localization/localization'
import { PlainHeader } from '../../../common/components/Headers'
import { Button } from '../../../common/components/Button'
import { ButtonNoBg } from '../../../common/components/ButtonNoBg'

interface IStateProps {
	newWord: string
	newTranslateWord: string
}

interface IDispatchProps {
	navigateToAssumptionInstr (): void,
	addNewWordPost(data: any): void
}

interface IState {
	newWord: string
	newTranslateWord: string
	added: string
}

interface IProps {
	
}

@connectAdv(
  null,
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToAssumptionInstr(): void {
      dispatch(NavigationActions.navigateToAssumptionInstr(1))
		},
			addNewWordPost(data):void {
			//@ts-ignore
			dispatch(trainingActionsAsync.addNewWordPost(data))
		},
  })
)
export class DictionaryAddWord extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  state = {
		newWord: '',
		newTranslateWord: '',
		added: '',
	}
    
	static navigationOptions = PlainHeader(l.training.addWord, true, false)

	onChangeNewText(text:string){
		this.setState({newWord:text})
	}

	onChangeNewTranText(text:string){
		this.setState({newTranslateWord:text})
	}

	addNewWord(){
		let { newWord, newTranslateWord } = this.state
		let data = {newWord, newTranslateWord}
		this.dispatchProps.addNewWordPost(data)
		this.setState({added: 'добавлено'})
		this.setState({newWord: ''})
		this.setState({newTranslateWord: ''})
		setTimeout(() => this.setState({added: ''}), 2000)
	}

  render(): JSX.Element {

		let { added, newWord, newTranslateWord } = this.state

    return (
			<View style={styles.bgStyle}>
				<KeyboardAwareScrollView style={{flexGrow: 1}}>
					<View style={styles.container} >
						<View style={styles.content} >
							<View>
								<View style={styles.inputContainer}>
									<TextInput
										editable={true}
										value={newWord}
										placeholder={l.training.word}
										onChangeText={(text:string)=>this.onChangeNewText(text)}
									/>
								</View>
								<View style={styles.emptySpace} />
								<TouchableOpacity>
									<ButtonNoBg
										titleButton={l.training.translate} 
										width={windowWidth * .7}
									/>
								</TouchableOpacity>
								<View style={styles.emptySpace} />
								<View style={styles.inputContainer}>
									<TextInput
										editable={true}
										value={newTranslateWord}
										onChangeText={(text:string)=>this.onChangeNewTranText(text)}
										placeholder={l.training.traslationWord}
									/>
								</View>
								<View style={styles.emptySpace}>
									<Text style={styles.added}>{added}</Text>
								</View>
								<TouchableOpacity onPress={this.addNewWord.bind(this)}>
									<Button
										titleButton={l.training.add}
										width={windowWidth * .7} 
									/>
								</TouchableOpacity>
							</View>
						</View>
					</View> 
				</KeyboardAwareScrollView>
			</View>
    )
  }
}

const styles = styleSheetCreate({
	bgStyle: {
    flexGrow: 1,
		justifyContent: 'center',
    backgroundColor: Colors.blueEC,
	} as ViewStyle,
  container: {
		flexGrow: 1,
		marginTop: windowWidth * .04,
    alignItems: 'center',
		justifyContent: 'center',
  } as ViewStyle,
  content: {
    width: windowWidth * .9,
    backgroundColor: Colors.white,
		borderRadius: windowWidth * .03,
		alignItems: 'center',
		justifyContent: 'space-between',
		paddingHorizontal: windowWidth * .07,
		paddingVertical: windowWidth * .12,
		marginBottom: windowWidth * .07,
		...Platform.select({
      ios: {
				shadowRadius: 3,
				shadowOpacity: .1,
				shadowOffset: { width: 0, height: 7 },
      },
      android: {
        elevation: 8,
      }
  	}),
	} as ViewStyle,
	inputContainer: {
		paddingHorizontal: windowWidth * .04,
		borderRadius: windowWidth * .025,
		borderStyle: 'solid',
		justifyContent: 'center',
		height: windowWidth * .155,
		borderColor: Colors.greyCE,
		borderWidth: windowWidth * .003,
	} as ViewStyle,
	emptySpace: {
		paddingVertical: windowWidth * .03,
		alignItems: 'center',
		justifyContent: 'center',
	} as ViewStyle,
	added: {
		color: Colors.blueEC,
		paddingVertical: windowWidth * .02,
	} as TextStyle,
})
