import React from 'react'
import {
	View,
	ViewStyle,
	Text,
	TextStyle,
	TouchableOpacity,
	Platform,
	ScrollView,
} from 'react-native'
import { BaseReduxComponent } from '../../../core/BaseComponent'
import { NavigationActions } from '../../../navigation/navigation'
import { Dispatch } from 'redux'
import { connectAdv } from '../../../core/store/connectAdv'
import { Colors, windowWidth } from '../../../core/theme'
import { styleSheetCreate } from '../../../common/utils'
import { localization as l } from '../../../common/localization/localization'
import { PlainHeader } from '../../../common/components/Headers'
import { Answer } from '../../../common/components/Answer'

interface IStateProps {

}

interface IDispatchProps {
  navigateToAssumptionInstr (): void,
}

interface IState {
  
}

interface IProps {
  
}

@connectAdv(
  null,
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToAssumptionInstr(): void {
      dispatch(NavigationActions.navigateToAssumptionInstr(1))
    },
  })
)
export class DictionaryTraining extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {
 
	state = {
		
	}
    
	static navigationOptions = PlainHeader(l.training.training, true, false)
	
  render(): JSX.Element {
    return (
			<ScrollView contentContainerStyle={styles.bgStyle}>
				<View style={styles.container} >
					<View style={styles.content} >
						<Text style={styles.description}>Машина</Text>
						<View>
							<TouchableOpacity>
								{/* answerState - isSelected, isNotSelected, isError, isCorrect, isNotSelectedNoCircle */}
								<Answer title={'Phone'} answerState={'isSelected'}/>
							</TouchableOpacity>
							<TouchableOpacity>
								<Answer title={'Car'} answerState={'isNotSelected'}/>
							</TouchableOpacity>  
              <TouchableOpacity>
								<Answer title={'Picture'} answerState={'isNotSelected'}/>
							</TouchableOpacity>             
						</View>
					</View>
					<View>
						<Text style={styles.numberSlide}><Text>2</Text>/<Text>25</Text></Text>
					</View>
				</View> 
			</ScrollView>
    )    
  }
}

const styles = styleSheetCreate({
	bgStyle: {
    flexGrow: 1,
    justifyContent: 'center',
    backgroundColor: Colors.blueEC,
	} as ViewStyle,
  container: {
		flexGrow: 1,
		marginTop: windowWidth * .04,
    alignItems: 'center',
		justifyContent: 'space-around',
  } as ViewStyle,
  content: {
    width: windowWidth * .8,
    backgroundColor: Colors.white,
		borderRadius: windowWidth * .03,
		alignItems: 'center',
		justifyContent: 'space-between',
		paddingHorizontal: windowWidth * .07,
		paddingVertical: windowWidth * .08,
		marginBottom: windowWidth * .07,
		...Platform.select({
      ios: {
				shadowRadius: 3,
				shadowOpacity: .1,
				shadowOffset: { width: 0, height: 7 },
      },
      android: {
        elevation: 8,
      }
  	}),
	} as ViewStyle,
	description: {
    paddingBottom: windowWidth * .1,
    paddingHorizontal: windowWidth * .05,
    textAlign: 'center',
		fontSize: windowWidth * .045,
    color: Colors.dark5E,
    fontWeight: '500',
	} as TextStyle,
	buttonStyle: {
		paddingHorizontal: windowWidth * .05,
	} as ViewStyle,	
	numberSlide: {
		marginTop: windowWidth * .04,
		color: Colors.white,
		fontSize: windowWidth * .045,
	} as TextStyle,	
})
