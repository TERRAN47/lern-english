import React from 'react'
import {
  View,
  ViewStyle,
  TouchableOpacity,
  Text,
  TextStyle,
  Image,
  ImageStyle,
  ScrollView,
  Platform,
} from 'react-native'
import { Dispatch } from 'redux'
import _ from 'underscore'
import { BaseReduxComponent } from '../../../core/BaseComponent'
import { localization as l } from '../../../common/localization/localization'
import { ImageResources } from '../../../common/ImageResources.g'
import { NavigationActions } from '../../../navigation/navigation'
import { connectAdv } from '../../../core/store/connectAdv'
import { Colors, windowWidth } from '../../../core/theme'
import { styleSheetCreate } from '../../../common/utils'
import { PlainHeader } from '../../../common/components/Headers'
import { DictionaryItem } from '../../../common/components/DictionaryItem'

interface IStateProps {
  phoneticsResult: any,
  error: {} | string | null,
  isLoading: boolean,
}

interface IDispatchProps {
  navigateToWritingEssayDone(): void
  navigateToWritingEssay(): void
}

interface IState {
  isClose: boolean
}

interface IProps {
  
}

@connectAdv(
  null,
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToWritingEssayDone(): void {
      dispatch(NavigationActions.navigateToWritingEssayDone())
    },
    navigateToWritingEssay(): void {
      dispatch(NavigationActions.navigateToWritingEssay())
    },
  })
)
export class DictionaryWordDetail extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  static navigationOptions = PlainHeader(l.training.dictionary, true, false)

  render(): JSX.Element {
    //let {isLoading} = this.stateProps
    // console.log(66, phoneticsResult)
    
    return (
      <View>
        <ScrollView style={styles.container} >
          <View>
            <DictionaryItem
              en={'Help'}
              ru={'Помощь'}
            />
          </View>
          <View style={styles.content}>
            <Text style={styles.text}>
              /verb/
              1. помогать, способствовать, содействовать, 
              оказывать помощь, помочь, посодействовать (assist, contribute)
              also help - также помогать
              help young people - помогать молодым людям
              help the victims - помочь пострадавшим
              2. выручить, выручать (rescue)
              3. удержаться (hold)
              /noun/
              1. помощь, содействие, подмога (assistance)
              emergency medical help - скорая медицинская помощь
              help in difficult situations - помощь в сложных ситуациях
              help in solving problems - помощь в решении проблем
              great deal of help - большая помощь
              help of another person - помощь другого человека
              2. поддержка, подспорье (support)
              psychological help - психологическая поддержка
              invaluable help - неоценимое подспорье
              3. помощник (assistant)
              indispensable help - незаменимый помощник
              4. справка (reference)
              5. подсказка (tip)
              /adjective/
              справочный (reference)
              online help system - справочная система
            </Text>
          </View>
        </ScrollView>
        <View style={styles.buttonsBox}>
          <TouchableOpacity style={styles.button}>
            <Image
              style={styles.icon}
              source={ImageResources.edit}
            />
          </TouchableOpacity>
          <View style={styles.line} />
          <TouchableOpacity style={styles.button}>
            <Image
              style={styles.icon}
              source={ImageResources.delete}
            />
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = styleSheetCreate({
  container: {
    flexGrow: 1,
    backgroundColor: Colors.greyE5,
  } as ViewStyle,
  content: {
    backgroundColor: Colors.white,
    padding: windowWidth * .05,
  } as ViewStyle,
  text: {
    fontSize: windowWidth * .039,
    lineHeight: windowWidth * .075,
  } as TextStyle,
  buttonsBox: {
    flexDirection: 'row',
    position: 'absolute',
    zIndex: 3,
    width: windowWidth,
    height: windowWidth * .15,
    bottom: 0,
    backgroundColor: Colors.white,
    ...Platform.select({
      ios: {
				shadowRadius: 10,
				shadowOpacity: .4,
				shadowOffset: { width: 0, height: 24 },
      },
      android: {
        elevation: 9,
      }
  	}),
  } as ViewStyle,
  button: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  } as ViewStyle,
  line: {
    height: windowWidth * .15,
    width: windowWidth * .004,
    backgroundColor: Colors.greyDB,
  } as ViewStyle,
  icon: {
    width: windowWidth * .05,
    height: windowWidth * .0535,
  } as ImageStyle,
})