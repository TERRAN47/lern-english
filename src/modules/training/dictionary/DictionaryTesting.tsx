import React from 'react'
import {
  View,
  ViewStyle,
  TouchableOpacity,
  FlatList,
  Text,
  TextStyle,
} from 'react-native'
import { Dispatch } from 'redux'
import _ from 'underscore'
import { BaseReduxComponent } from '../../../core/BaseComponent'
import { NavigationActions } from '../../../navigation/navigation'
import { connectAdv } from '../../../core/store/connectAdv'
import { Colors, windowWidth } from '../../../core/theme'
import { styleSheetCreate } from '../../../common/utils'
import { localization as l } from '../../../common/localization/localization'
import { PlainHeader } from '../../../common/components/Headers'
import { DictionaryCheckBoxItem } from '../../../common/components/DictionaryCheckBoxItem'

interface IStateProps {
  phoneticsResult: any,
  error: {} | string | null,
  isLoading: boolean,
}

interface IDispatchProps {
  navigateToDictionaryWordDetail(): void
  navigateToDictionaryTraining(): void
  navigateToDictionaryTesting(): void
}

interface IState {
  isClose: boolean
}

interface IProps {
  
}

interface IDictionaryItems {
  en: string
  ru: string
  onPress(): void
}

@connectAdv(
  null,
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToDictionaryWordDetail(): void {
      dispatch(NavigationActions.navigateToDictionaryWordDetail())
    },
    navigateToDictionaryTraining(): void {
      dispatch(NavigationActions.navigateToDictionaryTraining())
    },
    navigateToDictionaryTesting(): void {
      dispatch(NavigationActions.navigateToDictionaryTesting())
    },
  })
)
export class DictionaryTesting extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  static navigationOptions = PlainHeader(l.training.testing, true, false)

  dictionaryItems: IDictionaryItems[] = [
    {
      en: 'Help',
      ru: 'Помощь',
      onPress: this.dispatchProps.navigateToDictionaryWordDetail
    },
    {
      en: 'Sound',
      ru: 'Звук',
      onPress: this.dispatchProps.navigateToDictionaryWordDetail
    },
    {
      en: 'Phone',
      ru: 'Телефон',
      onPress: this.dispatchProps.navigateToDictionaryWordDetail
    },
    {
      en: 'Phone',
      ru: 'Телефон',
      onPress: this.dispatchProps.navigateToDictionaryWordDetail
    },
    {
      en: 'Phone',
      ru: 'Телефон',
      onPress: this.dispatchProps.navigateToDictionaryWordDetail
    },
    {
      en: 'Phone',
      ru: 'Телефон',
      onPress: this.dispatchProps.navigateToDictionaryWordDetail
    },
    {
      en: 'Phone',
      ru: 'Телефон',
      onPress: this.dispatchProps.navigateToDictionaryWordDetail
    },
    {
      en: 'Phone',
      ru: 'Телефон',
      onPress: this.dispatchProps.navigateToDictionaryWordDetail
    },
    {
      en: 'Phone',
      ru: 'Телефон',
      onPress: this.dispatchProps.navigateToDictionaryWordDetail
    },
    {
      en: 'Phone',
      ru: 'Телефон',
      onPress: this.dispatchProps.navigateToDictionaryWordDetail
    },
    {
      en: 'Phone',
      ru: 'Телефон',
      onPress: this.dispatchProps.navigateToDictionaryWordDetail
    },
    {
      en: 'Phone',
      ru: 'Телефон',
      onPress: this.dispatchProps.navigateToDictionaryWordDetail
    },
  ]
  
  keyExtractor = (item: any): string => item.title

  dictionaryItem = ({ item: { en, ru, onPress } }: any): JSX.Element => (
    <View>
      <DictionaryCheckBoxItem
        ru={ru}
        en={en}
        onPress={onPress}
      />
       <View style={styles.underLine} />
    </View>
  )


  render(): JSX.Element {
    
    return (
      <View style={styles.container} >
        <FlatList            
          data={this.dictionaryItems}
          renderItem={this.dictionaryItem}
          keyExtractor={this.keyExtractor}
        />
        <TouchableOpacity onPress={this.dispatchProps.navigateToDictionaryTraining}>
          <View  style={styles.buttonBg}>
            <Text style={styles.buttonText}>
              {l.training.train}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = styleSheetCreate({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    alignItems: 'center',
  } as ViewStyle,
  buttonBg: {
    width: windowWidth,
    height: windowWidth * .15,
    backgroundColor: Colors.red61,
    justifyContent: 'center',
    alignItems: 'center',
  } as ViewStyle,
  buttonText: {
    color: Colors.white,
    fontSize: windowWidth * .039,
    fontWeight: '500',
    bottom: 0,
    zIndex: 3,
  } as TextStyle,
  underLine: {
    backgroundColor: Colors.greyCE,
    height: windowWidth * .003,
  } as ViewStyle,
})