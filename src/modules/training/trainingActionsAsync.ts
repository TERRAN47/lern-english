import {Dispatch} from "redux";
// import {AuthHelper} from "../../common/helpers/authHelper";
// import {localization} from "../../common/localization/localization";
import {SimpleThunk} from "../../common/simpleThunk";
import {requestsRepository} from "../../core/api/requestsRepository";
import { TrainingActions } from "./trainingActions";

export class trainingActionsAsync {

  static getPhonetics(netStatus: boolean): SimpleThunk {
    return async (dispatch: Dispatch): Promise<void> => {
      try {
        dispatch(TrainingActions.getPhonetics.started({}));
        const response = await requestsRepository.trainingApiRequest.getPhonetics(netStatus);

        const mapObject =  response.user_ex["27"];

        dispatch(TrainingActions.getPhonetics.done({
          params: {}, result: mapObject
        }));
      } catch (err) {
        console.log("err", err);
        dispatch(TrainingActions.getPhonetics.failed({params: {}, error: err}));
      }
    };
  }

  static getVocabulary(netStatus: boolean): SimpleThunk {
    return async (dispatch: Dispatch): Promise<void> => {
      try {
        dispatch(TrainingActions.getVocabularys.started({}));
        const response = await requestsRepository.trainingApiRequest.getVocabularys(netStatus);

        const mapObject = response.user_ex["215"];

        dispatch(TrainingActions.getVocabularys.done({
          params: {}, result: mapObject
        }));
      } catch (err) {
        console.log("err", err);
        dispatch(TrainingActions.getVocabularys.failed({params: {}, error: err}));
      }
    };
  }

  static getGrammar(netStatus: boolean): SimpleThunk {
    return async (dispatch: Dispatch): Promise<void> => {
      try {
        dispatch(TrainingActions.getGrammars.started({}));
        const response = await requestsRepository.trainingApiRequest.getGrammars(netStatus);

        const mapObject = response.user_ex["30"];

        dispatch(TrainingActions.getGrammars.done({

          params: {}, result: mapObject
        }));
      } catch (err) {
        console.log("err", err);

        dispatch(TrainingActions.getGrammars.failed({params: {}, error: err}));
      }
    };
  }

  static  getReading(netStatus: boolean): SimpleThunk {
    return async (dispatch: Dispatch): Promise<void> => {
      try {
        dispatch(TrainingActions.getReading.started({}));
        const response = await requestsRepository.trainingApiRequest.getReading(netStatus);
        console.log("RESPONSE", response);
        const mapObject = Object.keys(response.user_ex["31"]).map((elem) => {
          return response.user_ex["31"][elem];
        });
        console.log("mapObject", mapObject);

        dispatch(TrainingActions.getReading.done({
          params: {}, result: mapObject
        }));
      } catch (err) {
        console.log("err", err);
        dispatch(TrainingActions.getReading.failed({params: {}, error: err}));
      }
    };
  }

  static  sendAssumption(request: any): SimpleThunk {
    return async (dispatch: Dispatch): Promise<void> => {
      try {
        dispatch(TrainingActions.sendAssumption.started({isLoading: true}));
        const response = await requestsRepository.trainingApiRequest.sendAssumption(request);
        dispatch(TrainingActions.sendAssumption.done({
          params: {isLoading: false},
          result: response,
        }));
      } catch (err) {
        dispatch(TrainingActions.sendAssumption.failed({params: {}, error: err}));
      }
    };
  }

  static  addNewWordPost(data: any): SimpleThunk {
    return async (dispatch: Dispatch): Promise<any> => {
      try{
        dispatch(TrainingActions.addNewWordPost.started({isLoading: true}))
        const response = await requestsRepository.trainingApiRequest.addNewWordPost(data)
        dispatch(TrainingActions.addNewWordPost.done({
          params: {isLoading: false},
          result: response,
        }))
      }catch(err){
        dispatch(TrainingActions.addNewWordPost.failed({params:{}, error:err}))
      }
    }
  }
  
  static getAudition(netStatus: boolean): SimpleThunk {
    return async (dispatch: Dispatch): Promise<void> => {
      try {
        dispatch(TrainingActions.getAuditions.started({}));
        const response = await requestsRepository.trainingApiRequest.getAuditions(netStatus);

        const mapObject = response.user_ex["28"];

        dispatch(TrainingActions.getAuditions.done({

          params: {}, result: mapObject
        }));
      } catch (err) {
        console.log("err", err);

        dispatch(TrainingActions.getAuditions.failed({params: {}, error: err}));
      }
    };
  }
}

