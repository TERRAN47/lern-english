import { IUserInfo } from '../../../core/api/generated/dto/PlaygroundResponse.g'

export interface IPlaygroundState {
  isLoading: boolean
  userInfo: null | IUserInfo
  error: null | string
}

export const PlaygroundInitialState = {
  isLoading: false,
  userInfo: null,
  error: null,
}
