import React from 'react'
import {
  View,
  ViewStyle,
  Text,
} from 'react-native'
import { Dispatch } from 'redux'
import { BaseReduxComponent } from '../../../core/BaseComponent'
import { connectAdv } from '../../../core/store/connectAdv'
import { IAppState } from '../../../core/store/appState'
import { PlaygroundActionsAsync } from './playgroundActionsAsync'
import { IUserInfoResponse } from '../../../core/api/generated/dto/PlaygroundResponse.g'
import { styleSheetCreate } from '../../../common/utils'
import { PlainHeader } from '../../../common/components/Headers'
import { LoadingView } from '../../../common/components/LoadingView'

interface IStateProps {
  isLoading: boolean
  userInfo: IUserInfoResponse | null
}

interface IDispatchProps {
  getUserInfo(): void
}

interface IState {

}

interface IProps {

}

@connectAdv(
  (state: IAppState): IStateProps => ({
    isLoading: state.playground.isLoading,
    //@ts-ignore
    userInfo: state.playground.userInfo,
  }),
  (dispatch: Dispatch): IDispatchProps => ({
    getUserInfo(): void{
      //@ts-ignore
      dispatch(PlaygroundActionsAsync.getUserInfo())
    }
  })
)
export class MyScreen extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {
  static navigationOptions = PlainHeader('MyScreen', true)

  componentDidMount() {
    this.dispatchProps.getUserInfo()
  }
  
  render(): JSX.Element {
    
    if(this.stateProps.isLoading) {
      return <LoadingView isLoading={true} />
    }

    

    return (
      <View style={styles.container}>
        <Text>{JSON.stringify(this.stateProps.userInfo)}</Text>
      </View>
    )
  }
}
 

const styles = styleSheetCreate({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  } as ViewStyle,
})
