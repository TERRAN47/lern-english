import { actionCreator } from '../../../core/store'

export class PlaygroundActions {
  static getUserInfo = actionCreator.async<IEmpty, any, Error>('Playgroud/GET_USER_INFO')
}