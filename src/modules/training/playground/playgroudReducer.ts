import { reducerWithInitialState } from 'typescript-fsa-reducers'
import { Success, Failure } from 'typescript-fsa'
import { newState } from '../../../common/newState'
import {
  PlaygroundInitialState,
  IPlaygroundState,
} from './playgroundState'
import { PlaygroundActions } from './playgroundActions'

function userInfoLoadingStarted(state: IPlaygroundState) {
  return newState(state, { isLoading: true })
}

function userInfoLoadingDone(state: IPlaygroundState, payload: Success<IEmpty, any>) {
  return newState(state, { isLoading: false, error: null, userInfo: payload.result })
}

function userInfoLoadingFailed(state: IPlaygroundState, failed: Failure<IEmpty, Error>) {
  return newState(state, { isLoading: false, error: failed.error.message })
}

export const playgroundReducer = reducerWithInitialState(PlaygroundInitialState)
  //@ts-ignore
  .case(PlaygroundActions.getUserInfo.started, userInfoLoadingStarted)
  .case(PlaygroundActions.getUserInfo.done, userInfoLoadingDone)
  .case(PlaygroundActions.getUserInfo.failed, userInfoLoadingFailed)
