import { Dispatch } from 'redux'
import { SimpleThunk } from '../../../common/simpleThunk'
import { PlaygroundActions } from './playgroundActions'
import { requestsRepository } from '../../../core/api/requestsRepository'

export class PlaygroundActionsAsync {
  static getUserInfo(): SimpleThunk {
    return async (dispatch: Dispatch): Promise<void> => {
      try {
        dispatch(PlaygroundActions.getUserInfo.started({}))
        const response = await requestsRepository.PlaygroundApiRequest.getUserInfo()
        dispatch(PlaygroundActions.getUserInfo.done({ params: {}, result: response}))
      } catch (error) {
        dispatch(PlaygroundActions.getUserInfo.failed({params: {}, error}))
      }
    }
  }
}