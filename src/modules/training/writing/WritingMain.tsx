import React from "react";
import {
  View,
  ViewStyle,
  TouchableOpacity,
  FlatList,
  // Text,
  // TextStyle,
  //Platform,
  //ScrollView,
} from "react-native";
import { Dispatch } from "redux";
import _ from "underscore";
//import { TrainingActions } from '../trainingActions'
import { BaseReduxComponent } from "../../../core/BaseComponent";
import { NavigationActions } from "../../../navigation/navigation";
import { connectAdv } from "../../../core/store/connectAdv";
import { Colors, windowWidth } from "../../../core/theme";
//import {IAppState} from '../../../core/store/appState'
import { localization as l } from '../../../common/localization/localization'
import { styleSheetCreate } from "../../../common/utils";
import { PlainHeader } from "../../../common/components/Headers";
import { WritingItem } from "../../../common/components/WritingItem";
//import { LoadingView } from '../../../common/components/LoadingView'
import { Button } from "../../../common/components/Button";

interface IStateProps {
  phoneticsResult: any;
  error: {} | string | null;
  isLoading: boolean;
}

interface IDispatchProps {
  navigateToWritingEssayDone(): void;
  navigateToWritingEssay(): void;
  navigateToWritingEssayNoFeedBack(): void;
  navigateToWritingEssayFeedBack(): void;
}

interface IState {
  isClose: boolean;
}

interface IProps {

}

interface IWritingMainItem {
  title: string;
  status: string;
  date: string;
  stars: number;
  onPress(): void;
}

@connectAdv(
  null,
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToWritingEssayDone(): void {
      dispatch(NavigationActions.navigateToWritingEssayDone());
    },
    navigateToWritingEssay(): void {
      dispatch(NavigationActions.navigateToWritingEssay());
    },
    navigateToWritingEssayFeedBack(): void {
      dispatch(NavigationActions.navigateToWritingEssayFeedBack());
    },
    navigateToWritingEssayNoFeedBack(): void {
      dispatch(NavigationActions.navigateToWritingEssayNoFeedBack());
    },
  })
)
export class WritingMain extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  static navigationOptions = PlainHeader(l.training.writing, true, false);

  

  writingMainItems: IWritingMainItem[] = [
    {
      title: "The Boy Who Cried Wolf",
      onPress: this.dispatchProps.navigateToWritingEssayDone,
      status: "isDone",
      date: "string",
      stars: 4,
    },
    {
      title: "The Boy Who Cried Wolf",
      onPress: this.dispatchProps.navigateToWritingEssayFeedBack,
      status: "onCheck",
      date: "string",
      stars: 0,
    },
    {
      title: "The Boy Who Cried Wolf",
      onPress: this.dispatchProps.navigateToWritingEssayNoFeedBack,
      status: "isRework",
      date: "string",
      stars: 0,
    },
  ];
  
  // componentWillUnmount(){
  //   this.dispatchProps.clearState()
  // }
  
  keyExtractor = (item: any): string => item.title;

  writingItem = ({ item: { title, onPress, status, stars } }: any): JSX.Element => (
    <WritingItem
      title={title}
      onPress={onPress} 
      status={status}
      date={"string"}
      stars={stars}
    />
  )


  render(): JSX.Element {
    //let {isLoading} = this.stateProps
    // console.log(66, phoneticsResult)
    
    return (
      <View style={styles.container} >
        <FlatList            
          data={this.writingMainItems}
          renderItem={this.writingItem}
          keyExtractor={this.keyExtractor}
        />
        <TouchableOpacity onPress={this.dispatchProps.navigateToWritingEssay}>
          <Button 
            width={windowWidth * .92}
            titleButton={l.training.writeEssay}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = styleSheetCreate({
  container: {
    flex: 1,
    backgroundColor: Colors.greyE5,
    alignItems: "center",
    paddingVertical: windowWidth * .03,
  } as ViewStyle,
});
