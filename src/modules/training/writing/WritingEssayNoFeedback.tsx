import React from 'react'
import { 
  View,
  ViewStyle,
  Text,
  TextStyle,
  TextInput,
  ScrollView,
 } from 'react-native'
 //import { Dispatch } from 'redux'
 import { BaseReduxComponent } from '../../../core/BaseComponent'
//  import { NavigationActions } from '../../../navigation/navigation'
//  import { connectAdv } from '../../../core/store'
import { localization as l } from '../../../common/localization/localization'
 import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { styleSheetCreate } from '../../../common/utils'
import { 
  Colors,
  windowWidth,
  windowHeight,
 } from '../../../core/theme'
import { PlainHeader } from '../../../common/components/Headers'
 
interface IState {

}

interface IStateProps {
  
}

interface IProps {

}

interface IDispatchProps {
  //navigateToEssayFinish(): void
}

// @connectAdv(
//   null,
//   (dispatch: Dispatch): IDispatchProps => ({
//     navigateToEssayFinish(): void {
//       dispatch(NavigationActions.navigateToEssayFinish())
//     },
//   })
// )
export class WritingEssayNoFeedback extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {
  static navigationOptions = PlainHeader(l.training.essay, true)
  
  render(): JSX.Element {
    return( 
      <ScrollView contentContainerStyle={styles.bgStyle}>
        <View>
          <KeyboardAwareScrollView>
            <View style={styles.content}>
              <View>
                <View style={styles.titleContainer}>
                  <Text style={styles.title}>There are a lot of cars and buses</Text>
                  <View style={styles.underline} />
                </View>
              </View>
              <View style={styles.inputContainer}>
                <TextInput 
                  editable={true}
                  multiline={true}
                  placeholder={l.training.writeHereYourEssay}
                />
              </View>
            </View>
          </KeyboardAwareScrollView>
        </View>
      </ScrollView>
    )
  }
}

const styles = styleSheetCreate({
  bgStyle: {
    flexGrow: 1,
    padding: windowWidth * .05,
    backgroundColor: Colors.greyE5,
  } as ViewStyle,
  content: {
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
  } as ViewStyle,
  titleContainer: {
    paddingTop: windowWidth * .02,
    width: windowWidth * .9,
    alignItems: 'flex-start',
  } as ViewStyle,
  inputContainer: {
    marginVertical: windowWidth * .04,
    height: windowHeight * .6,
    padding: windowWidth * .02,
    backgroundColor: Colors.white,
    borderWidth: windowWidth * .005,
    borderColor: Colors.greyDB,
    borderStyle: 'solid',
    borderRadius: windowWidth * .04,
    width: windowWidth * .9,
  } as ViewStyle,
  title: {
    color: Colors.darkGrey9F,
		fontSize: windowWidth * .045,
  } as TextStyle,
  description: {
    paddingLeft: windowWidth * .04,
    color: Colors.dark5E,
		fontSize: windowWidth * .045,
  } as TextStyle,
  underline: {
    backgroundColor: Colors.greyDB,
    marginTop: windowWidth * .02,
    height: windowWidth * .005,
    width: windowWidth * .9,
  } as ViewStyle,
})
