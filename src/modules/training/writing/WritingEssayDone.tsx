import React from 'react'
import { 
  View,
  ViewStyle,
  Text,
  TextStyle,
  ScrollView,
  Image,
  ImageStyle,
 } from 'react-native'
 import { Dispatch } from 'redux'
 import LinearGradient from 'react-native-linear-gradient'
 import { ImageResources } from "../../../common/ImageResources.g"
 import { localization as l } from '../../../common/localization/localization'
 import { BaseReduxComponent } from '../../../core/BaseComponent'
 import { NavigationActions } from '../../../navigation/navigation'
 import { connectAdv } from '../../../core/store'
import { styleSheetCreate } from '../../../common/utils'
import { 
  Colors,
  windowWidth,
 } from '../../../core/theme'
import { PlainHeader } from '../../../common/components/Headers'
 
interface IState {

}

interface IStateProps {
  
}

interface IProps {

}

interface IDispatchProps {
  navigateToEssayFinish(data: any): void
}

@connectAdv(
  null,
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToEssayFinish(data): void {
      dispatch(NavigationActions.navigateToEssayFinish(data))
    },
  })
)
export class WritingEssayDone extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {
  static navigationOptions = PlainHeader(l.training.essay, true)
  
  render(): JSX.Element {
    return( 
      <ScrollView contentContainerStyle={styles.bgStyle}>
        <View style={styles.content}>
          <View style={styles.titleContainer}>
            <Text style={styles.title}>There are a lot of cars and buses</Text>
            <View style={styles.underline} />
          </View>
          <View style={styles.feedbackContainer}>
            <Text style={styles.description}>{l.training.teacherFeedback}:</Text>
            <LinearGradient
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              colors={['#4E71EC', '#4EAAEC']}
              style={styles.gradientContainer}
            >
              <Text style={styles.feedbackText}>{l.training.greatComplete}</Text>

              <View style={styles.stars}>
                <Image 
                  style={styles.star}               
                  source={ImageResources.starOn}
                />
                <Image
                  style={styles.star} 
                  source={ImageResources.starOn}
                />
                <Image
                  style={styles.star}                
                  source={ImageResources.starOn}
                />
                <Image   
                  style={styles.star}                
                  source={ImageResources.starOn}
                />
                <Image 
                  style={styles.star}                  
                  source={ImageResources.starOff}
                />
              </View>
            </LinearGradient>
          </View>
        </View>
      </ScrollView>
    )
  }
}

const styles = styleSheetCreate({
  bgStyle: {
    flexGrow: 1,
    padding: windowWidth * .05,
    backgroundColor: Colors.greyE5,
  } as ViewStyle,
  content: {
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
  } as ViewStyle,
  titleContainer: {
    paddingTop: windowWidth * .02,
    width: windowWidth * .9,
    alignItems: 'flex-start',
  } as ViewStyle,
  title: {
    color: Colors.darkGrey9F,
		fontSize: windowWidth * .039,
  } as TextStyle, 
  description: {
    color: Colors.dark5E,
    fontSize: windowWidth * .039,
    paddingBottom: windowWidth * .04,
  } as TextStyle,
  underline: {
    backgroundColor: Colors.greyDB,
    marginTop: windowWidth * .02,
    height: windowWidth * .005,
    width: windowWidth * .9,
  } as ViewStyle,
  stars: {
    paddingTop: windowWidth * .025,
    flexDirection: 'row',
  } as ViewStyle,
  star: {
    width: windowWidth * .047,
    height: windowWidth * .045,
    marginRight: windowWidth * .014,
  } as ImageStyle,
  feedbackContainer: {
    
  } as ViewStyle,
  feedbackText: {
    color: Colors.white,
    fontSize: windowWidth * .039,
  } as TextStyle,
  gradientContainer: {
    width: windowWidth * .9,
    padding: windowWidth * .05,
    borderRadius: windowWidth * .025,
  } as ViewStyle,
})
