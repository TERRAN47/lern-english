import React from 'react'
import {
  View,
  ViewStyle,
  Text,
  TextStyle,
  ScrollView,
} from 'react-native'
import { BaseReduxComponent } from '../core/BaseComponent'
import { styleSheetCreate } from '../common/utils'
import { Colors, windowWidth } from '../core/theme'
import { PlainHeader } from '../common/components/Headers'

interface IStateProps {

}

interface IDispatchProps {

}

interface IState {

}

interface IProps {

}

export class About extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  static navigationOptions = PlainHeader("О компании", true, false)

  render(): JSX.Element {
    return (
      <View style={styles.container}>
        <ScrollView>
          <Text style={styles.info}>
            Почему он используется?
            Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. 
            Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение 
            шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при 
            простой дубликации "Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст.." Многие программы 
            электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, 
            так что поиск по ключевым словам "lorem ipsum" сразу показывает, как много веб-страниц всё 
            ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много 
            версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).
            Откуда он появился?
            Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов, но это не совсем так. 
            Его корни уходят в один фрагмент классической латыни 45 года н.э., то есть более двух тысячелетий назад. 
            Ричард МакКлинток, профессор латыни из колледжа Hampden-Sydney, штат Вирджиния, взял одно из самых 
            странных слов в Lorem Ipsum, "consectetur", и занялся его поисками в классической латинской литературе. 
            В результате он нашёл неоспоримый первоисточник Lorem Ipsum в разделах 1.10.32 и 1.10.33 книги "de Finibus 
            Bonorum et Malorum" ("О пределах добра и зла"), написанной Цицероном в 45 году н.э. Этот трактат по теории 
            этики был очень популярен в эпоху Возрождения. Первая строка Lorem Ipsum, "Lorem ipsum dolor sit amet..", 
            происходит от одной из строк в разделе 1.10.32
            Классический текст Lorem Ipsum, используемый с XVI века, приведён ниже. Также даны разделы 1.10.32 и 1.10.33
            "de Finibus Bonorum et Malorum" Цицерона и их английский перевод, сделанный H. Rackham, 1914 год.
          </Text>
        </ScrollView>
      </View>
    )
  }
}

const styles = styleSheetCreate({
  container: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    backgroundColor: Colors.white,
    padding: windowWidth * .047,
  } as ViewStyle,
  contacts: {
    paddingTop: windowWidth * .06,
    marginBottom: windowWidth * .02,
    justifyContent: 'space-around',
    height: windowWidth * 0.57,
  } as ViewStyle,
  info: {
    color: Colors.dark5E,
    fontSize: windowWidth * .0415,
    paddingTop: windowWidth * .025,
    lineHeight: windowWidth * .071,
  } as TextStyle,
})
