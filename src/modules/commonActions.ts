import {actionCreator} from "../core/store";

export class CommonActions {
  static netStatus = actionCreator<boolean>("Common/NET_STATUS")
  static saveProvile = actionCreator<{}>("Common/SAVE_PROFILE")
  static updateEssayStatus = actionCreator<{}>("Common/UPDATE_ESSAY_STATUS")
}