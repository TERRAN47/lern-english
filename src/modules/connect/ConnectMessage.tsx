import React from "react"
import { BaseReduxComponent } from '../../core/BaseComponent'
import {
  View,
  ViewStyle,
  FlatList,
  TextInput,
  Image,
  ImageStyle,
  TouchableOpacity,
} from 'react-native'
import { styleSheetCreate } from '../../common/utils'
import { ImageResources } from '../../common/ImageResources.g'
import { Colors, windowWidth } from '../../core/theme'
import { PlainHeader } from '../../common/components/Headers'
import { MessageItem } from '../../common/components/MessageItem'


interface IStateProps {

}

interface IDispatchProps {

}

interface IState {

}

interface IProps {

}

interface IConnectItems {
  foto?: ImageResources
  text: string
  name: string
  messageIn?: boolean
}



export class ConnectMessage extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

static navigationOptions = PlainHeader('Преподаватель', true, true)
  
  connectItems: IConnectItems[] = [
    {
      foto: ImageResources.user1,
      text: 'Добрый день, Иван! Я буду вашим куратором английского языка. У Вас какие нибудь вопросы по обучению?',
      name: 'Преподаватель',
      messageIn: true,
    },
    {
      text: 'Добрый день! Сколько домашних заданий у меня будет в неделю? И как быстро их проверяют?', 
      name: 'Преподаватель',
    },
    {
      foto: ImageResources.user1,
      text: 'Добрый день, Иван! 4 Домашних задания. Обычно проверяют в течении суток',
      name: 'Преподаватель',
      messageIn: true,
    },
  ]

  changeEmail = (text:string): void => {
    this.setState({email:text})
  }

  keyExtractor = (item: any): string => item.title

  connectItem = ({ item: { foto, text, messageIn, name } }: any): JSX.Element => (
    <View>
      <MessageItem
        foto={foto}
        text={text}
        messageIn={messageIn}
        name={name}
      />
    </View>
  )

  render(): JSX.Element {
    return (
      <View style={styles.container}>
        <FlatList            
          data={this.connectItems}
          renderItem={this.connectItem}
          keyExtractor={this.keyExtractor}
        />
        <View style={styles.sendMessage}>
          <TextInput 
            style={styles.input}
            editable={true}
            multiline={true}
            placeholder={'Send a message'}
          />
          <TouchableOpacity>
            <Image
              style={styles.sendIcon}
              source={ImageResources.sendMessage}
            />
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = styleSheetCreate({
  container: {
    flex: 1,
    backgroundColor: Colors.greyE5,
    alignItems: 'center',
  } as ViewStyle,
  sendMessage: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: windowWidth * .05,
    height: windowWidth * .15,
    width: windowWidth,
    backgroundColor: Colors.white,
    borderStyle: 'solid',
    borderColor: Colors.greyCE,
    borderBottomWidth: windowWidth * .004,
    borderTopWidth: windowWidth * .004,
  } as ViewStyle, 
  input: {
    width: windowWidth * .8,
  } as ViewStyle,
  sendIcon: {
    width: windowWidth * .05,
    height: windowWidth * .042,
  } as ImageStyle,
})
