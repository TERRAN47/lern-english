import React from "react"
import {
  View,
  ViewStyle,
  FlatList,
} from 'react-native'
import { Dispatch } from 'redux'
import { BaseReduxComponent } from '../../core/BaseComponent'
import { NavigationActions } from '../../navigation/navigation'
import { connectAdv } from '../../core/store/connectAdv'
import { PlainHeader } from '../../common/components/Headers'
import { styleSheetCreate } from '../../common/utils'
import { ImageResources } from '../../common/ImageResources.g'
import { Colors, windowWidth } from '../../core/theme'
import { ConnectItem } from '../../common/components/ConnectItem'


interface IStateProps {

}

interface IDispatchProps {
  navigateToConnectMessage(): void
}

interface IState {

}

interface IProps {

}

interface IConnectItems {
  foto?: ImageResources
  title: string
  message?: number
  online?: boolean
  onPress(): void
}


@connectAdv(
  null,
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToConnectMessage(): void {
      dispatch(NavigationActions.navigateToConnectMessage())
    },
  })
)
export class Connect extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

static navigationOptions = PlainHeader('Обратная связь', true, true)
  
  connectItems: IConnectItems[] = [
    {
      foto: ImageResources.user1,
      title: 'Преподаватель',
      message: 2,
      online: true,
      onPress: this.dispatchProps.navigateToConnectMessage,
    },
    {
      title: 'Куратор',      
      onPress: () => {alert('alert')}
    },
  ]

  keyExtractor = (item: any): string => item.title

  connectItem = ({ item: { foto, title, message, online, onPress } }: any): JSX.Element => (
    <View>
      <ConnectItem
        foto={foto}
        title={title}
        message={message}
        online={online}
        onPress={onPress}
      />
    </View>
  )

  render(): JSX.Element {
    return (
      <View style={styles.container}>
        <FlatList            
          data={this.connectItems}
          renderItem={this.connectItem}
          keyExtractor={this.keyExtractor}
        />
      </View>
    )
  }
}

const styles = styleSheetCreate({
  container: {
    flex: 1,
    backgroundColor: Colors.greyE5,
    alignItems: 'center',
    paddingTop: windowWidth * .05,
  } as ViewStyle,
})
