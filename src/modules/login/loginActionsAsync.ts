import {Dispatch} from "redux";
// import {AuthHelper} from "../../common/helpers/authHelper";
// import {localization} from "../../common/localization/localization";
import {SimpleThunk} from "../../common/simpleThunk";
// import {SignInRequestDto} from "../../core/api/generated/dto/SignInRequest.g";
import {requestsRepository} from "../../core/api/requestsRepository";
// import {AuthActions, IAuthParams} from "./authActions";
import { LoginActions } from "./loginActions";

const uploadFiles = async (): Promise<any> => {

  try {
    await requestsRepository.trainingApiRequest.getPhonetics(true);
    await requestsRepository.trainingApiRequest.getVocabularys(true);
    await requestsRepository.homeWorkApiRequest.getHomeWork(true);
    await requestsRepository.homeApiRequest.getHomeCalendar(true);
    await requestsRepository.homeApiRequest.getHomeStatistic(true);
    await requestsRepository.trainingApiRequest.getGrammars(true);
    await requestsRepository.trainingApiRequest.getAuditions(true);

    setTimeout((): any => {
      return "ok";
    }, 3500);
  } catch (error) {
    console.log("ERROR AUTH LOAD FILE", error);
  }
};

export class LoginActionsAsync {
  static login(request: any): SimpleThunk {
    return async (dispatch: Dispatch): Promise<void> => {
      try {
        dispatch(LoginActions.authUser.started(request));
        const respons = await requestsRepository.loginApiRequest.signIn(request);
        await requestsRepository.loginApiRequest.getLists();
        await uploadFiles();

        dispatch(LoginActions.authUser.done({
          params: request, result: respons
        }));
      } catch (error) {
        console.log(7, error);
        dispatch(LoginActions.authUser.failed(error));
      }
    };
  }

  static registration(request: any): SimpleThunk {
    return async (dispatch: Dispatch): Promise<void> => {
      try {
        dispatch(LoginActions.registerUser.started(request));
        const response = await requestsRepository.loginApiRequest.registration(request);
        await requestsRepository.loginApiRequest.getLists();
        await uploadFiles();

        dispatch(LoginActions.registerUser.done({
          params: request, result: response
        }));
      } catch (err) {
        dispatch(LoginActions.registerUser.failed(err));
      }
    };
  }
}