import React from "react";
import {
  View,
  ViewStyle,
  Text,
  TouchableOpacity,
  TextStyle,
} from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { Dispatch } from "redux";
import { BaseReduxComponent } from "../../core/BaseComponent";
import { NavigationActions } from "../../navigation/navigation";
import { connectAdv } from "../../core/store/connectAdv";
import { styleSheetCreate } from "../../common/utils";
import { Colors, windowWidth, windowHeight } from "../../core/theme";
import { localization as l } from "../../common/localization/localization";
import { LoginActionsAsync } from "./loginActionsAsync";
//import { string } from 'prop-types';
import {IAppState} from "../../core/store/appState";
import { ButtonType } from "../../common/enums/buttonType";
import { LoginActions } from "./loginActions";
import { PlainHeader } from "../../common/components/Headers";
import { Button } from "../../common/components/Button";
import { LoadingView } from "../../common/components/LoadingView";
import { MainModal } from "../../common/components/MainModal";
import { TransparentButton } from "../../common/components/TransparentButton";
import { InputLogin } from "../../common/components/InputLogin";

interface IStateProps {
  registerResult: {x_token: string} | null;
  error: {} | string | null;
  isLoading: boolean;
  modalStatus: boolean;
}

interface IDispatchProps {
  registUser(username: string, password: string): void;
  clearState(): void;
  modalStatus(status: boolean): void;
  navigateToHome(): void;
}

interface IState {
  username: string;
  password: string;
  rePass: string;
  errorRequest: string;
  isModalVisible: boolean;
  emailError: boolean;
  passError: boolean;
}

interface IProps {
}

@connectAdv(
  (state: IAppState): IStateProps => ({
    registerResult: state.login.registerResult,
    error: state.login.error,
    isLoading: state.login.isLoading,
    modalStatus: state.login.modalStatus
  }),
  (dispatch: Dispatch): IDispatchProps => ({

    registUser(username: string, password: string): void {
      //@ts-ignore
      dispatch(LoginActionsAsync.registration({username, password}));
    },
    navigateToHome(): void {
      dispatch(NavigationActions.navigateToHome());
    },
    clearState(): void {
      //@ts-ignore
      dispatch(LoginActions.clearState());
    },
    modalStatus(status: boolean): void {
      dispatch(LoginActions.modalStatus(status));
    }
  })
)

export class Registration extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {
  constructor(props: any) {
    super(props);

    this.state = {
      username: "",
      password: "",
      rePass: "",
			errorRequest: "",
			isModalVisible: false,
			passError: false,
			emailError: false
		};
	}

	static navigationOptions = PlainHeader(l.common.regestration, true,  false, false)
	
  componentWillUnmount(): any{
    this.dispatchProps.clearState()
  }

  componentWillMount = async (): Promise<any> => {
    await this.dispatchProps.clearState();
  }

  changeEmail = (text: string): void => {
    this.setState({username: text});
  }

  changePass = (text: string): void => {
    this.setState({password: text});
  }

  changeRePasss = (text: string): void => {
    this.setState({rePass: text});
  }

  private closeModal = (): void => {
    this.setState({ isModalVisible: false});
  }

  private openModal = (error: string): void => {
    this.setState({ isModalVisible: true, errorRequest: error});
  }

  private registUser = (): any => {
		const {username, password, rePass} = this.state
		if(rePass === password && username !== ''){
			this.dispatchProps.registUser(username, password)
		}else if(username == ''){
			this.setState({emailError:true, passError:false})
			this.openModal(l.auth.fillEmailField)			
		}else{
			this.setState({emailError:false, passError:true})
			this.openModal(l.auth.passDoNotMatch)
		}
	}
	
	render(): JSX.Element {

    const {isLoading, registerResult, error} = this.stateProps;

    console.log(4, registerResult, isLoading, error);

    const {isModalVisible, passError, emailError, errorRequest} = this.state;

    return (
      <KeyboardAwareScrollView>
        <View style={styles.container}>
					<LoadingView isLoading={isLoading} />
					<Text style={styles.title}>
						{l.common.regestration.toUpperCase()}
					</Text>
					<View>
						{/* <InputLogin
							placeholder={l.common.firstName}
							secureTextEntry={false}
						/> */}
            <InputLogin
							placeholder={l.common.email}
							secureTextEntry={false}
							isError={error || emailError ? true : false}
							changeText={this.changeEmail}
						/>
						{/* <InputLogin
							placeholder={l.common.tel}
							secureTextEntry={false}
						/> */}
						<InputLogin
							placeholder={l.common.pass}
							secureTextEntry={true}
							isError={passError ? true : false}
							changeText={this.changePass}
						/>
						<InputLogin
							placeholder={l.common.repeatPass}
							isError={passError ? true : false}
							secureTextEntry={true}
							changeText={this.changeRePasss}
						/>
					</View>
					<MainModal
						title={l.common.error}
						body={errorRequest}
						isVisible={isModalVisible}
						closeModal={this.closeModal}
					>
						<TransparentButton
							title={l.common.ok}
							buttonType={ButtonType.positive}
							onPress={this.closeModal}
						/>
					</MainModal>
					<TouchableOpacity onPress={this.registUser}>
						<Button titleButton={l.common.send} />
					</TouchableOpacity>
				</View>
			</KeyboardAwareScrollView>
    );
  }
}

const styles = styleSheetCreate({
  container: {
    height: windowHeight * .8,
    alignItems: "center",
    justifyContent: "space-around",
    backgroundColor: Colors.white,
  } as ViewStyle,
  title: {
    color: Colors.dark5E,
    fontSize: windowWidth * .051,
  } as TextStyle,
});
