import {Failure, Success} from "typescript-fsa";
import {reducerWithInitialState} from "typescript-fsa-reducers";
import {newState} from "../../common/newState";
//import {CoreActions} from "../../core/store";
import {LoginActions} from "./loginActions";
import {LoginInitialState, ILoginState} from "./loginState";

function clearState(state: ILoginState): ILoginState {
  return newState(state, {modalStatus:false, error: null});
}

function modalState(state: ILoginState, payload: boolean): ILoginState {
  return newState(state, {modalStatus:payload});
}

function signInStarted(state: ILoginState): ILoginState {
  return newState(state, {
    isLoading: true,
    loginResult:null,
    error: null
  })
}
function signInDone(state: ILoginState, payload: Success<any, any>): ILoginState {
  return newState(state, {
    isLoading: false,
    loginResult:payload.result,
    error: null
  })
}
function signInFailed(state: ILoginState, failed: Failure<any, Error>): ILoginState {
  return newState(state, {
    isLoading: false,
    loginResult:null,
    modalStatus:true,
    error: failed.error,
  })
}

//registartion
function registrationStarted(state: ILoginState): ILoginState {
  return newState(state, {
    isLoading: true,
    registerResult:null,
    error: null
  })
}
function registrationDone(state: ILoginState, payload: Success<any, any>): ILoginState {
  return newState(state, {
    isLoading: false,
    registerResult:payload.result,
    error: null
  })
}
function registrationFailed(state: ILoginState, failed: Failure<any, Error>): ILoginState {

  return newState(state, {
    isLoading: false,
    registerResult:null,
    modalStatus:true,
    error: failed.error,
  })
}

export const loginReducer = reducerWithInitialState(LoginInitialState)
  .case(LoginActions.clearState, clearState)

  .case(LoginActions.modalStatus, modalState)
  
  .case(LoginActions.authUser.started, signInStarted)
  .case(LoginActions.authUser.done, signInDone)
  .case(LoginActions.authUser.failed, signInFailed)

  //registration
  .case(LoginActions.registerUser.started, registrationStarted)
  .case(LoginActions.registerUser.done, registrationDone)
  .case(LoginActions.registerUser.failed, registrationFailed)
