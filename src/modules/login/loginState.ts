export interface ILoginState {
  isLoading: boolean;
  loginResult:{token:string} | null;
  registerResult:{x_token:string} | null;
  modalStatus:boolean;
  error: {} | string | null;
}

export const LoginInitialState: ILoginState = {
  isLoading: false,
  loginResult:{token:''},
  registerResult:{x_token:''},
  modalStatus:false,
  error: "",
};
