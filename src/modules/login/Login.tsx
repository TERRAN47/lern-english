import React from "react";
import {
  View,
  ViewStyle,
  TouchableOpacity,
  Image,
  ImageStyle,
} from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { Dispatch } from "redux";
import { BaseReduxComponent } from "../../core/BaseComponent";
import { connectAdv } from "../../core/store/connectAdv";
import { NavigationActions } from "../../navigation/navigation";
import { styleSheetCreate } from "../../common/utils";
import { Colors, windowWidth, windowHeight } from "../../core/theme";
import { InputLogin } from "../../common/components/InputLogin";
import { localization as l } from "../../common/localization/localization";
import { ImageResources } from "../../common/ImageResources.g";
import { Button } from "../../common/components/Button";
import { LoadingView } from "../../common/components/LoadingView";
import { MainModal } from "../../common/components/MainModal";
import { TransparentButton } from "../../common/components/TransparentButton";
import { ButtonType } from "../../common/enums/buttonType";
import { NoHeader } from "../../common/components/Headers";
import { LoginActionsAsync } from "./loginActionsAsync";
import { IAppState } from "../../core/store/appState";
import { LoginActions } from "./loginActions";

interface IStateProps {
  loginResult: {token: string} | null;
  error: {} | string | null;
  isLoading: boolean;
  modalStatus: boolean;
}
interface IDispatchProps {
  navigateToRegistration(): void;
  signIn(username: string, password: string): void;
  clearState(): void;
  modalStatus(status: boolean): void;
}

interface IState {
	email: string;
  pass: string;
  errorRequest: string | null;
	isModalVisible: boolean;
	typeError: {title: string, body: string};
}

interface IProps {

}
@connectAdv(
  (state: IAppState): IStateProps => ({
    loginResult: state.login.loginResult,
    error: state.login.error,
    isLoading: state.login.isLoading,
    modalStatus: state.login.modalStatus
  }),
  (dispatch: Dispatch): IDispatchProps => ({
    navigateToRegistration(): void {
      dispatch(NavigationActions.navigateToRegistration());
    },
    signIn(username, password): void {
      //@ts-ignore
      dispatch(LoginActionsAsync.login({username, password}));
    },
    clearState(): void {
      dispatch(LoginActions.clearState());
    },
    modalStatus(status: boolean): void {
      dispatch(LoginActions.modalStatus(status));
    }
  })
)

export class Login extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {
  static navigationOptions = NoHeader();

  constructor(props: any) {
    super(props);

    this.state = {
      email: "",
      pass: "",
      errorRequest: "",
      isModalVisible: false,
      typeError: {title: "", body: ""}
    };
  }

  componentWillUnmount() {
    this.dispatchProps.clearState();
  }

  async componentWillMount() {
    await this.dispatchProps.clearState();
  }

  changeEmail = (text: string): void => {
    this.setState({email: text});
  }

  changePass = (text: string): void => {
		this.setState({pass: text});
  }
  
  private openModal = (error: string | null): void => {
    this.dispatchProps.modalStatus(true);
    this.setState({ isModalVisible: true, errorRequest: error});
  };

  private closeModal = (): void => {
    this.setState({ isModalVisible: false, errorRequest: null});
    this.dispatchProps.modalStatus(false);
  }

  private loginApi = (): any =>{
    const {email, pass} = this.state
    if(email && pass){
      this.dispatchProps.signIn(email, pass)
    }else{
      this.openModal(l.auth.fillLoginPassword)
    }
  }

  render(): JSX.Element {
    let {isLoading, modalStatus, error} = this.stateProps;
    let {errorRequest, isModalVisible} = this.state;

    return (
      <KeyboardAwareScrollView style={{flexGrow: 1}}>
        <View style={styles.container}>
          <LoadingView isLoading={isLoading} />
          <View>
            <Image
              style={styles.imageLogo}
              source={ImageResources.logo}
            />
          </View>
          <View>
            <InputLogin
              placeholder={l.common.login}
              secureTextEntry={false}
              isError={error ? true : false}
              changeText={this.changeEmail}
            />
            <InputLogin
              placeholder={l.common.pass}
              secureTextEntry={true}
              isError={error ? true : false}
              changeText={this.changePass}
            />
          </View>
          <View>
            <TouchableOpacity onPress={this.loginApi}>
              <Button titleButton={l.common.enter} />
            </TouchableOpacity>
            <View style={{height: 20}}></View>
            <TouchableOpacity onPress={this.dispatchProps.navigateToRegistration}>
              <Button titleButton={l.common.regestration} />
            </TouchableOpacity>
          </View>
          <MainModal
            title={l.common.error}
            body={errorRequest ? errorRequest : error}
            isVisible={modalStatus ? modalStatus : isModalVisible}
            closeModal={this.closeModal}
            >
            <TransparentButton
              title={l.common.ok}
              buttonType={ButtonType.positive}
              onPress={this.closeModal}
            />
          </MainModal>
        </View >
      </KeyboardAwareScrollView>
    );
  }
}

const styles = styleSheetCreate({
  container: {
    height: windowHeight,
    backgroundColor: Colors.white,
    alignItems: "center",
    justifyContent: "space-around",
  } as ViewStyle,
  imageLogo: {
    width: windowWidth * .4,
    height: windowWidth * .18,
  } as ImageStyle,
});
