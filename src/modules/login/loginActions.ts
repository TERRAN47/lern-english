import {actionCreator} from "../../core/store";
//import {ErrorSource} from "./authState";
//import {SignInRequestDto} from "../../core/api/generated/dto/SignInRequest.g";
//import {Profile} from "../../core/api/generated/dto/Profile.g";

export class LoginActions {
  static authUser = actionCreator.async<IEmpty, any, Error>("Login/AUTH_USER");
  static registerUser = actionCreator.async<IEmpty, any, Error>("Login/REGISTER_USER");
  static clearState = actionCreator("Login/CLEAR_STATE");
  static modalStatus = actionCreator<boolean>("Login/MODAL_STATE");
}