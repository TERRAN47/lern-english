import { Profile, IEssayStatus } from "../core/api/generated/dto/CommonResponse.g"

export interface CommonState {
  netStatus: boolean
  sentEssayStatus: IEssayStatus
  user: Profile
}

export const CommonInitialState: CommonState  = {
  netStatus: true,

  sentEssayStatus: {
    sentEssayReading: false,
  },
  user:{ 
    userLevel: "9", 
    firstName: "", 
    surName: "", 
    userPhone: "", 
    userEmail: "",
    company: "", 
    userTariff: "5",
  }
}
