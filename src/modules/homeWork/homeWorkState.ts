export interface IhomeWorkState {
  isLoading: boolean;
  homeResult: {} | null;
  error: {} | string | null;
}

export const HomeWorkInitialState: IhomeWorkState = {
  isLoading: false,
  homeResult: null,
  error: null,
};
