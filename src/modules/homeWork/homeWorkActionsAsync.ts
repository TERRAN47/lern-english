import {Dispatch} from "redux";
//import {SimpleThunk} from "../../common/simpleThunk";
// import {SignInRequestDto} from "../../core/api/generated/dto/SignInRequest.g";
import {requestsRepository} from "../../core/api/requestsRepository";
import { HomeWorkActions } from './homeWorkActions'
import _ from 'underscore'
export class homeWorkActionsAsync {
  static getHomeWork(netStatus:boolean): any {
    return async (dispatch: Dispatch): Promise<void> => {
      try{
        dispatch(HomeWorkActions.homeWork.started({isLoading: true}))
        const response = await requestsRepository.homeWorkApiRequest.getHomeWork(netStatus)
        let mapObject = response.user_dz

        // if(mapObject){
        //   mapObject.map((elem:any)=>{
        //     _.findWhere(elem, {
              
        //     })
        //   })

        // } 

        console.log('work',mapObject)
        dispatch(HomeWorkActions.homeWork.done({
          params:{isLoading: true}, result:mapObject
        }))
      }catch(err){
        dispatch(HomeWorkActions.homeWork.failed(err))
      }
    };
  }
}