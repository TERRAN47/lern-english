import React from "react"
import { BaseReduxComponent } from '../../core/BaseComponent'
import {
  View,
  ViewStyle,
  Text,
  FlatList,
  ListRenderItemInfo,
} from 'react-native'
import { PlainHeader } from '../../common/components/Headers'
import { styleSheetCreate } from '../../common/utils'
import { Colors, windowWidth } from '../../core/theme'
import { localization as l } from '../../common/localization/localization'
import { HomeworkItem } from '../../common/components/HomeworkItem'
import { connectAdv } from '../../core/store/connectAdv'
import { Dispatch } from 'redux'
import { IAppState } from "../../core/store/appState";
import { homeWorkActionsAsync } from './homeWorkActionsAsync'
import { LoadingView } from '../../common/components/LoadingView'
//import { getJsonFile, writeJsonFile } from '../../../common/helpers/appFiles'

interface IStateProps {
  homeResult: any,
  error: {} | string | null,
  isLoading: boolean,
  netStatus: boolean
}

interface IDispatchProps {
  getHomeWork:(netStatus: boolean)=>void
}

interface IState {

}

interface IProps {

}

interface IHomeworkListItem {
  category: string,
  title: string,
  status: string,
  onPress(): void
}

interface ListItem extends ListRenderItemInfo<IHomeworkListItem> {
  item: IHomeworkListItem
}

@connectAdv(
  (state: IAppState): IStateProps => ({
    homeResult: state.homeWork.homeResult,
    error: state.homeWork.error,
    isLoading: state.homeWork.isLoading,
    netStatus: state.common.netStatus,
  }),
  (dispatch: Dispatch): IDispatchProps => ({
    getHomeWork(netStatus){
     
      dispatch(homeWorkActionsAsync.getHomeWork(netStatus))
    }
  })
)
export class HomeWork extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  static navigationOptions = PlainHeader(l.training.homework, true, true)
  
  homeworkItems: IHomeworkListItem[] = [
    // {
    //   category: '1. Грамматика',
    //   title: 'Reported speech (A1)',
    //   status: 'isDone',
    //   onPress: () => { alert('TODO: onHomeworkListItemPress') }
    // },
    // {
    //   category: '2. Идиомы',
    //   title: 'Animal world',
    //   status: 'isSentReview',
    //   onPress: () => { alert('TODO: onHomeworkListItemPress') }
    // },
    // {
    //   category: '3. Сленг',
    //   title: 'Money',
    //   status: 'isInToDo',
    //   onPress: () => { alert('TODO: onHomeworkListItemPress') }
    // },
    // {
    //   category: '4. Материалы',
    //   title: 'Глоссарии',
    //   status: '',
    //   onPress: () => { alert('TODO: onHomeworkListItemPress') }
    // },
    // {
    //   category: '5. Грамматика',
    //   title: 'Animal world',
    //   status: '',
    //   onPress: () => { alert('TODO: onHomeworkListItemPress') }
    // },
    // {
    //   category: '6. Грамматика',
    //   title: 'Animal world',
    //   status: '',
    //   onPress: () => { alert('TODO: onHomeworkListItemPress') }
    // },
    // {
    //   category: '7. Грамматика',
    //   title: 'Animal world',
    //   status: '',
    //   onPress: () => { alert('TODO: onHomeworkListItemPress') }
    // },
    // {
    //   category: '8. Грамматика',
    //   title: 'Animal world',
    //   status: '',
    //   onPress: () => { alert('TODO: onHomeworkListItemPress') }
    // }
  ]

  keyExtractor = (item: IHomeworkListItem): string => item.category

  renderItem = ({ item, index }: ListItem): JSX.Element => {
    const { category, title, status, onPress } = item

    return (
      <HomeworkItem
        category={category}
        title={title}
        status={status}
        onPress={onPress}
        isFirstItem={!index}
        isLastItem={index === this.homeworkItems.length - 1}
      />
    )
  }

  async compoentDidMount(){
    let {netStatus} = this.stateProps
    this.dispatchProps.getHomeWork(netStatus)
  }

  render(): JSX.Element {
    let {homeResult, isLoading, error} = this.stateProps
    console.log(12, homeResult, isLoading, error)
    
    return (
      <View style={styles.container}>
        <LoadingView isLoading={isLoading} />
        {
          homeResult && homeResult.user_dz ?
            <FlatList
              data={this.homeworkItems}
              renderItem={this.renderItem}
              keyExtractor={this.keyExtractor}
            />
          :
            isLoading ?
              null
            :
              <View style={styles.containerNone}>
                <Text>Домашних заданий нет!</Text>
              </View>
        }
      </View>
    )
  }
}

const styles = styleSheetCreate({
  container: {
    flex: 1,
    backgroundColor: Colors.greyE5,
    paddingHorizontal: windowWidth * .03,
  } as ViewStyle,
  containerNone: {
    flex: 1,
    backgroundColor: Colors.greyE5,
    paddingHorizontal: windowWidth * .03,
    justifyContent:'center',
    alignItems:'center'
  } as ViewStyle,
})