import {Failure, Success} from "typescript-fsa";
import {reducerWithInitialState} from "typescript-fsa-reducers";
import {newState} from "../../common/newState";
import {HomeWorkActions} from "./homeWorkActions";
import {HomeWorkInitialState, IhomeWorkState} from "./homeWorkState";

// function clearState(state: IhomeWorkState): IhomeWorkState {
//   return newState(state, {modalStatus:false, error: null});
// }

// function modalState(state: IhomeWorkState, payload: boolean): IhomeWorkState {
//   return newState(state, {modalStatus:payload});
// }

function homeWorkStarted(state: IhomeWorkState): IhomeWorkState {
  return newState(state, {
    isLoading: true,
    homeResult:null,
    error: null
  })
}

function homeWorkDone(state: IhomeWorkState, payload: Success<any, any>): IhomeWorkState {
  return newState(state, {
    isLoading: false,
    homeResult:payload.result,
    error: null
  })
}

function homeWorkFailed(state: IhomeWorkState, failed: Failure<any, Error>): IhomeWorkState{

  return newState(state, {
    isLoading: false,
    homeResult:null,
    //modalStatus:true,
    error: failed.error,
  })
}

export const homeWorkReducer = reducerWithInitialState(HomeWorkInitialState)
  // .case(HomeWorkActions.clearState, clearState)

  // .case(HomeWorkActions.modalStatus, modalState)
  
  .case(HomeWorkActions.homeWork.started, homeWorkStarted)
  .case(HomeWorkActions.homeWork.done, homeWorkDone)
  .case(HomeWorkActions.homeWork.failed, homeWorkFailed)
