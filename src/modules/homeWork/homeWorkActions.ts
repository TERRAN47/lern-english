import {actionCreator} from "../../core/store";
//import {ErrorSource} from "./authState";
//import {SignInRequestDto} from "../../core/api/generated/dto/SignInRequest.g";
//import {Profile} from "../../core/api/generated/dto/Profile.g";

export class HomeWorkActions {
  static homeWork = actionCreator.async<IEmpty, any, Error>("homeWork/GET_HOMEWORK");
}