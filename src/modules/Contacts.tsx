import React from 'react'
import {
  View,
  ViewStyle,
  Text,
  TextStyle,
} from 'react-native'
import { BaseReduxComponent } from '../core/BaseComponent'
import { styleSheetCreate } from '../common/utils'
import { Colors, windowWidth } from '../core/theme'
import { localization as l } from '../common/localization/localization'
import { PlainHeader } from '../common/components/Headers'

interface IStateProps {

}

interface IDispatchProps {

}

interface IState {

}

interface IProps {

}

export class Contacts extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  static navigationOptions = PlainHeader(l.common.aboutCompany, true, false)

  render(): JSX.Element {
    return (
      <View style={styles.container}>
        <Text style={styles.info}>
          {l.common.company}
        </Text>
        <Text style={styles.info}>
          {l.common.innNumber}
        </Text>
        <View style={styles.contacts}>
          <Text style={styles.contactBold}>
            {l.common.ourAddress}
          </Text>
          <Text style={styles.info}>
            {l.common.address}
          </Text>
          <View style={styles.contactStyle}>
            <Text style={styles.contactTitle}>
              {l.common.phones}
            </Text>
            <Text style={styles.contactBold}>
              {l.common.phoneNumber}
            </Text>
          </View>
          <View style={styles.contactStyle}>
            <Text style={styles.contactTitle}>
              {l.common.telegram24}  
            </Text>
            <Text style={styles.contactBold}>
              {l.common.telegramUser}
            </Text>
          </View>
          <View style={styles.contactStyle}>
            <Text style={styles.contactTitle}>
              {l.common.messengerFb}
            </Text>
            <Text style={styles.contactBold}>
              {l.common.fbUser}
            </Text>
          </View>
          <View style={styles.contactStyle}>
            <Text style={styles.contactTitle}>
              {l.common.whatsApp24}  
            </Text>
            <Text style={styles.contactBold}>
              {l.common.phoneNumber}  
            </Text>
          </View>
        </View>
        <Text style={styles.info}>
          {l.common.inMessengers}
        </Text>
      </View>
    )
  }
}

const styles = styleSheetCreate({
  container: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    backgroundColor: Colors.white,
    padding: windowWidth * .047,
  } as ViewStyle,
  contacts: {
    paddingTop: windowWidth * .06,
    marginBottom: windowWidth * .02,
    justifyContent: 'space-around',
    height: windowWidth * 0.57,
  } as ViewStyle,
  info: {
    color: Colors.dark5E,
    fontSize: windowWidth * .0415,
    paddingTop: windowWidth * .025,
    lineHeight: windowWidth * .071,
  } as TextStyle,
  contactStyle: {
    flexDirection: 'row',
    paddingTop: windowWidth * .06,
  } as TextStyle,
  contactBold: {
    fontWeight: 'bold',
    color: Colors.dark5E,
    fontSize: windowWidth * .0415,
  } as TextStyle,
  contactTitle: {
    color: Colors.dark5E,
    fontSize: windowWidth * .0415,
  } as TextStyle,
})
