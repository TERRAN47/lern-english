import React from 'react'
import {
  View,
  ViewStyle,
  Text,
  TextStyle,
  ScrollView,
  TouchableOpacity,
} from 'react-native'
import { BaseReduxComponent } from '../core/BaseComponent'
import { connectAdv } from '../core/store/connectAdv'
import { Dispatch } from 'redux'
import { IAppState } from '../core/store/appState'
import { CommonActions } from './commonActions'
import { Profile } from '../core/api/generated/dto/CommonResponse.g'
import { styleSheetCreate } from '../common/utils'
import { Colors, windowWidth } from '../core/theme'
import { localization as l } from '../common/localization/localization'
import { InputLogin } from '../common/components/InputLogin'
import { Button } from '../common/components/Button'
import { PlainHeader } from '../common/components/Headers'

interface IStateProps {
  user: Profile
}

interface IDispatchProps {
  saveProfile(user:any): void
}

interface IState {
  company: string,
  surName: string,
  firstName: string,
  userPhone: string,
  userEmail: string,
  saveStatus: boolean,
}

interface IProps {

}
@connectAdv(
  (state: IAppState): IStateProps => ({
    user: state.common.user,
  }),
  (dispatch: Dispatch): IDispatchProps => ({  
    saveProfile(user): void {
    
      dispatch(CommonActions.saveProvile(user))
    },
  })
)
export class ProfileSettings extends BaseReduxComponent<IStateProps, IDispatchProps, IState, IProps> {

  static navigationOptions = PlainHeader("Профиль", true, false)
  constructor(props: any) {
    super(props);
    this.state = {
      company:'',
      surName:'',
      firstName:'',
      userPhone:'',
      userEmail:'',
      saveStatus: true,
    }
  }
  componentDidMount(){
    let { user } = this.stateProps
    this.setState({
      company:user.company,
      surName:user.surName,
      firstName:user.firstName,
      userPhone:user.userPhone,
      userEmail:user.userEmail
    })
  }
  changeCompany = (text:string): void => {
    this.setState({company:text, saveStatus:true})
  }
  changeSurName = (text:string): void => {
		this.setState({surName:text, saveStatus:true})
  }
  changeFirtName = (text:string): void => {
		this.setState({firstName:text, saveStatus:true})
  }
  changePhone = (text:string): void => {
		this.setState({userPhone:text, saveStatus:true})
  }
  changeEmail = (text:string): void => {
		this.setState({userEmail:text, saveStatus:true})
  }

  saveProfile = () => {
    let { company, surName, firstName, userPhone, userEmail } = this.state
    let { user } = this.stateProps

    if(company) user.company = company
    if(surName) user.surName = surName
    if(firstName) user.firstName = firstName
    if(userPhone) user.userPhone = userPhone
    if(userEmail) user.userEmail = userEmail
    
    this.dispatchProps.saveProfile(user)

    this.setState({saveStatus:false})
  }

  render(): JSX.Element {
    let {
      saveStatus,
      company,
      surName,
      firstName,
      userPhone,
      userEmail
    } = this.state
    return (
      <View style={styles.container}>
        <ScrollView  showsVerticalScrollIndicator={false}>
          <View style={styles.personalDataContainer}>
            <Text style={styles.title}>
              {l.common.personalData}
            </Text>
            <InputLogin
              val={firstName}
              placeholder={l.common.firstName}
              changeText={this.changeFirtName}
            />
            <InputLogin
              val={surName}
              placeholder={l.common.surname}
              changeText={this.changeSurName}
            />
            <InputLogin
              val={userPhone}
              placeholder={l.common.phoneCountryCode}
              changeText={this.changePhone}
            />
            <InputLogin
              val={userEmail}
              placeholder={l.common.emailText}
              changeText={this.changeEmail}
            />
            <InputLogin
              val={company}
              placeholder={l.common.companyName}
              changeText={this.changeCompany}
            />
          </View>
{
//           <View style={styles.newPasContainer}>
//             <Text style={styles.title}>
//               {l.common.changePass}
//             </Text>
//             <InputLogin
//               placeholder={l.common.newPass}
//               secureTextEntry={true}
//               changeText={this.changeText}
//             />
//           </View>

}
        {
          saveStatus ?
            <TouchableOpacity onPress={this.saveProfile}>
              <Button titleButton={l.common.save}/>
            </TouchableOpacity>
          :
            <Button titleButton={l.common.saved}/>
        }

        </ScrollView>
      </View>
    )
  }
}

const styles = styleSheetCreate({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: Colors.greyE5,
    padding: windowWidth * .03,
  } as ViewStyle,
  title: {
    fontWeight: 'bold',
    color: Colors.black59,
    fontSize: windowWidth * .048,
  } as TextStyle,
  personalDataContainer: {
    paddingBottom: windowWidth * .02,
  } as ViewStyle,
  newPasContainer: {
    paddingBottom: windowWidth * .08,
    paddingTop: windowWidth * .08,
  } as ViewStyle,
})
