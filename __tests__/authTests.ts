/// <reference types="jest" />

import {localization} from "../src/common/localization/localization";

describe("Auth tests", () => {
    test("Wrong email format", () => {
        expect(() => {})
            .toThrowError(localization.errors.invalidEmail);
    });

    test("Wrong password format", () => {
        expect(() => {})
            .toThrowError(localization.errors.invalidPassword);
    });
});
